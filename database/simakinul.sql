-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2017 at 12:39 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simakinul`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen_dsn`
--

CREATE TABLE `absen_dsn` (
  `tgl_absen` date NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `id_smt` char(5) NOT NULL,
  `kode_mk` char(20) NOT NULL,
  `nidn` char(10) NOT NULL,
  `kode_ruangan` varchar(5) DEFAULT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `kehadiran` varchar(11) DEFAULT 'Tidak Hadir',
  `keterangan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absen_dsn`
--

INSERT INTO `absen_dsn` (`tgl_absen`, `kode_kelas`, `id_smt`, `kode_mk`, `nidn`, `kode_ruangan`, `hari`, `kehadiran`, `keterangan`) VALUES
('2017-02-17', 'SI11', '16172', 'S09404', '0429126501', 'LT1M', 'Friday', 'Tidak Hadir', ''),
('2017-02-17', 'SI14', '16172', 'S09110', '0422078805', 'LT1T', 'Friday', 'Hadir', ''),
('2017-02-17', 'SI14', '16172', 'S09112', '0404088003', 'LT1T', 'Friday', 'Hadir', ''),
('2017-02-17', 'TI11', '16172', 'T09404', '0429126501', 'LT1M', 'Friday', 'Tidak Hadir', ''),
('2017-02-17', 'TI14R1', '16172', 'T09106', '0424099011', 'LT1M', 'Friday', 'Hadir', ''),
('2017-02-24', 'SI11', '16172', 'S09404', '0429126501', 'LT1M', 'Friday', 'Hadir', ''),
('2017-02-24', 'SI14', '16172', 'S09110', '0422078805', 'LT1T', 'Friday', 'Hadir', ''),
('2017-02-24', 'SI14', '16172', 'S09112', '0404088003', 'LT1T', 'Friday', 'Hadir', ''),
('2017-02-24', 'TI11', '16172', 'T09404', '0429126501', 'LT1M', 'Friday', 'Hadir', ''),
('2017-02-24', 'TI14R1', '16172', 'T09106', '0424099011', 'LT1M', 'Friday', 'Tidak Hadir', '');

-- --------------------------------------------------------

--
-- Table structure for table `agama`
--

CREATE TABLE `agama` (
  `id_agama` smallint(16) NOT NULL,
  `nm_agama` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agama`
--

INSERT INTO `agama` (`id_agama`, `nm_agama`) VALUES
(1, 'Islam'),
(2, 'Katolik'),
(3, 'Protestan'),
(4, 'Hindu'),
(5, 'Budha');

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nidn` char(10) NOT NULL,
  `nm_ptk` varchar(100) NOT NULL,
  `id_ikatan_kerja` varchar(50) DEFAULT NULL,
  `nip` varchar(18) DEFAULT NULL,
  `jk` char(1) DEFAULT NULL,
  `tmpt_lahir` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `nik` char(16) DEFAULT NULL,
  `niy_nigk` varchar(30) DEFAULT NULL,
  `nuptk` char(16) NOT NULL,
  `id_stat_pegawai` smallint(6) DEFAULT NULL,
  `id_stat_aktif` decimal(2,0) DEFAULT NULL,
  `id_jns_ptk` decimal(2,0) DEFAULT NULL,
  `id_bid_pengawas` int(11) DEFAULT NULL,
  `id_agama` smallint(16) DEFAULT NULL,
  `jln` varchar(80) DEFAULT NULL,
  `rt` decimal(2,0) DEFAULT NULL,
  `rw` decimal(2,0) DEFAULT NULL,
  `nm_dsn` varchar(40) DEFAULT NULL,
  `ds_kel` varchar(40) DEFAULT NULL,
  `id_wil` char(8) DEFAULT NULL,
  `kode_pos` char(5) DEFAULT NULL,
  `no_tel_rmh` varchar(20) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `id_sp` varchar(50) DEFAULT NULL,
  `sk_cpns` varchar(40) DEFAULT NULL,
  `tgl_sk_cpns` date DEFAULT NULL,
  `sk_angkat` varchar(40) DEFAULT NULL,
  `tmt_sk_angkat` date DEFAULT NULL,
  `id_lemb_angkat` decimal(2,0) DEFAULT NULL,
  `id_pangkat_gol` decimal(2,0) DEFAULT NULL,
  `id_keahlian_lab` smallint(6) DEFAULT NULL,
  `id_sumber_gaji` decimal(2,0) DEFAULT NULL,
  `nm_ibu_kandung` varchar(60) DEFAULT NULL,
  `stat_kawin` decimal(1,0) DEFAULT NULL,
  `nm_suami_istri` varchar(60) DEFAULT NULL,
  `nip_suami_istri` char(18) DEFAULT NULL,
  `id_pekerjaan_suami_istri` int(11) DEFAULT NULL,
  `tmt_pns` date DEFAULT NULL,
  `a_lisensi_kepsek` decimal(1,0) DEFAULT NULL,
  `jml_sekolah_binaan` smallint(6) DEFAULT NULL,
  `a_diklat_awas` decimal(1,0) DEFAULT NULL,
  `akta_ijin_ajar` char(1) DEFAULT NULL,
  `nira` char(30) DEFAULT NULL,
  `stat_data` int(255) DEFAULT NULL,
  `mampu_handle_kk` int(11) DEFAULT NULL,
  `a_braille` decimal(1,0) DEFAULT NULL,
  `a_bhs_isyarat` decimal(1,0) DEFAULT NULL,
  `npwp` char(15) DEFAULT NULL,
  `kewarganegaraan` char(3) DEFAULT NULL,
  `id_ptk` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nidn`, `nm_ptk`, `id_ikatan_kerja`, `nip`, `jk`, `tmpt_lahir`, `tgl_lahir`, `nik`, `niy_nigk`, `nuptk`, `id_stat_pegawai`, `id_stat_aktif`, `id_jns_ptk`, `id_bid_pengawas`, `id_agama`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `no_tel_rmh`, `no_hp`, `email`, `id_sp`, `sk_cpns`, `tgl_sk_cpns`, `sk_angkat`, `tmt_sk_angkat`, `id_lemb_angkat`, `id_pangkat_gol`, `id_keahlian_lab`, `id_sumber_gaji`, `nm_ibu_kandung`, `stat_kawin`, `nm_suami_istri`, `nip_suami_istri`, `id_pekerjaan_suami_istri`, `tmt_pns`, `a_lisensi_kepsek`, `jml_sekolah_binaan`, `a_diklat_awas`, `akta_ijin_ajar`, `nira`, `stat_data`, `mampu_handle_kk`, `a_braille`, `a_bhs_isyarat`, `npwp`, `kewarganegaraan`, `id_ptk`) VALUES
('0002028401', 'Wahyu Hidayatullah, S.Kom', NULL, '', 'L', 'Serang', '1984-02-02', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Jl. Ki Hasyim 34 rt.16/06 kecamatan Ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, '085283713421', NULL, 'wahyu_sttikom_iu@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0002038301', 'Lamhari, S.Kom', NULL, '', 'P', 'Serang', '1983-03-02', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Kp.Krawen Ds.Dukuh Kragilan Serang-Banten', NULL, NULL, NULL, NULL, NULL, NULL, '085920065571', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0004048701', 'Aprillia Setyaningrum, S.Kom', NULL, '', 'W', 'Surabaya', '1987-04-04', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Gubeng Klingsingan 2 no 27 Surabaya 60281', NULL, NULL, NULL, NULL, NULL, NULL, '08563451201', NULL, 'aprilliasi05@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0005028001', 'ahmad Hermansyah, S.Si', NULL, '', 'P', 'Lebak', '1980-02-05', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0006067801', 'Rida Alfadiah, ST, MM', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0007095601', 'Alfauzi Salam, Drs,H, MBA', NULL, '', 'P', 'lirik Riau', '1973-09-07', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Taman Cilegon Cluster Halikonia C7 no 16 Cile', NULL, NULL, NULL, NULL, NULL, NULL, '081314835855', NULL, 'alfauzis.salam@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0008076801', 'Ir. Abdul Hadi Muslim', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0008087201', 'Hj. Rita Andriani Sukmawati, SH', NULL, '', 'P', 'Serang', '1974-08-08', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Perum Bumi Cibeber Kencana 2 Blok B2 No. 5', NULL, NULL, NULL, NULL, NULL, NULL, '08122540061', NULL, 'rita.andrianis@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0009055901', 'Raden Gunawan,IR,H, M.Eng', NULL, '', 'P', 'Blambangan Umpu ', '1974-05-09', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Jl. Industri No. 5, PO Box 14, Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '081314293922', NULL, 'radengunawan59@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0009079101', 'Faris Muhammad Syariati, S. Kom', NULL, '', 'P', 'Serang', '1991-07-09', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '087738712647', NULL, 'syariati.faris@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0010017901', 'Rahmat Gunawan, S.S', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0011027701', 'Ujang Jaenal Mutakin, S.Ag, MM', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0011028801', 'Mahmud Siddik, S.Kom', NULL, '', 'P', 'Cilegon', '1988-02-11', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Ketumbar No. 54 RT. 13/10 Siwaduk', NULL, NULL, NULL, NULL, NULL, NULL, '08569993074', NULL, 'me@mahmudsiddik.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0012017401', 'Sev Rahmiyanti, SE', NULL, '', 'W', 'Jakarta', '0000-00-00', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Bukit Palem Jln Lantana kuning n0.19', NULL, NULL, NULL, NULL, NULL, NULL, '081808645347', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0012018801', 'Muhammad Khaidir Fahram, M.Kom', NULL, '', 'P', 'Ujung Pandang', '1988-01-12', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'taman cilegon indah blok h4 no. 19', NULL, NULL, NULL, NULL, NULL, NULL, '081290392898', NULL, 'khaidir@fahram.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0012086201', 'Ir. Achmad Hudaya', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0013108701', 'Vina Oktarina Sari, S.Pd', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0014088601', 'Lukman Faruk, ST', NULL, '', 'P', 'Serang', '1986-08-14', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Link. Karotek No. 17 RT. 01/02 Kel. kalitimba', NULL, NULL, NULL, NULL, NULL, NULL, '081911182007', NULL, 'lukmanfaruk@yahoo.co.id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0014127901', 'Ipung Ernawati Setianingrum, ST, M.Si', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0017056401', 'Drs. Nurjaman', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0018058901', 'Andri Setyawan, ST', NULL, '', 'P', 'Surabaya', '1989-05-18', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Jakarta No. 7 Perum BBS II - Cilegon ', NULL, NULL, NULL, NULL, NULL, NULL, '085691020956 ', NULL, 'iam.andriy@yahoo.co.uk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0018127401', 'Nanang Salaludin, S.Ag', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0020018201', 'Ahmad Furqoni, S.Pd, M. Pd', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0021057906', 'Susy Katarina Sianturi, M.Kom', NULL, '', 'W', 'Tarutung', '1979-05-21', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Metro Primadona Blok A3 No. 10 - Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '081370246454', NULL, 'susykatarina@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0021118501', 'Devi Yudha Damara, ST', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0023058701', 'Fatkur Roji', NULL, '', 'P', 'Surabaya', '1987-05-23', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'BBS 3 blok D4/09, Cilegon - Banten', NULL, NULL, NULL, NULL, NULL, NULL, '085230023067', NULL, 'fatkur.roji@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0024059001', 'Lestari Hadi Wigatning, S.Kom', NULL, '', 'W', 'Pemalang', '1990-05-24', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Perum Griya Serdang Indah Blok A7 no.2 Margat', NULL, NULL, NULL, NULL, NULL, NULL, '081911251846', NULL, 'lestari.hadi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0026018001', 'Wahyu Iskandar, ST, MKom', NULL, '', 'P', 'Cilegon', '1980-01-26', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0026036801', 'Apandi, S.Ag', NULL, '', 'P', 'bandung', '2013-05-30', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0027028401', 'Agus Fakhru Rizal, ST', NULL, '', 'P', 'Karawang', '1984-02-27', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Komp. Metro Villa Blok C5 No. 15', NULL, NULL, NULL, NULL, NULL, NULL, '085780005666', NULL, 'agusfakhrurizal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0027036401', 'Imam Prasodjo, S. Mn', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0027095801', 'Kamarul Hajar NST', NULL, '', 'P', 'Medan', '1995-09-27', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '08129239384', NULL, 'kamarulhajar@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0028098001', 'Yanti Anggraini, SP', NULL, '', 'W', 'Jakarta', '1980-09-28', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Tegalwangi Solor No. 2 RT. 02/06 Kel. Rawa Ar', NULL, NULL, NULL, NULL, NULL, NULL, '085289661414', NULL, 'yeongi09@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0030018001', 'Thoha Nurhadiyan Hikmawan, M.Kom', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0030108801', 'Syaiful Bakhri, S.Kom', NULL, '', 'P', 'Indramayu', '1988-10-30', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0031056401', 'Dra. Atih N. Purwati, M.Si', NULL, '', 'W', 'Purwakarta', '1994-05-31', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0402088601', 'Rizki Mirhadi. Ir.MM', NULL, '', 'P', 'Serang', '1986-08-02', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Serang', NULL, NULL, NULL, NULL, NULL, NULL, '91010010', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0403116901', 'Bambang Wahyu Agung', NULL, '', 'P', 'Ponorogo', '1976-11-03', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0404068001', 'Zaenal Muttaqin, S.Kom, MM', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0404088003', 'Penny Hendriyati, S.Kom,M.Kom', NULL, '', 'W', 'Sukabumi', '1980-08-04', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Perumahan Gedong Cilegon Damai Blok E6 No 9 C', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'pennyhendriyati@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0405038401', 'Nanang Sutisna, S.Kom', NULL, '', 'P', 'PULOMERAK', '1984-03-05', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '085959927972', NULL, 'emresutisna@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0406067405', 'Darojat, S.Ag', NULL, '', 'P', 'dsfsfsdf', '2013-05-20', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, 'sfsdfsdf', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0407097404', 'Hetty Herawati, S.Pd', NULL, '', 'W', 'Bogor', '1974-09-07', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Taman Banten Lestari E2D/25 Rt005/022', NULL, NULL, NULL, NULL, NULL, NULL, '087771318373', NULL, 'hetty_siu@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0408047202', 'Subandi Wahyudi, S.Si,M.Kom', NULL, '', 'P', 'gresik', '1972-04-08', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0410027303', 'Jayadi, S.Kom', NULL, '', 'P', 'Serang', '2014-02-10', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Kubang Lampung No 12. Kompl. KS - Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '08176045881', NULL, 'jaya_yadi@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0410048102', 'Diah Ahdiati, S.Pd', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0410127002', 'ADE HENDRIANI, SE, MM', NULL, '', 'W', 'JAKARTA', '1976-12-10', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0411096202', 'Sawitri Nurhayati, S.Kom, M.Kom', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0411116903', 'Drs, Fachroji Ali, MM', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0412035901', 'PARYONO,IR,MM', NULL, '', 'P', 'KLATEN', '1976-01-01', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0412037301', 'Cecep Abdul Hakim, SE, MM.Ak', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0412057902', 'Helmi Ilham, S.Kom', NULL, '', 'P', 'Jakarta', '1979-05-12', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'PERUM Gedong Cilegon Damai Blok A17/25 Cibebe', NULL, NULL, NULL, NULL, NULL, NULL, '08176871348', NULL, 'helmi.ilham1205@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0414066905', 'Wahyuddin, S.Kom, M.Kom', NULL, '', 'P', 'Cianjur', '1969-06-14', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Bumi Cibeber Kencana F4 No. 22 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '0254396171', NULL, 'wahyu.iu@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0415010001', 'Dwi Dina Heriyanto, S.Kom', NULL, '', 'P', 'RANGKASBITUNG', '1991-02-07', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'TCP BLOK E3 NO.25', NULL, NULL, NULL, NULL, NULL, NULL, '087806763557', NULL, 'dwi.dina.heriyanto@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0415010002', 'AKBP Ade Kusnadi SH,MSi', NULL, '', 'P', 'SERANG', '1986-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '1111111', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0415010009', 'Vina Vijaya Kusuma SPd,MPd', NULL, '', 'W', 'Palembang', '1991-01-07', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Jl Bhayangkara No 35 Serang', NULL, NULL, NULL, NULL, NULL, NULL, '081808734305', NULL, 'vinavijaya@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0415088701', 'Sodik Nurdhin,MKom', NULL, '', 'P', 'SERANG', '1989-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'JL KAKAP', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0415106805', 'Ahmad Kautsar, ST, M. Kom', NULL, '', 'P', 'Serang', '1968-10-15', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Palem Putri Atas No 8 Bukit Palem Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '08176061520', NULL, 'agus@capcx.com ; akautsar@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0418087606', 'Dina Satriani Fansuri, SE, MM', NULL, '', 'W', 'jakarta', '1976-08-18', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'gsi j5 no 2, serdang', NULL, NULL, NULL, NULL, NULL, NULL, '08121203769', NULL, 'aylaku@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0419027405', 'Sulaeman, S.Kom', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0419077601', 'Darpi Supriyanto, S.Si, M.Kom', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0420088004', 'Agus Setyawan, S.Kom, M. Kom', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0421088103', 'Gustina, S.Kom,MKom', NULL, '', 'W', '', '0000-00-00', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422076301', 'Achmad Syaefudin, ST, MM ,MKom', NULL, '', 'P', 'Purwakarta', '1973-07-22', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Jl. Lumba lumba No. 22 Kavling Blok C - Cileg', NULL, NULL, NULL, NULL, NULL, NULL, '08129640135', NULL, 'a_didin1213@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422078802', 'Burhanudin,SE,MM.AK', NULL, '', 'P', 'Bogor', '1980-01-04', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '091999191', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422078805', 'Ali Faozin,Ir,MM', NULL, '', 'P', 'SERANG', '1996-01-01', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '121212', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422078815', 'Saleh Dwiyatno,ST,MKOM', NULL, '', 'P', 'Samarinda', '1996-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '091999191', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422078830', 'Rita Andriani,SH', NULL, '', 'P', 'SERANG', '1996-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '12121', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0422109001', 'Asep Saifudin, S.Kom', NULL, '', 'P', 'Serang', '1990-10-20', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Mataram Link. Pakuncen RT. 11 RW. 03 Ds. ', NULL, NULL, NULL, NULL, NULL, NULL, '087871608080', NULL, 'asep.saifudin22@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('042299999', 'Usep Sholahudin .MKom', NULL, '', 'P', 'Serang', '1995-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424018403', 'Eva Safa\'ah, ST, M.Kom', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424048401', 'Ikbal Nidauddin,MKom', NULL, '', 'P', 'Serang', '1984-04-24', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Serang', NULL, NULL, NULL, NULL, NULL, NULL, '0100101', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424086102', 'Andi Usri Usman, M.Kom', NULL, '', 'P', 'Pare Pare', '1961-08-24', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Perum. PCI Blok D89 No.21 Cibeber Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '081280623444', NULL, 'andiusri@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424096905', 'Padang Wardoyo, ST, MM', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099011', 'M.Amiruddin  Saddam, S.Kom', NULL, '', 'P', 'Batam', '0000-00-00', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099012', 'H.M. Sayuthi Ali,Drs,MAg', NULL, '', 'P', '', '1975-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099013', 'Siti Nurbanis Andriani,SH,MM', NULL, '', 'W', '', '1977-01-01', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Jl. Anggrek No. 64 Ciwedus, Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, '087809207755', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099014', 'Tri Sutomo', NULL, '', 'P', '', '1977-01-01', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099015', 'MULYADI.MKom', NULL, '', 'P', 'SERANG', '1985-01-14', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0424099016', 'Eka Prasetia Bakti', NULL, '', 'W', 'Serang', '1990-09-18', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '085921052144', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0426048201', 'Nisa Nuranisa, S.Pd', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0426066105', 'Dr. Ir. Sumadiono, M. MT, M. Si', NULL, '', 'P', 'Blora', '1973-06-26', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Jl. Radio 1 No. 23 Komp. Krakatau Steel Cileg', NULL, NULL, NULL, NULL, NULL, NULL, '081399230711', NULL, 'sumadiono@krakatausteel.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0426128702', 'Siti Desi Rohmawati, SE', NULL, '', NULL, NULL, NULL, NULL, NULL, '', NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0428118503', 'DWI NURINA PITASARI .M.IKOM', NULL, '', 'W', 'BANDUNG', '1985-11-28', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0429126501', 'Teguh Sutopo, S.Kom, M. Kom', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0430018003', 'Thoha Nurhadiyan Hikmawan, M.Kom', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '0', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '087808194057', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0430045803', 'Slamet Gunadi, ST, MM', NULL, '', 'P', '', '0000-00-00', NULL, NULL, '', NULL, '1', NULL, NULL, 0, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0430079001', 'ISNAINI MUHADA .MSI', NULL, '', 'W', 'JAKARTA', '1990-07-30', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'PCI', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0431077702', 'Afrasim Yusta, S.Kom,MKom', NULL, '', 'P', 'Bojonegoro', '1977-07-31', NULL, NULL, '', NULL, '1', NULL, NULL, 0, 'Perum GCD Blok E6/9', NULL, NULL, NULL, NULL, NULL, NULL, '081932322959', NULL, 'afrasimyusta@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0807877777', 'Arif Eko Julianto, S.Kom', NULL, '', 'P', 'Bitung', '1987-07-08', NULL, NULL, '', NULL, '0', NULL, NULL, 0, 'Kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, '085230139233', NULL, 'arifekojulianto@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
('0893114', 'Satam', NULL, '', 'P', 'Cilegon', '1980-10-10', '', NULL, '', NULL, '0', NULL, NULL, 2, 'Jl. Raya Cilegon', '8', '9', 'Cicaheum', 'Cisoak', NULL, '42315', NULL, '08973156131', NULL, NULL, '', '0000-00-00', '', '0000-00-00', NULL, '0', NULL, NULL, '', '0', '0', '', 0, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'WNI', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenjang_pendidikan`
--

CREATE TABLE `jenjang_pendidikan` (
  `id_jenj_didik` decimal(2,0) NOT NULL,
  `nm_jenj_didik` varchar(25) DEFAULT NULL,
  `u_jenj_lembaga` decimal(1,0) DEFAULT NULL,
  `u_jenj_org` decimal(1,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenjang_pendidikan`
--

INSERT INTO `jenjang_pendidikan` (`id_jenj_didik`, `nm_jenj_didik`, `u_jenj_lembaga`, `u_jenj_org`) VALUES
('0', 'Tidak Bersekolah', '0', '0'),
('1', 'SD', '0', '1'),
('2', 'SMP', '0', '1'),
('3', 'SMA', '0', '1'),
('4', 'S1', '0', '1'),
('5', 'S2', '0', '1'),
('6', 'S3', '0', '1'),
('7', 'D3', '0', '1'),
('8', 'D2', '0', '1'),
('9', 'D1', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jur` char(10) NOT NULL,
  `nm_jur` varchar(60) DEFAULT NULL,
  `nm_intl_jur` varchar(60) DEFAULT NULL,
  `u_sma` decimal(1,0) DEFAULT NULL,
  `u_pt` decimal(1,0) DEFAULT NULL,
  `u_slb` decimal(1,0) DEFAULT NULL,
  `id_jenj_didik` decimal(2,0) DEFAULT NULL,
  `id_induk_jurusan` char(10) DEFAULT NULL,
  `id_kel_bidang` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jur`, `nm_jur`, `nm_intl_jur`, `u_sma`, `u_pt`, `u_slb`, `id_jenj_didik`, `id_induk_jurusan`, `id_kel_bidang`) VALUES
('KA003', 'Komputer Akuntansi', NULL, '0', '1', '0', '7', NULL, NULL),
('MI003', 'Manajemen Informatika', NULL, '0', '1', '0', '7', NULL, NULL),
('SI001', 'Sistem Informasi', NULL, '0', '1', '0', '4', NULL, NULL),
('TI001', 'Teknik Informatika', NULL, '0', '1', '0', '4', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `tahun_msk` decimal(4,0) DEFAULT NULL,
  `jumlah_mhs` decimal(4,0) DEFAULT NULL,
  `semester_skrg` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `tahun_msk`, `jumlah_mhs`, `semester_skrg`) VALUES
('KA10', '2014', NULL, '6'),
('KA11', '2015', NULL, '4'),
('KA12', '2016', NULL, '2'),
('MI10', '2014', NULL, '6'),
('MI11', '2015', NULL, '4'),
('MI12', '2016', NULL, '2'),
('SI11', '2013', '5', '8'),
('SI12', '2014', NULL, '6'),
('SI13', '2015', NULL, '4'),
('SI14', '2016', NULL, '2'),
('TI11', '2013', '15', '8'),
('TI12', '2014', NULL, '6'),
('TI13', '2015', NULL, '4'),
('TI14R1', '2016', NULL, '2'),
('TI14R2', '2016', NULL, '2'),
('TI18', '2030', '31', '1');

-- --------------------------------------------------------

--
-- Table structure for table `kelas_kuliah`
--

CREATE TABLE `kelas_kuliah` (
  `kode_kelas` varchar(10) NOT NULL,
  `id_smt` char(5) NOT NULL,
  `kode_mk` char(20) NOT NULL,
  `nidn` char(10) DEFAULT NULL,
  `kode_ruangan` varchar(5) DEFAULT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `jam_mulai` time DEFAULT NULL,
  `jam_selesai` time DEFAULT NULL,
  `bahasan_case` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas_kuliahfinal`
--

CREATE TABLE `kelas_kuliahfinal` (
  `kode_kelas` varchar(10) NOT NULL,
  `id_smt` char(5) NOT NULL,
  `kode_mk` char(20) NOT NULL,
  `nidn` char(10) DEFAULT NULL,
  `kode_ruangan` varchar(5) DEFAULT NULL,
  `hari` varchar(10) DEFAULT NULL,
  `jam_mulai` time DEFAULT NULL,
  `jam_selesai` time DEFAULT NULL,
  `bahasan_case` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_kuliahfinal`
--

INSERT INTO `kelas_kuliahfinal` (`kode_kelas`, `id_smt`, `kode_mk`, `nidn`, `kode_ruangan`, `hari`, `jam_mulai`, `jam_selesai`, `bahasan_case`) VALUES
('SI11', '16172', 'S09404', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_kuliahtemp`
--

CREATE TABLE `kelas_kuliahtemp` (
  `kode_kelas` varchar(10) NOT NULL,
  `id_smt` char(5) NOT NULL,
  `kode_mk` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_kuliahtemp`
--

INSERT INTO `kelas_kuliahtemp` (`kode_kelas`, `id_smt`, `kode_mk`) VALUES
('KA10', '16172', 'K09305'),
('KA10', '16172', 'K09315');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nik` char(16) DEFAULT NULL,
  `nim` char(15) DEFAULT NULL,
  `nm_pd` varchar(100) DEFAULT NULL,
  `jk` char(1) DEFAULT NULL,
  `nisn` varchar(10) DEFAULT NULL,
  `tmpt_lahir` varchar(20) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `id_agama` smallint(16) DEFAULT NULL,
  `id_kk` int(32) DEFAULT NULL,
  `id_sp` varchar(50) DEFAULT NULL,
  `jln` varchar(200) DEFAULT NULL,
  `rt` decimal(3,0) DEFAULT NULL,
  `rw` decimal(3,0) DEFAULT NULL,
  `nm_dsn` varchar(40) DEFAULT NULL,
  `ds_kel` varchar(40) DEFAULT NULL,
  `id_wil` char(8) DEFAULT NULL,
  `kode_pos` char(5) DEFAULT NULL,
  `id_jns_tinggal` decimal(2,0) DEFAULT NULL,
  `id_alat_transport` decimal(2,0) DEFAULT NULL,
  `telepon_rumah` varchar(20) DEFAULT NULL,
  `telepon_seluler` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `a_terima_kps` decimal(1,0) DEFAULT NULL,
  `no_kps` varchar(40) DEFAULT NULL,
  `stat_pd` char(1) DEFAULT NULL,
  `nm_ayah` varchar(45) DEFAULT NULL,
  `tgl_lahir_ayah` date DEFAULT NULL,
  `id_jenjang_pendidikan_ayah` decimal(2,0) DEFAULT NULL,
  `id_pekerjaan_ayah` int(11) DEFAULT NULL,
  `id_penghasilan_ayah` int(11) DEFAULT NULL,
  `id_kebutuhan_khusus_ayah` int(11) DEFAULT NULL,
  `nm_ibu_kandung` varchar(45) DEFAULT NULL,
  `tgl_lahir_ibu` date DEFAULT NULL,
  `id_jenjang_pendidikan_ibu` decimal(2,0) DEFAULT NULL,
  `id_pekerjaan_ibu` int(11) DEFAULT NULL,
  `id_penghasilan_ibu` int(11) DEFAULT NULL,
  `id_kebutuhan_khusus_ibu` int(11) DEFAULT NULL,
  `nm_wali` varchar(30) DEFAULT NULL,
  `tgl_lahir_wali` date DEFAULT NULL,
  `id_jenjang_pendidikan_wali` decimal(2,0) DEFAULT NULL,
  `id_pekerjaan_wali` int(11) DEFAULT NULL,
  `id_penghasilan_wali` int(11) DEFAULT NULL,
  `kewarganegaraan` char(3) DEFAULT NULL,
  `id_pd` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(1, '', '2008.12.005', 'Anita Restuintina', 'W', '', 'Cilegon', '1983-10-10', 1, NULL, NULL, '', '0', '0', '', '', NULL, '', NULL, NULL, '', '08128347731', '', NULL, NULL, 'A', '', '0000-00-00', '0', 0, 0, NULL, '', '0000-00-00', '0', 0, 0, 0, '', '0000-00-00', '0', 0, 0, 'WNI', ''),
(2, NULL, '2009.11.001', 'Ahmad Aushi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(3, NULL, '2009.11.002', 'Abdurohim', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(4, NULL, '2009.11.003', 'Achmad Rifai', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(5, NULL, '2009.11.004', 'Adrian Gustira Gunawan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(6, NULL, '2009.11.005', 'Ahmad Amudiulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(7, NULL, '2009.11.006', 'Arifudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(8, NULL, '2009.11.007', 'Asep Rifki Zein', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(9, NULL, '2009.11.008', 'Dwi Dina Heriyanto', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(10, NULL, '2009.11.009', 'Dzajuli', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(11, NULL, '2009.11.010', 'Eko Ariwibowo', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(12, NULL, '2009.11.011', 'Fahru Roji', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(13, NULL, '2009.11.012', 'Fajar Martha', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(14, NULL, '2009.11.013', 'Fatahillah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(15, NULL, '2009.11.014', 'Fauzi Dwi Ardiyanto', 'P', NULL, '', '2013-06-01', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(16, NULL, '2009.11.015', 'Fiqh Sam Havazah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(17, NULL, '2009.11.016', 'Hizbullah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(18, NULL, '2009.11.017', 'Juni Indah Purnama Wati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(19, NULL, '2009.11.018', 'Listio Rahmat Saputro', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(20, NULL, '2009.11.019', 'Muhammad Ridho Ulumudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(21, NULL, '2009.11.020', 'Novian Achdi Prabowo', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(22, NULL, '2009.11.021', 'Nurhasanah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(23, NULL, '2009.11.022', 'Randi Desma Purnama', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(24, NULL, '2009.11.023', 'Rifqi Isnaini', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(25, NULL, '2009.11.024', 'Rizal Sapta', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(26, NULL, '2009.11.025', 'Rony Cahyadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(27, NULL, '2009.11.026', 'Suherman', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(28, NULL, '2009.11.027', 'Toni', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(29, NULL, '2009.11.028', 'Yocta Sehustian', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(30, NULL, '2009.11.029', 'Zakaria', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(31, NULL, '2009.11.030', 'Ruben Immanuel', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(32, NULL, '2009.11.031', 'Ei Majid Padli', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(33, NULL, '2009.11.032', 'Dedi Pandiangan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(34, NULL, '2009.11.033', 'Rara Rahmatullah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(35, NULL, '2009.11.034', 'Ngadiyanto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(36, NULL, '2009.11.035', 'Didik Saputro', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(37, NULL, '2009.11.036', 'Yudha Putra Pratama', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(38, NULL, '2009.11.037', 'Sofwatunaja', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(39, NULL, '2009.11.038', 'Basri', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(40, NULL, '2009.11.039', 'Mohamad Fikri Romdhoni', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(41, NULL, '2009.11.040', 'Tomas May Wibowo', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(42, NULL, '2009.11.041', 'Imam Dzikrillah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(43, NULL, '2009.12.001', 'Aat Atikah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(44, NULL, '2009.12.002', 'Ade Ismi Ilmi Nurul Hidayah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(45, NULL, '2009.12.003', 'Bayu Esa Surya', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(46, NULL, '2009.12.004', 'Ema Wulandari', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(47, NULL, '2009.12.005', 'Hesti Herawati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(48, NULL, '2009.12.006', 'Jezzi Fauzi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(49, NULL, '2009.12.008', 'Rhesa Setya Wardhana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(50, NULL, '2009.12.009', 'Rizki Ferdian', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(51, NULL, '2009.12.010', 'Samsudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(52, NULL, '2009.12.011', 'Sri Rezeki', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(53, NULL, '2009.12.012', 'Vega Grandis Gantina', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(54, NULL, '2009.12.013', 'Wicaksono Widianto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(55, NULL, '2009.12.014', 'Aan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(56, NULL, '2009.12.015', 'Didi Munadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(57, NULL, '2009.12.016', 'Fazrul Islam El Madany', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(58, NULL, '2009.12.017', 'Herri Mawan Subakti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(59, NULL, '2009.12.018', 'Indra Budiman', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(60, NULL, '2009.12.019', 'Peti Nurbayasari', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(61, NULL, '2009.12.020', 'TB. Masturo Al- Hariri', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(62, NULL, '2009.12.021', 'Yusuf', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(63, NULL, '2009.12.022', 'TB. Nurakhdiat', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(64, NULL, '2009.12.023', 'E. Ginanjar Azis', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(65, NULL, '2009.12.024', 'Agus Hernandi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(66, NULL, '2009.12.025', 'Caecelia Menzelthe', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(67, NULL, '2009.12.026', 'Mira Emiria', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(68, NULL, '2009.12.027', 'Andri Permana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(69, NULL, '2009.12.028', 'TB. M.Sophan Novana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(70, NULL, '2009.12.029', 'E. Iwan Fauzi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(71, NULL, '2009.12.030', 'Rifansyah Muliana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(72, NULL, '2009.12.031', 'Edwiyana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(73, NULL, '2009.12.032', 'Andri Lestian', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(74, NULL, '2009.12.033', 'Beni', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(75, NULL, '2009.21.001', 'Armi Sarah Kurniatin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(76, NULL, '2009.21.002', 'Ghea Pratama', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(77, NULL, '2009.21.004', 'Imam Dzikrillah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(78, NULL, '2009.21.005', 'Khoiron Mamun', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(79, NULL, '2009.21.006', 'Marfinggih Dede Saputra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(80, NULL, '2009.21.007', 'Nikco Irawan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(81, NULL, '2009.21.008', 'Nia Herniyawati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(82, NULL, '2009.21.009', 'Kholifah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(83, NULL, '2009.21.011', 'Dian Agustiani', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(84, NULL, '2009.22.001', 'Ade Irma Rukmana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(85, NULL, '2009.22.002', 'Amiatul Romadanti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(86, NULL, '2009.22.003', 'Anggraini', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(87, NULL, '2009.22.004', 'Anita Noviana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(88, NULL, '2009.22.005', 'Cinthya Debby Rianty S.G', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(89, NULL, '2009.22.006', 'Erwinsyah Putra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(90, NULL, '2009.22.007', 'Linda Ayu Fitria', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(91, NULL, '2009.22.008', 'Sri Mulyani', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(92, NULL, '2009.22.009', 'Riana Adhesyi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(93, NULL, '2009.22.010', 'Rina Yulianti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(94, NULL, '2009.22.011', 'Siti Mariah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(95, NULL, '2009.22.012', 'Ikbal Mutakin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(96, NULL, '2009.22.013', 'Indriyani Wijaya', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(97, NULL, '2010.11.001', 'Asep Rahayu', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(98, NULL, '2010.11.002', 'Eni Nuraini', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(99, NULL, '2010.11.003', 'Aniah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(100, NULL, '2010.11.004', 'Solihin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(101, NULL, '2010.11.005', 'Khairul Umami', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(102, NULL, '2010.11.006', 'Aliman', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(103, NULL, '2010.11.007', 'Harsali', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(104, NULL, '2010.11.008', 'Yassir WahyuIlah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(105, NULL, '2010.11.009', 'Muhammad Ilham', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(106, NULL, '2010.11.010', 'Sahuri', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(107, NULL, '2010.11.011', 'Anton Saputra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(108, NULL, '2010.11.012', 'Siti Nurhasanah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(109, NULL, '2010.11.013', 'Herley Rinto Nugroho', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(110, NULL, '2010.11.014', 'Gatot Adhi Chandra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(111, NULL, '2010.11.015', 'Saiful Anwar', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(112, NULL, '2010.11.016', 'Fatoni', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(113, NULL, '2010.11.017', 'Dede Nur Efendi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(114, NULL, '2010.11.018', 'Devia Isma', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(115, NULL, '2010.11.019', 'Erico Novan Feriyansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(116, NULL, '2010.11.020', 'Tri Sutomo', 'P', NULL, 'Malang', '1991-11-01', 1, NULL, NULL, 'bbs3 b1no3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '087808724111', '087808724111', 'triprisma@gmail.com', NULL, NULL, NULL, 'Iman Maulayi', NULL, '0', 0, 0, 0, 'Endang Astuti Ningsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(117, NULL, '2010.11.021', 'Ati Rohayati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(118, NULL, '2010.11.022', 'Karnata', 'P', NULL, 'Pandeglang', '1990-09-04', 1, NULL, NULL, 'Kp.Kubang Oleng Ds, Karangsari Kec, Angsana kab, Pandeglang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081911012016', 'saputran97@yahoo.com', NULL, NULL, NULL, 'H. Tawang', NULL, '0', 0, 0, 0, 'Hj. Yati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(119, NULL, '2010.11.023', 'Taryana', 'P', NULL, 'Pandeglang', '2013-02-15', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772994691', 'iyan.cool34@yahoo.com', NULL, NULL, NULL, 'M. Suparna SK', NULL, '0', 0, 0, 0, 'Sariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(120, NULL, '2010.11.024', 'Ratu Ghina Amalia', 'W', NULL, 'serang', '1992-06-24', 1, NULL, NULL, 'Link. Pabuaran Rt01/05 Rawa Arum Grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254572509', '08989602926', 'ratubaguz@yahoo.co.id', NULL, NULL, NULL, 'Tb. Muharam', NULL, '0', 0, 0, 0, 'Rohilahil Badriyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(121, NULL, '2010.11.025', 'Ari Wibowo', 'P', NULL, 'Jakarta', '1991-03-13', 1, NULL, NULL, 'Carita', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081380845856', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(122, NULL, '2010.11.026', 'Ananda Aditya Pradipta', 'P', NULL, 'serang', '1991-12-22', 1, NULL, NULL, 'JL.Lada block A2 no.02 BBS2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774011165', 'Akita.hana@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(123, NULL, '2010.11.027', 'Nurul Ilahiyah', 'W', NULL, 'Serang', '1992-06-07', 1, NULL, NULL, 'Jl. Sunan Gunung Jati Link. Lijajar-Jublin Rt/Rw 13/06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087775494393', 'ila.utez@gmail.com', NULL, NULL, NULL, 'Abdullah', NULL, '0', 0, 0, 0, 'Sulhiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(124, NULL, '2010.11.028', 'Asep Syaekhu', 'P', NULL, '', '1993-03-11', 1, NULL, NULL, 'Jl.fatahilah link.sambilawang ciwandan cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081906077939', 'ad31_5y4ekhu@yahoo.com', NULL, NULL, NULL, 'tamami', NULL, '0', 0, 0, 0, 'ayusah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(125, NULL, '2010.11.029', 'Dimas Rizky Fadillah', 'P', NULL, 'Jakarta', '1992-06-21', 1, NULL, NULL, 'Komp BPI BLOK JB No. 8 RT/RW : 006/004 Panggung Rawi kecamatan Jombang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771235521', 'dimasjr13@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(126, NULL, '2010.11.030', 'Meli Rusmaeli', 'W', NULL, 'serang', '1991-10-16', 1, NULL, NULL, 'link. pekalongan rt/rw 01/01 deringo citangkil ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089680555045', 'meli@fahram.com', NULL, NULL, NULL, 'Rohyadi', NULL, '0', 0, 0, 0, 'Sri Agustiani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(127, NULL, '2010.11.031', 'Ari Kuswani', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(128, NULL, '2010.11.032', 'Jarot Subrata', 'P', NULL, 'Serang', '1992-10-03', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089635257330', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(129, NULL, '2010.11.033', 'Hidayatullah', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, 'Kp.panibungan kel.tambang ayam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083878213269', 'hidayatformula@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(130, NULL, '2010.11.034', 'Sophan Sofyan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(131, NULL, '2010.11.035', 'Handi Arifan', 'P', NULL, 'SERANG', '1991-02-15', 1, NULL, NULL, 'Anyer, Kp.pegadungan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871612211', 'andi.ox57@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(132, NULL, '2010.11.036', 'Anggi Rizki Ardiansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(133, NULL, '2010.11.037', 'Khaerul Umam', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(134, NULL, '2010.11.038', 'Haerul Umam', 'P', NULL, 'SERANG', '1992-06-01', 1, NULL, NULL, 'KP.KOSAMBI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087981008006', 'HERU.GASKINZ@YAHOO.COM', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(135, NULL, '2010.11.039', 'Rizal Herdiansyah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085289889507', 'rzlherdiansyah@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(136, NULL, '2010.11.040', 'Nurul Huda', 'P', NULL, 'SERANG', '1990-07-20', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08569090885', 'webhuda@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(137, NULL, '2010.11.041', 'Busthomi', 'P', NULL, 'serang', '1990-11-11', 1, NULL, NULL, 'link.sukasari kel.randakari ciwandan cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774812998', 'busthomi_iu@yahoo.com', NULL, NULL, NULL, 'kurtubi', NULL, '0', 0, 0, 0, 'masturiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(138, NULL, '2010.11.042', 'Miftahul Jannah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(139, NULL, '2010.11.043', 'Vinny Nur Alfiany', 'W', NULL, 'Serang', '1993-07-05', 1, NULL, NULL, 'PERUM GCD BLOK B 07 NO 04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '085945653930', 'niifanadicky@yahoo.co.id', NULL, NULL, NULL, 'Gojali', NULL, '0', 0, 0, 0, 'Hudaefah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(140, NULL, '2010.11.044', 'Faisal Fasya', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(141, NULL, '2010.11.045', 'Muhamad Sadam', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772382898', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(142, NULL, '2010.11.046', 'CHULDI', 'P', NULL, 'Serang', '1991-10-31', 1, NULL, NULL, 'Link. Gerem Raya Rt.01/04 No. 51 Kel. Gerem Kec. Grogol Cilegon Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774445519', 'chuldinine@yahoo.com', NULL, NULL, NULL, 'H. HUJAENI YASIN ', NULL, '0', 0, 0, 0, 'HJ. MARFUAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(143, NULL, '2010.11.047', 'Bahrudin', 'P', NULL, 'serang', '2013-02-17', 1, NULL, NULL, 'link. jombang gardu Rt 01 Rw 09 kel. jombang wetan kec. jombang cilegon-banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081911150378', 'bahrudin_tib@rocketmail.com', NULL, NULL, NULL, 'Halawi', NULL, '0', 0, 0, 0, 'kasum', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(144, NULL, '2010.11.048', 'Sapto Pamungkas', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(145, NULL, '2010.11.049', 'Ahmad Jubaidie', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '082113334496', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(146, NULL, '2010.11.050', 'I Putu Giri Swastika', 'P', NULL, 'METRO', '1991-10-20', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911231321', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(147, NULL, '2010.11.051', 'Nurdin Parinoto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(148, NULL, '2010.11.052', 'Uli Supriyadi', 'P', NULL, 'Serang', '1991-10-13', 1, NULL, NULL, 'Link. pabuaran RT.04/05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08999667831', 'ulisupriyadi@yahoo.co.id', NULL, NULL, NULL, 'Tabrani Nawawi', NULL, '0', 0, 0, 0, 'Nurhayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(149, NULL, '2010.11.053', 'Maryadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(150, NULL, '2010.11.054', 'Santi Damayanti', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(151, NULL, '2010.11.055', 'Aji Argani Saputra', 'P', NULL, 'bojonegoro', '1989-07-16', 1, NULL, NULL, 'komp.twi blok FWA10 no 08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089650924228', 'Aji_argani_saputra@rocketmail.com', NULL, NULL, NULL, 'munaji', NULL, '0', 0, 0, 0, '-', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(152, NULL, '2010.11.056', 'Indra solihin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081297159321', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(153, NULL, '2010.11.057', 'Ahmad Rivai', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(154, NULL, '2010.11.058', 'Irma Yulianty', 'W', NULL, 'SERANG', '1992-07-21', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254571402', '085774369534', 'irmayulianti5@gmail.com', NULL, NULL, NULL, 'Yaya Sukria', NULL, '0', 0, 0, 0, 'Nengsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(155, NULL, '2010.11.059', 'Muhammad Yudi Rifahadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808236230', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(156, NULL, '2010.11.060', 'Ferry Antonius', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(157, NULL, '2010.11.061', 'Ghabirqum Ali Fahram', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(158, NULL, '2010.11.062', 'Suryadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(159, NULL, '2010.11.063', 'Muhtadi', 'P', NULL, 'Serang', '1988-06-05', 1, NULL, NULL, 'Link. Kubang Welingi RT 05/03 No.41 Kel./Kec. Purwakarta Kota Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08989660969', 'muhtadi@krakatauposco.co.id', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(160, NULL, '2010.11.064', 'Eriyanti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(161, NULL, '2010.11.065', 'Putra Yuda Adityra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(162, NULL, '2010.11.066', 'Anjar Soerya Sulistio', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(163, NULL, '2010.11.067', 'Tian Toni Harimansyah', 'P', NULL, 'Serang', '1989-06-04', 1, NULL, NULL, 'Link.Cereme RT/RW.05/03 LebakGede Pulomerak Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '085959761181', 'Toni_Tian@yahoo.co.id', NULL, NULL, NULL, 'Sopiyani(Alm)', NULL, '0', 0, 0, 0, 'Titi Hayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(164, NULL, '2010.11.068', 'Tasya Dasmilasari', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(165, NULL, '2010.11.069', 'Ari Suhardi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(166, NULL, '2010.11.070', 'Agung Bayu Pasi', 'P', NULL, 'Jakarta', '1989-01-21', 1, NULL, NULL, 'Komp.Pondok Cilegon Indah Blok D 104/ no. 14 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254-387505', '081288473787', 'agungbayupasi@gmail.com', NULL, NULL, NULL, 'Rudi Pasi', NULL, '0', 0, 0, 0, 'Meryana Gultom', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(167, NULL, '2010.11.071', 'Ali Yusup', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(168, NULL, '2010.11.072', 'Muchamad Iqbal Subchan', 'P', NULL, 'Serang', '1989-09-08', 1, NULL, NULL, 'Link. ciwaduk gede Rt 06/03 no. 152', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087780878418', 'iqbal_steel@yahoo.co.id', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(169, NULL, '2010.11.073', 'Feri Wijaya', 'P', NULL, 'serang', '1989-01-01', 1, NULL, NULL, 'Link. Ketileng barat RT/RW 06/02 Kel. Ketileng, Cilegon, Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906026561', 'feriwijaya89@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(170, NULL, '2010.11.074', 'Ahmad Rohimulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(171, NULL, '2010.11.075', 'Muhamad Said', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(172, NULL, '2010.11.076', 'Sap Adi Pranata', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(173, NULL, '2010.11.077', 'Maharani Novarianti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(174, NULL, '2010.11.078', 'Wiji Hastuti', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(175, NULL, '2010.11.079', 'Rendy Ruslan', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871717178', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(176, NULL, '2010.11.080', 'Bondhan Novianto', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(177, NULL, '2010.11.081', 'Ryan Apriansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(178, NULL, '2010.11.082', 'Sela Andini', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(179, NULL, '2010.11.083', 'Asep Kamal Hakim', 'P', NULL, 'Bandung', '1969-04-13', 1, NULL, NULL, 'Komplek PLN Lebakgede blok G no. 10 RT. 03 RW. 09 Kelurahan Lebakgede, Kecamatan Pulomerak, Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0811124713', 'asep.kamalhakim@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(180, NULL, '2010.11.084', 'Rijal Fatahillah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(181, NULL, '2010.11.085', 'Ema Wahyuni', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(182, NULL, '2010.11.086', 'Astri Meganingrum', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(183, NULL, '2010.11.087', 'Nanang Nurpriyanto', 'P', NULL, 'GK', '1991-04-28', 1, NULL, NULL, 'Buyutan,Ngalang,Gedangsari,GK,Yogyakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08989621636', 'nanangnurpriyanto@ymail.com', NULL, NULL, NULL, 'Bejo Friyanto', NULL, '0', 0, 0, 0, 'Wasinem', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(184, NULL, '2010.11.088', 'Soleh Sahuri', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(185, NULL, '2010.11.089', 'Muhammad Solihin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(186, NULL, '2010.11.090', 'Asep Farhanudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(187, NULL, '2010.11.091', 'Hendra ', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(188, NULL, '2010.11.092', 'Asep Setiawan', 'P', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0877710668888', 'asep.tkm@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(189, NULL, '2010.11.093', 'Nasrudin', 'P', NULL, 'SERANG', '1989-05-14', 1, NULL, NULL, 'JL.KH.ABDUL LATIF NO.118 RT/RW 001/001 PALAS,BENDUNGAN-CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081808106473', 'Nasrudin918@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(190, NULL, '2010.11.094', 'Devi Romansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(191, NULL, '2010.11.095', 'HAIRUZAMAN', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(192, NULL, '2010.12.001', 'Rt.Ratna Purwaningsih', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(193, NULL, '2010.12.002', 'Arti Pertama Dini', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(194, NULL, '2010.12.003', 'Rika Febriyanti', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(195, NULL, '2010.12.004', 'Dina Puji Lestari', 'W', NULL, 'Serang', '1991-08-18', 1, NULL, NULL, 'link.wates telu blok sempu4 no.18 ds.kebondalem kec.purwakarta kota cilegon - banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087808969403', 'dina.puji.lestari@gmail.com', NULL, NULL, NULL, 'Sutrasno', NULL, '0', 0, 0, 0, 'Tumirah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(196, NULL, '2010.12.005', 'Tati Mirnawati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(197, NULL, '2010.12.006', 'Bunga Sri Kurniawan', 'P', NULL, 'SERANG', '1993-04-02', 1, NULL, NULL, 'Komp. Bumi Sari Permai RT.02 RW.10 Kel.Kasemen Kec.Kasemen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085920101700', 'bunga.kurniawan@yahoo.co.id', NULL, NULL, NULL, 'Maskur', NULL, '0', 0, 0, 0, 'Euis Utami Budiati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(198, NULL, '2010.12.007', 'Siti Hariyah', 'W', NULL, 'serang', '1990-09-09', 1, NULL, NULL, 'link delingseng rt 15/01 kec citangkil ds.kebonsari cilegon banten 42442', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087771222802/0856940', 'st.h774@gmail.com', NULL, NULL, NULL, 'halabi', NULL, '0', 0, 0, 0, 'nuratul komariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(199, NULL, '2010.12.008', 'Agus Yusuf', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(200, NULL, '2010.12.009', 'Yusro', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(201, NULL, '2010.12.010', 'M.Irsyad Al Giffari', 'P', NULL, 'Cilegon', '2013-01-20', 1, NULL, NULL, 'LINK. BUMI WARAS DES.TAMANSARI KE.PULOMERAK RT001/003 NO.44', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772441235', 'mister.irsyad@gmail.com', NULL, NULL, NULL, 'SAYI MURI', NULL, '0', 0, 0, 0, 'MURTINI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(202, NULL, '2010.12.011', 'Ni Putu Rani Wiyanti', 'W', NULL, 'Serang', '1992-04-28', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085779559550', 'niputu.rani@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(203, NULL, '2010.12.012', 'Meliyani', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(204, NULL, '2010.12.013', 'Puspita Sari', 'P', NULL, 'Cirebon', '1991-11-01', 1, NULL, NULL, 'Taman krakatau blok J2/07 rt/rw 003/002 margatani-kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087729768841', 'puspita_pita01@yahoo.co.id', NULL, NULL, NULL, 'Sodikun', NULL, '0', 0, 0, 0, 'Imas masriah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(205, NULL, '2010.12.014', 'Rt.Meilinda Purnamasari', 'P', NULL, 'Serang', '1992-05-22', 1, NULL, NULL, 'Lingk. Curug No. 9 RT/RW. 004/003 Kelurahan Rawa Arum Kec. Gerogol Cilegon-Banten 42436', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089652212516', 'ratumeilinda@yahoo.com', NULL, NULL, NULL, 'H. TB Mulyadi Sapta (ALM)', NULL, '0', 0, 0, 0, 'Samiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(206, NULL, '2010.12.015', 'Iif Sarifah', 'P', NULL, 'serang', '1991-03-13', 1, NULL, NULL, 'kp.sempu Rt/rw 003/001 kel.mangkunegara kec.bojonegara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0818169626/083813185', 'difahiif@yahoo.co.id', NULL, NULL, NULL, 'H.A.Mahasyi', NULL, '0', 0, 0, 0, 'Hj.qowariroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(207, NULL, '2010.12.016', 'Ati Destiati', 'P', NULL, 'Serang', '1989-12-04', 1, NULL, NULL, 'Perumnas BCK blok A 14 No 20 RT 02 Rw 06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083890262089', 'dezty_90@yahoo.co.id', NULL, NULL, NULL, 'Binyati', NULL, '0', 0, 0, 0, 'Yanti Sumito', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(208, NULL, '2010.12.017', 'Chalvin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(209, NULL, '2010.21.001', 'Adah Mawadah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(210, NULL, '2010.21.002', 'Galuh Priagung', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(211, NULL, '2010.21.003', 'Rohimi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(212, NULL, '2010.21.004', 'Erwin Dwi Herlina', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(213, NULL, '2010.21.005', 'Ninggar Patsiwi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(214, NULL, '2010.21.006', 'Rikawati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(215, NULL, '2010.21.007', 'Ikhwan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(216, NULL, '2010.21.008', 'Yudi Apriansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(217, NULL, '2010.21.009', 'Siska Dwi Novita', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(218, NULL, '2010.21.010', 'Dila Yusmardila', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(219, NULL, '2010.21.011', 'Ferdinan Fransiskus Sianturi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(220, NULL, '2010.21.012', 'M. Chandra Kristiantono', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(221, NULL, '2010.21.013', 'Ubaidillah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(222, NULL, '2010.21.014', 'Hendrik', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(223, NULL, '2010.21.015', 'Elin Auliana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(224, NULL, '2010.22.001', 'Agung Gufroni', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(225, NULL, '2010.22.002', 'Meti Dera Meilita', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(226, NULL, '2010.22.003', 'Lalan Nurhidayah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(227, NULL, '2010.22.004', 'Diah Arianti', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(228, NULL, '2010.22.005', 'Hendrik', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(229, NULL, '2010.22.006', 'Nurul Khotimah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(230, NULL, '2010.22.007', 'Jumaatul Awaliah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(231, NULL, '2010.22.008', 'Eriska Rimbun Lestari', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(232, NULL, '2010.22.009', 'Wilda Septini Melinda', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(233, NULL, '2010.22.010', 'Yeni Rianawati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(234, NULL, '2010.22.011', 'Fika Ramadhani', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(235, NULL, '2010.22.012', 'Indah Mayasari', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(236, NULL, '2010.22.013', 'Iha sutihah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(237, NULL, '2010.22.014', 'Anita Tunisia', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(238, NULL, '2010.22.015', 'Saliatun', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(239, NULL, '2010.22.016', 'Miftahul Hasanah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(240, NULL, '2010.22.017', 'Rahmat Imanullah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(241, NULL, '2010.22.018', 'Ahmad Syamsudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(242, NULL, '2011.11.001', 'Pelindra', 'P', NULL, 'Liwa', '1988-06-25', 1, NULL, NULL, 'Jl. Belibis V Blok E15 No. 1 Perumnas Bumi Cebeber Kencana Cilegon_Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081386067265', 'indraprihandi@yahoo.co.id', NULL, NULL, NULL, 'Prihandiyono M', NULL, '0', 0, 0, 0, 'Nurwani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(243, NULL, '2011.11.002', 'Sohandi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081932700424', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(244, NULL, '2011.11.003', 'Bachrudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(245, NULL, '2011.11.004', 'Fortunata Betsheda', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(246, NULL, '2011.11.005', 'Asep nurul habibi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(247, NULL, '2011.11.006', 'Budiyono', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(248, NULL, '2011.11.007', 'Bimbi Islakhudin', 'P', NULL, 'Mojokerto', '1989-11-16', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085718413363', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(249, NULL, '2011.11.008', 'David Agung Junaedi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(250, NULL, '2011.11.009', 'Fribianto Dwi Cahya A', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(251, NULL, '2011.11.010', 'Sigit Dwi Siswanto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(252, NULL, '2011.11.011', 'Rendy', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(253, NULL, '2011.11.012', 'Ardhi Taufik Turmudzi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(254, NULL, '2011.11.013', 'Yopi Riskiyana Tanjung', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(255, NULL, '2011.11.014', 'Maman Komaruzzaman', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(256, NULL, '2011.11.015', 'M. Apip Pulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(257, NULL, '2011.11.016', 'Dian Rusmana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(258, NULL, '2011.11.017', 'Muhammad Syarifuddin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(259, NULL, '2011.11.018', 'Dian Andrianto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(260, NULL, '2011.11.019', 'Bambang Setyawan', 'P', NULL, 'SURAKARTA', '1975-05-08', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081316857321', 'bambang.setyawan@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(261, NULL, '2011.11.020', 'Ninggar Patsiwi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(262, NULL, '2011.11.021', 'Risna Mulyana', 'P', NULL, 'Serang', '1989-04-19', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812840570', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(263, NULL, '2011.11.022', 'Suci Khoirunnisa', 'W', NULL, 'SERANG', '1990-09-05', 1, NULL, NULL, 'JL.H.UMAR NO 177 LINK TEMUPUTIH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813227262', 'suci_khoirunnisa@ymail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(264, NULL, '2011.11.023', 'Hajuro', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(265, NULL, '2011.11.024', 'Rudi Santana Aripin', 'P', NULL, 'Garut', '1972-02-14', 1, NULL, NULL, 'Link.Tegal Tong RT 11 RW 06 No.138 Kebonsari Citangkil Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087808651959', 'rudisantana@yahoo.com', NULL, NULL, NULL, 'A. Rokanda', NULL, '0', 0, 0, 0, 'Itar Sunarti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(266, NULL, '2011.11.025', 'Ahmad Saifudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(267, NULL, '2011.11.026', 'Rohyadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(268, NULL, '2011.11.027', 'Maurid Simanjuntak', 'P', NULL, 'Sibolga', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081932332924', 'kamsinzarkasih@gmail.com', NULL, NULL, NULL, 'Simanjuntak', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(269, NULL, '2011.11.028', 'Achmad Ridwan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(270, NULL, '2011.11.029', 'Sandi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(271, NULL, '2011.11.030', 'Agust Rahmatulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(272, NULL, '2011.11.031', 'Ali Solehudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(273, NULL, '2011.11.032', 'Dini Aprilia', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'diniaprilia5@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(274, NULL, '2011.11.033', 'Juman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(275, NULL, '2011.11.034', 'Ari Irawan', 'P', NULL, 'Purbalingga', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911088860', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(276, NULL, '2011.11.035', 'Ovan Ady Santoso', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(277, NULL, '2011.11.036', 'Aqna', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(278, NULL, '2011.11.037', 'Fikri Octora', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(279, NULL, '2011.11.038', 'M. Sabilarrosyad', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(280, NULL, '2011.11.039', 'Risqoni', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(281, NULL, '2011.11.040', 'Asad Fausa Yoedhistira', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(282, NULL, '2011.11.041', 'Saptiyani Rahayu', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(283, NULL, '2011.11.042', 'Akhmad Tohir', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(284, NULL, '2011.11.043', 'Yana Mulyana', 'P', NULL, 'SUMEDANG', '2014-01-05', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081281952929', 'yanamulyana44@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(285, NULL, '2011.11.044', 'Ahmad zein ramadhan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(286, NULL, '2011.11.045', 'Kudus Abdurrohim', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(287, NULL, '2011.11.046', 'Ita Purnamasari', 'W', NULL, 'Serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(288, NULL, '2011.11.047', 'Ahmad eggih setiadi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(289, NULL, '2011.11.048', 'Husnul Fulqi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(290, NULL, '2011.11.049', 'Anggun Astuti Mustika', 'W', NULL, 'Cilegon', '1993-04-23', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774117762', 'Anggun281113@gmail.com', NULL, NULL, NULL, 'BADRUDIN', NULL, '0', 0, 0, 0, 'SRI ATUN HASANAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(291, NULL, '2011.11.050', 'Numan Sumantri', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(292, NULL, '2011.11.051', 'Arini', 'W', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771896380', 'arin_nca@ymail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(293, NULL, '2011.11.052', 'Isma Juwita', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(294, NULL, '2011.11.053', 'Lusy Maylani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(295, NULL, '2011.11.054', 'Nurhayati', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(296, NULL, '2011.11.055', 'Achmad Karyana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(297, NULL, '2011.11.056', 'Anis Habibie', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(298, NULL, '2011.11.057', 'Anton Ardiansyah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(299, NULL, '2011.11.058', 'Nunung Lesmana', 'W', NULL, 'SERANG', '1989-01-29', 1, NULL, NULL, 'JL.FATAHILLAH LINK.SERANG ILIR RT.07/02 KEL.RANDAKARI KEC.CIWANDAN - CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906240626', 'nunung_lesmana@rocketmail.com', NULL, NULL, NULL, 'HABIBI', NULL, '0', 0, 0, 0, 'SUAMAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(300, NULL, '2011.11.059', 'Azrul Azis', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(301, NULL, '2011.11.060', 'Mohamad Lazim Ichromi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871554953', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(302, NULL, '2011.11.061', 'Eray Atmada', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808546498', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(303, NULL, '2011.11.062', 'Alimudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(304, NULL, '2011.11.063', 'Ali Yusron', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(305, NULL, '2011.11.064', 'Lilis Septiani', 'W', NULL, 'serang', '1993-09-17', 1, NULL, NULL, 'Jl. Yos sudarso link.baru 2 Rt.01/06 Gg.cempaka no.23 kel. lebak gede kec. pulo merak kota.cilegon profinsi. banten ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254574374', '08976224022', '', NULL, NULL, NULL, 'Topan', NULL, '0', 0, 0, 0, 'wiwit', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(306, NULL, '2011.11.065', 'Abdul Malik Hakim', 'P', NULL, 'SERANG', '1993-03-26', 1, NULL, NULL, 'Link. Lebak Indah Rt.02/Rw.09 Desa. Lebak Gede Kec. PULOMERAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083878331351/0877738', 'abdulmalik_hakim@yahoo.com', NULL, NULL, NULL, 'JEMEAN', NULL, '0', 0, 0, 0, 'ANTIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(307, NULL, '2011.11.066', 'Heru', 'P', NULL, 'serang', '1992-06-03', 1, NULL, NULL, 'Kp.Curug No.10 Kec.Grogol Kel.Rawa Arum RT/RW 004/003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0878713235473', 'heruronaldo09@gmail.com', NULL, NULL, NULL, 'Ulung Sugianto', NULL, '0', 0, 0, 0, 'Rida Wati Harahap', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(308, NULL, '2011.11.067', 'Nastiar Adi Purwanto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(309, NULL, '2011.11.068', 'Rohman', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(310, NULL, '2011.11.069', 'Ade Hidayatullah', 'P', NULL, 'Serang', '2013-12-31', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774149553', 'adecantona88@yahoo.co.id', NULL, NULL, NULL, 'Badrudin', NULL, '0', 0, 0, 0, 'Salamah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(311, NULL, '2011.11.070', 'Harces Salry Efriawan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(312, NULL, '2011.11.071', 'Halwani', 'P', NULL, 'Cilegon', '1991-11-21', 1, NULL, NULL, 'LINK.KUBANG LAMPIT RT/RW: 001/001 KEL/DESA:Tegal Bunder KECAMATAN: Purwakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911190572', 'halwani2111.0212@gmail.com', NULL, NULL, NULL, 'Sanim', NULL, '0', 0, 0, 0, 'Halimah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(313, NULL, '2011.11.072', 'Usi Oktari', 'W', NULL, 'Serang', '1992-10-23', 1, NULL, NULL, 'Komp Harjatani Permai Blok P/12 RT 005/004 Kel. Harjatani Permai Kec. Kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254387338', '089651043673', 'oktari23@yahoo.com', NULL, NULL, NULL, 'Dadi Kusnadi', NULL, '0', 0, 0, 0, 'Sri Yuningsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(314, NULL, '2011.11.073', 'Firman Hidayat', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(315, NULL, '2011.11.074', 'M. Maulana Nurul Amin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(316, NULL, '2011.11.075', 'Yunus Prayogo', 'P', NULL, 'serang', '1992-11-28', 1, NULL, NULL, 'komp. pemda sumur pecung baru', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089667655098', 'yunus_iu@y7mail.com', NULL, NULL, NULL, 'Giyatmanto', NULL, '0', 0, 0, 0, 'marfuah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(317, NULL, '2011.11.076', 'Anna Stefhanny', 'W', NULL, 'Serang', '1992-09-03', 1, NULL, NULL, 'LINK.Lebak Indah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '574364', '081282765513', 'anna_stefhanny@yahoo.co.id', NULL, NULL, NULL, 'MULIHANTO', NULL, '0', 0, 0, 0, 'LENAWATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(318, NULL, '2011.11.077', 'M. Lingga Winata', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(319, NULL, '2011.11.078', 'Neri Januari Stiani', 'W', NULL, 'ciamis', '1992-01-16', 1, NULL, NULL, 'Dusun Cidadap', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085724534824', 'nerijanuari@yahoo.com', NULL, NULL, NULL, 'Endin Beni Sasmita, S.pd, M.pd', NULL, '0', 0, 0, 0, 'Nonoh Anisah, S.pd', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(320, NULL, '2011.11.079', 'Desi Ratnasari', 'P', NULL, 'Serang', '2013-10-22', 1, NULL, NULL, 'Link. Tegal Cabe Citangkil Cilegon-BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089608748984', 'dessyratnasari265@yahoo.com', NULL, NULL, NULL, 'H.AHMAD SOBIRIN', NULL, '0', 0, 0, 0, 'SITI JUHRIATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(321, NULL, '2011.11.080', 'Roni Aditya', 'P', NULL, 'Serang', '2013-09-30', 1, NULL, NULL, 'JL.Kakap No.26 Kav.Blok C Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '391257', '081906060692', 'aditya.roni@gmail.com', NULL, NULL, NULL, 'Muji ', NULL, '0', 0, 0, 0, 'Wakirah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(322, NULL, '2011.11.081', 'Muhamad Safei', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(323, NULL, '2011.11.082', 'Abdul Rojak', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(324, NULL, '2011.11.083', 'Gojali', 'P', NULL, 'SERANG', '1991-11-06', 1, NULL, NULL, 'link. kubang welingi rt/rw 007/003 kel/desa . purwakarta kec. purwakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906016159', '', NULL, NULL, NULL, 'sohari', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(325, NULL, '2011.11.084', 'Zanmifah Jeges Panogu', 'P', NULL, 'serang', '1993-03-03', 1, NULL, NULL, 'komp.BAPPL-STP no 17 RT 002/005 banten lama, kasemen, serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08998640049/08780927', 'jegezpanogu@gmail.com', NULL, NULL, NULL, 'Iskandar P', NULL, '0', 0, 0, 0, 'Elin S', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(326, NULL, '2011.11.085', 'Wisky Yuliana Sari', 'P', NULL, 'Serang,20 Agustus 19', '1992-08-20', 1, NULL, NULL, 'Jl.S.Kalijaga Link.Pangabuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906092434', 'wiskyyulianasari@yahoo.com', NULL, NULL, NULL, 'Masuki', NULL, '0', 0, 0, 0, 'Sarminah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(327, NULL, '2011.11.086', 'Septian Pratama', 'P', NULL, 'serang', '2013-09-29', 1, NULL, NULL, 'link. baru 1 desa lebak gede kecamatan pulomerak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871401229', 'undergrouners@rocketmail.com', NULL, NULL, NULL, 'sairan', NULL, '0', 0, 0, 0, 'tati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(328, NULL, '2011.11.087', 'Hendra Abdurrahman', 'P', NULL, 'Serang', '1993-10-19', 1, NULL, NULL, 'KP Sumuranja', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813118092', 'hendraphoenix@gmail.com', NULL, NULL, NULL, 'Alexander', NULL, '0', 0, 0, 0, 'N.Siti Mariam', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(329, NULL, '2011.11.088', 'Hendri Abdurrahman', 'P', NULL, 'Serang', '1993-10-19', 1, NULL, NULL, 'KP SUMURANJA RT/RW 002/001 Desa/Kel Sumuranja Kecamatan Puloampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087898461436', 'hendrimandrake@gmail.com', NULL, NULL, NULL, 'Alexander', NULL, '0', 0, 0, 0, 'N.Siti Mariam', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(330, NULL, '2011.11.089', 'Aan Lendra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(331, NULL, '2011.11.090', 'Randy Ashari Nugraha', 'P', NULL, 'serang', '1991-08-28', 1, NULL, NULL, 'PONDOK CILEGON INDAH BLOK C65 NO 6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'randyashari@ymail.com', NULL, NULL, NULL, 'DEDE MULYANA', NULL, '0', 0, 0, 0, 'ISTIANAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(332, NULL, '2011.11.091', 'Fikri Sidki Noval', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(333, NULL, '2011.11.092', 'Angga Elfian', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(334, NULL, '2011.11.093', 'Nurgustianto Badar (TI07-K)', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(335, NULL, '2011.11.094', 'Qaisul Alam', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(336, NULL, '2011.11.095', 'Afrida Ita Fitriani', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771666078', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(337, NULL, '2011.11.096', 'Hikmatulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(338, NULL, '2011.11.097', 'Adang Hermawan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(339, NULL, '2011.11.098', 'Riswanto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(340, NULL, '2011.11.099', 'Jaya Julyono', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(341, NULL, '2011.11.100', 'Heryanto Oktapriyatna', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(342, NULL, '2011.11.101', 'A. Hasan Bisri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081381846228', 'ach.hasbi@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(343, NULL, '2011.11.102', 'Muh. Asri Domut', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(344, NULL, '2011.11.103', 'Sandriansyah Derlauw (K1)', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(345, NULL, '2011.11.104', 'Saiful Rijal', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(346, NULL, '2011.11.105', 'Ujang Suhardi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(347, NULL, '2011.11.106', 'Sri Wulandari', 'P', NULL, 'Rancah', '1991-12-10', 1, NULL, NULL, 'Kp.Rawa Silem 06/08 N0.69 Kel.Kaliabang Tengah Kec.Bekasi Utara Kota Bekasi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871253494', 'sri591@ymail.com', NULL, NULL, NULL, 'Wawan Raswan', NULL, '0', 0, 0, 0, 'Esah Nani Amelia', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(348, NULL, '2011.11.115', 'Rika Febriyanti', 'W', NULL, 'Lampung', '2013-10-15', 1, NULL, NULL, 'Perumnas Bumi Cibeber Kencana Blok A.03 No.19 Rt/01 Rw/06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '087880108859', '081911121219', 'rikasttikomiu@yahoo.co.id', NULL, NULL, NULL, 'Suhaimi J', NULL, '0', 0, 0, 0, 'Roida Rais', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(349, NULL, '2011.12.001', 'Diya Isnanisa', 'P', NULL, 'SERANG', '1993-06-17', 1, NULL, NULL, 'LINK.KERAMAT RT/RW:018/005 Kel/Desa:TEGAL RATU Kecamatan: CIWANDAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(0254) 311571', '+6289683204655', 'Diyaz_always@rocketmail.com', NULL, NULL, NULL, 'MAHDI', NULL, '0', 0, 0, 0, 'Sudaryati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(350, NULL, '2011.12.002', 'Agung Mufti Silahudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(351, NULL, '2011.12.003', 'Agus Jubaidi', 'P', NULL, 'Cilegon', '1992-05-04', 1, NULL, NULL, 'Link. Pakuncen RT/RW 004/003 Ciwedus-Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08978927813', 'agus.jubaidi0@gmail.com', NULL, NULL, NULL, 'Hatibi', NULL, '0', 0, 0, 0, 'Arbaiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(352, NULL, '2011.12.004', 'Rizki Pratama Dunggio', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(353, NULL, '2011.12.005', 'Fanny Karmila', 'P', NULL, 'subang', '1991-02-19', 1, NULL, NULL, 'Perum cibeber blok c no 07/ 20 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774066695', 'fanny.karmila@gmail.com', NULL, NULL, NULL, 'hendra chandra', NULL, '0', 0, 0, 0, 'rini parnengsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(354, NULL, '2011.12.006', 'Ika Julaehah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(355, NULL, '2011.12.007', 'Nia Ardillah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(356, NULL, '2011.12.008', 'Saddam', 'P', NULL, 'Batam', '1991-11-09', 1, NULL, NULL, 'GSI Blok C7 No 7 RT 004 RW 005 Margatani Kec Kramatwatu, Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254 383172', '085779992582', 'asdamgrimson@gmail.com', NULL, NULL, NULL, 'Zarmiddin', NULL, '0', 0, 0, 0, 'Masyithah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(357, NULL, '2011.12.009', 'Dedy Saputra', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(358, NULL, '2011.12.010', 'Laura Apriliya Sitohang', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(359, NULL, '2011.12.011', 'Rahmawati', 'P', NULL, 'serang', '1994-03-08', 1, NULL, NULL, 'Link.Lijajar RT/RW : 013/006 Kel/Desa : Tegal Ratu Kec : Ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08998639204', 'rahmawatirara66@yahoo.com', NULL, NULL, NULL, 'saeful Hadi', NULL, '0', 0, 0, 0, 'Maryati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(360, NULL, '2011.12.012', 'Ryan Putra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(361, NULL, '2011.12.014', 'Rini Yunita Sari ', 'P', NULL, 'Serang', '1993-06-17', 1, NULL, NULL, 'Link.Baru satu RT04/Rw04 Lebakgede Pulomerak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0085945452366', 'rini_yuinta17@yahoo.com', NULL, NULL, NULL, 'H.jajang', NULL, '0', 0, 0, 0, 'Hj.Suwangsih(Alm)', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(362, NULL, '2011.12.015', 'Suimah', 'P', NULL, 'serang', '1993-11-12', 1, NULL, NULL, 'Jl.Fatahillah Link.Gelereng Rt/Rw:006/002 Kel/Desa:Randakari Kec.Ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089665539514', 'suimah81@yahoo.com', NULL, NULL, NULL, 'Sayuti', NULL, '0', 0, 0, 0, 'Rasiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(363, NULL, '2011.12.016', 'Cecep Hambali', 'P', NULL, 'cilegon', '2013-03-09', 1, NULL, NULL, 'link,lebak gebang ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771089869', 'cecep.takutV@gmail.com', NULL, NULL, NULL, 'h.saluhi alm', NULL, '0', 0, 0, 0, 'hj.open nurasiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(364, NULL, '2011.12.017', 'Mirnawati', 'P', NULL, 'MERAK', '1993-05-02', 1, NULL, NULL, 'LINK. SUKA SENANG RT 03/01 Desa. TAMANSARI Kec. PULOMERAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '097778505189', 'mirna.mbemm@yahoo.com', NULL, NULL, NULL, 'SUWARDI', NULL, '0', 0, 0, 0, 'MARYAM', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(365, NULL, '2011.12.018', 'Fustianah', 'P', NULL, 'serang', '1993-03-01', 1, NULL, NULL, 'jl.salira indah no12.rt 04/02 kp.raga sambilawang desa margasari kec.puloampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087709923180', 'fustianah@gmail.com', NULL, NULL, NULL, 'junaidi mustamin', NULL, '0', 0, 0, 0, 'asikah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(366, NULL, '2011.12.019', 'Tri Wahyu', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(367, NULL, '2011.12.020', 'Irfan Setyawan', 'P', NULL, 'Pulomersk', '1990-10-17', 1, NULL, NULL, 'Link. Batu Gepeng RT/RW 002/008 Kel/Desa Gerem Kecamatan Gerogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808828882', 'irfansetiawan35@gmail.com', NULL, NULL, NULL, 'Sahabat', NULL, '0', 0, 0, 0, 'Tunafiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(368, NULL, '2011.12.021', 'Ali Yusron', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(369, NULL, '2011.12.022', 'Robby Nurul Huda', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(370, NULL, '2011.21.001', 'Agus Jayatullah', 'P', NULL, 'Serang', '1990-08-01', 1, NULL, NULL, 'Kp.Legon Asem RT/RW 012/007 Des.Pengarengan Kec.Bojonegara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774456726', 'agus01jaya@gmail.com', NULL, NULL, NULL, 'Ismail', NULL, '0', 0, 0, 0, 'Sarwati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(371, NULL, '2011.21.002', 'Khoirul Umam', 'P', NULL, 'serang', '1992-06-25', 1, NULL, NULL, 'jl. lembang III no 7b', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771602225', '', NULL, NULL, NULL, 'suhilel', NULL, '0', 0, 0, 0, 'nasibah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(372, NULL, '2011.21.003', 'Dewi Kurniawati', 'W', NULL, 'Serang', '1992-10-11', 1, NULL, NULL, 'Link Tegal Cabe RT/RW: 04/02 No: 68 Kel/Desa: Citangkil Kecamatan: Citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085691527880', 'dewi53kurnia@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(373, NULL, '2011.21.004', 'Dimas Adityo Nugroho', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(374, NULL, '2011.21.005', 'Hery Setiawan', 'P', NULL, 'Cilegon, Banten', '1992-10-15', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254-391361', '087771310710', 'gardenfairy92@yahoo.com', NULL, NULL, NULL, 'Tugiyanto', NULL, '0', 0, 0, 0, 'Sumarni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(375, NULL, '2011.21.006', 'Ria Fathiah', 'W', NULL, 'serang', '1992-01-09', 1, NULL, NULL, 'BBS 2 jln anggrek 1 no 9 blok D2 - cilegon ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '389839', '087877964718', 'riafathiah@gmail.com ', NULL, NULL, NULL, 'Yuharman ', NULL, '0', 0, 0, 0, 'Yusmiana', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(376, NULL, '2011.21.008', 'Ani Nofiasari', 'W', NULL, 'Serang', '1993-03-05', 1, NULL, NULL, 'link.kepindis RT/RW 001/008 kel.LEBAKGEDE Kec.PULOMERAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945671219', '', NULL, NULL, NULL, 'Saffi', NULL, '0', 0, 0, 0, 'Mastuah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(377, NULL, '2011.21.009', 'Rikky Charles', 'P', NULL, 'cilegon', '2013-10-08', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087871345710', 'rikky.charles@yahoo.co.id', NULL, NULL, NULL, 'Ghortap jhoni Panggabean(alm)', NULL, '0', 0, 0, 0, 'sumiati (alm)', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(378, NULL, '2011.21.010', 'Andri Gunawan', 'P', NULL, 'SERANG', '1992-08-08', 1, NULL, NULL, 'LINK. JOMBANG KALI RT 03/RW 01 KEL. MASIGIT KEC. JOMBANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081910945252', 'drieandrhiie92@gmail.com', NULL, NULL, NULL, 'Madjueni', NULL, '0', 0, 0, 0, 'Mutiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(379, NULL, '2011.21.011', 'Shendy Arief Giwara', 'P', NULL, 'Serang', '1993-05-15', 1, NULL, NULL, 'Baros-serang, RT06/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089678834833', 'Shendyariefgiwara@gmail.com', NULL, NULL, NULL, 'Sugeng Santoso', NULL, '0', 0, 0, 0, 'Encop Shofia', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(380, NULL, '2011.21.012', 'Fredi Gunawan', 'P', NULL, 'serang', '1989-08-23', 1, NULL, NULL, 'Jl.kh Abdul Latif no 88 rt/rw 02/01 sumur pecung serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871598546', 'fredigunawan21@gmail.com', NULL, NULL, NULL, 'Tartusi Muchtar', NULL, '0', 0, 0, 0, 'Fatimah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(381, NULL, '2011.21.013', 'Bayu Tri Cahya', 'P', NULL, 'SERANG', '0000-00-00', 1, NULL, NULL, 'JL. GUNUNG KARANG NO.6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254 394373', '087871003248', '', NULL, NULL, NULL, 'SUPOYO EDI S', NULL, '0', 0, 0, 0, 'ZAHRA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(382, NULL, '2011.22.001', 'Rini Anggraini', 'P', NULL, 'Serang', '1993-03-21', 1, NULL, NULL, 'Kp. Sumuranja. RT/RW: 07/004 Kec.Pulo Ampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771239087', 'Rini.Anggraini7@yahoo.co.id', NULL, NULL, NULL, 'H. Abdul Muhit', NULL, '0', 0, 0, 0, 'H.Mahdufa', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(383, NULL, '2011.22.002', 'Renia Puan Pausoan', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(384, NULL, '2011.22.003', 'Siska Novianti', 'W', NULL, 'Pangkalan Babat', '0000-00-00', 1, NULL, NULL, 'Kragilan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'siska_marabes@yahoo.com', NULL, NULL, NULL, 'Mulyono', NULL, '0', 0, 0, 0, 'Rugaya', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(385, NULL, '2011.22.004', 'Eka Sartika', 'W', NULL, 'serang', '1992-10-26', 1, NULL, NULL, 'Jl.Anggrek 3 No 10 BBS 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808103297', 'ekasartika@ymail.com', NULL, NULL, NULL, 'H. Sulaiman', NULL, '0', 0, 0, 0, 'HJ.Sumarni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(386, NULL, '2011.22.005', 'Sulhiyah', 'W', NULL, 'Cilegon', '1991-04-28', 1, NULL, NULL, 'jl.kh ishak link seneja ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '391447', '083871044728', 'sulhiyah85@yahoo.com', NULL, NULL, NULL, 'jubaeidi ', NULL, '0', 0, 0, 0, 'bahariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(387, NULL, '2011.22.006', 'Siti Aminah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(388, NULL, '2011.22.007', 'Zulia Algantini', 'P', NULL, 'SERANG', '1992-04-03', 1, NULL, NULL, 'GEREM RAYA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087771861493', 'zalgantini@yahoo.com', NULL, NULL, NULL, 'DARHIM', NULL, '0', 0, 0, 0, 'DEDEH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(389, NULL, '2011.22.008', 'Asdewy Herlina', 'W', NULL, 'serang', '1993-08-06', 1, NULL, NULL, 'gerem raya rt/rw 002/004 no 44b gerem-grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254 572381', '087774499771', 'asdewiherlina@yahoo.com', NULL, NULL, NULL, 'alm. abdul hamid', NULL, '0', 0, 0, 0, 'hj maryati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(390, NULL, '2011.22.009', 'Tri Wulan', 'P', NULL, 'Indramayu', '1993-05-13', 1, NULL, NULL, 'Desa/Kel. Sukaslamet blok Lemah Rempag RT/RW 027/007 kec. Kroya - Indramayu Jawa Barat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774777676', 'wulan.yayank@yahoo.com', NULL, NULL, NULL, 'Sutaji', NULL, '0', 0, 0, 0, 'Eti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(391, NULL, '2011.22.010', 'Syahruroh ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(392, NULL, '2011.22.011', 'Esa Mahesa Sejati', 'P', NULL, 'Serang', '1989-06-12', 1, NULL, NULL, 'Link,ketileng timur Rt.03/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081905211079', 'esamahesasejati@rocketmail.com', NULL, NULL, NULL, 'Alm.bapak kasnoyo', NULL, '0', 0, 0, 0, 'Maesaroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(393, NULL, '2011.22.012', 'Syafianah', 'W', NULL, 'SERANG', '1992-08-01', 1, NULL, NULL, 'JOMBANG KALI RT001/RW001 NO.43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087778503535', 'SSYAFIANAH@YAHOO.COM', NULL, NULL, NULL, 'M. SYAFEI', NULL, '0', 0, 0, 0, 'SURYANAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(394, NULL, '2011.22.013', 'Muhammad Saddam Husen', 'P', NULL, 'Serang', '1991-07-14', 1, NULL, NULL, 'Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945459496', '', NULL, NULL, NULL, 'Alm H. Poerwadi H.S', NULL, '0', 0, 0, 0, 'Hj. Alinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(395, NULL, '2012.11.001', 'Ahmad Suryana', 'P', NULL, 'Serang', '1993-07-16', 1, NULL, NULL, 'Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911772496', 'ahmad.suryana10@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(396, NULL, '2012.11.002', 'Yuono', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(397, NULL, '2012.11.003', 'Astri Meganingrum', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(398, NULL, '2012.11.004', 'Vicko Augustian Pratama Putra', 'P', NULL, 'JAKARTA', '2013-08-11', 1, NULL, NULL, 'Komp. Puri Serang Hijau Blok K5 No.7 RT 04/15 Cipocok,Serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085966787949', '08988262785', 'vaugustian@gmail.com', NULL, NULL, NULL, 'EKO NUGROHO YULIANTO', NULL, '0', 0, 0, 0, 'NURHAYATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(399, NULL, '2012.11.005', 'Eni Kurniawati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085920003422', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(400, NULL, '2012.11.006', 'Mahmuda Aulia Rahman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(401, NULL, '2012.11.007', 'Edi Permana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(402, NULL, '2012.11.008', 'Andhika Sukma P', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(403, NULL, '2012.11.009', 'Saiful Rohmat', 'P', NULL, 'Serang', '1990-11-10', 1, NULL, NULL, 'Anyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906470318', 'saifulbae52@yahoo.co.id', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(404, NULL, '2012.11.010', 'TB. Arif Khalifah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(405, NULL, '2012.11.011', 'Dede Sudrajat', 'P', NULL, 'Serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(406, NULL, '2012.11.012', 'Rahmatul Hajiji', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(407, NULL, '2012.11.013', 'Umi Nazipah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(408, NULL, '2012.11.014', 'Muhammad Nugroho Susanto', 'P', NULL, 'KLATEN', '1991-12-16', 1, NULL, NULL, 'KP.CILODAN RT 017/005 GUNUNG SUGIH, CIWANDAN, CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087808277700', 'MUHAMMAD.NUGROHO91@GMAIL.COM', NULL, NULL, NULL, 'YUSMANTO', NULL, '0', 0, 0, 0, 'WARTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(409, NULL, '2012.11.015', 'Niko Adriansyah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(410, NULL, '2012.11.016', 'Didi Setiadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(411, NULL, '2012.11.017', 'Ade Setiawan', 'P', NULL, 'cilegon', '1994-03-15', 1, NULL, NULL, 'Jln Sunan Demak Link Gunung Asem Rt 13/03 Desa Kepuh Kec Ciwandan-Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774747907', 'Ade.setiawan670@gmail.com', NULL, NULL, NULL, 'Madnoh', NULL, '0', 0, 0, 0, 'Hayanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(412, NULL, '2012.11.018', 'Sofanudin', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(413, NULL, '2012.11.019', 'Chairul Umam', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(414, NULL, '2012.11.020', 'Suhendi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(415, NULL, '2012.11.021', 'Hilman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(416, NULL, '2012.11.022', 'Chalvin ', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(417, NULL, '2012.11.023', 'Rizki Indra Pratama', 'P', NULL, 'Jakarta', '1990-07-20', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871755777', 'q.winter666@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(418, NULL, '2012.11.024', 'Dede Kurniawan', 'P', NULL, 'Jakarta', '1989-04-15', 1, NULL, NULL, 'Jl. KH. Yasin Beji 02 Kp. Beji 05/02 Desa/Kec. Bojonegara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906125697', 'dedekurniawan_beji@yahoo.com', NULL, NULL, NULL, 'Mufrodi', NULL, '0', 0, 0, 0, 'Wagiyem', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(419, NULL, '2012.11.025', 'Irvan Rifai', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(420, NULL, '2012.11.026', 'Asep Indra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(421, NULL, '2012.11.027', 'Dedi Lasriadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(422, NULL, '2012.11.028', 'Guntur Hidayat', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(423, NULL, '2012.11.029', 'Erwin Anwar Sirait', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(424, NULL, '2012.11.030', 'Chaerul Amri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(425, NULL, '2012.11.031', 'Muhammad Sauki', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(426, NULL, '2012.11.032', 'Rosita', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(427, NULL, '2012.11.033', 'Avria Sih Nita Rahayu Tambunan', 'P', NULL, 'Sumbersari', '2014-04-10', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081928280080', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(428, NULL, '2012.11.034', 'Fajar Agung Prasetyo', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(429, NULL, '2012.11.035', 'Jonri Rubersan', 'P', NULL, 'Medan', '1990-10-31', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(430, NULL, '2012.11.036', 'Tifanny Fridayanti', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(431, NULL, '2012.11.037', 'Rudi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774514252', 'rudi.rr60@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(432, NULL, '2012.11.038', 'Ratu Ayu Fitriana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(433, NULL, '2012.11.039', 'Wachid Zakaria', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(434, NULL, '2012.11.040', 'Ade Rezki muhamad', 'P', NULL, 'Serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Muhamad.AdeRezki@yahoo.co.id', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(435, NULL, '2012.11.041', 'Iman Muhamad Nur', 'P', NULL, 'PANDEGLANG', '1992-07-01', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773833208', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(436, NULL, '2012.11.042', 'Ahmad Zawawi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(437, NULL, '2012.11.043', 'Uniyati', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(438, NULL, '2012.11.044', 'Muchlas', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871602252', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(439, NULL, '2012.11.045', 'Rizki Jaka Maulana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(440, NULL, '2012.11.046', 'Atik Ivo Widya Ningrum', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(441, NULL, '2012.11.047', 'Deni Arrasuly (TI08K2)', 'P', NULL, 'Serang', '1989-07-01', 1, NULL, NULL, 'Lingkungan Cubul Rt02/03 Kelurahan Suralaya Kecamatan Pulomerak cilegon Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085693744556/0859451', 'arrasully@yahoo.co.id/arrasuly1@gmail.com', NULL, NULL, NULL, 'Sahrudin', NULL, '0', 0, 0, 0, 'Turiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(442, NULL, '2012.11.048', 'Angga Renaldy', 'P', NULL, 'Jakarta.15 Mei 1990', '0000-00-00', 1, NULL, NULL, 'Jl.Alamanda 5 No.15, Bukit Palem, Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '340150', '081808434260', 'anggarenaldy@gmail.com', NULL, NULL, NULL, 'Yon Joni', NULL, '0', 0, 0, 0, 'Nelyati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(443, NULL, '2012.11.049', 'Teddy Marco Johali', 'P', NULL, 'Palembang', '1993-03-14', 1, NULL, NULL, 'Perum.PCI Blok D50 No 08 .RT 02 RW 04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089650766183', '', NULL, NULL, NULL, 'Johali Hassan', NULL, '0', 0, 0, 0, 'Mulyani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(444, NULL, '2012.11.050', 'Adi Cahyadi', 'P', NULL, 'serang', '1992-06-12', 1, NULL, NULL, 'link,bujang gadung no,46 Rt,02/03 kec,Grogol. Kec,Grogol Kota,cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08998639205', 'adiecahyadi44@yahoo.com', NULL, NULL, NULL, 'Hamid', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(445, NULL, '2012.11.051', 'Afif Rahmandani', 'P', NULL, 'Sukoharjo', '1994-01-14', 1, NULL, NULL, 'Mranggen, Polokarto, Sukoharjo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808110613', 'arahmandani@gmail.com', NULL, NULL, NULL, 'Abdul Nurlin', NULL, '0', 0, 0, 0, 'Siti Chotimah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(446, NULL, '2012.11.052', 'Hilmi', 'P', NULL, 'SERANG', '1992-10-17', 1, NULL, NULL, 'LINK KAROTEK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089696908835', 'hilmi_1752@ymail.com', NULL, NULL, NULL, 'H.Tusali (Alm)', NULL, '0', 0, 0, 0, 'Hj.Asmariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(447, NULL, '2012.11.053', 'Dandy Zakaria', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(448, NULL, '2012.11.054', 'Dede Heryadi', 'P', NULL, 'pandeglang', '1994-03-08', 1, NULL, NULL, 'Kp.Ciatuy, Ds.Mekarjaya, Kec.cikedal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085289704426', 'dedeheryadi212@gmail.com', NULL, NULL, NULL, 'Sadria', NULL, '0', 0, 0, 0, 'Sarimanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(449, NULL, '2012.11.055', 'Aa Cecep Nursamsi', 'P', NULL, 'Serang', '1994-04-06', 1, NULL, NULL, 'KP RAGAS AWURAN RT/RW 002/001 DESA MARGASARI KECAMATAN PULO AMPEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '09662215435', 'c_alenx@yahoo.co.id', NULL, NULL, NULL, 'AMIR', NULL, '0', 0, 0, 0, 'BAHYATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(450, NULL, '2012.11.056', 'Yuswendi', 'P', NULL, 'Serang', '1994-11-13', 1, NULL, NULL, 'Link. Kubang Lele, Kec. Purwakarta ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087771738579', 'yuswendi03@gmail.com', NULL, NULL, NULL, 'Asmuni', NULL, '0', 0, 0, 0, 'Saptiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(451, NULL, '2012.11.057', 'Satibi', 'P', NULL, 'Cilegon', '1992-05-22', 1, NULL, NULL, 'Link. Panasepan Rt 005/003 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '083812802815', 'satibi.iu@gmail.com', NULL, NULL, NULL, 'Armane', NULL, '0', 0, 0, 0, 'Syariah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(452, NULL, '2012.11.058', 'Adi Suryadi', 'P', NULL, 'CILEGON', '0000-00-00', 1, NULL, NULL, 'Link.Gardu Iman Ds.Warnasari Kec.Citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08998665598', 'adi.dalamterang@yahoo.com', NULL, NULL, NULL, 'Dodih Supriyadi', NULL, '0', 0, 0, 0, 'Karwati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(453, NULL, '2012.11.059', 'Boungeville', 'W', NULL, 'CILEGON', '1994-02-12', 1, NULL, NULL, 'jl.H Agus Salim Link.Weri rt02/01 kec.Citangkil Kebonsari Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087773286656', 'viiyansville8912@gmail.com', NULL, NULL, NULL, 'SAMBOJA', NULL, '0', 0, 0, 0, 'RATNA R', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(454, NULL, '2012.11.060', 'Linda Febrianti', 'W', NULL, 'SERANG', '1994-02-09', 1, NULL, NULL, 'LINK.SUMUR PECUNG RT002/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081808197865', 'ndafebri@gmail.com', NULL, NULL, NULL, 'HALILI', NULL, '0', 0, 0, 0, 'MURSINAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(455, NULL, '2012.11.061', 'Fajri Fitria', 'W', NULL, 'SERANG', '1995-03-08', 1, NULL, NULL, 'jl.lembang 2 RT 03 RW 03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08984012927', 'fajrifitria@ymail.com', NULL, NULL, NULL, 'syafarudin', NULL, '0', 0, 0, 0, 'asiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(456, NULL, '2012.11.062', 'Badrul Fahmi', 'P', NULL, 'serang ', '0000-00-00', 1, NULL, NULL, 'kp.karang sari cilodong', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, 'jahiri', NULL, '0', 0, 0, 0, 'jamsiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(457, NULL, '2012.11.063', 'Sendi Rafsanjani', 'P', NULL, 'wonorejo', '1991-08-18', 1, NULL, NULL, 'LINK DERINGO KIDUL  RT/RW 06.03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809235236', 'sendy.rafsanjany@gmail.com', NULL, NULL, NULL, 'Arsaman', NULL, '0', 0, 0, 0, 'Titi Pujiati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(458, NULL, '2012.11.064', 'Fahreizal', 'P', NULL, 'Soro', '1994-07-01', 1, NULL, NULL, 'JL.ISHAK LINK SENEJA RT/RW.001/001 DESA/KEL. SUKMAJAYA KEC. JOMBANG KOTA CILEGON-BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085311007979', 'fahreizal19@gmail.com', NULL, NULL, NULL, 'Salim Ahmad', NULL, '0', 0, 0, 0, 'Nurlaila', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(459, NULL, '2012.11.065', 'Dedi Wahyudi', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(460, NULL, '2012.11.066', 'Muh. Khotibul Hapid', 'P', NULL, 'Pandeglang', '1992-04-05', 1, NULL, NULL, 'Kp. Ciekek Pabuaran RT. 01/13 No. 36 Kel. Karaton Kec. Majasari Kab. Pandeglang â€“ Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089604704769', 'fuy_42@yahoo.co.id', NULL, NULL, NULL, 'H. Madroji', NULL, '0', 0, 0, 0, 'Yati Sumiati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(461, NULL, '2012.11.067', 'Andika Septian Andrianto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(462, NULL, '2012.11.068', 'Agung Pamungkas', 'P', NULL, 'SOLO', '2014-12-30', 1, NULL, NULL, 'PESONA CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809235266', 'Agungpamungkas33@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(463, NULL, '2012.11.069', 'Aprilia Megilatul', 'P', NULL, 'serang', '2013-04-17', 1, NULL, NULL, 'JL.Radio I no. 23 komp. KS RT/RW:02/01 Desa:kotabumi Kecamatan:purwakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774687916', '', NULL, NULL, NULL, 'DR. Sumadiono ST. MMT.,Msi', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(464, NULL, '2012.11.070', 'Anissa Islamiyani', 'P', NULL, 'cirebon', '1994-09-30', 1, NULL, NULL, 'perum bck blok a 12 no 14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087829295828', 'nisaislamiyani@gmail.com', NULL, NULL, NULL, 'mulyadi', NULL, '0', 0, 0, 0, 'nani suryani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(465, NULL, '2012.11.071', 'Eka Sabdono', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(466, NULL, '2012.11.072', 'Agga Apriyana Frastyanto', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(467, NULL, '2012.11.073', 'Yoga Purbawa ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(468, NULL, '2012.11.074', 'Firman Maulana', 'P', NULL, 'lampung', '1992-12-21', 1, NULL, NULL, 'kp.pejaten rt/rw 04/01 des.pejaten kec.kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081905287921', 'ffirmanmaulana1221@gmail.com', NULL, NULL, NULL, 'husni jamas', NULL, '0', 0, 0, 0, 'zainunah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(469, NULL, '2012.11.075', 'Yasirul Fahmi', 'P', NULL, 'serag', '2015-04-06', 1, NULL, NULL, 'jl.kh moehamad idris no 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812316143', 'yasirulfahmi10@gmail.com', NULL, NULL, NULL, 'masturi', NULL, '0', 0, 0, 0, 'suhawati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(470, NULL, '2012.11.076', 'Ilham', 'P', NULL, 'Serang', '1994-09-10', 1, NULL, NULL, 'Kp. wanasaba RT 003/001 Ds. toyomerto I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089613639918', 'ilhamdoanggg@gmail.com', NULL, NULL, NULL, 'HAFIDH', NULL, '0', 0, 0, 0, 'MAFADAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(471, NULL, '2012.11.077', 'Arif Cahya Ramadhan', 'P', NULL, 'serang', '1993-03-28', 1, NULL, NULL, 'Jl.Delima 3 No.43 Kramat permai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254231282', '08128347731', 'arifcahyaramadhan28@gmail.com', NULL, NULL, NULL, 'IWAN EFFENDI', NULL, '0', 0, 0, 0, 'ENIN SUARNIN', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(472, NULL, '2012.11.078', 'Eni Oktaviani', 'W', NULL, 'Cilegon', '1993-10-29', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774744425', 'enioktavianii@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(473, NULL, '2012.11.079', 'Roheli', 'P', NULL, 'Serang', '1990-12-19', 1, NULL, NULL, 'JL.Sunan kudus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'Roheli.Roheli@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(474, NULL, '2012.11.080', 'Ihsan Maulana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(475, NULL, '2012.11.081', 'Uswatun Hasanah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(476, NULL, '2012.11.082', 'Riva liby chairilibar', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(477, NULL, '2012.11.083', 'Dwi Yanti Juwita', 'W', NULL, 'Cilegon ', '1993-05-10', 1, NULL, NULL, 'link kelelet rt/rw 03/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813179949', 'Dwijuwita92@ymail.com', NULL, NULL, NULL, 'Juhri', NULL, '0', 0, 0, 0, 'Diah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(478, NULL, '2012.11.084', 'Muhammad Egi Nugraha', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(479, NULL, '2012.11.085', 'Richard Adhi Luhung', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(480, NULL, '2012.11.086', 'Iwan Tubagus', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(481, NULL, '2012.11.087', 'Muhamad Jaemi Jamaludin', 'P', NULL, 'Serang', '1989-12-14', 1, NULL, NULL, 'Kp. Cikiara RT 01 RW 06 Desa Bulakan Kec. Cinangka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087772590666', 'jemi@clubbali.com', NULL, NULL, NULL, 'Muhamad Mursid', NULL, '0', 0, 0, 0, 'Jamiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(482, NULL, '2012.11.088', 'Ahmad Ridwan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'ahmad.ridwan.iu@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(483, NULL, '2012.11.089', 'Yuliani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085212131493', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(484, NULL, '2012.11.090', 'Mochamad Dede Firdaus', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(485, NULL, '2012.11.091', 'Siti Erika Silvi Damayanti Iswara', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(486, NULL, '2012.11.092', 'Ali Yusron(TI09K3)', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(487, NULL, '2012.11.093', 'Agung Mufti Silahudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(488, NULL, '2012.11.094', 'Laura Apriliya Sitohang', 'P', NULL, 'Cilegon', '1994-04-07', 1, NULL, NULL, 'perum korem cilaku blok g2 no 16 ,RT 008 RW 004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871403359', 'Laura.aprilia94@yahoo.co.id', NULL, NULL, NULL, 'Sumindar Sitohang', NULL, '0', 0, 0, 0, 'Netty Idawaty Sitompul', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(489, NULL, '2012.11.095', 'Holidi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(490, NULL, '2012.11.096', 'Dwi Wibowo', 'P', NULL, 'GROBOGAN', '1988-08-14', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '082112712061', 'bowobtb@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(491, NULL, '2012.11.097', 'Desri Evanda Sitepu', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(492, NULL, '2012.11.098', 'Rubeya Litiloly', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(493, NULL, '2012.11.099', 'Hafid', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(494, NULL, '2012.11.100', 'Islahudin Angsori', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(495, NULL, '2012.11.101', 'Alan Nugraha', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(496, NULL, '2012.11.102', 'Yasir Wahyullah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(497, NULL, '2012.11.103', 'Anis Kurli', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(498, NULL, '2012.11.104', 'Taufiq risqon putra Perdana', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(499, NULL, '2012.11.105', 'Abu Rizal Ismail Wahyu Putra', 'P', NULL, 'Probolinggo', '1994-06-08', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(500, NULL, '2012.12.001', 'Adrian Sani Pratama', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(501, NULL, '2012.12.002', 'Nida Asysyifa F.', 'W', NULL, 'Serang', '1993-11-24', 1, NULL, NULL, 'JL. Sadewa NO.23 KAV BLOK E RT/RW 010/005 CIWADUK CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911154179', 'anisa.zihah@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(502, NULL, '2012.12.003', 'Iis Istianah', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(503, NULL, '2012.12.004', 'Gita Aprilyani', 'W', NULL, 'Sukabumi', '1993-04-13', 1, NULL, NULL, 'Jl.Sunan Ampel KOMP. TWI DWA 12A/03 No. 13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081289890468', 'gitaaprilya4@gmail.com', NULL, NULL, NULL, 'Suhendra', NULL, '0', 0, 0, 0, 'Ike Supriyati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(504, NULL, '2012.12.005', 'Ahmad Yahya', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(505, NULL, '2012.12.006', 'Muhamad Hikmatulloh', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(506, NULL, '2012.12.007', 'Rachmi Fathia', 'P', NULL, 'PALEMBANG', '1992-07-18', 1, NULL, NULL, 'PONDOK CILEGON INDAH BLOK C52 NO 2 RT 002 RW 007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254-390817', '085716592191', 'rahmi_fathia18@yahoo.com', NULL, NULL, NULL, 'ATENG SUTARDI', NULL, '0', 0, 0, 0, 'YENI MULYAWATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(507, NULL, '2012.12.008', 'Reviana', 'W', NULL, 'Serang', '1994-06-10', 1, NULL, NULL, 'Jl. Nakula no.44 kav blok.E rt/rw 010/006 ciwaduk cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089646439003', 'revianarepiana@yahoo.com', NULL, NULL, NULL, 'Achmad Tasripin', NULL, '0', 0, 0, 0, 'Sri Suparmi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(508, NULL, '2012.12.009', 'Ade Irma Rahmawati', 'W', NULL, 'serang ', '1994-04-15', 1, NULL, NULL, 'puri cilegon hijau blok b2 no.07 RT/RW 015/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08976210376', 'adeirma_15@yahoo.com', NULL, NULL, NULL, 'H Rahmat Sadeli', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(509, NULL, '2012.12.010', 'Yani Afriyani', 'W', NULL, 'serang', '1994-04-20', 1, NULL, NULL, 'Lingk.Curug Rt.04/03 Desa.Rawa Arum Kec.Grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813318229', '', NULL, NULL, NULL, 'Muhamad Syarif', NULL, '0', 0, 0, 0, 'Siti Masitoh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(510, NULL, '2012.12.011', 'Eka Wahyuni', 'W', NULL, 'serang', '1991-11-11', 1, NULL, NULL, 'JL.FATAHILLAH LINK.SERANG ILIR RT 01/RW 03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087709991142', '6287771266256', NULL, NULL, NULL, 'SUYATNO', NULL, '0', 0, 0, 0, 'MARTINI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(511, NULL, '2012.12.012', 'Bayu Novarianto Nurhidayat', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(512, NULL, '2012.12.013', 'Nunung Fuanah', 'W', NULL, 'Serang', '1993-06-06', 1, NULL, NULL, 'KP.KECAKUP', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08998655963', 'fuanahnunung@yahoo.co.id', NULL, NULL, NULL, 'FUAJUDIN', NULL, '0', 0, 0, 0, 'KHAIRIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(513, NULL, '2012.21.001', 'Vera Fitriyaningsih', 'P', NULL, 'serang', '1991-04-23', 1, NULL, NULL, 'pondok cilegon indah blok D.88 No 16,RT 06/RW 04,Cibeber,cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808714454', 'Vfitriyaningsih@gmail.com', NULL, NULL, NULL, 'Muntholib', NULL, '0', 0, 0, 0, 'Evi Darlian', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(514, NULL, '2012.21.002', 'Putri Zuliana Ardi Prismawanti', 'P', NULL, 'serang', '1991-07-24', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(0254)381.783', '087774638052', 'putrizuliana34@yahoo.co.id', NULL, NULL, NULL, 'sardi', NULL, '0', 0, 0, 0, 'sugiri', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(515, NULL, '2012.21.003', 'Jaka Subayu', 'P', NULL, 'Medan', '1995-01-15', 1, NULL, NULL, 'Nusa Raya Residence', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774344660', 'jakasubayu7@ymail.com', NULL, NULL, NULL, 'Tommy Sebayang SE', NULL, '0', 0, 0, 0, 'Nurhasanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(516, NULL, '2012.21.004', 'Bunga Pertiwi', 'P', NULL, 'serang', '1994-07-06', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(0254)398386', '08128347731', 'alaymantap18@yahoo.com', NULL, NULL, NULL, 'Markam', NULL, '0', 0, 0, 0, 'Sobiyati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(517, NULL, '2012.21.005', 'Bahrul Muhit', 'P', NULL, 'serang', '1994-07-04', 1, NULL, NULL, 'kp.sempu ds.mangkunegara kec.bojonegara kab.serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945244446', 'bahroel.moehiet@gmail.com', NULL, NULL, NULL, 'MADSIRAN', NULL, '0', 0, 0, 0, 'HERIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(518, NULL, '2012.21.006', 'Ahmad Haerul Arif', 'P', NULL, 'serang', '1992-12-08', 1, NULL, NULL, 'terate', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774445412', 'ahmadhaeularif@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(519, NULL, '2012.21.007', 'Sari Tiffani', 'P', NULL, 'Serang', '1993-12-22', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089680557919', 'Saritiffani@gmail.com', NULL, NULL, NULL, 'Alm. H.MARJONO', NULL, '0', 0, 0, 0, 'Almh. HJ.RIYATUN', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(520, NULL, '2012.21.008', 'Erna Maryani', 'P', NULL, 'lampung', '1993-06-21', 1, NULL, NULL, 'link cereme, rt/rw 005/003kel. lebak gede kec.pulau merak.kota cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'maryanierna6@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(521, NULL, '2012.21.009', 'Mulyadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(522, NULL, '2012.21.010', 'Zainul Riyanto', 'P', NULL, 'serang', '1994-01-13', 1, NULL, NULL, 'jl.purbaya kav.BlokJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', 'zainul1994@gmail.com', NULL, NULL, NULL, 'agus_suwito', NULL, '0', 0, 0, 0, 'casriah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(523, NULL, '2012.21.011', 'Ade Sulaiman', 'P', NULL, 'Serang', '1994-12-26', 1, NULL, NULL, 'JL.KIAJURUM.SEMPU GEDANG RT.08/RW18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771986794', 'adesquadmarley@yahoo.co.id', NULL, NULL, NULL, 'M.tohir', NULL, '0', 0, 0, 0, 'nonoh hasanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(524, NULL, '2012.21.012', 'Feby Winanda Putri', 'P', NULL, 'Tangerang', '1995-01-09', 1, NULL, NULL, 'TBL BLOK D5E/16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771444697', 'feby.pebong@gmail.com', NULL, NULL, NULL, 'FAUZI WARTA BONE', NULL, '0', 0, 0, 0, 'TUTI ARELAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(525, NULL, '2012.22.001', 'Anggriani', 'P', NULL, 'Serang', '1994-06-21', 1, NULL, NULL, 'Jl. Flamboyan No.50 Rt 019/rw 005 Ciwedus-Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087771412014', 'anggrianisttikom@rocketmail.com', NULL, NULL, NULL, 'Herry Fathur Rachman', NULL, '0', 0, 0, 0, 'Juhariah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(526, NULL, '2012.22.002', 'Diana Levitasari', 'P', NULL, 'SERANG', '1993-07-25', 1, NULL, NULL, 'jl.lumba-lumba kavling blok C no.23', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(0254)391245', '087879202035/(0254)3', 'levitasaridiana@ymail.com', NULL, NULL, NULL, 'Sumartono', NULL, '0', 0, 0, 0, 'Lilik Suryani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(527, NULL, '2012.22.003', 'Alvian Pratama', 'P', NULL, 'Narmada', '2013-10-23', 1, NULL, NULL, 'Perumnas BCK Blok A.01/02 Cibeber', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771267331', 'vienchmelodich@gmail.com', NULL, NULL, NULL, 'Drs. Asep Sanusi', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(528, NULL, '2012.22.004', 'Nur Fitri Maulani', 'P', NULL, 'SERANG', '2013-09-11', 1, NULL, NULL, 'jl.kemuning 7 blok k5 no:6 rt/rw:27/05 bbs 2,cilegon-ciwedus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087777001451', 'nurfitri.maulani@gmail.com', NULL, NULL, NULL, 'MARIZAL', NULL, '0', 0, 0, 0, 'TITIN MULYATIN', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(529, NULL, '2012.22.005', 'Fathurrohim', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(530, NULL, '2012.22.006', 'Faisal', 'P', NULL, 'serang', '1993-06-25', 1, NULL, NULL, 'kp.sukajadi Rt03/02 desa.mekarsari kec.pulomerak no.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808766769', 'faisalchiee@yahoo.com', NULL, NULL, NULL, 'Asri', NULL, '0', 0, 0, 0, 'Usnifa', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(531, NULL, '2012.22.007', 'Robiatul Adawiyah', 'P', NULL, 'serang,', '2013-03-30', 1, NULL, NULL, 'kp.ciakar DS.lambangsari kec.bojonegara kab.serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945953899/0856749', 'robiatul18@ovi.com', NULL, NULL, NULL, 'hudeli', NULL, '0', 0, 0, 0, 'ulfiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(532, NULL, '2012.22.008', 'Angga Hardianto', 'P', NULL, 'SERANG', '1994-07-21', 1, NULL, NULL, 'KP CURUG WARNASARI RT/RW 002/004 DESA/KEL TAMBANG AYAM KECAMATAN ANYAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085366707777', 'anggahardianto@yahoo.co.id', NULL, NULL, NULL, 'YULIANTO', NULL, '0', 0, 0, 0, 'HAYATI NUFUS', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(533, NULL, '2012.22.009', 'Ema Herzegovina', 'P', NULL, 'anyar', '1993-05-02', 1, NULL, NULL, 'kp.cikoneng rt/rw 003/004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813330736', 'emaherze@gmail.com', NULL, NULL, NULL, 'alm.jeck satria', NULL, '0', 0, 0, 0, 'Nanti tresnaningsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(534, NULL, '2012.22.010', 'Handayani', 'P', NULL, 'serang', '1993-06-06', 1, NULL, NULL, 'jl.ketileng timur rt04 rw01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089609553465', 'ndayhanday06@gmail.com', NULL, NULL, NULL, 'Subakir Syarif', NULL, '0', 0, 0, 0, 'Sunenah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(535, NULL, '2012.22.011', 'Anah Istianah', 'P', NULL, 'SERANG', '1993-05-12', 1, NULL, NULL, 'Jl.Raya Jakarta km.4 Rt 04/03 Pakupatan Serang Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871265720', 'Anah_Istianah@yahoo.co.id', NULL, NULL, NULL, 'M.Rufiq', NULL, '0', 0, 0, 0, 'Munajah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(536, NULL, '2012.22.012', 'Anah Muhawanah', 'W', NULL, 'Cilegon', '1994-10-14', 1, NULL, NULL, 'Jl.sunan gunung djati,link.kubang lumbra desa.tegal ratu kec.ciwandan 012/006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081905277684', 'anah.muhawanah@gmail.com', NULL, NULL, NULL, 'Asriya', NULL, '0', 0, 0, 0, 'undah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(537, NULL, '2012.22.013', 'Guntur Himawan', 'P', NULL, 'SERANG', '1992-02-22', 1, NULL, NULL, 'JL. GUNUNG GEDE NO.6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '02540380418', '087774008802', 'gunturhimawan666@yahoo.com', NULL, NULL, NULL, 'WILDAN', NULL, '0', 0, 0, 0, 'WINARTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(538, NULL, '2013..019', 'arif', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '756768', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(539, NULL, '2013.11.001', 'Dedy Saputra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(540, NULL, '2013.11.002', 'Rizki Pratama Dunggio', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(541, NULL, '2013.11.003', 'Hasbillah', 'P', NULL, 'Serang', '1989-12-15', 1, NULL, NULL, 'Link. Sumampir Timur RT. 01/04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773550283', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(542, NULL, '2013.11.004', 'Marisa Hasni', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(543, NULL, '2013.11.005', 'Manres Saprianto Simanullang', 'P', NULL, 'Sibuluan, Medan', '1994-06-02', 1, NULL, NULL, 'Taman Krakatau', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '087809597957', 'manres58@yahoo.com', NULL, NULL, NULL, 'Mulyater Simanullang', NULL, '0', 0, 0, 0, 'Nurli Situmorang', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(544, NULL, '2013.11.006', 'Alan Surya Kusuma', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(545, NULL, '2013.11.007', 'Mela Walalangi', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, 'kp.cikoneng rt.02 rt.03 ds.cikoneng-anyar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '087774589034', 'mela_walalangi@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(546, NULL, '2013.11.008', 'Nana Luciana', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(547, NULL, '2013.11.009', 'Yuyun Gunawan', 'P', NULL, 'Jakarta', '2014-04-10', 1, NULL, NULL, 'Komplek Graha walantaka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '085770678325', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(548, NULL, '2013.11.010', 'Umar Fauzir', 'P', NULL, 'Cilegon', '2013-08-02', 1, NULL, NULL, 'Link. Acing Rt. 002/005 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771619197', '', NULL, NULL, NULL, 'Sahil', NULL, '0', 0, 0, 0, 'Asia', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(549, NULL, '2013.11.011', 'Nur Maela', 'W', NULL, 'Serang', '1992-05-12', 1, NULL, NULL, 'Jl. Yos Sudarso RT. 001/003 Link.Lebak Gede Kec.Pulomerak Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085775360628', 'maela25@gmail.com', NULL, NULL, NULL, 'Abdullah', NULL, '0', 0, 0, 0, 'Hamsiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(550, NULL, '2013.11.012', 'Chalvin', 'P', NULL, 'Serang', '1991-09-10', 1, NULL, NULL, 'Pci Blok D12 No.20 Rt.007/004 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '088210015827', '', NULL, NULL, NULL, 'Suwirman', NULL, '0', 0, 0, 0, 'Syafniety', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(551, NULL, '2013.11.013', 'Nur Hanip Nasuha', 'P', NULL, 'Brebes', '1991-07-06', 1, NULL, NULL, 'Jl. Letjend Suprapto Gg Ramsim Rt.011/004 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906192933', 'hanip.nasuha@yahoo.com', NULL, NULL, NULL, 'Nurdin', NULL, '0', 0, 0, 0, 'Mudrikah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(552, NULL, '2013.11.014', 'Afrizal Hidayatulloh', 'P', NULL, 'Serang', '1994-04-18', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'contact@afrizalhidayatulloh.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(553, NULL, '2013.11.015', 'RUPA KALINAKA', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(554, NULL, '2013.11.016', 'Muhammad Jeffry Lubis', 'P', NULL, 'Banda Aceh', '2013-03-09', 1, NULL, NULL, 'Jl.suryodiningratan no:14 Yogyakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0274-417119', '0817431694', 'muhammad.jeffry.lubis@gmail.com', NULL, NULL, NULL, 'Amir Lubis', NULL, '0', 0, 0, 0, 'Dewi Larasati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(555, NULL, '2013.11.017', 'Ummayah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '08128347731', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(556, NULL, '2013.11.018', 'Ricca Rochmah', 'W', NULL, 'SERANG', '0000-00-00', 1, NULL, NULL, 'JL.SALIRA INDAH KP.PENGORENG 02/01.24 DESA MANGUNREJA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '081808024755', 'RICCAROCHMAH@GMAIL.COM', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(557, NULL, '2013.11.019', 'Dudi Ayudi', 'P', NULL, 'Pandeglang', '0000-00-00', 1, NULL, NULL, 'Kp. Pasir Jeruk Rt.04/02 Desa Cinoyong Kecamatan Carita Kab. Pandeglang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871102576', 'dudiayudi1@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(558, NULL, '2013.11.020', 'Herry Parmana', 'P', NULL, 'Serang', '1988-10-22', 1, NULL, NULL, 'Komp. KS Jl. Tekukur No. 21 Type D-flat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '08988686097', 'herryparmana212@gmail.com', NULL, NULL, NULL, 'Mad Ishak', NULL, '0', 0, 0, 0, 'Antinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(559, NULL, '2013.11.021', 'Ganda Imawan', 'P', NULL, '', '2013-10-25', 1, NULL, NULL, 'link, pengampelan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906333257', 'gandaimawan@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(560, NULL, '2013.11.022', 'Muhammad Sahruroji', 'P', NULL, 'Cilegon', '1993-10-24', 1, NULL, NULL, 'Link. Seneja Cilegon Rt. 001/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087884282326', '', NULL, NULL, NULL, 'Adi Suhardi', NULL, '0', 0, 0, 0, 'Saidah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(561, NULL, '2013.11.023', 'Romi Gunawan', 'P', NULL, 'Kalianda Lampung Sel', '1991-02-12', 1, NULL, NULL, 'Link. Ramanuju Baru Rt. 009/009 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809711138', 'romigunawan1991@gmail.com', NULL, NULL, NULL, 'Armad', NULL, '0', 0, 0, 0, 'Ratnawiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(562, NULL, '2013.11.024', 'Muhamad Asep', 'P', NULL, 'Serang', '1990-07-03', 1, NULL, NULL, 'Link. Tegal Wangi Rt. 001/002 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808541900', '', NULL, NULL, NULL, 'Hasbulloh', NULL, '0', 0, 0, 0, 'Rohdiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(563, NULL, '2013.11.025', 'M. Malik Artanto', 'P', NULL, 'Serang', '1994-10-16', 1, NULL, NULL, 'Jl. Pala A1/43 BBS II Rt. 009/010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911250095', 'Artanto.malik@gmail.com', NULL, NULL, NULL, 'Maryanto', NULL, '0', 0, 0, 0, 'Ashabul Maimanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(564, NULL, '2013.11.026', 'Rintania Oktaviani', 'W', NULL, 'Purbalingga', '1993-10-21', 1, NULL, NULL, 'Link. Ciora Wetan Rt. 006/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808655081', '', NULL, NULL, NULL, 'Sayuni', NULL, '0', 0, 0, 0, 'Pariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(565, NULL, '2013.11.027', 'Chairo Ardianto', 'P', NULL, 'Cilegon', '1992-06-21', 1, NULL, NULL, 'Link. Ketileng Timur Rt. 001/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081912346555', 'chairoardianto@gmail.com', NULL, NULL, NULL, 'Robi Ussani', NULL, '0', 0, 0, 0, 'Yumitarsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(566, NULL, '2013.11.028', 'Oken Fatoni', 'P', NULL, 'Serang', '1991-01-10', 1, NULL, NULL, 'Cilanggir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945705373', 'okenfatoni1001@gmail.com', NULL, NULL, NULL, 'Nurhani', NULL, '0', 0, 0, 0, 'Nuryanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(567, NULL, '2013.11.029', 'Afan Fathoni', 'P', NULL, 'Serang', '1991-10-17', 1, NULL, NULL, 'Link. Luwung Sawo Rt. 016/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906303175', 'fathoni.afan@yahoo.co.id', NULL, NULL, NULL, 'A. Syafei', NULL, '0', 0, 0, 0, 'Almiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(568, NULL, '2013.11.030', 'ADI RIFKIYADI', 'P', NULL, 'SERANG', '1989-11-19', 1, NULL, NULL, 'LINK. ASEM GEBOG RT. 003/006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085920006761', 'arifkiyadi@gmail.com', NULL, NULL, NULL, 'M. DARIP ALIUDIN', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(569, NULL, '2013.11.031', 'Febrian Setiadi', 'P', NULL, 'SERANG', '1994-02-09', 1, NULL, NULL, 'PERUM PESONA CILEGON BLOK A7 NO. 02 RT. 001/004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087887333507', 'febriansiboccah@yahoo.co.id', NULL, NULL, NULL, 'ADI SUJAROT', NULL, '0', 0, 0, 0, 'SURTIYANI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(570, NULL, '2013.11.032', 'Hadid Ruhyana', 'P', NULL, 'SERANG', '1995-05-10', 1, NULL, NULL, 'JL. YOS SUDARSO LINK. KEPINDIS RT. 002/008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089609453443', 'hadidruhyana@yahoo.co.id', NULL, NULL, NULL, 'ACE SUMARNA', NULL, '0', 0, 0, 0, 'SITI MASNAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(571, NULL, '2013.11.033', 'HABIEB MUSTOFA', 'P', NULL, 'GARUT', '2013-08-05', 1, NULL, NULL, 'BLOK DESA RT. 001/005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085722337282', 'habibiyahabibi@gmail.com', NULL, NULL, NULL, 'PEPEN MULYANA', NULL, '0', 0, 0, 0, 'EMPONG SULASTRI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(572, NULL, '2013.11.034', 'Fauzul Ulum', 'P', NULL, 'SERANG', '1995-07-06', 1, NULL, NULL, 'KAMPUNG SUMURANJA RT. 001/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813190163', '', NULL, NULL, NULL, 'SOFWAN ', NULL, '0', 0, 0, 0, 'UTI ULVIANTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(573, NULL, '2013.11.035', 'AFENDI', 'P', NULL, 'SERANG', '1990-07-03', 1, NULL, NULL, 'KAMPUNG KESAMBILAWANG RT. 004/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771288125', 'afendi.iu@gmail.com', NULL, NULL, NULL, 'SAMHARI (ALM)', NULL, '0', 0, 0, 0, 'ASMARIAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(574, NULL, '2013.11.036', 'Ika Rosmiati', 'W', NULL, 'SERANG', '1995-07-26', 1, NULL, NULL, 'KAMPUNG BAROS RT. 002/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871700887', '', NULL, NULL, NULL, 'HOJALI', NULL, '0', 0, 0, 0, 'ROHIMAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(575, NULL, '2013.11.037', 'Yap Sukamto', 'P', NULL, 'Belinyu', '1992-04-04', 1, NULL, NULL, 'Jl. Tirtayasa Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808910117', '', NULL, NULL, NULL, 'Jafar', NULL, '0', 0, 0, 0, 'Tutik Andiwati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(576, NULL, '2013.11.038', 'Mukhtar', 'P', NULL, 'Pandeglang ', '1992-11-07', 1, NULL, NULL, 'Jl. Rajawali Komp. KS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906286247', '', NULL, NULL, NULL, 'Alm. Mardais', NULL, '0', 0, 0, 0, 'Asni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(577, NULL, '2013.11.039', 'Muhammad Rafei', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(578, NULL, '2013.11.040', 'Faturochman Hidayat', 'P', NULL, 'Bandung', '1995-07-01', 1, NULL, NULL, 'Perum Kota Maja Prasadha Rangkasbitung RT. 003/004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773974651', '', NULL, NULL, NULL, 'Nurhidayat', NULL, '0', 0, 0, 0, 'Tatin Suhartini', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(579, NULL, '2013.11.041', 'Muhamad Solahhudin', 'P', NULL, 'Bogor', '1993-02-23', 1, NULL, NULL, 'Kp. Cibanteng Rt. 003/001 Bogor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085694117397', '', NULL, NULL, NULL, 'Achmadi', NULL, '0', 0, 0, 0, 'Laswati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(580, NULL, '2013.11.042', 'HASAN BASRI', 'P', NULL, 'LAMPUNG UTARA', '1992-06-08', 1, NULL, NULL, 'BBS III BLOK E6 NO. 09 RT. 017/009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '082112299940', '', NULL, NULL, NULL, 'AHMAD SARKAWI', NULL, '0', 0, 0, 0, 'MAYSAROH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(581, NULL, '2013.11.043', 'ROHMATULLAH', 'P', NULL, 'CILEGON', '1991-12-05', 1, NULL, NULL, 'LINK. CIGICEH RT. 003/001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771151264', 'rahmatthamenx@rocketmail.com', NULL, NULL, NULL, 'MUHTADI', NULL, '0', 0, 0, 0, 'SABITA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(582, NULL, '2013.11.044', 'Yana Suryana', 'P', NULL, 'Rangkasbitung', '1993-12-28', 1, NULL, NULL, 'Kp. Kosa Rt. 016/006 Rangkasbitung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906286071', '', NULL, NULL, NULL, 'Oji Dahroji', NULL, '0', 0, 0, 0, 'Eha Julaeha', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(583, NULL, '2013.11.045', 'MASKUR', 'P', NULL, 'DEPOK', '2013-08-08', 1, NULL, NULL, 'KAMPUNG SUGUTAMU RT. 02/08 NO. 45', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087788967284', 'maskur.rj@gmail.com', NULL, NULL, NULL, 'MAAH .T.', NULL, '0', 0, 0, 0, 'AMAH .A.', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(584, NULL, '2013.11.046', 'Mujianto', 'P', NULL, 'Banyumas', '1987-10-20', 1, NULL, NULL, 'Kuntili Rt. 006/003 Slimpiuh Banyumas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085729720708', 'muji.sttikomiu@gmail.com', NULL, NULL, NULL, 'Alm. Sujadi', NULL, '0', 0, 0, 0, 'Martinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(585, NULL, '2013.11.047', 'Mastum', 'P', NULL, 'Serang', '1989-12-10', 1, NULL, NULL, 'Sanbironyok Rt.012/003 Ciwandan Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774632470', '', NULL, NULL, NULL, 'Sukmajaya', NULL, '0', 0, 0, 0, 'Nurhayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(586, NULL, '2013.11.048', 'Ronniawan Fadhlillah', 'P', NULL, 'Serang', '1994-06-25', 1, NULL, NULL, 'Perum Griya Serdang Indah B4 No.10 Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089674301073', 'f.ronniawan@windowslive.com', NULL, NULL, NULL, 'Moch.Ary Manuputty', NULL, '0', 0, 0, 0, 'Karti Saenah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(587, NULL, '2013.11.049', 'Ruswandi', 'P', NULL, 'Cilegon', '1994-05-17', 1, NULL, NULL, 'Kp. Ciluit desa dringo kec. Citangkil Cilegon Rt. 004/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089651085719', '', NULL, NULL, NULL, 'Rusdi', NULL, '0', 0, 0, 0, 'Suanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(588, NULL, '2013.11.050', 'Muhammad Rouf', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(589, NULL, '2013.11.051', 'Laode Moh. Akbar Saputra', 'P', NULL, 'Jakarta', '1991-11-01', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'akbarrikudo@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(590, NULL, '2013.11.052', 'Adelia Sandra', 'W', NULL, 'Tangerang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(591, NULL, '2013.11.053', 'Ilham Fahri santiar', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(592, NULL, '2013.11.054', 'Susi Yanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(593, NULL, '2013.11.055', 'Een Hunaenah', 'W', NULL, 'Serang', '1994-07-01', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808445118', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(594, NULL, '2013.11.056', 'M. Ikhlas Adiguna', 'P', NULL, 'Bandar lampung', '1995-12-28', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'adiguna272@gmail.com', NULL, NULL, NULL, 'Hendra irawan', NULL, '0', 0, 0, 0, 'Futihat', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(595, NULL, '2013.11.057', 'Kresna Aldi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(596, NULL, '2013.11.058', 'Aldi Riwandi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(597, NULL, '2013.11.059', 'Clara Diva Zahara', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(598, NULL, '2013.11.060', 'Redi Setiawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(599, NULL, '2013.11.061', 'Muhammad Hasan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(600, NULL, '2013.11.062', 'Dickzy Eka Putranto', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813089666', 'dickzy.eka23@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(601, NULL, '2013.11.063', 'Dikri Syarif hidayatullah', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'dikrisyarif76@gmail.com', NULL, NULL, NULL, 'madali', NULL, '0', 0, 0, 0, 'saadah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(602, NULL, '2013.11.064', 'Uzi Faulus', 'P', NULL, 'cilegon ', '2015-08-11', 1, NULL, NULL, 'jl.fatahillah link.gelereng 3 kec.ciwandan kota cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809502334', 'uzyfauluz@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(603, NULL, '2013.11.065', 'Fajri Gusnandar', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(604, NULL, '2013.11.066', 'Tifah Sari Putri ', 'W', NULL, 'Serang', '1992-12-26', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771388788', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(605, NULL, '2013.11.067', 'Abdul Hamid', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(606, NULL, '2013.11.068', 'Entol Moh. Maulana Irfan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(607, NULL, '2013.11.069', 'Mumun Muafiyah', 'W', NULL, 'Serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(608, NULL, '2013.11.070', 'Muhamad Iqbal Fahlefi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(609, NULL, '2013.11.071', 'Nova Apriyanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(610, NULL, '2013.11.072', 'Nasrullah', 'P', NULL, 'Cilegon', '1995-07-21', 1, NULL, NULL, 'Kota Cilegon, Desa Panggung Rawi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+6289650771729', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(611, NULL, '2013.11.073', 'Ade Jaka Ruhiyat', 'P', NULL, 'Ciamis', '1986-02-20', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945618444', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(612, NULL, '2013.11.074', 'Ajmal Muhammad Fawz', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(613, NULL, '2013.11.075', 'Yovi Odrianda', 'P', NULL, 'Batunanggai', '1984-03-15', 1, NULL, NULL, 'Komp BPI Blok S1 No6 Panggung Rawi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087896001984', 'yovie@royalekrakatau.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(614, NULL, '2013.11.076', 'Rohim Asna', 'P', NULL, 'Serang ', '0000-00-00', 1, NULL, NULL, 'Link. Jeruk Rt 03/01 Desa Mekar Sari Kec. Pulo Merak Cilegon Banten ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081806807392', 'rohimasna@yahoo.com', NULL, NULL, NULL, 'Asnawi ', NULL, '0', 0, 0, 0, 'Sunah ', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(615, NULL, '2013.11.077', 'Hesti Wulandari', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(616, NULL, '2013.11.078', 'Adam Nurohman', 'P', NULL, 'Wonosobo', '1994-03-05', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'adamnurohmanur@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(617, NULL, '2013.11.079', 'Heri Haryadi', 'P', NULL, 'SERANG', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871285011', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(618, NULL, '2013.11.080', 'Dodi Setiadi', 'P', NULL, 'Jakarta', '1994-09-13', 1, NULL, NULL, 'Notog RT.03/05 kec.Patikraja, Kab.Banyumas, Purwokerto - Jawa Tengah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089651051626', 'dodhie@outlook.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(619, NULL, '2013.11.081', 'Eka Rosmawati', 'W', NULL, 'Cilegon', '1993-01-07', 1, NULL, NULL, 'Kp.Pabuaran Dsa.Ciwedus Cilegon Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945955675', 'ekarosmawati16@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(620, NULL, '2013.11.082', 'Heriyanto', 'P', NULL, 'SOLO', '0000-00-00', 1, NULL, NULL, 'komplek Griya serdang indah blok B14 no.8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083890863777', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(621, NULL, '2013.11.083', 'Eliza', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(622, NULL, '2013.11.084', 'Valen Anggia Chrisdelina', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(623, NULL, '2013.11.085', 'M Arief Setya Nugraha', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(624, NULL, '2013.11.086', 'Nita Maulina', 'W', NULL, 'Bandung', '0000-00-00', 1, NULL, NULL, 'link. kependilan rt 02 rw 01 kec jombang, desa panggung rawi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '396212', '087871421900', 'nita.maulina21@yahoo.com', NULL, NULL, NULL, 'udin tajudin', NULL, '0', 0, 0, 0, 'wati susilawati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(625, NULL, '2013.11.087', 'Shinta', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(626, NULL, '2013.11.088', 'Astri Syafina', 'W', NULL, 'jakarta, ', '1994-03-12', 1, NULL, NULL, 'taman anggrek jakarta.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'bapa', NULL, '0', 0, 0, 0, 'mamah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(627, NULL, '2013.11.089', 'Irfan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(628, NULL, '2013.11.090', 'Hidayatullah', 'P', NULL, 'serang', '1990-06-09', 1, NULL, NULL, 'link.tegal buntu desa tegal ratu. kecamatan ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'hidayatullah381@gmail.com', NULL, NULL, NULL, 'asiman', NULL, '0', 0, 0, 0, 'sabihah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(629, NULL, '2013.11.091', 'Roni Tri Aji', 'P', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(630, NULL, '2013.11.092', 'Doni Prabowo', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(631, NULL, '2013.11.093', 'Mukhamad Aldi Rasyid P', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(632, NULL, '2013.11.094', 'Kasika Rany', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(633, NULL, '2013.11.095', 'Sandi Adi Putra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(634, NULL, '2013.11.096', 'Eka Armanu', 'P', NULL, 'Serang', '1990-09-27', 1, NULL, NULL, 'Komp PCI Blok D 33 No 10 Cilegon-Banten 42421', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911277151', 'bluez_neo@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(635, NULL, '2013.11.097', 'Jasindra Irawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(636, NULL, '2013.11.098', 'Iis Islahiyah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(637, NULL, '2013.11.099', 'Rifki Arif', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(638, NULL, '2013.12.001', 'Jufera', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(639, NULL, '2013.12.002', 'Eti Nihlayati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(640, NULL, '2013.12.003', 'Ratu Bella Maviora', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(641, NULL, '2013.12.004', 'Dwi Dharmayanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(642, NULL, '2013.12.005', 'Fita Muspita', 'P', NULL, 'Serang', '1994-02-03', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773454547', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(643, NULL, '2013.12.006', 'Ferdinand Fransiskus Sianturi', 'P', NULL, 'Sibosur', '1991-07-18', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08561214550', 'ferdinand.fransiskus@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(644, NULL, '2013.12.007', 'Ubaydillah', 'P', NULL, 'Serang', '2013-09-09', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(645, NULL, '2013.12.008', 'Eka Widiyastuti', 'W', NULL, 'Serang', '2013-10-26', 1, NULL, NULL, 'Kp.Kubang Laban rt/rw 006/003 ds.Lambangsari kec.BOJONEGARA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085716429726', '', NULL, NULL, NULL, 'Muslihin', NULL, '0', 0, 0, 0, 'Isti Faiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(646, NULL, '2013.21.001', 'Hanin Nafia', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(647, NULL, '2013.21.002', 'Sandi Sanjaya', 'P', NULL, 'Serang', '1995-11-28', 1, NULL, NULL, 'Link.Sukajaya Rt.18/Rw.07 Kel:Kebonsari Kec:Citangkil Kota:Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808513516', 'sanjaya.sandi321@gmail.com', NULL, NULL, NULL, 'M.Sudarmaji', NULL, '0', 0, 0, 0, 'Samsimar', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(648, NULL, '2013.21.003', 'Barep surya. M', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(649, NULL, '2013.21.004', 'Mochammad Firdaus zulmi', 'P', NULL, 'serang', '1995-11-06', 1, NULL, NULL, 'link.bujanggadung Rt/Rw 03/03 No.21   kec.grogol  kel.rawa arum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911251817', '', NULL, NULL, NULL, 'zulkarnaini', NULL, '0', 0, 0, 0, 'khomizah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(650, NULL, '2013.21.005', 'Novi Hasmaniyah', 'W', NULL, 'T.Pulau Sebesi Kec.R', '2013-10-27', 1, NULL, NULL, 'Tejang Pulau Sebesi rt/rw 007/012 desa Tejang P.Besi kec.Rajabasa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087712205759', '', NULL, NULL, NULL, 'HASBULLOH', NULL, '0', 0, 0, 0, 'Hilmiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(651, NULL, '2013.22.001', 'Syella Dwi Andiani', 'W', NULL, 'Serang', '1995-01-08', 1, NULL, NULL, 'jalan kecubung 4 blok E7 no 2 BBS 2 RT 23/05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871288228', 'shella.andiani@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(652, NULL, '2013.22.002', 'Maya Budiarthi', 'W', NULL, 'cilegon', '1995-01-03', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(653, NULL, '2014.11.001', 'Dina Arifani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(654, NULL, '2014.11.002', 'Anjar Tri Budi Kusumo', 'P', NULL, '', '1992-08-14', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(655, NULL, '2014.11.003', 'Mega Lestari', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(656, NULL, '2014.11.004', 'Angga Darussulur', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(657, NULL, '2014.11.005', 'Abdurohman', 'P', NULL, 'jakarta', '2014-05-26', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085782584420', 'ocm.big@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(658, NULL, '2014.11.006', 'Hermanto', 'P', NULL, 'Blitar', '1978-12-05', 1, NULL, NULL, 'Perum Puri Anggrek Blok C18 no 5 Serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08170858563', 'Java_big72@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(659, NULL, '2014.11.007', 'Misnan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(660, NULL, '2014.11.008', 'Yuda Kurniawan', 'P', NULL, 'Bandung', '1992-02-11', 1, NULL, NULL, 'Jl.Cilengkrang 02 Rt.03/12 Kel.Palasari kec.Cibiru Kota Bandung 40615', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08988288233', 'Yuda.47x@gmail.com', NULL, NULL, NULL, 'Makhful Hermawan', NULL, '0', 0, 0, 0, 'Iis Rohayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(661, NULL, '2014.11.009', 'Kaerul Umam', 'P', NULL, 'Brebes', '0000-00-00', 1, NULL, NULL, 'cipelem Bulakamba Brebes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'umamalfaritsi@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(662, NULL, '2014.11.010', 'Phill Norman Ibrahim', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(663, NULL, '2014.11.011', 'Agung Setiawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(664, NULL, '2014.11.012', 'Alif Gema prasetya', 'P', NULL, 'Serang', '1994-10-03', 1, NULL, NULL, 'Anyer, Kp. Kenanga RT 01/ Rw01, Serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081808177071', 'alifgema1@gmail.com', NULL, NULL, NULL, 'Johani', NULL, '0', 0, 0, 0, 'Siti Aliyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(665, NULL, '2014.11.013', 'Muhammad Jaja', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906417270', 'muhammad.jaja@ymail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(666, NULL, '2014.11.014', 'Hendar Mawan', 'P', NULL, 'CILEGON', '1993-06-05', 1, NULL, NULL, 'sawah luhur kasemen ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809197901', 'syahferdiyan@rocketmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(667, NULL, '2014.11.015', 'Ekantoro Fauji', 'P', NULL, 'SERANG', '1993-07-25', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089678692859', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(668, NULL, '2014.11.016', 'Danu Wardian', 'P', NULL, 'Serang', '1992-05-29', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906488802', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(669, NULL, '2014.11.017', 'Ihwanul Hakim', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(670, NULL, '2014.11.018', 'Aris Munandar', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(671, NULL, '2014.11.019', 'Maria Andea', 'P', NULL, 'SERANG', '2014-10-26', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871443395', 'maria.andea@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(672, NULL, '2014.11.020', 'Nanine Marela Hannah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771237280', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(673, NULL, '2014.11.021', 'Ifan Gunawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(674, NULL, '2014.11.022', 'Ridwan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(675, NULL, '2014.11.023', 'Maulana Yusuf Rachman', 'P', NULL, 'Serang', '1994-05-05', 1, NULL, NULL, 'BBS 2 Jalan Kecubung 4 E.4 No.9', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+628998649029', 'keluarsanasini@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(676, NULL, '2014.11.024', 'Candra Budiman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(677, NULL, '2014.11.025', 'Abdurrahman Aroziki', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(678, NULL, '2014.11.026', 'Fatulloh Ashari', 'P', NULL, 'Cilegon', '1994-02-10', 1, NULL, NULL, 'Link. Baru Rt 01 Rw 04 Kel. Tamansari Kec. Pulomerak Kota Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+628999619011', 'fatulloh.ashari@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(679, NULL, '2014.11.027', 'Rengga Sulistyaji', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(680, NULL, '2014.11.028', 'Bambang Sutardi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(681, NULL, '2014.11.029', 'Hayati Nufus', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(682, NULL, '2014.11.030', 'Sandi Rahmatullah', 'P', NULL, 'serang 01 januari 19', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089677974477', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(683, NULL, '2014.11.031', 'Yoga Murtabroni', 'P', NULL, 'serang-27-03-1995', '0000-00-00', 1, NULL, NULL, 'link.gunung buntu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906245715', '', NULL, NULL, NULL, 'imam tabroni', NULL, '0', 0, 0, 0, 'mur tafiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(684, NULL, '2014.11.032', 'Ahmad Juhri', 'P', NULL, 'serang', '2014-06-25', 1, NULL, NULL, 'kp.gunung buntu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '_', '081911278640', 'ahmadjuhri32@gmail.com', NULL, NULL, NULL, 'Alm.kesidin', NULL, '0', 0, 0, 0, 'nafisah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(685, NULL, '2014.11.033', 'Ivan Yogi Saputra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(686, NULL, '2014.11.034', 'Sandi Eka Putra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'BBS III BLOK D-5 NO. 24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808166200', 'kiweout@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(687, NULL, '2014.11.035', 'Rio agus Suparman', 'P', NULL, 'Tangerang', '1994-08-10', 1, NULL, NULL, 'PORIS GAGA BARU NO. 59 RT.02/01 KEL. PORIS GAGA BARU KEC. BARU CEPER KOTA TANGERANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081289874724', 'Rio108as@gmail.com', NULL, NULL, NULL, 'SURATMAN', NULL, '0', 0, 0, 0, 'PARNI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(688, NULL, '2014.11.036', 'Wanto', 'P', NULL, 'Tangerang', '1993-12-18', 1, NULL, NULL, 'kp.sukamandi rt03/08 kel.karang sari,kec.neglasari tangerang banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087788347435', 'wantosadewa@yahoo.co.id', NULL, NULL, NULL, 'jaih', NULL, '0', 0, 0, 0, 'juni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(689, NULL, '2014.11.037', 'Efendi Yusuf', 'P', NULL, 'serang', '2010-06-10', 1, NULL, NULL, 'kubang lesung pabuaran rt o7/03 kel.taman baru kec.citangkil kota cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911083117', 'endiefendi76@yahoo.co.id', NULL, NULL, NULL, 'maskud', NULL, '0', 0, 0, 0, 'johariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(690, NULL, '2014.11.038', 'Rizky Gumilang', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'Jl. KH.Ahmad Chotib Gang Ksatria Dalam No 14A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087862014515', 'Rizky_Gumilang_1993@yahoo.co.id', NULL, NULL, NULL, 'H.Tatang Muchtar', NULL, '0', 0, 0, 0, 'Hj.Tutiah Saidi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(691, NULL, '2014.11.039', 'Ikhwanul Muslimin', 'P', NULL, 'serang', '1984-01-13', 1, NULL, NULL, 'jl.H LEMAN PINTU AIR KEL.GEREM LINK.KAWISTA RT003/011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774430911', 'ikhwanulm1318@yahoo.com', NULL, NULL, NULL, 'BARMAWI', NULL, '0', 0, 0, 0, 'MASANI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(692, NULL, '2014.11.040', 'Rendy Setiawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(693, NULL, '2014.11.041', 'Kemas Evian Bagus Pratama', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085646564934', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(694, NULL, '2014.11.042', 'Nirwan Pangaribuan', 'P', NULL, 'cilegon', '1990-11-07', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771340984', 'nirwanpangaribuan@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(695, NULL, '2014.11.043', 'Ahmad Wahyu Ismu', 'P', NULL, 'serang', '1996-10-09', 1, NULL, NULL, 'link.sumur wuluh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089695237781', 'ahmadwahyuismu@yahoo.com', NULL, NULL, NULL, 'syamsudin', NULL, '0', 0, 0, 0, 'ita nurita', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(696, NULL, '2014.11.044', 'Saepulloh', 'P', NULL, 'serang', '1991-09-03', 1, NULL, NULL, 'Link. Tanjung Sekong RT/RW 02/02 Kel. Lebakgede Kec. Pulomerak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773974263', 'saepulloh91@yahoo.co.id', NULL, NULL, NULL, 'Misnan', NULL, '0', 0, 0, 0, 'Tarmah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(697, NULL, '2014.11.045', 'Akhsanudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(698, NULL, '2014.11.046', 'Muhammad Yazid muzaki', 'P', NULL, 'CILEGON', '0000-00-00', 1, NULL, NULL, 'JL.RAYA ANYER KOMP.SINYAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774720751', 'yazidmuzaki@gmail.com', NULL, NULL, NULL, 'ABU BAKAR JAHIDIN', NULL, '0', 0, 0, 0, 'SUNIATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(699, NULL, '2014.11.047', 'Madhayakti Tantrio', 'P', NULL, 'Cilegon', '2014-06-01', 1, NULL, NULL, 'Jl. Arga burangrang blok D8/18. komp. arga baja pura', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773675051', 'madhayaktitantriomail@gmail.com', NULL, NULL, NULL, 'Iwan Sumarwan', NULL, '0', 0, 0, 0, 'Tri Ratmini', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(700, NULL, '2014.11.048', 'Trio Pamungkas', 'P', NULL, 'SERANG', '1992-04-16', 1, NULL, NULL, 'Link. BabakanTuri rt.07/02 Ds. Tamansari Kec. Pulomerak Cilegon-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813324616', 'trio1618@gmail.com', NULL, NULL, NULL, 'Dasar Sopan', NULL, '0', 0, 0, 0, 'Sunarti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(701, NULL, '2014.11.049', 'Dwi Muktiarso', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'kp.cilanggir rt08 rw02 des.kosambironyok kec.anyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774372772', 'ddwimukti@yahoo.com', NULL, NULL, NULL, 'suparyno', NULL, '0', 0, 0, 0, 'romlah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(702, NULL, '2014.11.050', 'Yogie Septiadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'link. leuweung sawo ,Rt/Rw.005/009, desa.kotabumi, kec.purwakarta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809192338', 'yogie.septiadi@gmail.com', NULL, NULL, NULL, 'Fuadi as', NULL, '0', 0, 0, 0, 'anniyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(703, NULL, '2014.11.051', 'Wakonadori', 'P', NULL, 'Pandeglang', '1987-07-27', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772566720', 'wakonadori.r@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(704, NULL, '2014.11.052', 'Kilas Amangtu', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'link kubangkutu 02-03 cilegon banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808563393', 'ias_am@yahoo.com', NULL, NULL, NULL, 'moch dahlan seno', NULL, '0', 0, 0, 0, 'kholifah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(705, NULL, '2014.11.053', 'Bondan Ari Setiawan', 'P', NULL, 'serang', '1997-07-04', 1, NULL, NULL, 'link.weri RT o2/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089661024758', '', NULL, NULL, NULL, 'SUPAYA', NULL, '0', 0, 0, 0, 'EULIS LINA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(706, NULL, '2014.11.054', 'Miftahul Ulum', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, 'link.mekarjaya RT 01 RW 07 KEC.PULOMERAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089680553339', 'miftahululum556@yahoo.com', NULL, NULL, NULL, 'sarhasi', NULL, '0', 0, 0, 0, 'naiya', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(707, NULL, '2014.11.055', 'Solihin', 'P', NULL, 'SERANG', '1992-10-08', 1, NULL, NULL, 'kp. mulyaulung 002/004, ds. cikoneng, kec. anyer, serang-banten 42166', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771048417', 'cansolihin99@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(708, NULL, '2014.11.056', 'Risa Indah Rofikoh', 'W', NULL, 'serang', '1993-10-25', 1, NULL, NULL, 'Jl. S. Gunung Jati Lingk. Lijajar RT/RW 013/006 Kel./Desa Tegal Ratu Kecamatan Ciwandah Kota Cilegon - Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774814966', 'risandah@gmail.com / rindah10@yahoo.com', NULL, NULL, NULL, 'Cecep Rifai', NULL, '0', 0, 0, 0, 'Hanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(709, NULL, '2014.11.057', 'Hasan Basri', 'P', NULL, 'Sumenep', '2014-03-11', 1, NULL, NULL, 'kalentemu ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083890580254', 'acans94@yahoo.com', NULL, NULL, NULL, 'H.Ach Zaini', NULL, '0', 0, 0, 0, 'Hj. Nur imamah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(710, NULL, '2014.11.058', 'Siti Amenah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(711, NULL, '2014.11.059', 'Joharudin', 'P', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'm.subane', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(712, NULL, '2014.11.060', 'Randy S. Herlambang', 'P', NULL, 'Jakarta', '1991-09-28', 1, NULL, NULL, 'komp. Arga Baja Pura Blok c 2 no. 5 Grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085715316682', 'randy_resonate@yahoo.com', NULL, NULL, NULL, 'Alm. Herwansyah', NULL, '0', 0, 0, 0, 'Lela Warni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(713, NULL, '2014.11.061', 'Restu Fujiadi', 'P', NULL, 'SERANG', '0000-00-00', 1, NULL, NULL, 'Kp. Pondokwaru Rt-01 Rw-01 Jl. Karang Bolong Desa. Bulakan Kec. Cinangka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085778935238', 'fujiadiace@gmail.com', NULL, NULL, NULL, 'Muhaemin', NULL, '0', 0, 0, 0, 'Yati Asturiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(714, NULL, '2014.11.062', 'M. Johan Rosadi', 'P', NULL, 'SERANG', '2014-08-07', 1, NULL, NULL, 'kp.cikiara ds.bulakan kec.cinangka kab.serang-banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085946383411', '', NULL, NULL, NULL, 'moch.mursid', NULL, '0', 0, 0, 0, 'jamiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(715, NULL, '2014.11.063', 'Nifsal Adhi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(716, NULL, '2014.11.064', 'Ponco Adi S.', 'P', NULL, 'SERANG', '2014-12-30', 1, NULL, NULL, 'LINK. TEGAL WANGI REJANE DS.RAWA ARUM KEC. GEROGOL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087888652155', 'ponco.adisuparman@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(717, NULL, '2014.11.065', 'Indah Ayu Hervian', 'W', NULL, 'Serang', '2014-04-22', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'Hermanto', NULL, '0', 0, 0, 0, 'Evi Yulita Sari', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(718, NULL, '2014.11.066', 'A. Pandji Widhianto', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(719, NULL, '2014.11.067', 'Cipto Kisworo', 'P', NULL, 'indramayu', '1992-10-21', 1, NULL, NULL, 'rt/rw 01/03 desa ujungaris kec.widasari indramayu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089641479198', 'cipto.kisworo@pgn.co.id', NULL, NULL, NULL, 'rana', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(720, NULL, '2014.11.068', 'Erlan Priatna', 'P', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, 'lk.kubang wuluh Rt03/06 kel. kebondalem kec. purwakarta cilegon, banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087871794556', 'erlan.priatna@yahoo.co.id', NULL, NULL, NULL, 'Ahmad Suhandi', NULL, '0', 0, 0, 0, 'siti Aminah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(721, NULL, '2014.11.069', 'M. Ikhwan Halim', 'P', NULL, 'MADIUN', '1987-10-07', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085217233210', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(722, NULL, '2014.11.070', 'Adhi Saputra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(723, NULL, '2014.11.071', 'Asmariyah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(724, NULL, '2014.11.072', 'Muhamar Aji', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(725, NULL, '2014.11.073', 'Fatwa Dwi Sandyasa', 'P', NULL, 'Serang', '0000-00-00', 1, NULL, NULL, 'Griya serdang indah blok a7 no8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089612098433', 'Fatwadwisandyasa@yahoo.co.id', NULL, NULL, NULL, 'Drs.suhadi mualif', NULL, '0', 0, 0, 0, 'Iin sintaningsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(726, NULL, '2014.11.074', 'Ahmad Afriandi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'Desa Legundi RT/RW 004/002 Kec.Ketapang Lampung Selatan Lampung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085768363667', 'ahmad.afriandi24@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(727, NULL, '2014.11.075', 'Sudarnita', 'W', NULL, 'cilegon', '2014-01-12', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089687454872', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(728, NULL, '2014.11.076', 'Mia Sri Anggraeni', 'W', NULL, 'Cirebon', '1996-11-26', 1, NULL, NULL, 'Link. Tegal Wangi Rejane RT/RW 002/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089635124358', '', NULL, NULL, NULL, 'Achmad Tachroni', NULL, '0', 0, 0, 0, 'Nurhasanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(729, NULL, '2014.11.077', 'Sulhi Leliyadi', 'P', NULL, 'cilegon', '1996-09-24', 1, NULL, NULL, 'link.ciputat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '081906108119', 'sulhileliyadi077@gmail.com', NULL, NULL, NULL, 'jihadi', NULL, '0', 0, 0, 0, 'latifah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(730, NULL, '2014.11.078', 'Amilatul Hasanah', 'W', NULL, 'cilegon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(731, NULL, '2014.11.079', 'Fiqi Atikah', 'W', NULL, 'serang', '1996-09-20', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(732, NULL, '2014.11.080', 'Arifurrohman', 'P', NULL, '17-05-1997', '0000-00-00', 1, NULL, NULL, 'Kp.Buah Gede Argawana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089612095412', 'Ariefelamin@yahoo.co.id', NULL, NULL, NULL, 'Hudori', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(733, NULL, '2014.11.081', 'Unayah', 'W', NULL, 'SERANG', '2014-09-18', 1, NULL, NULL, 'KP.SOLOR-LOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089689361066', 'Unayaciemuetz@yahoo.co.id', NULL, NULL, NULL, 'HANAFI', NULL, '0', 0, 0, 0, 'SUARIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(734, NULL, '2014.11.082', 'Muslimah Nurjanah', 'W', NULL, 'cilegon', '1996-04-28', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(735, NULL, '2014.11.083', 'Adi Abd Rahman Fahram', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(736, NULL, '2014.11.084', 'Hikmat Hidayat', 'P', NULL, 'lebak', '1995-09-27', 1, NULL, NULL, 'KP.LEBAKWARU DESA.SUMURBATU RT/RW03/02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'hikmathidayat60@yahoo.com/hikmathidayat9@gmail.com', NULL, NULL, NULL, 'USUP', NULL, '0', 0, 0, 0, 'SITI ATIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(737, NULL, '2014.11.085', 'Dadang Fahrudin', 'P', NULL, 'SERANG', '1996-04-05', 1, NULL, NULL, 'Link. Jombang Tangsi RT:002 RW:002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774171472', 'wgagah96@gmail.com', NULL, NULL, NULL, 'Khaerudin', NULL, '0', 0, 0, 0, 'Raisah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(738, NULL, '2014.11.086', 'Nurwanto', 'P', NULL, 'pandeglang', '1993-09-23', 1, NULL, NULL, 'kp.cilurah rt.02 rw.05 ds.sukanagara kec.carita pandeglang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085779565864', 'nurwanto.in@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(739, NULL, '2014.11.088', 'Mumtaz Aziz', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(740, NULL, '2014.11.089', 'Adi Muhtadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(741, NULL, '2014.11.090', 'Dinda Tiara Pertiwi', 'P', NULL, 'Bontang', '1996-11-18', 1, NULL, NULL, 'Link.Pagebangan Rt/010 Rw/003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089620661259', 'Dindatiarapertiwi@yahoo.com', NULL, NULL, NULL, 'Yuspra Hamdali', NULL, '0', 0, 0, 0, 'Sunartin', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(742, NULL, '2014.11.091', 'Muhamad Sahrul Mubarok', 'P', NULL, 'serang', '1996-09-28', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'RIDWAN', NULL, '0', 0, 0, 0, 'MUHIBAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(743, NULL, '2014.11.092', 'Ramadhan Pratama', 'P', NULL, 'KALIANDA,Lampung,Ind', '1997-01-09', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089664563362', 'Ramadhanpeatama@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(744, NULL, '2014.11.093', 'Gilang Ramadhan', 'P', NULL, 'SERANG', '1996-02-08', 1, NULL, NULL, 'PONDOK CILEGON INDAH BLOK C76 NO 28 RT 02/ RW 06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089683119659', 'gilangg.ramadhann@yahoo.com', NULL, NULL, NULL, 'ABDUL HIDAYAT', NULL, '0', 0, 0, 0, 'ENDAS', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(745, NULL, '2014.11.094', 'Verawati', 'W', NULL, 'SERANG', '1995-04-09', 1, NULL, NULL, 'Link. Tunggak RT 001 RW 002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089683208267', 'Verawati_09@yahoo.com', NULL, NULL, NULL, 'M.YUSUF', NULL, '0', 0, 0, 0, 'Umayah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(746, NULL, '2014.11.095', 'TB. Ilham Maulana A. M.', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'JL. Raya Cilegon KM3 Legok Assalam Rt.01/11 Kec. Taktakan Serang Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08979343472', 'tb.ilham26@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(747, NULL, '2014.11.096', 'Tri Indah Widiastuti', 'W', NULL, 'Serang', '1996-08-17', 1, NULL, NULL, 'Kp. Mangga Dua RT003/001 Ds. Margasana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'tri.indah14@yahoo.co.id', NULL, NULL, NULL, 'Dardja', NULL, '0', 0, 0, 0, 'Sulistyoningsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(748, NULL, '2014.11.097', 'Riki Krismas', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(749, NULL, '2014.11.098', 'Mustakim Nawaldi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(750, NULL, '2014.11.099', 'Muhamad Andrian ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(751, NULL, '2014.11.100', 'Adi Akbar Adriyan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'metro cendana M15 No 17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771171402', 'a_adiakbar@yahoo.com', NULL, NULL, NULL, 'hasannudin', NULL, '0', 0, 0, 0, 'Junariyah ', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(752, NULL, '2014.11.101', 'Feti Rosliyawati', 'W', NULL, 'serang', '1995-08-11', 1, NULL, NULL, 'jln. salira indah kp.pengoreng ds.mangunreja kec.puloampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774596429', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(753, NULL, '2014.11.102', 'Abdul Rosid', 'P', NULL, 'serang', '1995-11-27', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871760818', 'abdul.rosid85@yahoo.com', NULL, NULL, NULL, 'harun', NULL, '0', 0, 0, 0, 'Nadiroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(754, NULL, '2014.11.103', 'A. Syaikhul Millah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089653808548', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(755, NULL, '2014.11.104', 'Ade Supardin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(756, NULL, '2014.11.105', 'Ayu Nafiroh', 'W', NULL, 'Cilegon', '2014-12-12', 1, NULL, NULL, 'Link.Kerotek RT/003 RW/002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089610760839', 'AyuNafiroh@ymail.com', NULL, NULL, NULL, 'H.Asnawi', NULL, '0', 0, 0, 0, 'Hj.Madihah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(757, NULL, '2014.11.106', 'Randi Desma Purnama', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(758, NULL, '2014.11.107', 'Septian Jaka Lesmana', 'P', NULL, 'Jakarta', '1996-09-26', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0859-4526-8260', 'rakafvckinway@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(759, NULL, '2014.11.108', 'Fredi Gunawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(760, NULL, '2014.11.109', 'yusta', '', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'mhs_teleponrmh', '081932322959', NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(761, NULL, '2014.12.001', 'Silviana Renita Ramadhan', 'W', NULL, 'serang', '1994-02-17', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(762, NULL, '2014.12.002', 'Yuliza Oktaviani Ilham', 'W', NULL, 'Serang', '1996-10-18', 1, NULL, NULL, 'Jl. H. Agus Salim Link. Weri RT/RW 002/001 Kebonsari, Citangkil, Cilegon, Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771837324', '', NULL, NULL, NULL, 'Zainul Abidin', NULL, '0', 0, 0, 0, 'Hamroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(763, NULL, '2014.12.003', 'Dede Hendrawan', 'P', NULL, 'serang', '1993-02-15', 1, NULL, NULL, 'cilegon kavling blok c', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813442903', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(764, NULL, '2014.12.004', 'Melati Wasti', 'W', NULL, 'SERANG', '1995-09-11', 1, NULL, NULL, 'TAMAN KRAKATAU BLOK G.14/12A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'melatiwasti@gmail.com', NULL, NULL, NULL, 'RAMLAN SITOMPUL', NULL, '0', 0, 0, 0, 'MERI SIMANJUNTAK', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(765, NULL, '2014.12.005', 'Yulia Mutiara Madarina', 'W', NULL, 'Nganjuk', '1996-07-04', 1, NULL, NULL, 'Komp.Metro Victoria K2 NO.17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'yuliamutiara15@yahoo.com', NULL, NULL, NULL, 'MUDJIB', NULL, '0', 0, 0, 0, 'SUTIAWATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(766, NULL, '2014.12.006', 'Wiryadi Sulasmana', 'P', NULL, 'Medan', '1996-04-29', 1, NULL, NULL, 'Anyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '62877-7403-4345', 'wiryadi_sulasmana45@yahoo.co.d', NULL, NULL, NULL, 'Marta', NULL, '0', 0, 0, 0, 'Nurita', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(767, NULL, '2014.12.007', 'Harvia Soendari', 'W', NULL, 'Cilegon', '1996-10-17', 1, NULL, NULL, 'Link. Sumampir Timur RT:01 RW:04 NO:066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089678714796', 'harvias17@gmail.com', NULL, NULL, NULL, 'Ujang Sudari', NULL, '0', 0, 0, 0, 'Masduri', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(768, NULL, '2014.12.008', 'Bib Nova Melati', 'W', NULL, 'Serang', '1995-11-10', 1, NULL, NULL, 'Jl. H Agus Salim link. weri RT/RW 02/01 kebonsari, citangkil, cilegon, banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771119626', 'bibnova_melati@yahoo.com', NULL, NULL, NULL, 'Samboja', NULL, '0', 0, 0, 0, 'Ratna Rohayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(769, NULL, '2014.12.009', 'Qurotul Ainie', 'W', NULL, 'serang', '1995-04-25', 1, NULL, NULL, 'kp.walikukun ds.terate 03/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08999620085', 'ainieniz_niz@yahoo.co.id', NULL, NULL, NULL, 'H.ismail marjuki', NULL, '0', 0, 0, 0, 'Hj.suirat', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(770, NULL, '2014.12.010', 'Suharyati', 'W', NULL, 'serang', '1995-07-10', 1, NULL, NULL, 'jl.fatahij', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087778554995', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(771, NULL, '2014.12.011', 'Nikmatullah', 'P', NULL, 'serang', '1995-03-13', 1, NULL, NULL, 'KP RAGAS AWURAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+6283875158624', 'nikmatulloh585@gmail.com', NULL, NULL, NULL, 'madudin', NULL, '0', 0, 0, 0, 'LAYINAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(772, NULL, '2014.12.012', 'Rohimatul Maula', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(773, NULL, '2014.12.013', 'Egi Andrian', 'P', NULL, 'Serang', '1995-12-31', 1, NULL, NULL, 'Link. Pegantungan RT04 RW07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08972111463', 'egiandrian1337@gmail.com', NULL, NULL, NULL, 'Sugeng Handaya', NULL, '0', 0, 0, 0, 'Mulayatinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(774, NULL, '2014.12.014', 'Ditha Resti Aryana', 'W', NULL, 'bandar lampung', '1995-12-12', 1, NULL, NULL, 'kp.ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085946179389', 'dyetha12@gmail.com', NULL, NULL, NULL, 'alan heryana', NULL, '0', 0, 0, 0, 'sunawati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(775, NULL, '2014.12.015', 'Rian Apriyana', 'P', NULL, 'Serang', '1996-04-10', 1, NULL, NULL, 'KP.CIKONENG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085770569902', 'apriyanarian@gmail.com', NULL, NULL, NULL, 'Edi Rolly', NULL, '0', 0, 0, 0, 'Nani Sunarni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(776, NULL, '2014.12.016', 'Susiyanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(777, NULL, '2014.12.017', 'Rohmatul Mafazah', 'W', NULL, 'serang', '1996-08-07', 1, NULL, NULL, 'LINK.KUBANG LESUNG KULON RT/RW 008/004 Kel/Desa Taman Baru Kec.Citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '6289-7538-9331', 'rohmatul_mafazah96@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(778, NULL, '2014.12.018', 'Mumtaz Aziz', 'P', NULL, 'serang', '1995-12-09', 1, NULL, NULL, 'rahayu utara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089651215048', '', NULL, NULL, NULL, 'kidzwini', NULL, '0', 0, 0, 0, 'MASIKAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(779, NULL, '2014.21.001', 'Nia Fitriani', 'W', NULL, 'serang', '1996-03-18', 1, NULL, NULL, 'martapura RT 01/03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085929844684', 'nia_fitriani15@.yahoo.com', NULL, NULL, NULL, 'PARDI', NULL, '0', 0, 0, 0, 'suwarti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(780, NULL, '2014.21.002', 'Surya Rusmita', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(781, NULL, '2014.21.003', 'Fitri Octavia Ardi Handayani', 'W', NULL, 'serang', '1994-10-08', 1, NULL, NULL, 'PCI BLOK D 32 NO 15 RT/RW.001/004 CIBEBER,CILEGON-BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254 381-783', '02454 381-783', '', NULL, NULL, NULL, 'SARDI HD', NULL, '0', 0, 0, 0, 'SUGIRI PURWANTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(782, NULL, '2014.21.004', 'Anisa Pahmi', 'W', NULL, 'TEBING TINGGI', '1996-11-05', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087744747276', '', NULL, NULL, NULL, 'aminullah', NULL, '0', 0, 0, 0, 'suci murniati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(783, NULL, '2014.22.001', 'Ludvia', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087773407735', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(784, NULL, '2014.22.002', 'Ikrima Nurhanifa', 'W', NULL, 'serang', '1995-06-23', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085966533132', 'rhyma219@gmail.com', NULL, NULL, NULL, 'suwanto', NULL, '0', 0, 0, 0, 'ida farida', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(785, NULL, '2014.22.003', 'Eulis Marwati', 'W', NULL, 'serang', '1996-08-08', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089680549618', 'eulis.marwati@yahoo.com', NULL, NULL, NULL, 'aliyudin', NULL, '0', 0, 0, 0, 'martinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(786, NULL, '2015.11.001', 'Jodi Ginandra Paksi', 'P', NULL, 'Serang', '1996-06-06', 1, NULL, NULL, 'Link. Pegantungan Baru RT 003/014 No.22 Kel. Jombang Wetan. Cilegon - Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771617180', 'jodiginandrapaksi@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(787, NULL, '2015.11.002', 'Faisal Riyandi', 'P', NULL, 'SERANG', '1991-10-08', 1, NULL, NULL, 'JL. SERANG CILEGON KM 8 KRAMATWATU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087875875352', 'faisalriyandi3@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(788, NULL, '2015.11.003', 'Nunu Anugrah', 'P', NULL, 'cirebon', '1995-01-25', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087744425231', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(789, NULL, '2015.11.004', 'Juen Suhandi', 'P', NULL, 'SERANG', '1993-12-20', 1, NULL, NULL, 'Link.Sudimampir RT/RW 002/005 Kel.Tamansari Kec.Pulomerak Kota.Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087808719252', 'juensuhandi@hotmail.com', NULL, NULL, NULL, 'TURAH', NULL, '0', 0, 0, 0, 'TUTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(790, NULL, '2015.11.005', 'Rt. Wahyu Alda Purnama', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(791, NULL, '2015.11.006', 'Krista Andrianto', 'P', NULL, 'Serang', '1994-10-30', 1, NULL, NULL, 'link.Sukajadi Rt03/Rw02 Kel.Mekarsari Kec.Pulomerak Cilegon-Banten Indonesia', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089650786958', 'kristaandrian12@gmail.com', NULL, NULL, NULL, 'Karno', NULL, '0', 0, 0, 0, 'Trisnawati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(792, NULL, '2015.11.007', 'Faturohman Hidayat', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(793, NULL, '2015.11.008', 'Manwa Ramadhan Ambarita', 'P', NULL, 'Tangerang', '1994-02-16', 1, NULL, NULL, 'Link. Kenanga RT/RT 04/02 Masigit, Jombang, Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '083872208742', 'manwa.ramadhan@gmail.com', NULL, NULL, NULL, 'Syariffudin Ambarita', NULL, '0', 0, 0, 0, 'Sri Sudarmi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(794, NULL, '2015.11.009', 'Noviyanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(795, NULL, '2015.11.010', 'Hendra Pramana', 'P', NULL, 'Metro, Lampung', '1993-07-23', 1, NULL, NULL, 'jl. merica RT.08 RW.04 Kel. IringMulyo Kec. Metro Timur, Kota metro,Lampung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945160819', 'wawwaendhra@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(796, NULL, '2015.11.011', 'Angga Berliata', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(797, NULL, '2015.11.012', 'Dede Gunawan', 'P', NULL, '', '1992-10-13', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(798, NULL, '2015.11.013', 'Itty Herlina', 'W', NULL, 'SERANG', '2015-09-29', 1, NULL, NULL, 'LINK ACING BARU RT 01/07 KEL MASIGIT KEC JOMBANG CILEGON ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089602913452', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(799, NULL, '2015.11.014', 'Fery Dwi Ramadhan', 'P', NULL, 'serang', '1994-02-12', 1, NULL, NULL, 'JL.KUBANG LAMPUNG NO.24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+628999613365', 'ferydwiramadhan@rocketmail.com', NULL, NULL, NULL, 'Slamet Untung', NULL, '0', 0, 0, 0, 'Sri Lestari', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(800, NULL, '2015.11.015', 'TB. Deden Supriyatna', 'P', NULL, 'Serang', '1993-06-12', 1, NULL, NULL, 'Jl. Ciptayasa Kp. Ciruas Pasar Kec. Ciruas Kab. Serang - Banten 42182', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+628998645422', 'tubagusdedens@gmail.com', NULL, NULL, NULL, 'Tubagus Subhi', NULL, '0', 0, 0, 0, 'Siti Asiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(801, NULL, '2015.11.016', 'Sugi Nouri Yandi', 'P', NULL, 'serang', '1994-11-13', 1, NULL, NULL, 'Link.Tegal wangi kp.Baru rt02/01 kel.Rawaarum Kec.Gerogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812628526', 'snovriyandi@gmail.com', NULL, NULL, NULL, 'Muhammad Hasusi', NULL, '0', 0, 0, 0, 'Sunaenah ( Almrh )', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(802, NULL, '2015.11.017', 'Roby Yansah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(803, NULL, '2015.11.018', 'OSMAR CHARLIE', 'P', NULL, 'SERANG', '1993-03-23', 1, NULL, NULL, 'Link. Baru Rt 01 / Rw 03 No.53 Kel.Kebon Dalem Kec.Purwakarta Cilegon - Banten 42433', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772324715', 'os.simbolon@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(804, NULL, '2015.11.019', 'Hendrawan', 'P', NULL, 'Serang', '1994-09-23', 1, NULL, NULL, 'Link. Kedawung rt 014/007 kel. Taman baru kec. Citangkil kota Cilegon Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254 377040', '087777922221', 'hendra_wan31@yahoo.co.id', NULL, NULL, NULL, 'Nikmat', NULL, '0', 0, 0, 0, 'Sanawiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(805, NULL, '2015.11.020', 'Rifqillah Umami', 'P', NULL, 'Cilegon', '1991-05-30', 1, NULL, NULL, 'link.ciberko kel.kalitimbang kec.cibeber cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087839392244', 'rifqillah.umami@yahoo.com', NULL, NULL, NULL, 'Ahmad Taufik Sandim', NULL, '0', 0, 0, 0, 'Ibah Muhibah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(806, NULL, '2015.11.021', 'Muhammad Ramdan Nur Sidik', 'P', NULL, 'Cilegon', '2015-03-23', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(807, NULL, '2015.11.022', 'Rani Apriani', 'W', NULL, '', '2015-04-05', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'sofyaaaaaaaaaaaaaan', NULL, '0', 0, 0, 0, 'aasnayanti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(808, NULL, '2015.11.023', 'Muhammad Iqbal', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(809, NULL, '2015.11.024', 'Ranggata Pramudia putra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(810, NULL, '2015.11.025', 'Cachyono', 'P', NULL, 'BANJARNEGARA JAWA TE', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871280632', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(811, NULL, '2015.11.026', 'Indra Adi Wijaya', 'P', NULL, 'Serang', '1995-01-12', 1, NULL, NULL, 'Link.kubang sepat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '(0254) 399735', '(0254) 399735', 'wijaya.indra88@yahoo.com', NULL, NULL, NULL, 'Hudari', NULL, '0', 0, 0, 0, 'Susilawati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(812, NULL, '2015.11.027', 'Muhammad Andi RIzaldi', 'P', NULL, 'CILEGON', '1998-01-04', 1, NULL, NULL, 'KOMP.TAMAN WARNASARI INDAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771732759', '', NULL, NULL, NULL, 'IYEN YAYAN S', NULL, '0', 0, 0, 0, 'N.RITA ZAHARA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(813, NULL, '2015.11.028', 'Fauzan Ifni', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(814, NULL, '2015.11.029', 'Abdul Azis Nugraha', 'P', NULL, 'cilegon', '1997-11-12', 1, NULL, NULL, 'link.Cipinang Atas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089661441830', '', NULL, NULL, NULL, 'Muhamad Buang', NULL, '0', 0, 0, 0, 'Dudeh Juarsih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(815, NULL, '2015.11.030', 'Muhamad Safei', 'P', NULL, 'Cilegon', '1996-03-18', 1, NULL, NULL, 'Jalan Yos sudarso Link Babakanseri Rt/Rw 001/006, Merak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089642569636', 'feymuhamad@yahoo.com', NULL, NULL, NULL, 'Jahrudin', NULL, '0', 0, 0, 0, 'Robiyatul adawiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(816, NULL, '2015.11.031', 'Senah', 'W', NULL, 'bekasi', '1996-05-05', 1, NULL, NULL, 'kp.turi jaya ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089696217313', '', NULL, NULL, NULL, 'manis ', NULL, '0', 0, 0, 0, 'alm.waselah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(817, NULL, '2015.11.032', 'Suhaili', 'P', NULL, 'serang', '1996-11-01', 1, NULL, NULL, 'Kp.Buah Gede RT 018 RW 010 Desa. Argawana Kec.Puloampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089665842162', '', NULL, NULL, NULL, 'Alm. Rusdi', NULL, '0', 0, 0, 0, 'Suhaya', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(818, NULL, '2015.11.033', 'Adi Wahyu Budi Utomo', 'P', NULL, 'serang', '1997-03-17', 1, NULL, NULL, 'Kp Buah Gede RT 018 RW 010 Desa Argawana Kecamatan Pulo Ampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089638410486', '', NULL, NULL, NULL, 'MOH. WAHID', NULL, '0', 0, 0, 0, 'ATIFAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(819, NULL, '2015.11.034', 'Winda Lestari', 'W', NULL, 'Cilegon', '1997-03-22', 1, NULL, NULL, 'JL.KIMUDZAKIR LINK.CIGADING 03.01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089661420256', '', NULL, NULL, NULL, 'BASARUDIN', NULL, '0', 0, 0, 0, 'RUKHSOH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(820, NULL, '2015.11.035', 'Naimatul Aulia', 'W', NULL, 'serang', '1996-11-08', 1, NULL, NULL, 'link. tegal wangi solor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089606424974', '', NULL, NULL, NULL, 'hasbirin', NULL, '0', 0, 0, 0, 'nasibah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(821, NULL, '2015.11.036', 'Siman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(822, NULL, '2015.11.037', 'Eka Febryanto Ramadhani', 'P', NULL, 'Cianjur', '1996-02-15', 1, NULL, NULL, 'KOMP PURI HIJAU REGENCY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '6281297253860', 'fmurderri@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(823, NULL, '2015.11.038', 'Sri Ani Umayah', 'W', NULL, 'serang', '1997-08-02', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'abdul wahid', NULL, '0', 0, 0, 0, 'rohayah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(824, NULL, '2015.11.039', 'Muhsen', 'P', NULL, 'woha', '1997-05-14', 1, NULL, NULL, 'rasabou bima ntb', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '082236001916', '', NULL, NULL, NULL, 'Ahmad', NULL, '0', 0, 0, 0, 'Lilis Anggriani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(825, NULL, '2015.11.040', 'Siti Rohminyani', 'P', NULL, 'serang', '1996-09-15', 1, NULL, NULL, 'link.kagungan RT/RW 001/006 KEL. GEREM, KEC.GROGOL,KOT. CILEGON/BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771957043', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(826, NULL, '2015.11.041', 'Supriyanto', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(827, NULL, '2015.11.042', 'Novia Indriyati', 'W', NULL, 'cilegon', '1998-11-25', 1, NULL, NULL, 'jl.s.kali jaga link.pangabuan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089621579629', '', NULL, NULL, NULL, 'SAYUTI', NULL, '0', 0, 0, 0, 'hayati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(828, NULL, '2015.11.043', 'Dede Suhendar', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(829, NULL, '2015.11.044', 'Muhidi Rohman', 'P', NULL, 'CILEGON', '1995-10-01', 1, NULL, NULL, 'PAKUNCEN CIWEDUS CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'HAMBALI', NULL, '0', 0, 0, 0, 'MARIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(830, NULL, '2015.11.045', 'Hery Susanto', 'P', NULL, 'serang', '2015-10-19', 1, NULL, NULL, 'kp.mandaya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771496284', '', NULL, NULL, NULL, 'Sadeli', NULL, '0', 0, 0, 0, 'Hindun', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(831, NULL, '2015.11.046', 'Muhammad Erwin Saputra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(832, NULL, '2015.11.047', 'Tb. Muhamad Fauzan Hilmany', 'P', NULL, 'pandeglang', '1997-03-18', 1, NULL, NULL, 'link.curug rt 04/rw 03 no.71', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089635212136', 'tb.hilman03@gmail.com', NULL, NULL, NULL, 'tb.nana sujana', NULL, '0', 0, 0, 0, 'edah saodah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(833, NULL, '2015.11.048', 'Sofandi Saputra Jaya', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(834, NULL, '2015.11.049', 'Nur Wahyudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(835, NULL, '2015.11.050', 'M. Rasyid Elgani', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(836, NULL, '2015.11.051', 'Billy augustin', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(837, NULL, '2015.11.052', 'Saiful Hasan', 'P', NULL, 'wonosobo', '1993-01-09', 1, NULL, NULL, 'Jl. Lingkar Utara km. 5 Rt. 002/001 Bomerto, Wonosobo, jawa Tengah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089 671 881 869', 'saiful_hasan19@yahoo.com', NULL, NULL, NULL, 'Abu Tholip', NULL, '0', 0, 0, 0, 'Jumiati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(838, NULL, '2015.11.053', 'Rustam Affandi', 'P', NULL, 'Cilegon', '1998-02-26', 1, NULL, NULL, 'Jl. Kayumanis No. 3c Kav Blok G RT/RW 007/006 Kel Ciwaduk Kec Cilegon Kota Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771591640', 'rustamaffandi335@gmail.com', NULL, NULL, NULL, 'Jasmani', NULL, '0', 0, 0, 0, 'Tatu Rohabiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(839, NULL, '2015.11.054', 'Anggi Kurniawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(840, NULL, '2015.11.055', 'Hani Pertiwi', 'W', NULL, 'Talang padang', '1995-05-24', 1, NULL, NULL, 'Link.delingseng rt 001 rw 001 kel.kebon sari kec.citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809193971', '', NULL, NULL, NULL, 'Hamdan', NULL, '0', 0, 0, 0, 'Nurbaiti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(841, NULL, '2015.11.056', 'Dika Meliandana', 'P', NULL, 'Tangerang', '1994-03-10', 1, NULL, NULL, 'BATUCEPER TIMUR TANGERANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085946068151', 'andikajuhandi@gmail.com', NULL, NULL, NULL, 'Andi Juhandi', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(842, NULL, '2015.11.057', 'Reni Nuraeni', 'W', NULL, 'SERANG', '1978-08-10', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772590003', '', NULL, NULL, NULL, 'SLAMET IRIANTO', NULL, '0', 0, 0, 0, 'FITRIYATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(843, NULL, '2015.11.058', 'Khoirul Amri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(844, NULL, '2015.11.059', 'Attika Adha Kurnia', 'W', NULL, '', '1998-04-06', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(845, NULL, '2015.11.060', 'Muhamad Alfaris', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(846, NULL, '2015.11.061', 'Isnu Awalin', 'P', NULL, '29 November 1990', '0000-00-00', 1, NULL, NULL, 'Link.pegantungan lama Rt.004 Rw.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '083812586911', 'Nueyassa@gmail.com', NULL, NULL, NULL, 'Ikhsan', NULL, '0', 0, 0, 0, 'Nurwahidah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(847, NULL, '2015.11.062', 'Amirul Faisal', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(848, NULL, '2015.11.063', 'Anton', 'P', NULL, 'Serang', '2015-06-05', 1, NULL, NULL, 'Kampung jalumprit, kecamatan waringin kurung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813192515', 'abaanton15@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(849, NULL, '2015.11.064', 'Rhian Rizki Ramadhan', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774407654', 'rhianrizky@gmail. com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(850, NULL, '2015.11.065', 'Wanda Wulandari safitri', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(851, NULL, '2015.11.066', 'Adam Faisal Sentana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(852, NULL, '2015.11.067', 'Asep Saepulloh', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(853, NULL, '2015.11.068', 'Ahmad Rifai Amin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(854, NULL, '2015.11.069', 'Pesti Manik', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(855, NULL, '2015.11.070', 'Alex Sudarman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(856, NULL, '2015.11.071', 'Rahmawati', 'W', NULL, 'muaradua', '2015-09-15', 1, NULL, NULL, 'LINK SUKAJAYA KEL KEBONSARI KEC CITANGKIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087796932226', 'rahmawatii1562@yahoo.com', NULL, NULL, NULL, 'YON HENDRI', NULL, '0', 0, 0, 0, 'ZALEHA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(857, NULL, '2015.11.072', 'Erfian Kurniawan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(858, NULL, '2015.11.073', 'Raden Tirta Gala Dwitia', 'P', NULL, 'Serang', '1993-09-06', 1, NULL, NULL, 'Link,Bumi Waras Rt 03/03 No.34', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087772424134', 'radentirta71@yahoo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(859, NULL, '2015.11.074', 'Gita Ismalia', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(860, NULL, '2015.11.075', 'Arfebriansyah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(861, NULL, '2015.11.076', 'Khosnu Husen', 'P', NULL, 'serang', '1992-10-16', 1, NULL, NULL, 'link. linggar jati rt:02/01 kel. lebak denok kec. citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808351554', '', NULL, NULL, NULL, 'MULYADI B.A', NULL, '0', 0, 0, 0, 'SARTINA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(862, NULL, '2015.11.077', 'Asep Supriatna', 'P', NULL, 'cilegon', '1992-02-09', 1, NULL, NULL, 'jl. h. agus salim link. weri rt:002/001 kel. kebonsari kec. citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085770112245', 'asepsupriatna733@gmail.com', NULL, NULL, NULL, 'sarifudin', NULL, '0', 0, 0, 0, 'rohaniah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(863, NULL, '2015.11.078', 'Khasiroh', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(864, NULL, '2015.11.079', 'Agus Rivandy', 'P', NULL, 'Serang', '1994-08-17', 1, NULL, NULL, 'Link. Kopo Nagreg RT 04/01 Desa Gunung Sugih Kec. Ciwandan Kota Cilegon Kode Pos 42447', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808236958', 'agusrivandy17@gmail.com', NULL, NULL, NULL, 'Sukyani', NULL, '0', 0, 0, 0, 'Lailah Suliyani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(865, NULL, '2015.11.080', 'Adi Suadi', 'P', NULL, 'Serang', '1990-02-01', 1, NULL, NULL, 'Kp. Sait Masjid Desa. Cisait RT. 01/02 Kec. Kragilan Kab. Serang - Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871356953', 'adhi.fleksible@gmail.com', NULL, NULL, NULL, 'Nasim', NULL, '0', 0, 0, 0, 'Jumaeni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(866, NULL, '2015.11.081', 'Nurahman Hasanudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(867, NULL, '2015.11.082', 'Khoerul Umam', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'Desa Krajan Rt 03 Rw 04 Pekuncen Banyumas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085742609143', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(868, NULL, '2015.11.083', 'Muhammad Firman Ariansyah', 'P', NULL, 'Serang', '2015-09-26', 1, NULL, NULL, 'Link. bujang gadung RT.03/03 kec.grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089671218521', 'ariansyahf26@gmail.com', NULL, NULL, NULL, 'Drs. asmanik', NULL, '0', 0, 0, 0, 'asmara dewi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(869, NULL, '2015.11.084', 'Muhadi', 'P', NULL, 'magelang', '1980-07-30', 1, NULL, NULL, 'panauan, tanjung manis, anyar, serang, banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774068310', 'muhadi_888@yahoo.co.id', NULL, NULL, NULL, 'ponijan', NULL, '0', 0, 0, 0, 'tumijah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(870, NULL, '2015.11.085', 'Ardhan Putra Syafrida', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(871, NULL, '2015.11.086', 'Ageng Surosowan Kaibon', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(872, NULL, '2015.11.087', 'Agus Saepullah', 'P', NULL, 'serang', '2015-05-24', 1, NULL, NULL, 'kp.mandaya lor', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089667056715', '', NULL, NULL, NULL, 'tohiri', NULL, '0', 0, 0, 0, 'urerah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(873, NULL, '2015.11.088', 'Agus Supriyanto', 'P', NULL, 'SERANG', '1995-03-28', 1, NULL, NULL, 'Kp. Kedung Ingas. Rt/Rw:014/005, Kel. Pakuncen, Kec. Bojonegara. Serang-Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '087871655918', 'Agus.supry1@gmail.com', NULL, NULL, NULL, 'Setiman', NULL, '0', 0, 0, 0, 'Masubah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(874, NULL, '2015.11.089', 'Agus Sulaiman', 'P', NULL, 'Serang', '1997-08-10', 1, NULL, NULL, 'Link. CILENTRANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085966625697', 'aguz.fiverz123@gmail.com', NULL, NULL, NULL, 'Sarifudin', NULL, '0', 0, 0, 0, 'Murtasiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(875, NULL, '2015.11.090', 'Ade Ilham', 'P', NULL, 'Serang', '1997-03-16', 1, NULL, NULL, 'Kp.pengrango RT/RW.004/002 Ds.Lambangsari Kec.Bojonegara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08981223402', '', NULL, NULL, NULL, 'Bahsumi', NULL, '0', 0, 0, 0, 'Lutfiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(876, NULL, '2015.11.091', 'Wely', 'P', NULL, 'SERANG', '1997-03-30', 1, NULL, NULL, 'Kp.Mulya jadi serang - banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '087808371170', '083812885445', 'sevengold33@gmail.com', NULL, NULL, NULL, 'WAKIMIN', NULL, '0', 0, 0, 0, 'SURYATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(877, NULL, '2015.11.092', 'Anisudin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(878, NULL, '2015.11.093', 'Irwan Septiansyah', 'P', NULL, 'serang', '1996-09-14', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, 'sopian', NULL, '0', 0, 0, 0, 'tutti hardiawati s.pd', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(879, NULL, '2015.11.094', 'Varonica Yulia', 'W', NULL, 'Rembang', '1997-08-19', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081282767958', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(880, NULL, '2015.11.095', 'Lusiana Mardiyani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(881, NULL, '2015.11.096', 'Masaliyah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(882, NULL, '2015.11.097', 'Akhmad Khaerussoleh', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(883, NULL, '2015.11.098', 'Rima Rahmawati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(884, NULL, '2015.11.099', 'Akbar Nugraha Apri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(885, NULL, '2015.11.100', 'Dadan Handiyana', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(886, NULL, '2015.11.101', 'Maman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087709888354', 'maman@sulfindo.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(887, NULL, '2015.11.102', 'Tarno', 'P', NULL, 'subang', '2016-08-07', 1, NULL, NULL, 'Link serdag Rt 04/08 kel. kotabumi kec. purwakarta Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087785703507', 'giga.clg@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(888, NULL, '2015.11.103', 'Yusef Renaldi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(889, NULL, '2015.11.104', 'Haeroni', 'P', NULL, '', '1988-06-03', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808849658', 'haerony93@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(890, NULL, '2015.11.105', 'Dhita Ariyanti Hastuti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(891, NULL, '2015.11.106', 'Cici Anariyah', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(892, NULL, '2015.11.107', 'Lalu Sofyan Efendi', 'P', NULL, 'cirebon', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083891789515', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(893, NULL, '2015.12.001', 'Pipit Mutiara', 'W', NULL, 'serang', '1997-05-25', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089601471243', '', NULL, NULL, NULL, 'Muadi', NULL, '0', 0, 0, 0, 'Hunaiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(894, NULL, '2015.12.002', 'Linda Evan Englista', 'W', NULL, 'SER', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(895, NULL, '2015.12.003', 'Arina Sulistiani', 'W', NULL, '', '2015-03-29', 1, NULL, NULL, 'jl.sunan kudus link.pekalongan rt 01/01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089729921214', 'a3arina.sulistiani@gmail.com', NULL, NULL, NULL, 'safrudin', NULL, '0', 0, 0, 0, 'lilis suriati ', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(896, NULL, '2015.12.004', 'Muhamad Rifki Saputra', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(897, NULL, '2015.12.005', 'Nur Safitri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(898, NULL, '2015.12.006', 'Devi Aprilianti', 'W', NULL, 'SERANG', '1997-04-02', 1, NULL, NULL, 'KP.CIKUBANG 5, DES.ARGAWANA, KEC.PULO AMPEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08994628630', '', NULL, NULL, NULL, 'JUFRON', NULL, '0', 0, 0, 0, 'DEWI MULYANI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(899, NULL, '2015.12.007', 'Nur Aisyah Meirizka W. P.', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(900, NULL, '2015.12.008', 'Novi Alviani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(901, NULL, '2015.21.001', 'Diana Novianti', 'W', NULL, 'Sukabumi', '1994-11-08', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081908087117', 'aristiniskandar88@gmail.com', NULL, NULL, NULL, 'Syamsam Iskandar', NULL, '0', 0, 0, 0, 'Sri Sunarni', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(902, NULL, '2015.21.002', 'Reza Resti Maulani', 'W', NULL, 'Bandung', '1995-08-09', 1, NULL, NULL, 'pondok golf asri blok e4 no 1 sumampir cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '388241', '087808351147', '', NULL, NULL, NULL, 'eddy achmad sudjana', NULL, '0', 0, 0, 0, 'almh.y.suryani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(903, NULL, '2015.21.003', 'Rusdianto', 'P', NULL, 'indramayu', '2015-12-16', 1, NULL, NULL, 'kp.laharmas.sudimampir', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(904, NULL, '2015.21.004', 'Arden Ardianto', 'P', NULL, 'Serang', '1997-07-07', 1, NULL, NULL, 'Perum.Warnasari TWI FWA 38C NO.27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089501163635', '', NULL, NULL, NULL, 'Dedi Purwanto', NULL, '0', 0, 0, 0, 'Siti Aisah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(905, NULL, '2015.22.001', 'Ifat Fauziah', 'W', NULL, 'cilegon', '1997-11-30', 1, NULL, NULL, 'jln.S. Kali jagalink.kopo masjid', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812570552', '', NULL, NULL, NULL, 'jumalisi', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(906, NULL, '2015.22.002', 'Lisa Agustina', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(907, NULL, '2015.22.003', 'Devi Rovita', 'W', NULL, 'serang', '1995-12-30', 1, NULL, NULL, 'link.kaligandu bujang boros', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081911181605', '', NULL, NULL, NULL, 'Asfiroh', NULL, '0', 0, 0, 0, 'Maisah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(908, NULL, '2015.22.004', 'Reza Resti Maulani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(909, NULL, '2015.22.005', 'Arden Ardianto', 'P', NULL, 'Serang', '1997-07-07', 1, NULL, NULL, 'Komp TWI FWA 38 C NO. 27 RT 004 RW 005 cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089501163635', '', NULL, NULL, NULL, 'Dedi Purwanto', NULL, '0', 0, 0, 0, 'Siti Aisah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(910, NULL, '2016.11.001', 'Imran Ayatul Holiq', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(911, NULL, '2016.11.002', 'Aceng', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(912, NULL, '2016.11.003', 'Ruben Immanuel', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(913, NULL, '2016.11.004', 'Yusuf Kurniawan', 'P', NULL, 'serang ', '1998-02-10', 1, NULL, NULL, 'kp.pamekser RT/RW:013/003- Kel/desa:Batukuda- Kec.Mancak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813505317', 'yusufkur98@gmail.com', NULL, NULL, NULL, 'HAMAMI', NULL, '0', 0, 0, 0, 'ROUDOTUL UYUN', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(914, NULL, '2016.11.005', 'Indra Laksono Aji', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'KETILENG PANJAITAN ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085780498429', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(915, NULL, '2016.11.006', 'Fery Mustami Harja', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'LINK. KUBNAG WATES RT/RW 001/008 KEC. PURWAKARTA KEL. KOTABUMI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871410262', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(916, NULL, '2016.11.007', 'Heni', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'Kp. Pasir Juwet', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089502502914', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(917, NULL, '2016.11.008', 'Arief Rohmanul Hakim ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'LINK. SAMBIRAGGON ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089502502914', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(918, NULL, '2016.11.009', 'Dicky Maulana ', 'P', NULL, 'BanjarNegara', '2016-11-14', 1, NULL, NULL, 'LINK. PENGAIRAN BARU RT/RW 06/08 KEC. PURWAKARTA KEL. KOTA BUMI ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089673125009', 'Maulanadicky41@yahoo.co.id', NULL, NULL, NULL, 'Legino Sumarto', NULL, '0', 0, 0, 0, 'Patomah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(919, NULL, '2016.11.010', 'Andi Lesmana ', 'P', NULL, 'SERANG', '1987-08-29', 1, NULL, NULL, 'LINK. PENYAIRAN BARU RT/RW 05/08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0878 09486629/081212', 'BOIMLESMANA420@GMAIL.COM', NULL, NULL, NULL, 'ARSIM', NULL, '0', 0, 0, 0, 'sulastri', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(920, NULL, '2016.11.011', 'Daud', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'KOMP. PCI block D77 No 16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089614961784', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(921, NULL, '2016.11.012', 'Nungky Nursepti ', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, 'LINK LEBAK GEDE RT/RW 02/03 KEL. LEBAK GEDE ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813898267', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(922, NULL, '2016.11.013', 'Yetty Kurniawati ', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, 'JL. HELICONIA 3 NO 1 GRAND CILEGON RESIDENCE ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08989881208', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(923, NULL, '2016.11.014', 'Mela Ardika', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, 'KP CANDI DS PULOMERAK KEC PULO AMPEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '082311377771', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(924, NULL, '2016.11.015', 'Andri Firgianto ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'KP. SUMUR AMPAR RT/RW 03/02 DS. BANYUWANGI KEC. PULOAMPEL BOJONEGARA ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771847449', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(925, NULL, '2016.11.016', 'Afifullah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'KP. SAMBILANDAK RT/RW 009/002 DS BATUKUDA KEC MANCAK ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871942190', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(926, NULL, '2016.11.017', 'Riki Maszulki Pradana ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'KP. GUNUNG SANTRI RT/RW 2/6 DS/KEC BOJONEGARA SERANG ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945561997', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(927, NULL, '2016.11.018', 'Didi Suryadi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'JL FATAHILLAH LINK CIRUBUH CIWANDAN CILEGON ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087876004500', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(928, NULL, '2016.11.019', 'Habibullah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'JL. JERUK TIPIS KP. LEBAK WALUH RT/RW 02/03 KEL. BULAKAN KEC. CIBEBER ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089623990025', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(929, NULL, '2016.11.020', 'Mahmudi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(930, NULL, '2016.11.021', 'Muhammad Erryawan Aringga ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'LINK KESERANGAN LAMA NO 25 RT/RW 03/04 KEL. RAWA ARUM KEC GROGOL ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081284079015', 'ae.ryloo546@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(931, NULL, '2016.11.022', 'Muhamad Tedi Fauzu Redo ', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'JL. H AGUS SALIM RT/RW 016/002 LUWUNG SAWO KEBON SARI CITANGKIL CILEGON ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08999656994', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(932, NULL, '2016.11.023', 'Via Desiana', 'W', NULL, 'SERANG', '1996-12-03', 1, NULL, NULL, 'KP. CIPARAY RT 001/ RW 002, DESA SINDANGLAYA, KECAMATAN CINANGKA, KABUPATEN SERANG, PROVINSI BANTEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085939033683', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(933, NULL, '2016.11.024', 'Rangga Whisnu Qusyairi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, 'Perum. GCD Blok. A27 No. 07 RT. 02 RW. 05 Kel. Kalitimbang Kec. Cibeber', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(934, NULL, '2016.11.025', 'Nurfatah Saka Mulya', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(935, NULL, '2016.11.026', 'M. Yusuf Ikhsan', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(936, NULL, '2016.11.027', 'Gita Tri Cahyanti', 'W', NULL, 'Bogor', '1997-10-20', 1, NULL, NULL, 'Griya Alam Sentosa P.1A/9 RT. 002 RW. 010 Kel. Pasirangin Kec. Cileungsi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085210552591', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(937, NULL, '2016.11.028', 'Evi Lutfianah', 'W', NULL, 'Cilegon', '1992-04-04', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(938, NULL, '2016.11.029', 'Ade Widodo', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(939, NULL, '2016.11.030', 'Dwi Gatot Laksono', 'P', NULL, 'SERANG', '1998-04-23', 1, NULL, NULL, 'Perum. Taman Raya Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081281383789', 'dwigatot23@yahoo.com', NULL, NULL, NULL, 'Abdul Syukur Syugiarto', NULL, '0', 0, 0, 0, 'Eti Eriyani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(940, NULL, '2016.11.031', 'Andhika Hadi Rusfandi', 'P', NULL, 'Serang', '1999-02-17', 1, NULL, NULL, 'Anyar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809583618', 'andhikahadi17@gmail.com', NULL, NULL, NULL, 'Agus Rustandi', NULL, '0', 0, 0, 0, 'Adawiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(941, NULL, '2016.11.032', 'Aldy Kurniatama', 'P', NULL, 'Serang', '1997-11-13', 1, NULL, NULL, 'KP. Pejaten RT/RW 004/001 Desa.Pejaten Kec.Kramatwatu Kab.Serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813153953', 'aldykurniatama13@gmail.com', NULL, NULL, NULL, 'DRIYANTO', NULL, '0', 0, 0, 0, 'AMROH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(942, NULL, '2016.11.033', 'Renaldo Manurung', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(943, NULL, '2016.11.034', 'Ahmad Ulfi', 'P', NULL, 'SERANG', '1998-06-04', 1, NULL, NULL, 'Kp. Pejaten, Kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0896-6944-9091', 'Userangid@gmail.com', NULL, NULL, NULL, 'Saunul Hafid', NULL, '0', 0, 0, 0, 'Sutihat', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(944, NULL, '2016.11.035', 'Djody Prasetyo Surya Putra', 'P', NULL, 'MADIUN', '1997-03-27', 1, NULL, NULL, 'LINK.SUKAJAYA RT.002 RW.006 KEL.KEBONSARI KEC.CITANGKIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089646936701', 'djody.cilegon@gmail.com', NULL, NULL, NULL, 'SUPARJO', NULL, '0', 0, 0, 0, 'MUSRIFAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(945, NULL, '2016.11.036', 'Hanifah', 'W', NULL, 'Cilegon', '1998-03-10', 1, NULL, NULL, 'Anyar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087774198152', '10hanifah21@gmail.com', NULL, NULL, NULL, 'Hajali', NULL, '0', 0, 0, 0, 'Maskanah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(946, NULL, '2016.11.037', 'Adam Priyoga', 'P', NULL, 'Serang', '1997-10-11', 1, NULL, NULL, 'Link. Sukajaya, RT/RW 003/006, Kel. Kebonsari, Kec. Citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0895-1458-1736', 'Adampriyoga@gmail.com', NULL, NULL, NULL, 'Suprio Atmojo', NULL, '0', 0, 0, 0, 'Rahayu Puspitasari', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(947, NULL, '2016.11.038', 'Dias Pratiwi Ningrum', 'W', NULL, 'Serang', '1997-12-17', 1, NULL, NULL, 'LINK TEGAL WANGI rt/rw 003/002 kel/kec RAWA ARUM/GEROGOL ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0895320076129', 'diasprtw@gmail.com', NULL, NULL, NULL, 'ATOK PRASTOWO', NULL, '0', 0, 0, 0, 'SULUSIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(948, NULL, '2016.11.039', 'Mustabsiroh', 'W', NULL, 'Serang', '1998-01-15', 1, NULL, NULL, 'Kp. Sumur Wuluh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812759839', 'Mustabsiroh98@gmail.com', NULL, NULL, NULL, 'Mustari', NULL, '0', 0, 0, 0, 'Mastiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(949, NULL, '2016.11.040', 'Siti Rahmawati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(950, NULL, '2016.11.041', 'Susilowati', 'W', NULL, 'CILEGON', '1996-08-07', 1, NULL, NULL, 'LINK.KEDUNG BAYA RT/RW 001/004 KELURAHAN KALITIMBANG KECAMATAN CIBEBER KOTA CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085959452214', 'susilowati7897@gmail.com', NULL, NULL, NULL, 'SONTO', NULL, '0', 0, 0, 0, 'DARWATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(951, NULL, '2016.11.042', 'Fani Illahi', 'P', NULL, 'serang', '1997-04-09', 1, NULL, NULL, 'Kp. Ragas Awuran RT/RW : 001/001 Kel/Desa : Margasari Kec : PuloAmpel Kab : Serang Prov : Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813959080', 'faniillahi68@gmail.com', NULL, NULL, NULL, 'Najiulloh', NULL, '0', 0, 0, 0, 'Suryana', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(952, NULL, '2016.11.043', 'Wawan Bahruni', 'P', NULL, 'SERANG', '1998-02-28', 1, NULL, NULL, 'kp.ragas awuran', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812759849', 'wawanbahruni57@gmail.com', NULL, NULL, NULL, 'Sayuti', NULL, '0', 0, 0, 0, 'Bahriya', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(953, NULL, '2016.11.044', 'Muhammad Choerul Soleh', 'P', NULL, 'serang', '1995-08-16', 1, NULL, NULL, 'komp.bpi blok. fb no.17 rt 01 rw 06 desa panggung rawi kecamatan jombang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085946954277', 'chaerulsoleh7@gmail.com', NULL, NULL, NULL, 'Decky Kurniawan', NULL, '0', 0, 0, 0, 'Nia Kurniasih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(954, NULL, '2016.11.045', 'Hairussoleh', 'P', NULL, 'serang', '1997-07-15', 1, NULL, NULL, 'Link.kubang lampit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083890570796', 'khairuss20@gmail.com', NULL, NULL, NULL, 'Alm.Hambali arsudin', NULL, '0', 0, 0, 0, 'Nadaroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(955, NULL, '2016.11.046', 'Ferry Setiawan', 'P', NULL, 'dipasena', '1998-05-11', 1, NULL, NULL, 'Kp.Bengras,RT/RW:004/003,Ds.sindang mandi,Kec.Anyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085880125608', 'ferrys557@gmail.com', NULL, NULL, NULL, 'Karwia', NULL, '0', 0, 0, 0, 'Rofiatu Adiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(956, NULL, '2016.11.047', 'Iman Rustiaman', 'P', NULL, 'SERANG', '1997-10-29', 1, NULL, NULL, 'Link. Pekalongan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0838-1373-9105', 'imanrustiaman@gmail.com', NULL, NULL, NULL, 'Rohyadi', NULL, '0', 0, 0, 0, 'Sri Agustiani', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(957, NULL, '2016.11.048', 'Haqqy Ahmad Albantany', 'P', NULL, 'Serang', '1992-11-24', 1, NULL, NULL, 'Komp. Griya Serdang Indah Blok B2 No. 7 Rt 003/005 Kel. Margatani Kec. Kramatwatu Kab. Serang Prov. Banten 42161', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '+6287808960870', 'ryuahmad@gmail.com', NULL, NULL, NULL, 'Moch. Arief', NULL, '0', 0, 0, 0, 'Lilies Fatimah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(958, NULL, '2016.11.049', 'Ibun Aris Maulana', 'P', NULL, 'SERANG', '1997-10-23', 1, NULL, NULL, 'Kp.Pejaten, Kramatwatu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089695525069', 'ariskasep03@gmail.com', NULL, NULL, NULL, 'Halimi', NULL, '0', 0, 0, 0, 'Nayroh', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(959, NULL, '2016.11.050', 'Samsul Hidayat', 'P', NULL, 'SERANG', '1995-12-26', 1, NULL, NULL, 'Kp.kubar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089631260348', 'hdiayatsamsul828@yahoo.co.id', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(960, NULL, '2016.11.051', 'Resti Ayu Meilani', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(961, NULL, '2016.11.052', 'Ahmad Maliki', 'P', NULL, 'Serang', '1993-08-16', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(962, NULL, '2016.11.053', 'Resi Juliyanti', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, 'Lingk. Warung Juwet Kel. Samangraya Kec. Citangkil, Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085946923337', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(963, NULL, '2016.11.054', 'Aat Fatmatullah', 'P', NULL, 'Serang-Banten', '1996-12-23', 1, NULL, NULL, 'Kp. Pabuaran RT 003/005 Ds. Kosambironyok Anyer-Serang Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771544964', 'aatinofficial@gmail.com', NULL, NULL, NULL, 'Achmad Suryadi', NULL, '0', 0, 0, 0, 'Yati Astutu', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(964, NULL, '2016.11.055', 'Nasrudin', 'P', NULL, 'Serang', '2017-11-10', 1, NULL, NULL, 'Link. Sak Sak Jambu ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '081911167655', '081911167655', 'nasrudin4870@gmail.com', NULL, NULL, NULL, 'Tarmidi', NULL, '0', 0, 0, 0, 'Sarnah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(965, NULL, '2016.11.056', 'Bagus Sudrajat', 'P', NULL, 'Serang', '1998-06-02', 1, NULL, NULL, 'Kp Keracak Rt/Rw 019/004 Des.Batukuda Kec.Mancak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085888507294', 'bagussudrajat52@gmail.com', NULL, NULL, NULL, 'Ilyas', NULL, '0', 0, 0, 0, 'Mas Siti', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(966, NULL, '2016.11.057', 'Fitrah Nikmatullah', 'P', NULL, 'SERANG', '1998-02-06', 1, NULL, NULL, 'kp.lebak kelapa rt 005/001 desa batukuda kec. mancak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812982980', 'fitrahnikmatullah06@gmail.com', NULL, NULL, NULL, 'JALAWI', NULL, '0', 0, 0, 0, 'Nur KHOLILLAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(967, NULL, '2016.11.058', 'Amaludin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(968, NULL, '2016.11.059', 'Anisalimatul Abadiyah', 'W', NULL, 'serang', '1998-06-25', 1, NULL, NULL, 'Kp. Cinanggung RT/RW 001/002 Ds. Cikoneng Kec. Anyar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087809192013', 'anisalimatulabadiyah5@gmail.com', NULL, NULL, NULL, 'ADIL', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(969, NULL, '2016.11.060', 'Indriyanto Dwi Saputro', 'P', NULL, 'Purworejo', '1998-06-12', 1, NULL, NULL, 'Komp.Bukit Kramatwatu Indah Blok E No.04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083813644562', 'yayanmanchesterunited@gmail.com', NULL, NULL, NULL, 'Priyadi', NULL, '0', 0, 0, 0, 'Guntari', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(970, NULL, '2016.11.061', 'Fachril Eka Restu Fauzi', 'P', NULL, 'Serang', '1997-06-26', 1, NULL, NULL, 'LINK.BARU I RT/RW:004/004 Kel:LEBAKGEDE Kec:PULOMERAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087808725369', 'taruna.fachril@yahoo.com', NULL, NULL, NULL, 'Ahmad Fauzi', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(971, NULL, '2016.11.062', 'Andreas Kurniawan', 'P', NULL, 'Serang', '1998-02-03', 1, NULL, NULL, 'LINK. METRO VILLA BLOK D6 NO.16 RT/RW 003/006 GEDONG DALEM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089676394536', 'amrtaowr@gmail.com', NULL, NULL, NULL, 'antonius luky sutarto', NULL, '0', 0, 0, 0, 'dede suaedah ', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(972, NULL, '2016.11.063', 'Agus Fijaitullah', 'P', NULL, 'SERANG', '1996-08-02', 1, NULL, NULL, 'Kp. Pasar RT/RW 005/002, Ds. Labuan, Kec. Mancak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771717508', 'fijay12345@gmail.com', NULL, NULL, NULL, 'FUADI', NULL, '0', 0, 0, 0, 'ASMUNAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(973, NULL, '2016.11.064', 'Nurseli', 'W', NULL, 'SERANG', '1998-09-19', 1, NULL, NULL, 'KP.BANJAR, RT/RW 011/009 DESA KARANG KEPUH KEC BOJONEGARA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812300035', 'nurseli779@gmail.com', NULL, NULL, NULL, 'JAMANURI', NULL, '0', 0, 0, 0, 'WATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(974, NULL, '2016.11.065', 'Argina Aryanto', 'W', NULL, 'Cilegon', '1996-11-30', 1, NULL, NULL, 'Jl.Ir sutami Link.Cimerak Rt013/004 kec.Citangkil Cilegon Banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083875537310', 'Arginaaryanto8@gmail.com', NULL, NULL, NULL, 'Suminto', NULL, '0', 0, 0, 0, 'Aryati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(975, NULL, '2016.11.066', 'Fasanatul Jannah', 'W', NULL, 'SERANG', '1998-04-16', 1, NULL, NULL, 'KP.CIKUBANG 5, RT/RW 011/005, DESA/KEL.ARGAWANA, KECAMATAN.PULO AMPEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089622631142', 'fasanatuljannah16@gmail.com', NULL, NULL, NULL, 'FAHORI', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(976, NULL, '2016.11.067', 'Ubaidillah', 'P', NULL, 'serang', '1995-06-12', 1, NULL, NULL, 'link.palasem rt.01 rw.o7 cilegon banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '085966517028', '087871063024', 'ubai91233@gmail.com', NULL, NULL, NULL, 'asnawi', NULL, '0', 0, 0, 0, 'junariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(977, NULL, '2016.11.068', 'Ani Fitriyani', 'W', NULL, 'Serang-Banten', '1996-01-01', 1, NULL, NULL, 'Link Sak-Sak Jambu RT 003/002 Ds. Randakari Ciwandan-Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087871571357', '', NULL, NULL, NULL, 'H.Ali akhmad', NULL, '0', 0, 0, 0, 'Hj.Sarfah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(978, NULL, '2016.11.069', 'Anwar Sadad', 'P', NULL, 'serang', '1997-09-29', 1, NULL, NULL, 'LINK.SAK SAK JAMBU 003 / 002 RANDAKARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0895329004441', 'sadadanwar@gmail.com', NULL, NULL, NULL, 'jamin', NULL, '0', 0, 0, 0, 'muniatul', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(979, NULL, '2016.11.070', 'Novi Jayanti', 'W', NULL, 'SERANG', '1998-11-02', 1, NULL, NULL, 'Kp. Ragas Buntalan Rt/Rw 013/007 Desa Argawana Kecamatan Pulo Ampel', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906221054', 'Novijayantinb02@gmail.com', NULL, NULL, NULL, 'SOFYAN', NULL, '0', 0, 0, 0, 'BADRIAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(980, NULL, '2016.11.071', 'Hanis Ansor', 'P', NULL, 'ciilegon', '1996-07-31', 1, NULL, NULL, 'Jl.Perumnas Link.Palas Rt.11 Rw.002 Kec.Cilegon Kel.Bendungan ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08593900193', 'ansor.hanis@yahoo.com', NULL, NULL, NULL, 'H.Syarifulloh', NULL, '0', 0, 0, 0, 'Haddriyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(981, NULL, '2016.11.072', 'Natanael Berutu', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(982, NULL, '2016.11.073', 'Ahmad Fauki', 'P', NULL, 'cilegon', '1999-10-13', 1, NULL, NULL, 'jl.fatahillah link.sambilawang RT/RW 002/003 kel.randakari kec.ciwandan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '083312971926', 'faukiahmad13@gmail.com', NULL, NULL, NULL, 'MAD EFENDI', NULL, '0', 0, 0, 0, 'HAMDIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(983, NULL, '2016.11.075', 'Dikrian Maulana', 'P', NULL, 'Serang', '1996-11-08', 1, NULL, NULL, 'Jl.buyut Arman no.96 RT/RW 01/01 citangkil kota Cilegon ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Dikrianmaulana19@gmail.com', NULL, NULL, NULL, 'Rahmatullah', NULL, '0', 0, 0, 0, 'Murtafiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(984, NULL, '2016.11.076', 'Ina Mulyasanti', 'W', NULL, 'CILEGON', '1998-05-02', 1, NULL, NULL, 'LINK.CURUG KATIMAHA RT/RW 001/001 BAGENDUNG-CILEGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089698096025', 'inamulyasantis@gmail.com', NULL, NULL, NULL, 'Imron', NULL, '0', 0, 0, 0, 'Mukaromah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(985, NULL, '2016.11.077', 'Yusuf Arrauf Hikmahwan', 'P', NULL, 'CILEGON', '1997-11-10', 1, NULL, NULL, 'LINK. RAMANUJU TEGAL RT.01 RW.11 NO.08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '+6289643201946', 'yusuf_arrauf@programmer.net', NULL, NULL, NULL, 'SUGENG CHANDRA SUSILO', NULL, '0', 0, 0, 0, 'YATMINI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(986, NULL, '2016.11.078', 'Bayu Putra Pamungkas', 'P', NULL, 'palembang', '1998-10-02', 1, NULL, NULL, 'perum BCK blok c 18 n0 25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0895347650968', 'pamungkasbayuputra2@gmail.com', NULL, NULL, NULL, 'Ngadiyono', NULL, '0', 0, 0, 0, 'Laksmi Sapta Dewi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(987, NULL, '2016.11.079', 'Virmansyah', 'P', NULL, 'serang', '1997-05-17', 1, NULL, NULL, 'link.kubang bale', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254378354', '085946888895', 'vsyah97@yahoo.com', NULL, NULL, NULL, 'sunardi', NULL, '0', 0, 0, 0, 'imuyati', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(988, NULL, '2016.11.080', 'Muhammad Andri Purnama', 'P', NULL, 'serang ', '1998-03-10', 1, NULL, NULL, 'kp.cikebel, desa banyuwangi, pulo ampel , serang - banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0895342304044', 'andripurnama10@gmail.com', NULL, NULL, NULL, 'Rifai Khalawi', NULL, '0', 0, 0, 0, 'junariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(989, NULL, '2016.11.081', 'Nurul Habibi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(990, NULL, '2016.11.082', 'Abdul Ghofur', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(991, NULL, '2016.11.083', 'Lady Yuhar Soekarno Putri', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(992, NULL, '2016.11.084', 'Anggi Listia Ningsih', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(993, NULL, '2016.11.085', 'Ari Guntara', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(994, NULL, '2016.11.086', 'Amin', 'P', NULL, 'cibeber', '1994-04-04', 1, NULL, NULL, 'link. karotek rt/rw 003/002 kel kalitimbang kec. cibeber cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085211767957', 'amin.alfaqot4494@gmail.com', NULL, NULL, NULL, 'H.samsudin', NULL, '0', 0, 0, 0, '(alm) hj. hamiyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, '');
INSERT INTO `mahasiswa` (`id`, `nik`, `nim`, `nm_pd`, `jk`, `nisn`, `tmpt_lahir`, `tgl_lahir`, `id_agama`, `id_kk`, `id_sp`, `jln`, `rt`, `rw`, `nm_dsn`, `ds_kel`, `id_wil`, `kode_pos`, `id_jns_tinggal`, `id_alat_transport`, `telepon_rumah`, `telepon_seluler`, `email`, `a_terima_kps`, `no_kps`, `stat_pd`, `nm_ayah`, `tgl_lahir_ayah`, `id_jenjang_pendidikan_ayah`, `id_pekerjaan_ayah`, `id_penghasilan_ayah`, `id_kebutuhan_khusus_ayah`, `nm_ibu_kandung`, `tgl_lahir_ibu`, `id_jenjang_pendidikan_ibu`, `id_pekerjaan_ibu`, `id_penghasilan_ibu`, `id_kebutuhan_khusus_ibu`, `nm_wali`, `tgl_lahir_wali`, `id_jenjang_pendidikan_wali`, `id_pekerjaan_wali`, `id_penghasilan_wali`, `kewarganegaraan`, `id_pd`) VALUES
(995, NULL, '2016.11.087', 'Raden Okta', 'P', NULL, 'Cilegon', '1997-10-09', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'radenokta13@gmail.com', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(996, NULL, '2016.11.088', 'Hidir Ardinal', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(997, NULL, '2016.11.089', 'Saefuri', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(998, NULL, '2016.11.090', 'Siti Maemanah', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(999, NULL, '2016.11.091', 'Zainus Shiddiqin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1000, NULL, '2016.11.092', 'Faturrohman', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1001, NULL, '2016.11.093', 'Asep Awaludin', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1002, NULL, '2016.11.094', 'Pandi Herwandi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1003, NULL, '2016.11.095', 'Rizki Rahayu Pratama', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1004, NULL, '2016.11.096', 'Awang Anggareksa', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1005, NULL, '2016.11.74', 'Nurhayati', 'W', NULL, 'Kudus', '1997-02-17', 1, NULL, NULL, 'Link. Gerem Dermaga Malang RT 001/RW 011 Kel. Gerem Kec. Grogol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '08987999309', 'nurhayati1781@yahoo.com', NULL, NULL, NULL, 'Haryono', NULL, '0', 0, 0, 0, 'Sri Yatun', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1006, NULL, '2016.11074', 'Nurhayati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1007, NULL, '2016.12.001', 'Firda Safayanti', 'W', NULL, 'cilegon', '1999-02-12', 1, NULL, NULL, 'Jl. Sunan Bonang Link. Cigading RT. 01/02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081294286104', 'Firdasafa12@gmail.com', NULL, NULL, NULL, 'Abdul Hadi', NULL, '0', 0, 0, 0, 'Astariah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1008, NULL, '2016.12.002', 'Fadillah Hardiman', 'P', NULL, 'Tangerang', '1996-02-12', 1, NULL, NULL, 'palem hills cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '02547915472', '081288884177', 'fadillahhardiman65@gmail.com', NULL, NULL, NULL, 'JERU B.TUMBUAN', NULL, '0', 0, 0, 0, 'R.PARBUDIARTI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1009, NULL, '2016.12.003', 'Kris Monika Dian Saputra', 'W', NULL, 'serang', '1998-05-07', 1, NULL, NULL, 'kp.Lizazar jublin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085213028063', '', NULL, NULL, NULL, 'SAHRUDI', NULL, '0', 0, 0, 0, 'MARDIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1010, NULL, '2016.12.004', 'Yuli Triana', 'W', NULL, 'LAMPUNG', '1998-07-23', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085788234897', 'yulitriana23@gmail.com', NULL, NULL, NULL, 'SENEN', NULL, '0', 0, 0, 0, 'HASANAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1011, NULL, '2016.12.005', 'Muhammad Faris Dhiya\'uddin ', 'P', NULL, 'Serang', '1998-05-25', 1, NULL, NULL, 'Jl. Melati 07 blok G-1 no.06 RT01/05 BBS2 Ciwedus cilegon-banten', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '', 'rezkanoniyama98@gmail.com', NULL, NULL, NULL, 'Kuswanto', NULL, '0', 0, 0, 0, 'Rusipah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1012, NULL, '2016.12.006', 'Galih Fatwa Yudhistira', 'W', NULL, 'Serang', '1997-09-07', 1, NULL, NULL, 'Kp.kopibera Rt04/Rw03 Desa Cinangka Kec. Cinangka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '083812800817', 'galihfy@rocketmail.com', NULL, NULL, NULL, 'Ahmad Firdaus Hendayana', NULL, '0', 0, 0, 0, 'Mursiah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1013, NULL, '2016.12.007', 'Ayu Destiyani', 'W', NULL, 'PULOMERAK', '1997-12-23', 1, NULL, NULL, 'LINK.JOMBANG KALI GG.MELATI III', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081297104221', 'Ayudestiyani23@gmail.com', NULL, NULL, NULL, 'HIKMATULLAH', NULL, '0', 0, 0, 0, 'A.HERAWATI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1014, NULL, '2016.12.008', 'Sigit Abadi', 'P', NULL, 'Cilegon', '1997-12-15', 1, NULL, NULL, 'jl.fatahillah link.gelereng rt/rw 006/002 kel randakari kec ciwandan kota cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089629604479', '', NULL, NULL, NULL, 'DARMANTO', NULL, '0', 0, 0, 0, 'MASLIHAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1015, NULL, '2016.12.009', 'Elinda Citra Pratiwi', 'W', NULL, 'SERANG', '1997-08-11', 1, NULL, NULL, 'LINK.GEREM KAWISTA ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085966761246', 'citraelinda@gmail.com', NULL, NULL, NULL, 'AGUS SURYANTO', NULL, '0', 0, 0, 0, 'SUKMARIAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1016, NULL, '2016.12.010', 'Siti Wihda Amaliae', 'W', NULL, 'CILEGON', '1998-08-22', 1, NULL, NULL, 'JL.FATAHILLAH LINK.GELERENG RT/RW 006/002 KEL.RANDAKARI KEC.CIWANDAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '089620838894', 'sitiwihda.amalia22@gmail.com', NULL, NULL, NULL, 'Arifin Bj', NULL, '0', 0, 0, 0, 'Julehah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1017, NULL, '2016.12.011', 'Feti Fatimah', 'W', NULL, 'SERANG', '1997-10-18', 1, NULL, NULL, 'KOMP GSI BLOK B3 NO2 Rt/Rw 03/05 Desa. Margatani Kec. Kramatwatu Kab. Serang Provinsi. Banten ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '087771207274', 'fetifatimah20@gmail.com', NULL, NULL, NULL, 'ASEP ROBANA', NULL, '0', 0, 0, 0, 'INA SUPIANA', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1018, NULL, '2016.12.012', 'Cyntia Dianni Feby', 'W', NULL, 'Serang', '1999-02-19', 1, NULL, NULL, 'Link. Ramanuju RT/RW: 001/004 Kel/Kec: Citangkil', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0254394103', '083873504819', 'cyntiadiannifeby19@gmail.com', NULL, NULL, NULL, 'Mulyadi', NULL, '0', 0, 0, 0, 'Selvita Dewi', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1019, NULL, '2016.12.013', 'Rani Saputri', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1020, NULL, '2016.12.014', 'Reza Purnama', 'P', NULL, 'Serang', '1998-09-08', 1, NULL, NULL, 'Kp.Kosambi 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+6283875537881', 'rezapurnama8998@gmail.com', NULL, NULL, NULL, 'Rasim', NULL, '0', 0, 0, 0, 'Rokayah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1021, NULL, '2016.12.015', 'SUHERI', 'P', NULL, 'SERANG', '1998-06-15', 1, NULL, NULL, 'KP. KEDUNG INGAS RT/RW 12/05 KEL. PAKUNCEN KEC. BOJONEGARA KAB. SERANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '-', '083870835225', 'KANGSUHER@GMAIL.COM', NULL, NULL, NULL, 'MAD SAFEI', NULL, '0', 0, 0, 0, 'SAMIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1022, NULL, '2016.12.016', 'Afriani Fitriyah', 'W', NULL, 'SERANG', '1998-01-20', 1, NULL, NULL, 'Kp.Capang Calung Rt.002/001 Ds.Mancak Kec.Mancak Kab. Serang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08982773477', 'afriani.fitriyah@gmail.com', NULL, NULL, NULL, 'Alm.Jainul Arifin', NULL, '0', 0, 0, 0, 'Yulihat', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1023, NULL, '2016.12.017', 'Priska Nadia Resma', 'W', NULL, 'serang', '1997-10-21', 1, NULL, NULL, 'Kp.Pegadungan RT/RW: 003/007 Kel/Kec: Anyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089674887320', 'nadiaresmapriska@gmail.com', NULL, NULL, NULL, 'suhardi', NULL, '0', 0, 0, 0, 'sukesih', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1024, NULL, '2016.12.018', 'Masfufah', 'W', NULL, 'SERANG', '1996-02-27', 1, NULL, NULL, 'KP.TURUS DES.WINONG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '087774197890', '083895498225', 'Masfufahifah@gmail.com', NULL, NULL, NULL, 'CHAERUDIN', NULL, '0', 0, 0, 0, 'MAISAROH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1025, NULL, '2016.12.019', 'Imanuella Rohana Putri Simatupang', 'W', NULL, 'TANGERANG', '1997-06-29', 1, NULL, NULL, 'TAMAN KRAKATAU BLOK I4 NO 14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'ellasimatupang@gmail.com', NULL, NULL, NULL, 'GANDA TUA SIMATUPANG', NULL, '0', 0, 0, 0, 'PIOAN GANDARIA HUTAHAEAN', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1026, NULL, '2016.12.020', 'Yadiyan Efendi', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1027, NULL, '2016.12.021', 'Rizqi Nurul Fitri', 'W', NULL, 'CILEGON', '1998-01-28', 1, NULL, NULL, 'JL.SUNAN DRAJAT LINK.PINTU AIR RT/RW 01/04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089612111172', 'rizqinurulfitri1998@gmail.com', NULL, NULL, NULL, 'FATULLOH', NULL, '0', 0, 0, 0, 'SITI NASAROH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1028, NULL, '2016.12.09', 'Elinda Citra Pratiwi', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1029, NULL, '2016.21.001', 'Feri Febriansyah', 'P', NULL, 'Cilegon', '1998-04-18', 1, NULL, NULL, 'Link. Makam Maja', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+6289615386555', 'Febriansyahf14@yahoo.com', NULL, NULL, NULL, 'Sayuti', NULL, '0', 0, 0, 0, 'Jubaedah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1030, NULL, '2016.21.002', 'Rini', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1031, NULL, '2016.21.003', 'Muhammad Usup', 'P', NULL, 'jakarta', '1998-06-09', 1, NULL, NULL, 'kp.kavling abm marina', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '081906445876', 'yusufpermana52@gmail.com', NULL, NULL, NULL, 'ruby cahya permana', NULL, '0', 0, 0, 0, 'sitifatihah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1032, NULL, '2016.21.004', 'Maulana Willy Bagusti', 'P', NULL, 'serang', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1033, NULL, '2016.21.005', 'Anggi Larasati', 'W', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1034, NULL, '2016.21.006', 'Mulyanah', 'W', NULL, 'serang', '1995-12-03', 1, NULL, NULL, 'mancak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '085945922091', '', NULL, NULL, NULL, 'h.jamsir', NULL, '0', 0, 0, 0, 'hj.junariyah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1035, NULL, '2016.22.001', 'Tian Sartika', 'P', NULL, '', '0000-00-00', 1, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, '', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1036, NULL, '2016.22.002', 'Kesuma Pinilih', 'P', NULL, 'TUBAN', '1997-11-25', 1, NULL, NULL, 'RUSUNAWA MARUNDA BLK BANDENG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '083896504886', '083895197526', 'kesuma.pinilih@yahoo.com', NULL, NULL, NULL, 'JOKO MULYONO', NULL, '0', 0, 0, 0, 'LESTARI', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1037, NULL, '2016.22.003', 'Rizkiatun Nafilah', 'W', NULL, 'Serang', '2016-11-16', 1, NULL, NULL, 'Link. Kubang Wates RT/RW. 01/08 Kec. Purwakarta Kel. Kotabumi Cilegon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '08984442825', 'nafilah94@gmail.com', NULL, NULL, NULL, 'Marzuki', NULL, '0', 0, 0, 0, '', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1038, NULL, '2016.22.004', 'SUTROYATI', 'W', NULL, 'SERANG', '1998-03-17', 1, NULL, NULL, 'KP.CIKUBANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '089608975396', '', NULL, NULL, NULL, 'SAYUDIN', NULL, '0', 0, 0, 0, 'MARKIYAH', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1039, NULL, '2016.22.005', 'Helena Anggraini', 'W', NULL, 'Bandar Lampung', '1996-05-20', 1, NULL, NULL, 'Tanjung Karang', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '+6289609597431', '', NULL, NULL, NULL, 'Paryanto', NULL, '0', 0, 0, 0, 'Ursinah', NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, NULL, ''),
(1050, '', '20171187', 'Satam', 'P', '', 'Cilegon', '1996-10-10', 2, NULL, NULL, 'Jl. Raya Cilegon', '3', '5', 'Cicaheum', 'Cisoak', NULL, '42511', NULL, NULL, NULL, '08776521456', NULL, NULL, NULL, 'C', 'jumadil ula', '1950-10-10', '2', 2, 3, 0, 'juminten', '1960-09-09', '1', 7, 3, 0, 'jumadil ula', '1950-10-10', '2', 2, 3, 'WNI', NULL),
(1051, '', '20171187', 'Satam', 'P', '', 'Cilegon', '1996-10-10', 1, NULL, NULL, 'Jl. Raya Cilegon', '3', '5', 'Cicaheum', 'Cisoak', NULL, '42511', NULL, NULL, NULL, '08776521456', NULL, NULL, NULL, 'A', NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, 0, NULL, NULL, '0', 0, 0, '', NULL),
(1052, '', '20171188', 'Salman Alfarisi', 'P', '', 'Cilegon', '1998-08-08', 1, NULL, NULL, 'Jl. Raya Cilegon', '3', '5', 'Cicaheum', 'Cisoak', NULL, '45312', NULL, NULL, NULL, '0897316731', NULL, NULL, NULL, 'A', 'Jumadi', '0000-00-00', '0', 0, 0, 0, 'Zainab', '0000-00-00', '0', 0, 0, 0, 'Jamjuri', '0000-00-00', '0', 0, 0, '', NULL),
(1054, '3604050911910005', '201711073', 'Saddam', 'P', '', 'Batam', '1990-11-09', 1, NULL, NULL, 'GSI Block C7 No 7', '4', '5', '', 'Margatani', NULL, '42161', NULL, NULL, NULL, '087808811858', NULL, NULL, NULL, 'A', 'Zarmiddin', '1960-03-08', '3', 5, 3, 0, 'Masyithah', '1970-11-07', '2', 7, 2, NULL, 'Zarmiddin', '1960-03-08', '3', 5, 3, 'WNI', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa_pt`
--

CREATE TABLE `mahasiswa_pt` (
  `nim` varchar(15) NOT NULL,
  `id_reg_pd` varchar(50) DEFAULT NULL,
  `id_sms` varchar(50) DEFAULT NULL,
  `id_pd` varchar(50) DEFAULT NULL,
  `id_sp` varchar(50) DEFAULT NULL,
  `id_jns_daftar` decimal(1,0) DEFAULT NULL,
  `tgl_masuk_sp` date DEFAULT NULL,
  `id_jns_keluar` char(1) DEFAULT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `ket` varchar(128) DEFAULT NULL,
  `skhun` char(20) DEFAULT NULL,
  `a_pernah_paud` decimal(1,0) DEFAULT NULL,
  `a_pernah_tk` decimal(1,0) DEFAULT NULL,
  `mulai_smt` varchar(5) DEFAULT NULL,
  `sks_diakui` decimal(3,0) DEFAULT NULL,
  `jalur_skripsi` decimal(1,0) DEFAULT NULL,
  `judul_skripsi` varchar(250) DEFAULT NULL,
  `bln_awal_bimbingan` date DEFAULT NULL,
  `bln_akhir_bimbingan` date DEFAULT NULL,
  `sk_yudisium` varchar(30) DEFAULT NULL,
  `tgl_sk_yudisium` date DEFAULT NULL,
  `ipk` double(53,0) DEFAULT NULL,
  `no_seri_ijazah` varchar(40) DEFAULT NULL,
  `sert_prof` varchar(40) DEFAULT NULL,
  `a_pindah_mhs_asing` decimal(1,0) DEFAULT NULL,
  `nm_pt_asal` varchar(50) DEFAULT NULL,
  `nm_prodi_asal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa_pt`
--

INSERT INTO `mahasiswa_pt` (`nim`, `id_reg_pd`, `id_sms`, `id_pd`, `id_sp`, `id_jns_daftar`, `tgl_masuk_sp`, `id_jns_keluar`, `tgl_keluar`, `ket`, `skhun`, `a_pernah_paud`, `a_pernah_tk`, `mulai_smt`, `sks_diakui`, `jalur_skripsi`, `judul_skripsi`, `bln_awal_bimbingan`, `bln_akhir_bimbingan`, `sk_yudisium`, `tgl_sk_yudisium`, `ipk`, `no_seri_ijazah`, `sert_prof`, `a_pindah_mhs_asing`, `nm_pt_asal`, `nm_prodi_asal`) VALUES
('2008.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.12.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.21.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2009.22.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.12.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.21.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2010.22.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mahasiswa_pt` (`nim`, `id_reg_pd`, `id_sms`, `id_pd`, `id_sp`, `id_jns_daftar`, `tgl_masuk_sp`, `id_jns_keluar`, `tgl_keluar`, `ket`, `skhun`, `a_pernah_paud`, `a_pernah_tk`, `mulai_smt`, `sks_diakui`, `jalur_skripsi`, `judul_skripsi`, `bln_awal_bimbingan`, `bln_akhir_bimbingan`, `sk_yudisium`, `tgl_sk_yudisium`, `ipk`, `no_seri_ijazah`, `sert_prof`, `a_pindah_mhs_asing`, `nm_pt_asal`, `nm_prodi_asal`) VALUES
('2011.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.11.115', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.12.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.21.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2011.22.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.11.105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.12.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.21.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2012.22.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013..019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mahasiswa_pt` (`nim`, `id_reg_pd`, `id_sms`, `id_pd`, `id_sp`, `id_jns_daftar`, `tgl_masuk_sp`, `id_jns_keluar`, `tgl_keluar`, `ket`, `skhun`, `a_pernah_paud`, `a_pernah_tk`, `mulai_smt`, `sks_diakui`, `jalur_skripsi`, `judul_skripsi`, `bln_awal_bimbingan`, `bln_akhir_bimbingan`, `sk_yudisium`, `tgl_sk_yudisium`, `ipk`, `no_seri_ijazah`, `sert_prof`, `a_pindah_mhs_asing`, `nm_pt_asal`, `nm_prodi_asal`) VALUES
('2013.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.11.099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.21.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2013.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.108', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.11.109', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.12.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2014.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.071', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.072', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.076', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.077', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.078', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.079', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.080', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.081', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.082', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `mahasiswa_pt` (`nim`, `id_reg_pd`, `id_sms`, `id_pd`, `id_sp`, `id_jns_daftar`, `tgl_masuk_sp`, `id_jns_keluar`, `tgl_keluar`, `ket`, `skhun`, `a_pernah_paud`, `a_pernah_tk`, `mulai_smt`, `sks_diakui`, `jalur_skripsi`, `judul_skripsi`, `bln_awal_bimbingan`, `bln_akhir_bimbingan`, `sk_yudisium`, `tgl_sk_yudisium`, `ipk`, `no_seri_ijazah`, `sert_prof`, `a_pindah_mhs_asing`, `nm_pt_asal`, `nm_prodi_asal`) VALUES
('2015.11.083', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.084', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.085', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.086', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.087', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.088', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.089', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.090', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.091', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.092', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.093', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.094', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.095', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.098', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.099', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.100', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.101', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.102', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.103', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.104', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.105', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.106', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.11.107', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.22.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.22.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2015.22.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.013', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.014', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.015', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.016', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.017', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.018', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.019', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.020', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.021', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.022', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.023', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.024', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.025', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.026', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.027', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.028', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.029', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.030', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.031', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.032', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.033', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.034', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.035', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.036', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.037', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.038', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.039', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.040', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.041', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.042', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.043', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.044', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.045', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.046', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.047', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.048', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.049', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.050', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.051', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.052', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.053', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.054', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.055', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.056', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.057', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.058', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.059', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.060', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.061', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.062', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.063', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.064', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.065', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.066', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.067', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.068', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.069', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.070', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.073', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11.075', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.11074', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.12.09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.21.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.21.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.21.003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.21.004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.22.001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('2016.22.002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Array..1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `kode_mk` char(20) NOT NULL,
  `nm_mk` varchar(100) NOT NULL,
  `jns_mk` char(1) DEFAULT NULL,
  `kel_mk` char(3) DEFAULT NULL,
  `id_jenj_didik` decimal(2,0) NOT NULL,
  `sks_mk` decimal(2,0) DEFAULT NULL,
  `sks_tatapmuka` decimal(2,0) DEFAULT NULL,
  `mk_semester` int(11) DEFAULT NULL,
  `metode_pelaksanaan_kuliah` varchar(50) DEFAULT NULL,
  `a_sap` decimal(1,0) DEFAULT NULL,
  `a_silabus` decimal(1,0) DEFAULT NULL,
  `a_bahan_ajar` decimal(1,0) DEFAULT NULL,
  `acara_prak` decimal(1,0) DEFAULT NULL,
  `a_diktat` decimal(1,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`kode_mk`, `nm_mk`, `jns_mk`, `kel_mk`, `id_jenj_didik`, `sks_mk`, `sks_tatapmuka`, `mk_semester`, `metode_pelaksanaan_kuliah`, `a_sap`, `a_silabus`, `a_bahan_ajar`, `acara_prak`, `a_diktat`) VALUES
('K09305', 'Kecakapan Antar Personal', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09315', 'Auditing', 'B', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12101', 'Aplikasi Komputer I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12102', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12103', 'Algoritma & Pemrograman', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12104', 'PBO I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12105', 'Pengantar TI', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12106', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12107', 'Akuntansi Dasar', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12108', 'Akuntansi Biaya', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12110', 'Akuntansi Menengah', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12111', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12113', 'Pengantar Ekonomi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12201', 'APS', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12203', 'SIM', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12204', 'Bahasa Inggris II', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12205', 'Adm. Basis Data', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12206', 'PBO II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12207', 'E Commerce', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12208', 'SIA', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12209', 'Pengantar Bisnis', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12210', 'Perpajakan', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12211', 'Statistik', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12212', 'Praktek Akuntansi', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12213', 'Akuntansi Lanjut', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12303', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, '', '0', '0', '0', '0', '0'),
('K12304', 'Tugas Akhir', 'S', 'MKU', '7', '6', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12305', 'Bisnis & Kewirausahaan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12306', 'Analisa Laporan Keuangan2', 'A', 'MKU', '7', '4', '16', 2, 'Teori', '1', '1', '1', '1', '1'),
('K12307', 'Kewarganegaraan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12309', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12310', 'Budgeting', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12311', 'Agama', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12313', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12315', 'MYOB', 'B', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12319', 'Auditing', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12321', 'MYOB', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12453', 'Analisa Laporan Keuangan 4', 'B', 'MKK', '7', '4', '16', 4, 'Teori', '1', '1', '1', '0', '1'),
('K12559', 'Analisa Laporan Keuangan3', 'A', 'MKU', '7', '4', '16', 2, 'Teori', '1', '1', '1', '1', '1'),
('M09101', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09102', 'Bahasa Inggris II', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09103', 'Aplikasi Komputer', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09104', 'Statistik', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09106', 'Struktur Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09107', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09108', 'Pengantar Bisnis', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09110', 'Akuntansi Dasar', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09112', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09113', 'Sistem Operasi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09201', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09203', 'Sistem Informasi Akuntansi', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09204', 'Bisnis Dan Kewirausahaan', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09205', 'Sistem Informasi Manajemen', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09206', 'Agama', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09208', 'Teknologi Jaringan II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09209', 'Teknologi Jaringan I', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09210', 'Desain Dan Pemrograman Web', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09211', 'Sistem Operasi Linux', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09212', 'Analisa dan Perancangan SI', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09303', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09304', 'Ecommerce', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M09305', 'Kecakapan Antar Personal', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09306', 'Datawarehousing dan Datamining', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09307', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09309', 'Pemrograman Berorientasi Objek II', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09311', 'Adm. Pemrog. Basis Data', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09313', 'Desain & Pemrog. Web II / Pil. I', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12101', 'Aplikasi Komputer', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12102', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12103', 'Algoritma & Pemrograman', 'A', 'MKK', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12104', 'Pemograman Berorientasi Object I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12105', 'Pengantar Teknologi Informasi', 'A', 'MKK', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12106', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12107', 'Akuntansi Dasar', 'A', 'MKK', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12108', 'Desain dan Pemograman Web I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12110', 'Struktur Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12111', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12113', 'Sistem Operasi', 'A', 'MKK', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12201', 'Analisa Perancangan SI', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12203', 'Sistem Informasi Manajemen', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12204', 'Bahasa Inggris II', 'A', 'MBB', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12205', 'Adm. Basis Data', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12206', 'Pemograman Berorientasi Object II', 'A', 'MKK', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M12207', 'E-Commerce', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12208', 'Sistem Informasi Akuntansi', 'A', 'MKK', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12209', 'Pengantar Bisnis', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12210', 'Desain dan Pemograman Web II', 'A', 'MKK', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12211', 'Statistik', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12212', 'Teknologi Jaringan I', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12213', 'Sistem Operasi Linux', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12303', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12304', 'Tugas Akhir', 'S', 'MKB', '7', '5', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12305', 'Bisnis dan Kewirausahaan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12306', 'Pemrograman Web', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12307', 'Kewarganegaraan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12308', 'ERP', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12309', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12311', 'Agama', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12313', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12317', 'Grafika Komputer', 'B', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12319', 'Datawarehouse', 'B', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12321', 'Teknologi Jaringan II', 'B', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09101', 'Bahasa Inggris I', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09102', 'Bahasa Inggris II', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09103', 'Aplikasi Komputer', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09104', 'Matematika Diskrit', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09106', 'Struktur Data', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09107', 'Kalkulus', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09108', 'Akuntansi Dasar', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09109', 'Sistem Operasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09110', 'Pengantar Manajemen', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09112', 'Konsep Sistem Informasi', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09113', 'Bahasa Indonesia', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09114', 'Sistem Berkas', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09201', 'Bahasa Inggris III', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09202', 'Sistem Operasi Linux', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09203', 'Teknologi Jaringan', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09204', 'Pemrograman Berorientasi Obyek II', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09205', 'Desain Web I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09206', 'Pengantar Bisnis', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09208', 'Basis Data', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09209', 'Sistem Informasi Akuntansi', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09210', 'Analisa dan Perancangan SI', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09211', 'Statistik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09212', 'Agama', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09301', 'Multimedia', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09302', 'Testing Implementasi Sistem', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09303', 'Riset TI', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09304', 'Pancasila', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09305', 'Desain Web II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09306', 'E Commerce', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09307', 'Adm.Pemrog. Basis Data', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09308', 'Kecakapan Antar Personal', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09309', 'Interaksi manusia & Komputer', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09310', 'Bisnis Kewirausahaan', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09311', 'Rekayasa Perangkat Lunak', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09312', 'Keamanan & Audit Sistem Informasi', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09314', 'Datawarehouse & Datamining', 'B', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09401', 'ERP', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09402', 'Tugas Akhir', 'S', 'MKU', '4', '6', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('S09403', 'Etika Profesi', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09404', 'Pemrograman Bisnis Terapan', 'B', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('S09405', 'Komputer dan Masyarakat', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09407', 'Manajemen Proyek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09409', 'Kerja Praktek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09411', 'Sistem Penunjang Keputusan', 'B', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09101', 'Bahasa Inggris I', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09102', 'Bahasa Inggris II', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09103', 'Aplikasi Komputer', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09104', 'Matematika Diskrit', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09106', 'Struktur Data', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09107', 'Kalkulus', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09108', 'Logika Matematika', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09109', 'Sistem Operasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09110', 'Organisasi & Arsitektur Komputer', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09112', 'Sistem Informasi Manajemen', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09113', 'Bahasa Indonesia', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09114', 'Sistem Berkas', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09201', 'Bahasa Inggris III', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09202', 'Sistem Operasi Linux', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09203', 'Tek. Jaringan I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09204', 'Pemrograman Berorientasi Obyek II', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09205', 'Desain Web I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09206', 'Pengantar Bisnis', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09208', 'Basis Data', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09209', 'Metode Numerik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09210', 'Analisa dan Perancangan SI', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09211', 'Statistik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09212', 'Sistem Digital', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09214', 'Agama', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09301', 'Perencanaan Bisnis', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09302', 'Grafika Komputer', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09303', 'Riset TI', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09304', 'Pancasila', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09305', 'Desain Web II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09306', 'E Commerce', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09307', 'Tek. Jaringan II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09308', 'Kecakapan Antar Personal', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09309', 'Interaksi Manusia & Komputer', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09310', 'Bisnis & Kewirausahaan', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09311', 'Rekayasa Perangkat Lunak', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09312', 'Keamanan & Audit Sistem Informasi', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09314', 'Datawarehousing dan Datamining', 'B', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09401', 'ERP', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09402', 'Tugas Akhir', 'S', 'MKU', '4', '6', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('T09403', 'Etika Profesi', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09404', 'Proyek Perangkat Lunak', 'B', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('T09405', 'Komputer dan Masyarakat', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09407', 'Manajemen Proyek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09409', 'Kerja Praktek', 'A', 'MKU', '4', '2', '16', 7, 'Teori', '0', '0', '0', '0', '0'),
('T09411', 'Sistem Penunjang Keputusan', 'B', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mata_kuliah_kurlama`
--

CREATE TABLE `mata_kuliah_kurlama` (
  `kode_mk` char(20) NOT NULL,
  `nm_mk` varchar(100) NOT NULL,
  `jns_mk` char(1) DEFAULT NULL,
  `kel_mk` char(3) DEFAULT NULL,
  `id_jenj_didik` decimal(2,0) NOT NULL,
  `sks_mk` decimal(2,0) DEFAULT NULL,
  `sks_tatapmuka` decimal(2,0) DEFAULT NULL,
  `mk_semester` int(11) DEFAULT NULL,
  `metode_pelaksanaan_kuliah` varchar(50) DEFAULT NULL,
  `a_sap` decimal(1,0) DEFAULT NULL,
  `a_silabus` decimal(1,0) DEFAULT NULL,
  `a_bahan_ajar` decimal(1,0) DEFAULT NULL,
  `acara_prak` decimal(1,0) DEFAULT NULL,
  `a_diktat` decimal(1,0) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mata_kuliah_kurlama`
--

INSERT INTO `mata_kuliah_kurlama` (`kode_mk`, `nm_mk`, `jns_mk`, `kel_mk`, `id_jenj_didik`, `sks_mk`, `sks_tatapmuka`, `mk_semester`, `metode_pelaksanaan_kuliah`, `a_sap`, `a_silabus`, `a_bahan_ajar`, `acara_prak`, `a_diktat`) VALUES
('T09101', 'Bahasa Inggris I', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09103', 'Aplikasi Komputer', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09107', 'Kalkulus', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09109', 'Sistem Operasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09113', 'Bahasa Indonesia', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('T09102', 'Bahasa Inggris II', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09104', 'Matematika Diskrit', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09106', 'Struktur Data', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09108', 'Logika Matematika', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09110', 'Organisasi & Arsitektur Komputer', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09112', 'Sistem Informasi Manajemen', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09114', 'Sistem Berkas', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('T09201', 'Bahasa Inggris III', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09203', 'Tek. Jaringan I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09205', 'Desain Web I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09209', 'Metode Numerik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09211', 'Statistik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('T09202', 'Sistem Operasi Linux', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09204', 'Pemrograman Berorientasi Obyek II', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09206', 'Pengantar Bisnis', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09208', 'Basis Data', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09210', 'Analisa dan Perancangan SI', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09212', 'Sistem Digital', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09214', 'Agama', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('T09301', 'Perencanaan Bisnis', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09303', 'Riset TI', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09305', 'Desain Web II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09307', 'Tek. Jaringan II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09309', 'Interaksi Manusia & Komputer', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09311', 'Rekayasa Perangkat Lunak', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('T09302', 'Grafika Komputer', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09304', 'Pancasila', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09306', 'E Commerce', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09308', 'Kecakapan Antar Personal', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09310', 'Bisnis & Kewirausahaan', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09312', 'Keamanan & Audit Sistem Informasi', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09401', 'ERP', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09403', 'Etika Profesi', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09405', 'Komputer dan Masyarakat', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09407', 'Manajemen Proyek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09409', 'Kerja Praktek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09402', 'Tugas Akhir', 'S', 'MKU', '4', '6', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('T09314', 'Datawarehousing dan Datamining', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09411', 'Sistem Penunjang Keputusan', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09404', 'Proyek Perangkat Lunak', 'A', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('T09316', 'Multimedia I', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09413', 'Multimedia II', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('T09318', 'Teknologi Jaringan III', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('T09415', 'Teknologi Jaringan IV', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09101', 'Bahasa Inggris I', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09103', 'Aplikasi Komputer', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09107', 'Kalkulus', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09109', 'Sistem Operasi', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '4', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09113', 'Bahasa Indonesia', 'A', 'MKU', '4', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('S09102', 'Bahasa Inggris II', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09104', 'Matematika Diskrit', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09106', 'Struktur Data', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09108', 'Akuntansi Dasar', 'A', 'MKU', '4', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09110', 'Pengantar Manajemen', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09112', 'Konsep Sistem Informasi', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09114', 'Sistem Berkas', 'A', 'MKU', '4', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('S09201', 'Bahasa Inggris III', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09203', 'Teknologi Jaringan', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09205', 'Desain Web I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '4', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09209', 'Sistem Informasi Akuntansi', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09211', 'Statistik', 'A', 'MKU', '4', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('S09202', 'Sistem Operasi Linux', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09204', 'Pemrograman Berorientasi Obyek II', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09206', 'Pengantar Bisnis', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09208', 'Basis Data', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09210', 'Analisa dan Perancangan SI', 'A', 'MKU', '4', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09212', 'Agama', 'A', 'MKU', '4', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('S09301', 'Multimedia', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09303', 'Riset TI', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09305', 'Desain Web II', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09307', 'Adm.Pemrog. Basis Data', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09309', 'Interaksi manusia & Komputer', 'A', 'MKU', '4', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09311', 'Rekayasa Perangkat Lunak', 'A', 'MKU', '4', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('S09302', 'Testing Implementasi Sistem', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09304', 'Pancasila', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09306', 'E Commerce', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09308', 'Kecakapan Antar Personal', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09310', 'Bisnis Kewirausahaan', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09312', 'Keamanan & Audit Sistem Informasi', 'A', 'MKU', '4', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09401', 'ERP', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09403', 'Etika Profesi', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09405', 'Komputer dan Masyarakat', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09407', 'Manajemen Proyek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09409', 'Kerja Praktek', 'A', 'MKU', '4', '2', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09402', 'Tugas Akhir', 'S', 'MKU', '4', '6', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('S09314', 'Datawarehouse & Datamining', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09411', 'Sistem Penunjang Keputusan', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09404', 'Pemrograman Bisnis Terapan', 'A', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('S09316', 'Sistem Informasi Kependudukan', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09413', 'Sistem Informasi Anggaran', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09406', 'Sistem Informasi Geografis', 'A', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('S09318', 'Akuntansi Menengah', 'A', 'MKU', '4', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('S09415', 'Akuntansi Biaya', 'A', 'MKU', '4', '4', '0', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('S09408', 'Perpajakan', 'A', 'MKU', '4', '4', '0', 8, NULL, NULL, NULL, NULL, NULL, NULL),
('M09101', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09103', 'Aplikasi Komputer', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09107', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09111', 'Algoritma dan Pemrograman', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09113', 'Sistem Operasi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M09102', 'Bahasa Inggris II', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09104', 'Statistik', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09106', 'Struktur Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09108', 'Pengantar Bisnis', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09110', 'Akuntansi Dasar', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09112', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09201', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09203', 'Sistem Informasi Akuntansi', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09205', 'Sistem Informasi Manajemen', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09207', 'Pemrograman Berorientasi Obyek I', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09209', 'Teknologi Jaringan I', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09211', 'Sistem Operasi Linux', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M09202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09204', 'Entrepreneurship', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09206', 'Agama', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09208', 'Teknologi Jaringan II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09210', 'Desain Dan Pemrograman Web', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09212', 'Analisa dan Perancangan SI', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09214', 'Aplikasi Multimedia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M09301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09303', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09305', 'Kecakapan Antar Personal', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09307', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09309', 'Pemrograman Berorientasi Objek II', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09302', 'Tugas Akhir', 'S', 'MKU', '7', '6', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M09304', 'Ecommerce', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M09311', 'Adm. Pemrog. Basis Data', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M09313', 'Desain & Pemrog. Web II / Pil. I', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09101', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09103', 'Aplikasi Komputer I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09105', 'Pengantar Teknologi Informasi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09107', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09111', 'Akuntansi Dasar', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09113', 'Pengantar Ekonomi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K09102', 'Bahasa Inggris II', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09104', 'Statistik', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09106', 'Aplikasi Komputer II', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09108', 'Pengantar Bisnis', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09110', 'Akuntansi Menengah', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09112', 'Perpajakan', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K09201', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K09203', 'Akuntansi Lanjut', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K09205', 'Akuntansi Biaya', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K09207', 'Aplikasi Komp. Akuntansi I', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K09209', 'Sistem Informasi Akuntansi', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K09202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09204', 'Entrepreneurship', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09206', 'Agama', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09208', 'Aplikasi Komp. Akuntansi II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09210', 'Manajemen Keuangan', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09212', 'Praktek Akuntansi', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K09301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09303', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09305', 'Kecakapan Antar Personal', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09307', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09309', 'Teknologi Web', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09311', 'Budgeting', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09302', 'Tugas Akhir', 'S', 'MKU', '7', '6', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K09304', 'E Commerce', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K09313', 'Akuntansi Syariah', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09315', 'Auditing', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09317', 'Perpajakan Lanjut', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K09306', 'Analisa Laporan Keuangan', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K09308', 'Perbankan', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K09310', 'Akuntansi Pemerintahan', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12101', 'Aplikasi Komputer I', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12103', 'Algoritma & Pemrograman', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12105', 'Pengantar TI', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12107', 'Akuntansi Dasar', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12111', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12113', 'Pengantar Ekonomi', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('K12102', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12104', 'PBO I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12106', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12108', 'Akuntansi Biaya', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12110', 'Akuntansi Menengah', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('K12201', 'APS', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12203', 'SIM', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12205', 'Adm. Basis Data', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12207', 'E Commerce', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12209', 'Pengantar Bisnis', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12211', 'Statistik', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12213', 'Akuntansi Lanjut', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('K12202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12204', 'Bahasa Inggris II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12206', 'PBO II', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12208', 'SIA', 'A', 'MKU', '7', '2', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12210', 'Perpajakan', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12212', 'Praktek Akuntansi', 'A', 'MKU', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('K12301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12303', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12305', 'Bisnis & Kewirausahaan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12307', 'Kewarganegaraan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12309', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12311', 'Agama', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12313', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12315', 'Mk. Pilihan I / MYOB', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12302', 'Mk. Pilihan II', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12304', 'Tugas Akhir', 'S', 'MKU', '7', '6', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12317', 'Grafika Komputer', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12319', 'Datawarehouse', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12321', 'Teknologi Jaringan II', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12306', 'Pemrograman Web', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12308', 'ERP', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12310', 'Teknologi Jaringan III', 'A', 'MKU', '7', '4', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12317', 'Manajemen Keuangan', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12319', 'Auditing', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12321', 'MYOB', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('K12306', 'ALK', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12308', 'Perbankan', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('K12310', 'Budgeting', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12102', 'Basis Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12104', 'Pemograman Berorientasi Object I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12106', 'Bahasa Inggris I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12108', 'Desain dan Pemograman Web I', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12110', 'Struktur Data', 'A', 'MKU', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M09306', 'Datawarehousing dan Datamining', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12201', 'Analisa Perancangan SI', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12203', 'Sistem Informasi Manajemen', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12205', 'Adm. Basis Data', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12207', 'E-Commerce', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12209', 'Pengantar Bisnis', 'A', 'MKK', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12211', 'Statistik', 'A', 'MKU', '7', '2', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12213', 'Sistem Operasi Linux', 'A', 'MKU', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12101', 'Aplikasi Komputer', 'A', 'MKU', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12103', 'Algoritma & Pemrograman', 'A', 'MKK', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12105', 'Pengantar Teknologi Informasi', 'A', 'MKK', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12107', 'Akuntansi Dasar', 'A', 'MKK', '7', '4', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12109', 'Pancasila', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12111', 'Matematika', 'A', 'MKU', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12113', 'Sistem Operasi', 'A', 'MKK', '7', '2', '0', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('M12202', 'Bahasa Indonesia', 'A', 'MKU', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12204', 'Bahasa Inggris II', 'A', 'MBB', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12206', 'Pemograman Berorientasi Object II', 'A', 'MKK', '7', '4', '0', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('M12208', 'Sistem Informasi Akuntansi', 'A', 'MKK', '7', '2', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12210', 'Desain dan Pemograman Web II', 'A', 'MKK', '7', '4', '0', 2, NULL, NULL, NULL, NULL, NULL, NULL),
('M12212', 'Teknologi Jaringan I', 'A', 'MKK', '7', '4', '0', 3, NULL, NULL, NULL, NULL, NULL, NULL),
('M12301', 'Metodologi Penelitian', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12303', 'Kerja Praktek', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12305', 'Bisnis dan Kewirausahaan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12309', 'Etika Profesi', 'A', 'MKU', '7', '2', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL),
('M12311', 'Agama', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12313', 'Bahasa Inggris III', 'A', 'MKU', '7', '4', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('MI12307', 'Kewarganegaraan', 'A', 'MKU', '7', '3', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('KA12307', 'Kewarganegaraan', 'A', 'MKU', '7', '3', '0', 0, NULL, NULL, NULL, NULL, NULL, NULL),
('M12307', 'Kewarganegaraan', 'A', 'MKU', '7', '2', '0', 5, NULL, NULL, NULL, NULL, NULL, NULL),
('M12304', 'Tugas Akhir', 'S', 'MKB', '7', '5', '0', 6, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

CREATE TABLE `pekerjaan` (
  `id_pekerjaan` int(11) NOT NULL,
  `nm_pekerjaan` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id_pekerjaan`, `nm_pekerjaan`) VALUES
(0, 'Tidak Bekerja'),
(1, 'Pegawai Negeri'),
(2, 'TNI / POLRI'),
(3, 'Pegawai BUMN'),
(4, 'Wiraswasta'),
(5, 'Pegawai Swasta'),
(6, 'Pensiunan'),
(7, 'Lain - Lain');

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan`
--

CREATE TABLE `penghasilan` (
  `id_penghasilan` int(11) NOT NULL,
  `penghasilan` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penghasilan`
--

INSERT INTO `penghasilan` (`id_penghasilan`, `penghasilan`) VALUES
(0, 'Tidak Berpenghasilan'),
(1, '<100000'),
(2, '100000 - 999000'),
(3, '1000000 - 9999000'),
(4, '>=10000000');

-- --------------------------------------------------------

--
-- Table structure for table `ruang_kelas`
--

CREATE TABLE `ruang_kelas` (
  `kode_ruangan` varchar(5) NOT NULL,
  `nm_ruangan` varchar(50) DEFAULT NULL,
  `posisi` enum('Lantai 1','Lantai 2','Lantai 3') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang_kelas`
--

INSERT INTO `ruang_kelas` (`kode_ruangan`, `nm_ruangan`, `posisi`) VALUES
('LT1M', 'Mandrake', 'Lantai 1'),
('LT1T', 'Trustix', 'Lantai 1'),
('LT2D', 'Debian', 'Lantai 2'),
('LT2F', 'Fedora', 'Lantai 2'),
('LT2S', 'Suse', 'Lantai 2'),
('LT2TC', 'Trustcafe', 'Lantai 2'),
('LT3K', 'Knoppix', 'Lantai 3'),
('LT3R', 'Redhat', 'Lantai 3'),
('LT3SL', 'Slackware', 'Lantai 3'),
('LT4E', 'Empty Room', 'Lantai 3');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_smt` char(5) NOT NULL,
  `id_thn_ajaran` decimal(4,0) NOT NULL,
  `nm_smt` varchar(20) DEFAULT NULL,
  `smt` decimal(1,0) DEFAULT NULL,
  `a_periode_aktif` decimal(1,0) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_smt`, `id_thn_ajaran`, `nm_smt`, `smt`, `a_periode_aktif`, `tgl_mulai`, `tgl_selesai`) VALUES
('15161', '2015', 'TA 2015/2016 Ganjil', '1', '0', '2015-11-21', NULL),
('16171', '2016', 'TA 2016/2017 Ganjil', '1', '0', '2016-09-19', NULL),
('16172', '2016', 'TA 2016/2017 Genap', '2', '1', '2017-02-13', NULL),
('17181', '2017', 'TA 2017/2018 Ganjil', '1', '0', '2017-10-10', NULL),
('17182', '2017', 'TA 2017/2018 Genap', '2', '0', '2017-10-10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `statusdosen`
--

CREATE TABLE `statusdosen` (
  `nidn` char(10) NOT NULL,
  `nm_ptk` varchar(100) NOT NULL,
  `status` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statusdosen`
--

INSERT INTO `statusdosen` (`nidn`, `nm_ptk`, `status`) VALUES
('0002028401', 'Wahyu Hidayatullah, S.Kom', 'A'),
('0002038301', 'Lamhari, S.Kom', 'N'),
('0004048701', 'Aprillia Setyaningrum, S.Kom', 'N'),
('0005028001', 'ahmad Hermansyah, S.Si', 'N'),
('0006067801', 'Rida Alfadiah, ST, MM', 'N'),
('0007095601', 'Alfauzi Salam, Drs,H, MBA', 'A'),
('0008076801', 'Ir. Abdul Hadi Muslim', 'N'),
('0008087201', 'Hj. Rita Andriani Sukmawati, SH', 'N'),
('0009055901', 'Raden Gunawan,IR,H, M.Eng', 'A'),
('0009079101', 'Faris Muhammad Syariati, S. Kom', 'N'),
('0010017901', 'Rahmat Gunawan, S.S', 'N'),
('0011027701', 'Ujang Jaenal Mutakin, S.Ag, MM', 'N'),
('0011028801', 'Mahmud Siddik, S.Kom', 'N'),
('0012017401', 'Sev Rahmiyanti, SE', 'N'),
('0012018801', 'Muhammad Khaidir Fahram, M.Kom', 'A'),
('0012086201', 'Ir. Achmad Hudaya', 'N'),
('0013108701', 'Vina Oktarina Sari, S.Pd', 'N'),
('0014088601', 'Lukman Faruk, ST', 'N'),
('0014127901', 'Ipung Ernawati Setianingrum, ST, M.Si', 'N'),
('0017056401', 'Drs. Nurjaman', 'N'),
('0018058901', 'Andri Setyawan, ST', 'N'),
('0018127401', 'Nanang Salaludin, S.Ag', 'A'),
('0020018201', 'Ahmad Furqoni, S.Pd, M. Pd', 'N'),
('0021057906', 'Susy Katarina Sianturi, M.Kom', 'A'),
('0021118501', 'Devi Yudha Damara, ST', 'N'),
('0023058701', 'Fatkur Roji', 'N'),
('0024059001', 'Lestari Hadi Wigatning, S.Kom', 'N'),
('0026018001', 'Wahyu Iskandar, ST, MKom', 'N'),
('0026036801', 'Apandi, S.Ag', 'A'),
('0027028401', 'Agus Fakhru Rizal, ST', 'N'),
('0027036401', 'Imam Prasodjo, S. Mn', 'N'),
('0027095801', 'Kamarul Hajar NST', 'N'),
('0028098001', 'Yanti Anggraini, SP', 'N'),
('0030018001', 'Thoha Nurhadiyan Hikmawan, M.Kom', 'N'),
('0030108801', 'Syaiful Bakhri, S.Kom', 'N'),
('0031056401', 'Dra. Atih N. Purwati, M.Si', 'N'),
('0402088601', 'Rizki Mirhadi. Ir.MM', 'N'),
('0403116901', 'Bambang Wahyu Agung', 'N'),
('0404068001', 'Zaenal Muttaqin, S.Kom, MM', 'N'),
('0404088003', 'Penny Hendriyati, S.Kom,M.Kom', 'A'),
('0405038401', 'Nanang Sutisna, S.Kom', 'A'),
('0406067405', 'Darojat, S.Ag', 'N'),
('0407097404', 'Hetty Herawati, S.Pd', 'A'),
('0408047202', 'Subandi Wahyudi, S.Si,M.Kom', 'N'),
('0410027303', 'Jayadi, S.Kom', 'N'),
('0410048102', 'Diah Ahdiati, S.Pd', 'A'),
('0410127002', 'ADE HENDRIANI, SE, MM', 'A'),
('0411096202', 'Sawitri Nurhayati, S.Kom, M.Kom', 'N'),
('0411116903', 'Drs, Fachroji Ali, MM', 'N'),
('0412035901', 'PARYONO,IR,MM', 'A'),
('0412037301', 'Cecep Abdul Hakim, SE, MM.Ak', 'N'),
('0412057902', 'Helmi Ilham, S.Kom', 'A'),
('0414066905', 'Wahyuddin, S.Kom, M.Kom', 'N'),
('0415010001', 'Dwi Dina Heriyanto, S.Kom', 'A'),
('0415010002', 'AKBP Ade Kusnadi SH,MSi', 'N'),
('0415010009', 'Vina Vijaya Kusuma SPd,MPd', 'A'),
('0415088701', 'Sodik Nurdhin,MKom', 'N'),
('0415106805', 'Ahmad Kautsar, ST, M. Kom', 'N'),
('0418087606', 'Dina Satriani Fansuri, SE, MM', 'A'),
('0419027405', 'Sulaeman, S.Kom', 'N'),
('0419077601', 'Darpi Supriyanto, S.Si, M.Kom', 'N'),
('0420088004', 'Agus Setyawan, S.Kom, M. Kom', 'N'),
('0421088103', 'Gustina, S.Kom,MKom', 'A'),
('0422076301', 'Achmad Syaefudin, ST, MM ,MKom', 'A'),
('0422078802', 'Burhanudin,SE,MM.AK', 'N'),
('0422078805', 'Ali Faozin,Ir,MM', 'A'),
('0422078815', 'Saleh Dwiyatno,ST,MKOM', 'N'),
('0422078830', 'Rita Andriani,SH', 'N'),
('0422109001', 'Asep Saifudin, S.Kom', 'A'),
('042299999', 'Usep Sholahudin .MKom', 'N'),
('0424018403', 'Eva Safa\'ah, ST, M.Kom', 'N'),
('0424048401', 'Ikbal Nidauddin,MKom', 'N'),
('0424086102', 'Andi Usri Usman, M.Kom', 'N'),
('0424096905', 'Padang Wardoyo, ST, MM', 'A'),
('0424099011', 'M.Amiruddin  Saddam, S.Kom', 'A'),
('0424099012', 'H.M. Sayuthi Ali,Drs,MAg', 'A'),
('0424099013', 'Siti Nurbanis Andriani,SH,MM', 'A'),
('0424099014', 'Tri Sutomo', 'A'),
('0424099015', 'MULYADI.MKom', 'A'),
('0424099016', 'Eka Prasetia Bakti', 'A'),
('0426048201', 'Nisa Nuranisa, S.Pd', 'N'),
('0426066105', 'Dr. Ir. Sumadiono, M. MT, M. Si', 'N'),
('0426128702', 'Siti Desi Rohmawati, SE', 'N'),
('0428118503', 'DWI NURINA PITASARI .M.IKOM', 'N'),
('0429126501', 'Teguh Sutopo, S.Kom, M. Kom', 'A'),
('0430018003', 'Thoha Nurhadiyan Hikmawan, M.Kom', 'N'),
('0430045803', 'Slamet Gunadi, ST, MM', 'A'),
('0430079001', 'ISNAINI MUHADA .MSI', 'A'),
('0431077702', 'Afrasim Yusta, S.Kom,MKom', 'A'),
('0807877777', 'Arif Eko Julianto, S.Kom', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `status_ruang`
--

CREATE TABLE `status_ruang` (
  `tgl_skrg` date NOT NULL,
  `kode_ruangan` varchar(5) NOT NULL,
  `status_ruangan` enum('Digunakan','Tidak Digunakan') DEFAULT NULL,
  `ket_penggunaan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_ruang`
--

INSERT INTO `status_ruang` (`tgl_skrg`, `kode_ruangan`, `status_ruangan`, `ket_penggunaan`) VALUES
('2017-02-14', 'LT1T', 'Digunakan', 'Kumpul Bem'),
('2017-02-15', 'LT1M', 'Tidak Digunakan', NULL),
('2017-02-15', 'LT2D', 'Digunakan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_thn_ajaran` decimal(4,0) NOT NULL,
  `nm_thn_ajaran` varchar(10) DEFAULT NULL,
  `a_periode_aktif` decimal(1,0) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_thn_ajaran`, `nm_thn_ajaran`, `a_periode_aktif`, `tgl_mulai`, `tgl_selesai`) VALUES
('2014', '2014/2015', '0', '2014-10-21', NULL),
('2015', '2015/2016', '0', '2015-11-21', NULL),
('2016', '2016/2017', '1', '2017-02-13', NULL),
('2017', '2017/2018', '0', '2017-10-10', NULL),
('2019', '2019/2020', '1', '2018-10-10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(20) NOT NULL,
  `passwd` varchar(50) DEFAULT NULL,
  `level` varchar(15) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `passwd`, `level`, `last_login`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'Super Admin', '2017-04-26 09:30:41');

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_absendos`
-- (See below for the actual view)
--
CREATE TABLE `view_absendos` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_alumni`
-- (See below for the actual view)
--
CREATE TABLE `view_alumni` (
`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_banyakmhsthn`
-- (See below for the actual view)
--
CREATE TABLE `view_banyakmhsthn` (
`tahun` varchar(4)
,`jmlmhsthn` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jmlmhspecah`
-- (See below for the actual view)
--
CREATE TABLE `view_jmlmhspecah` (
`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`kd_prodi` varchar(2)
,`tahun` varchar(4)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahalumni`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahalumni` (
`jumlahalumni` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahdsnall`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahdsnall` (
`jumlahdsnall` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmhsaktf`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmhsaktf` (
`jumlahmhsaktf` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmhsall`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmhsall` (
`jumlahmhsall` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkbaru`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkbaru` (
`jumlahmkbaru` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkpilihan`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkpilihan` (
`jumlahmkpilihan` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkpilihanminat`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkpilihanminat` (
`jumlahmkpilihanminat` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkskripsi`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkskripsi` (
`jumlahmkskripsi` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkwajib`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkwajib` (
`jumlahmkwajib` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_jumlahmkwajibminat`
-- (See below for the actual view)
--
CREATE TABLE `view_jumlahmkwajibminat` (
`jumlahmkwajibminat` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kelaskuliah`
-- (See below for the actual view)
--
CREATE TABLE `view_kelaskuliah` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kelaskuliahfinal`
-- (See below for the actual view)
--
CREATE TABLE `view_kelaskuliahfinal` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kelaskuliahformatjam`
-- (See below for the actual view)
--
CREATE TABLE `view_kelaskuliahformatjam` (
`kode_kelas` varchar(10)
,`id_smt` char(5)
,`kode_mk` char(20)
,`nidn` char(10)
,`kode_ruangan` varchar(5)
,`hari` varchar(10)
,`jam_mulai` varchar(10)
,`jam_selesai` varchar(10)
,`bahasan_case` varchar(200)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_kelaskuliahtemp`
-- (See below for the actual view)
--
CREATE TABLE `view_kelaskuliahtemp` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`id_smt` char(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_laporanrekapdsn`
-- (See below for the actual view)
--
CREATE TABLE `view_laporanrekapdsn` (
`nm_ptk` varchar(100)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`kode_kelas` varchar(10)
,`8` varchar(3)
,`9` varchar(3)
,`10` varchar(3)
,`11` varchar(3)
,`12` varchar(3)
,`13` varchar(3)
,`14` varchar(3)
,`15` varchar(3)
,`16` varchar(3)
,`17` varchar(3)
,`18` varchar(3)
,`19` varchar(3)
,`20` varchar(3)
,`21` varchar(3)
,`22` varchar(3)
,`23` varchar(3)
,`24` varchar(3)
,`25` varchar(3)
,`26` varchar(3)
,`27` varchar(3)
,`28` varchar(3)
,`29` varchar(3)
,`30` varchar(3)
,`31` varchar(3)
,`jumlahsks` decimal(24,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_laporanrekapdsn_copy`
-- (See below for the actual view)
--
CREATE TABLE `view_laporanrekapdsn_copy` (
`nm_ptk` varchar(100)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`kode_kelas` varchar(10)
,`8` varchar(3)
,`9` varchar(3)
,`10` varchar(3)
,`11` varchar(3)
,`12` varchar(3)
,`13` varchar(3)
,`14` varchar(3)
,`15` varchar(3)
,`16` varchar(3)
,`17` varchar(3)
,`18` varchar(3)
,`19` varchar(3)
,`20` varchar(3)
,`21` varchar(3)
,`22` varchar(3)
,`23` varchar(3)
,`24` varchar(3)
,`25` varchar(3)
,`26` varchar(3)
,`27` varchar(3)
,`28` varchar(3)
,`29` varchar(3)
,`30` varchar(3)
,`31` varchar(3)
,`jumlahsks` decimal(24,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_listdosen`
-- (See below for the actual view)
--
CREATE TABLE `view_listdosen` (
`nidn` char(10)
,`nm_ptk` varchar(100)
,`jk` char(1)
,`statusdsn` varchar(21)
,`no_hp` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_listmahasiswa`
-- (See below for the actual view)
--
CREATE TABLE `view_listmahasiswa` (
`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`prodi` varchar(26)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_matakuliahtemp`
-- (See below for the actual view)
--
CREATE TABLE `view_matakuliahtemp` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
,`kel_mk` char(3)
,`sks_tatapmuka` decimal(2,0)
,`metode_pelaksanaan_kuliah` varchar(50)
,`ket_sap` varchar(30)
,`ket_silabus` varchar(34)
,`ket_bahanajar` varchar(37)
,`ket_acara_prak` varchar(41)
,`ket_diktat` varchar(33)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mata_kuliah`
-- (See below for the actual view)
--
CREATE TABLE `view_mata_kuliah` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
,`metode_pelaksanaan_kuliah` varchar(50)
,`a_sap` decimal(1,0)
,`a_silabus` decimal(1,0)
,`a_bahan_ajar` decimal(1,0)
,`acara_prak` decimal(1,0)
,`a_diktat` decimal(1,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_matrixkul`
-- (See below for the actual view)
--
CREATE TABLE `view_matrixkul` (
`hari` varchar(19)
,`waktu` varchar(23)
,`mandrake` varchar(218)
,`trustix` varchar(218)
,`suse` varchar(218)
,`debian` varchar(218)
,`trustcafe` varchar(218)
,`fedora` varchar(218)
,`knoppix` varchar(218)
,`redhat` varchar(218)
,`nm_mk` varchar(100)
,`nm_ptk` varchar(100)
,`kode_kelas` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mhsaktf`
-- (See below for the actual view)
--
CREATE TABLE `view_mhsaktf` (
`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mhscomplete`
-- (See below for the actual view)
--
CREATE TABLE `view_mhscomplete` (
`id` int(11)
,`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`nisn` varchar(10)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`id_agama` smallint(16)
,`jln` varchar(200)
,`rt` decimal(3,0)
,`rw` decimal(3,0)
,`nm_dsn` varchar(40)
,`ds_kel` varchar(40)
,`kode_pos` char(5)
,`telepon_seluler` varchar(20)
,`stat_pd` char(1)
,`nm_ayah` varchar(45)
,`tgl_lahir_ayah` date
,`id_pekerjaan_ayah` int(11)
,`pekerjaanayah` varchar(25)
,`id_jenjang_pendidikan_ayah` decimal(2,0)
,`jenjangdidikayah` varchar(25)
,`id_penghasilan_ayah` int(11)
,`penghasilanayah` varchar(40)
,`nm_ibu_kandung` varchar(45)
,`tgl_lahir_ibu` date
,`id_pekerjaan_ibu` int(11)
,`pekerjaanibu` varchar(25)
,`id_jenjang_pendidikan_ibu` decimal(2,0)
,`jenjangdidikibu` varchar(25)
,`id_penghasilan_ibu` int(11)
,`penghasilanibu` varchar(40)
,`nm_wali` varchar(30)
,`tgl_lahir_wali` date
,`id_pekerjaan_wali` int(11)
,`pekerjaanwali` varchar(25)
,`id_jenjang_pendidikan_wali` decimal(2,0)
,`jenjangdidikwali` varchar(25)
,`id_penghasilan_wali` int(11)
,`penghasilanwali` varchar(40)
,`kewarganegaraan` char(3)
,`id_pd` varchar(50)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mhsshort`
-- (See below for the actual view)
--
CREATE TABLE `view_mhsshort` (
`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`prodi` varchar(26)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkpilihan`
-- (See below for the actual view)
--
CREATE TABLE `view_mkpilihan` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkpilihanpeminatan`
-- (See below for the actual view)
--
CREATE TABLE `view_mkpilihanpeminatan` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkshort`
-- (See below for the actual view)
--
CREATE TABLE `view_mkshort` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
,`kel_mk` char(3)
,`sks_tatapmuka` decimal(2,0)
,`metode_pelaksanaan_kuliah` varchar(50)
,`a_sap` decimal(1,0)
,`a_silabus` decimal(1,0)
,`a_bahan_ajar` decimal(1,0)
,`acara_prak` decimal(1,0)
,`a_diktat` decimal(1,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkshortdetail`
-- (See below for the actual view)
--
CREATE TABLE `view_mkshortdetail` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
,`kel_mk` char(3)
,`sks_tatapmuka` decimal(2,0)
,`metode_pelaksanaan_kuliah` varchar(50)
,`ket_sap` varchar(30)
,`ket_silabus` varchar(34)
,`ket_bahanajar` varchar(37)
,`ket_acara_prak` varchar(41)
,`ket_diktat` varchar(33)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkskripsi`
-- (See below for the actual view)
--
CREATE TABLE `view_mkskripsi` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mksms1`
-- (See below for the actual view)
--
CREATE TABLE `view_mksms1` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mksmtganjil`
-- (See below for the actual view)
--
CREATE TABLE `view_mksmtganjil` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mksmtgenap`
-- (See below for the actual view)
--
CREATE TABLE `view_mksmtgenap` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkwajib`
-- (See below for the actual view)
--
CREATE TABLE `view_mkwajib` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_mkwajibpeminatan`
-- (See below for the actual view)
--
CREATE TABLE `view_mkwajibpeminatan` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`kel_mk` char(3)
,`prodi` varchar(49)
,`sks_mk` decimal(2,0)
,`sks_tatapmuka` decimal(2,0)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_nimpecah`
-- (See below for the actual view)
--
CREATE TABLE `view_nimpecah` (
`nim` char(15)
,`nm_pd` varchar(100)
,`thnmsuk` varchar(4)
,`kdprodi` varchar(2)
,`nmrurut` varchar(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_paketmkka`
-- (See below for the actual view)
--
CREATE TABLE `view_paketmkka` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_paketmkmi`
-- (See below for the actual view)
--
CREATE TABLE `view_paketmkmi` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_paketmksi`
-- (See below for the actual view)
--
CREATE TABLE `view_paketmksi` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_paketmkti`
-- (See below for the actual view)
--
CREATE TABLE `view_paketmkti` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nmajns_mk` varchar(26)
,`sks_mk` decimal(2,0)
,`prodi` varchar(49)
,`mk_semester` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_prodi`
-- (See below for the actual view)
--
CREATE TABLE `view_prodi` (
`prodi` varchar(88)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliahtemp`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliahtemp` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`jns_mk` char(1)
,`kel_mk` char(3)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_mksmtgenap` char(20)
,`nm_mksmtgenap` varchar(100)
,`jns_mksmtgenap` char(1)
,`kel_mksmtgenap` char(3)
,`sks_mksmtgenap` decimal(2,0)
,`mksemester_mksmtgenap` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_thnajar`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_thnajar` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`thn_ajar` varchar(24)
,`smt` decimal(1,0)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_thnajarbackup`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_thnajarbackup` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`thn_ajar` varchar(24)
,`smt` decimal(1,0)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_thnajarfinal`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_thnajarfinal` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_update`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_update` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_updateindo`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_updateindo` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
,`nm_smt` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_rancangkuliah_updateindo_copy`
-- (See below for the actual view)
--
CREATE TABLE `view_rancangkuliah_updateindo_copy` (
`kode_mk` char(20)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`mk_semester` int(11)
,`kode_kelas` varchar(10)
,`nm_ptk` varchar(100)
,`hari` varchar(19)
,`waktu` varchar(23)
,`nm_ruangan` varchar(50)
,`kode_ruangan` varchar(5)
,`id_smt` char(5)
,`nidn` char(10)
,`nm_smt` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ruangan`
-- (See below for the actual view)
--
CREATE TABLE `view_ruangan` (
`kode_ruangan` varchar(5)
,`nm_ruangan` varchar(50)
,`status_ruangan` enum('Digunakan','Tidak Digunakan')
,`posisi` enum('Lantai 1','Lantai 2','Lantai 3')
,`ket_penggunaan` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_statusdosen`
-- (See below for the actual view)
--
CREATE TABLE `view_statusdosen` (
`nidn` char(10)
,`nm_ptk` varchar(100)
,`statusdosen` varchar(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_statusklskul`
-- (See below for the actual view)
--
CREATE TABLE `view_statusklskul` (
`kode_kelas` varchar(10)
,`nm_mk` varchar(100)
,`nm_ruangan` varchar(50)
,`waktu` varchar(23)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_statusmhs`
-- (See below for the actual view)
--
CREATE TABLE `view_statusmhs` (
`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`statusmhs` varchar(26)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_statusruangan`
-- (See below for the actual view)
--
CREATE TABLE `view_statusruangan` (
`nm_ruangan` varchar(50)
,`posisi` enum('Lantai 1','Lantai 2','Lantai 3')
,`kode_kelas` varchar(10)
,`nm_mk` varchar(100)
,`sks_mk` decimal(2,0)
,`waktu` varchar(23)
,`statusruangan` varchar(15)
,`kode_ruangan` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_stsabsendos`
-- (See below for the actual view)
--
CREATE TABLE `view_stsabsendos` (
`tgl_absen` date
,`kode_kelas` varchar(10)
,`id_smt` char(5)
,`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nidn` char(10)
,`nm_ptk` varchar(100)
,`kode_ruangan` varchar(5)
,`nm_ruangan` varchar(50)
,`hari` varchar(10)
,`kehadiran` varchar(11)
,`keterangan` varchar(50)
,`mk_semester` int(11)
,`sks_mk` decimal(2,0)
,`tgl` varchar(2)
,`waktu` varchar(23)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_stsabsendosbulanan`
-- (See below for the actual view)
--
CREATE TABLE `view_stsabsendosbulanan` (
`tgl_absen` date
,`kode_kelas` varchar(10)
,`id_smt` char(5)
,`kode_mk` char(20)
,`nm_mk` varchar(100)
,`nidn` char(10)
,`nm_ptk` varchar(100)
,`kode_ruangan` varchar(5)
,`nm_ruangan` varchar(50)
,`hari` varchar(10)
,`kehadiran` varchar(11)
,`keterangan` varchar(50)
,`mk_semester` int(11)
,`sks_mk` decimal(2,0)
,`tgl` varchar(2)
,`waktu` varchar(23)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_thnajaran`
-- (See below for the actual view)
--
CREATE TABLE `view_thnajaran` (
`id_thn_ajaran` decimal(4,0)
,`nm_thn_ajaran` varchar(10)
,`statusthnajaran` varchar(28)
,`tgl_mulai` date
,`tgl_selesai` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_thnajaransmt`
-- (See below for the actual view)
--
CREATE TABLE `view_thnajaransmt` (
`id_thn_ajaran` decimal(4,0)
,`nm_thn_ajaran` varchar(10)
,`nm_smt` varchar(20)
,`semester` varchar(24)
,`keadaan_periode` varchar(24)
,`tgl_mulai` date
,`id_smt` char(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ttgayahmhs`
-- (See below for the actual view)
--
CREATE TABLE `view_ttgayahmhs` (
`id` int(11)
,`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`stat_pd` char(1)
,`nm_ayah` varchar(45)
,`tgl_lahir_ayah` date
,`id_pekerjaan_ayah` int(11)
,`pekerjaanayah` varchar(25)
,`id_jenjang_pendidikan_ayah` decimal(2,0)
,`jenjangdidikayah` varchar(25)
,`id_penghasilan_ayah` int(11)
,`penghasilan` varchar(40)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ttgibumhs`
-- (See below for the actual view)
--
CREATE TABLE `view_ttgibumhs` (
`id` int(11)
,`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`stat_pd` char(1)
,`nm_ibu_kandung` varchar(45)
,`tgl_lahir_ibu` date
,`id_pekerjaan_ibu` int(11)
,`pekerjaanibu` varchar(25)
,`id_jenjang_pendidikan_ibu` decimal(2,0)
,`jenjangdidikibu` varchar(25)
,`id_penghasilan_ibu` int(11)
,`penghasilan` varchar(40)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ttgwalimhs`
-- (See below for the actual view)
--
CREATE TABLE `view_ttgwalimhs` (
`id` int(11)
,`nik` char(16)
,`nim` char(15)
,`nm_pd` varchar(100)
,`jk` char(1)
,`tmpt_lahir` varchar(20)
,`tgl_lahir` date
,`stat_pd` char(1)
,`nm_wali` varchar(30)
,`tgl_lahir_wali` date
,`id_pekerjaan_wali` int(11)
,`pekerjaanwali` varchar(25)
,`id_jenjang_pendidikan_wali` decimal(2,0)
,`jenjangdidikwali` varchar(25)
,`id_penghasilan_wali` int(11)
,`penghasilanwali` varchar(40)
);

-- --------------------------------------------------------

--
-- Structure for view `view_absendos`
--
DROP TABLE IF EXISTS `view_absendos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_absendos`  AS  select `view_rancangkuliah_update`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_update`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_update`.`sks_mk` AS `sks_mk`,`view_rancangkuliah_update`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_update`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_update`.`nm_ptk` AS `nm_ptk`,`view_rancangkuliah_update`.`hari` AS `hari`,`view_rancangkuliah_update`.`waktu` AS `waktu`,`view_rancangkuliah_update`.`nm_ruangan` AS `nm_ruangan`,`view_rancangkuliah_update`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_update`.`id_smt` AS `id_smt`,`view_rancangkuliah_update`.`nidn` AS `nidn` from `view_rancangkuliah_update` where (convert(`view_rancangkuliah_update`.`hari` using utf8mb4) = date_format(now(),'%W')) ;

-- --------------------------------------------------------

--
-- Structure for view `view_alumni`
--
DROP TABLE IF EXISTS `view_alumni`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_alumni`  AS  select `view_statusmhs`.`nik` AS `nik`,`view_statusmhs`.`nim` AS `nim`,`view_statusmhs`.`nm_pd` AS `nm_pd`,`view_statusmhs`.`jk` AS `jk`,`view_statusmhs`.`tmpt_lahir` AS `tmpt_lahir`,`view_statusmhs`.`tgl_lahir` AS `tgl_lahir` from `view_statusmhs` where (`view_statusmhs`.`statusmhs` = 'Alumni') ;

-- --------------------------------------------------------

--
-- Structure for view `view_banyakmhsthn`
--
DROP TABLE IF EXISTS `view_banyakmhsthn`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_banyakmhsthn`  AS  select `view_jmlmhspecah`.`tahun` AS `tahun`,count(`view_jmlmhspecah`.`tahun`) AS `jmlmhsthn` from `view_jmlmhspecah` group by `view_jmlmhspecah`.`tahun` order by `view_jmlmhspecah`.`tahun` desc limit 5 ;

-- --------------------------------------------------------

--
-- Structure for view `view_jmlmhspecah`
--
DROP TABLE IF EXISTS `view_jmlmhspecah`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jmlmhspecah`  AS  select `mahasiswa`.`nik` AS `nik`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,substr(`mahasiswa`.`nim`,6,2) AS `kd_prodi`,substr(`mahasiswa`.`nim`,1,4) AS `tahun` from `mahasiswa` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahalumni`
--
DROP TABLE IF EXISTS `view_jumlahalumni`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahalumni`  AS  select count(`view_alumni`.`nim`) AS `jumlahalumni` from `view_alumni` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahdsnall`
--
DROP TABLE IF EXISTS `view_jumlahdsnall`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahdsnall`  AS  select count(`dosen`.`nidn`) AS `jumlahdsnall` from `dosen` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmhsaktf`
--
DROP TABLE IF EXISTS `view_jumlahmhsaktf`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmhsaktf`  AS  select count(`view_mhsaktf`.`nim`) AS `jumlahmhsaktf` from `view_mhsaktf` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmhsall`
--
DROP TABLE IF EXISTS `view_jumlahmhsall`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmhsall`  AS  select count(`mahasiswa`.`nik`) AS `jumlahmhsall` from `mahasiswa` group by `mahasiswa`.`nik` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkbaru`
--
DROP TABLE IF EXISTS `view_jumlahmkbaru`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkbaru`  AS  select count(`mata_kuliah`.`kode_mk`) AS `jumlahmkbaru` from `mata_kuliah` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkpilihan`
--
DROP TABLE IF EXISTS `view_jumlahmkpilihan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkpilihan`  AS  select count(`view_mkpilihan`.`kode_mk`) AS `jumlahmkpilihan` from `view_mkpilihan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkpilihanminat`
--
DROP TABLE IF EXISTS `view_jumlahmkpilihanminat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkpilihanminat`  AS  select count(`view_mkpilihanpeminatan`.`kode_mk`) AS `jumlahmkpilihanminat` from `view_mkpilihanpeminatan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkskripsi`
--
DROP TABLE IF EXISTS `view_jumlahmkskripsi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkskripsi`  AS  select count(`view_mkskripsi`.`kode_mk`) AS `jumlahmkskripsi` from `view_mkskripsi` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkwajib`
--
DROP TABLE IF EXISTS `view_jumlahmkwajib`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkwajib`  AS  select count(`view_mkwajib`.`kode_mk`) AS `jumlahmkwajib` from `view_mkwajib` ;

-- --------------------------------------------------------

--
-- Structure for view `view_jumlahmkwajibminat`
--
DROP TABLE IF EXISTS `view_jumlahmkwajibminat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_jumlahmkwajibminat`  AS  select count(`view_mkwajibpeminatan`.`kode_mk`) AS `jumlahmkwajibminat` from `view_mkwajibpeminatan` ;

-- --------------------------------------------------------

--
-- Structure for view `view_kelaskuliah`
--
DROP TABLE IF EXISTS `view_kelaskuliah`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kelaskuliah`  AS  select `kelas_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`jns_mk` AS `jns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliah`.`kode_kelas` AS `kode_kelas` from (`kelas_kuliah` join `mata_kuliah` on((`kelas_kuliah`.`kode_mk` = `mata_kuliah`.`kode_mk`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_kelaskuliahfinal`
--
DROP TABLE IF EXISTS `view_kelaskuliahfinal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kelaskuliahfinal`  AS  select `kelas_kuliahfinal`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`jns_mk` AS `jns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliahfinal`.`kode_kelas` AS `kode_kelas` from (`kelas_kuliahfinal` join `mata_kuliah` on((`kelas_kuliahfinal`.`kode_mk` = `mata_kuliah`.`kode_mk`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_kelaskuliahformatjam`
--
DROP TABLE IF EXISTS `view_kelaskuliahformatjam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kelaskuliahformatjam`  AS  select `kelas_kuliah`.`kode_kelas` AS `kode_kelas`,`kelas_kuliah`.`id_smt` AS `id_smt`,`kelas_kuliah`.`kode_mk` AS `kode_mk`,`kelas_kuliah`.`nidn` AS `nidn`,`kelas_kuliah`.`kode_ruangan` AS `kode_ruangan`,`kelas_kuliah`.`hari` AS `hari`,if((`kelas_kuliah`.`jam_mulai` = '01:00:00'),'13:00:00',if((`kelas_kuliah`.`jam_mulai` = '03:00:00'),'15:00:00',if((`kelas_kuliah`.`jam_mulai` = '06:30:00'),'18:30:00',`kelas_kuliah`.`jam_mulai`))) AS `jam_mulai`,if((`kelas_kuliah`.`jam_selesai` = '04:40:00'),'16:40:00',if((`kelas_kuliah`.`jam_selesai` = '09:30:00'),'21:30:00',if((`kelas_kuliah`.`jam_selesai` = '02:40:00'),'14:40:00',if((`kelas_kuliah`.`jam_selesai` = '06:30:00'),'18:30:00',if((`kelas_kuliah`.`jam_selesai` = '03:40:00'),'15:40:00',`kelas_kuliah`.`jam_selesai`))))) AS `jam_selesai`,`kelas_kuliah`.`bahasan_case` AS `bahasan_case` from `kelas_kuliah` ;

-- --------------------------------------------------------

--
-- Structure for view `view_kelaskuliahtemp`
--
DROP TABLE IF EXISTS `view_kelaskuliahtemp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_kelaskuliahtemp`  AS  select `kelas_kuliahtemp`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`jns_mk` AS `jns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliahtemp`.`kode_kelas` AS `kode_kelas`,`kelas_kuliahtemp`.`id_smt` AS `id_smt` from (`kelas_kuliahtemp` join `mata_kuliah` on((`kelas_kuliahtemp`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (not(`kelas_kuliahtemp`.`kode_mk` in (select `kelas_kuliah`.`kode_mk` from `kelas_kuliah`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_laporanrekapdsn`
--
DROP TABLE IF EXISTS `view_laporanrekapdsn`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_laporanrekapdsn`  AS  select `view_stsabsendosbulanan`.`nm_ptk` AS `nm_ptk`,`view_stsabsendosbulanan`.`nm_mk` AS `nm_mk`,`view_stsabsendosbulanan`.`sks_mk` AS `sks_mk`,`view_stsabsendosbulanan`.`kode_kelas` AS `kode_kelas`,if((`view_stsabsendosbulanan`.`tgl` = '8'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `8`,if((`view_stsabsendosbulanan`.`tgl` = '9'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `9`,if((`view_stsabsendosbulanan`.`tgl` = '10'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `10`,if((`view_stsabsendosbulanan`.`tgl` = '11'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `11`,if((`view_stsabsendosbulanan`.`tgl` = '12'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `12`,if((`view_stsabsendosbulanan`.`tgl` = '13'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `13`,if((`view_stsabsendosbulanan`.`tgl` = '14'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `14`,if((`view_stsabsendosbulanan`.`tgl` = '15'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `15`,if((`view_stsabsendosbulanan`.`tgl` = '16'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `16`,if((`view_stsabsendosbulanan`.`tgl` = '17'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `17`,if((`view_stsabsendosbulanan`.`tgl` = '18'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `18`,if((`view_stsabsendosbulanan`.`tgl` = '19'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `19`,if((`view_stsabsendosbulanan`.`tgl` = '20'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `20`,if((`view_stsabsendosbulanan`.`tgl` = '21'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `21`,if((`view_stsabsendosbulanan`.`tgl` = '22'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `22`,if((`view_stsabsendosbulanan`.`tgl` = '23'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `23`,if((`view_stsabsendosbulanan`.`tgl` = '24'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `24`,if((`view_stsabsendosbulanan`.`tgl` = '25'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `25`,if((`view_stsabsendosbulanan`.`tgl` = '26'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `26`,if((`view_stsabsendosbulanan`.`tgl` = '27'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `27`,if((`view_stsabsendosbulanan`.`tgl` = '28'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `28`,if((`view_stsabsendosbulanan`.`tgl` = '29'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `29`,if((`view_stsabsendosbulanan`.`tgl` = '30'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `30`,if((`view_stsabsendosbulanan`.`tgl` = '31'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `31`,sum(`view_stsabsendosbulanan`.`sks_mk`) AS `jumlahsks` from `view_stsabsendosbulanan` group by `view_stsabsendosbulanan`.`nm_ptk`,`view_stsabsendosbulanan`.`nm_mk`,`view_stsabsendosbulanan`.`tgl` order by `view_stsabsendosbulanan`.`nm_ptk` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_laporanrekapdsn_copy`
--
DROP TABLE IF EXISTS `view_laporanrekapdsn_copy`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_laporanrekapdsn_copy`  AS  select `view_stsabsendosbulanan`.`nm_ptk` AS `nm_ptk`,`view_stsabsendosbulanan`.`nm_mk` AS `nm_mk`,`view_stsabsendosbulanan`.`sks_mk` AS `sks_mk`,`view_stsabsendosbulanan`.`kode_kelas` AS `kode_kelas`,if((`view_stsabsendosbulanan`.`tgl` = '8'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `8`,if((`view_stsabsendosbulanan`.`tgl` = '9'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `9`,if((`view_stsabsendosbulanan`.`tgl` = '10'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `10`,if((`view_stsabsendosbulanan`.`tgl` = '11'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `11`,if((`view_stsabsendosbulanan`.`tgl` = '12'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `12`,if((`view_stsabsendosbulanan`.`tgl` = '13'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `13`,if((`view_stsabsendosbulanan`.`tgl` = '14'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `14`,if((`view_stsabsendosbulanan`.`tgl` = '15'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `15`,if((`view_stsabsendosbulanan`.`tgl` = '16'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `16`,if((`view_stsabsendosbulanan`.`tgl` = '17'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `17`,if((`view_stsabsendosbulanan`.`tgl` = '18'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `18`,if((`view_stsabsendosbulanan`.`tgl` = '19'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `19`,if((`view_stsabsendosbulanan`.`tgl` = '20'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `20`,if((`view_stsabsendosbulanan`.`tgl` = '21'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `21`,if((`view_stsabsendosbulanan`.`tgl` = '22'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `22`,if((`view_stsabsendosbulanan`.`tgl` = '23'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `23`,if((`view_stsabsendosbulanan`.`tgl` = '24'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `24`,if((`view_stsabsendosbulanan`.`tgl` = '25'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `25`,if((`view_stsabsendosbulanan`.`tgl` = '26'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `26`,if((`view_stsabsendosbulanan`.`tgl` = '27'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `27`,if((`view_stsabsendosbulanan`.`tgl` = '28'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `28`,if((`view_stsabsendosbulanan`.`tgl` = '29'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `29`,if((`view_stsabsendosbulanan`.`tgl` = '30'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `30`,if((`view_stsabsendosbulanan`.`tgl` = '31'),`view_stsabsendosbulanan`.`sks_mk`,'-') AS `31`,sum(`view_stsabsendosbulanan`.`sks_mk`) AS `jumlahsks` from `view_stsabsendosbulanan` group by `view_stsabsendosbulanan`.`nm_ptk`,`view_stsabsendosbulanan`.`nm_mk`,`view_stsabsendosbulanan`.`tgl` order by `view_stsabsendosbulanan`.`nm_ptk` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_listdosen`
--
DROP TABLE IF EXISTS `view_listdosen`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_listdosen`  AS  select `dosen`.`nidn` AS `nidn`,`dosen`.`nm_ptk` AS `nm_ptk`,`dosen`.`jk` AS `jk`,if((`dosen`.`id_stat_aktif` = '0'),'Non-Aktif',if((`dosen`.`id_stat_aktif` = '1'),'Aktif','Dosen Tidak Terdaftar')) AS `statusdsn`,`dosen`.`no_hp` AS `no_hp` from `dosen` group by `dosen`.`nidn` ;

-- --------------------------------------------------------

--
-- Structure for view `view_listmahasiswa`
--
DROP TABLE IF EXISTS `view_listmahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_listmahasiswa`  AS  select `view_nimpecah`.`nim` AS `nim`,`view_nimpecah`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,if((`view_nimpecah`.`kdprodi` = '22'),'D3 - Komputer Akuntansi',if((`view_nimpecah`.`kdprodi` = '21'),'D3 - Manajemen Informatika',if((`view_nimpecah`.`kdprodi` = '12'),'S1 - Sistem Informasi',if((`view_nimpecah`.`kdprodi` = '11'),'S1 - Teknik Informatika','Jurusan Tidak Terdaftar')))) AS `prodi`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`id` AS `id` from (`view_nimpecah` join `mahasiswa` on((`view_nimpecah`.`nim` = `mahasiswa`.`nim`))) group by `mahasiswa`.`nim` ;

-- --------------------------------------------------------

--
-- Structure for view `view_matakuliahtemp`
--
DROP TABLE IF EXISTS `view_matakuliahtemp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_matakuliahtemp`  AS  select `view_mkshortdetail`.`kode_mk` AS `kode_mk`,`view_mkshortdetail`.`nm_mk` AS `nm_mk`,`view_mkshortdetail`.`nmajns_mk` AS `nmajns_mk`,`view_mkshortdetail`.`sks_mk` AS `sks_mk`,`view_mkshortdetail`.`prodi` AS `prodi`,`view_mkshortdetail`.`mk_semester` AS `mk_semester`,`view_mkshortdetail`.`kel_mk` AS `kel_mk`,`view_mkshortdetail`.`sks_tatapmuka` AS `sks_tatapmuka`,`view_mkshortdetail`.`metode_pelaksanaan_kuliah` AS `metode_pelaksanaan_kuliah`,`view_mkshortdetail`.`ket_sap` AS `ket_sap`,`view_mkshortdetail`.`ket_silabus` AS `ket_silabus`,`view_mkshortdetail`.`ket_bahanajar` AS `ket_bahanajar`,`view_mkshortdetail`.`ket_acara_prak` AS `ket_acara_prak`,`view_mkshortdetail`.`ket_diktat` AS `ket_diktat` from `view_mkshortdetail` where (not(`view_mkshortdetail`.`kode_mk` in (select `kelas_kuliahfinal`.`kode_mk` from `kelas_kuliahfinal`))) group by `view_mkshortdetail`.`kode_mk` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mata_kuliah`
--
DROP TABLE IF EXISTS `view_mata_kuliah`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mata_kuliah`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`jns_mk` AS `jns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,if((`mata_kuliah`.`kode_mk` like 'K%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Komputer Akuntansi'),if((`mata_kuliah`.`kode_mk` like 'M%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Manajemen Informatika'),if((`mata_kuliah`.`kode_mk` like 'S%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Sistem Informasi'),if((`mata_kuliah`.`kode_mk` like 'T%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Teknik Informatika'),'Jurusan Tidak Terdaftar')))) AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`mata_kuliah`.`metode_pelaksanaan_kuliah` AS `metode_pelaksanaan_kuliah`,`mata_kuliah`.`a_sap` AS `a_sap`,`mata_kuliah`.`a_silabus` AS `a_silabus`,`mata_kuliah`.`a_bahan_ajar` AS `a_bahan_ajar`,`mata_kuliah`.`acara_prak` AS `acara_prak`,`mata_kuliah`.`a_diktat` AS `a_diktat` from ((`mata_kuliah` join `jenjang_pendidikan` on((`mata_kuliah`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) join `jurusan` on((`jurusan`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) group by `mata_kuliah`.`kode_mk` ;

-- --------------------------------------------------------

--
-- Structure for view `view_matrixkul`
--
DROP TABLE IF EXISTS `view_matrixkul`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_matrixkul`  AS  select `view_rancangkuliah_updateindo`.`hari` AS `hari`,`view_rancangkuliah_updateindo`.`waktu` AS `waktu`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Mandrake'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `mandrake`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Trustix'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `trustix`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Suse'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `suse`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Debian'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `debian`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Trustcafe'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `trustcafe`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Fedora'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `fedora`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Knoppix'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `knoppix`,if((`view_rancangkuliah_updateindo`.`nm_ruangan` = 'Redhat'),concat(`view_rancangkuliah_updateindo`.`kode_kelas`,'<br>',`view_rancangkuliah_updateindo`.`nm_mk`,'<br>',`view_rancangkuliah_updateindo`.`nm_ptk`),'-') AS `redhat`,`view_rancangkuliah_updateindo`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_updateindo`.`nm_ptk` AS `nm_ptk`,`view_rancangkuliah_updateindo`.`kode_kelas` AS `kode_kelas` from `view_rancangkuliah_updateindo` where (not((`view_rancangkuliah_updateindo`.`nm_ruangan` like '%Slackware%'))) order by `view_rancangkuliah_updateindo`.`hari` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_mhsaktf`
--
DROP TABLE IF EXISTS `view_mhsaktf`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mhsaktf`  AS  select `view_statusmhs`.`nik` AS `nik`,`view_statusmhs`.`nim` AS `nim`,`view_statusmhs`.`nm_pd` AS `nm_pd`,`view_statusmhs`.`jk` AS `jk`,`view_statusmhs`.`tmpt_lahir` AS `tmpt_lahir`,`view_statusmhs`.`tgl_lahir` AS `tgl_lahir` from `view_statusmhs` where (`view_statusmhs`.`statusmhs` = 'Mahasiswa') ;

-- --------------------------------------------------------

--
-- Structure for view `view_mhscomplete`
--
DROP TABLE IF EXISTS `view_mhscomplete`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mhscomplete`  AS  select `mahasiswa`.`id` AS `id`,`mahasiswa`.`nik` AS `nik`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,`mahasiswa`.`nisn` AS `nisn`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`id_agama` AS `id_agama`,`mahasiswa`.`jln` AS `jln`,`mahasiswa`.`rt` AS `rt`,`mahasiswa`.`rw` AS `rw`,`mahasiswa`.`nm_dsn` AS `nm_dsn`,`mahasiswa`.`ds_kel` AS `ds_kel`,`mahasiswa`.`kode_pos` AS `kode_pos`,`mahasiswa`.`telepon_seluler` AS `telepon_seluler`,`mahasiswa`.`stat_pd` AS `stat_pd`,`mahasiswa`.`nm_ayah` AS `nm_ayah`,`mahasiswa`.`tgl_lahir_ayah` AS `tgl_lahir_ayah`,`view_ttgayahmhs`.`id_pekerjaan_ayah` AS `id_pekerjaan_ayah`,`view_ttgayahmhs`.`pekerjaanayah` AS `pekerjaanayah`,`view_ttgayahmhs`.`id_jenjang_pendidikan_ayah` AS `id_jenjang_pendidikan_ayah`,`view_ttgayahmhs`.`jenjangdidikayah` AS `jenjangdidikayah`,`view_ttgayahmhs`.`id_penghasilan_ayah` AS `id_penghasilan_ayah`,`view_ttgayahmhs`.`penghasilan` AS `penghasilanayah`,`mahasiswa`.`nm_ibu_kandung` AS `nm_ibu_kandung`,`mahasiswa`.`tgl_lahir_ibu` AS `tgl_lahir_ibu`,`view_ttgibumhs`.`id_pekerjaan_ibu` AS `id_pekerjaan_ibu`,`view_ttgibumhs`.`pekerjaanibu` AS `pekerjaanibu`,`view_ttgibumhs`.`id_jenjang_pendidikan_ibu` AS `id_jenjang_pendidikan_ibu`,`view_ttgibumhs`.`jenjangdidikibu` AS `jenjangdidikibu`,`view_ttgibumhs`.`id_penghasilan_ibu` AS `id_penghasilan_ibu`,`view_ttgibumhs`.`penghasilan` AS `penghasilanibu`,`mahasiswa`.`nm_wali` AS `nm_wali`,`mahasiswa`.`tgl_lahir_wali` AS `tgl_lahir_wali`,`view_ttgwalimhs`.`id_pekerjaan_wali` AS `id_pekerjaan_wali`,`view_ttgwalimhs`.`pekerjaanwali` AS `pekerjaanwali`,`view_ttgwalimhs`.`id_jenjang_pendidikan_wali` AS `id_jenjang_pendidikan_wali`,`view_ttgwalimhs`.`jenjangdidikwali` AS `jenjangdidikwali`,`view_ttgwalimhs`.`id_penghasilan_wali` AS `id_penghasilan_wali`,`view_ttgwalimhs`.`penghasilanwali` AS `penghasilanwali`,`mahasiswa`.`kewarganegaraan` AS `kewarganegaraan`,`mahasiswa`.`id_pd` AS `id_pd` from (((`mahasiswa` join `view_ttgayahmhs` on((`view_ttgayahmhs`.`id` = `mahasiswa`.`id`))) join `view_ttgibumhs` on((`view_ttgibumhs`.`id` = `mahasiswa`.`id`))) join `view_ttgwalimhs` on((`view_ttgwalimhs`.`id` = `mahasiswa`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_mhsshort`
--
DROP TABLE IF EXISTS `view_mhsshort`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mhsshort`  AS  select `view_nimpecah`.`nim` AS `nim`,`view_nimpecah`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,if((`view_nimpecah`.`kdprodi` = '22'),'D3 - Komputer Akuntansi',if((`view_nimpecah`.`kdprodi` = '21'),'D3 - Manajemen Informatika',if((`view_nimpecah`.`kdprodi` = '12'),'S1 - Sistem Informasi',if((`view_nimpecah`.`kdprodi` = '11'),'S1 - Teknik Informatika','Jurusan Tidak Terdaftar')))) AS `prodi`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`id` AS `id` from (`view_nimpecah` join `mahasiswa` on((`view_nimpecah`.`nim` = `mahasiswa`.`nim`))) group by `mahasiswa`.`nim` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkpilihan`
--
DROP TABLE IF EXISTS `view_mkpilihan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkpilihan`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`view_mkshort`.`nmajns_mk` = 'Pilihan') ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkpilihanpeminatan`
--
DROP TABLE IF EXISTS `view_mkpilihanpeminatan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkpilihanpeminatan`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`view_mkshort`.`nmajns_mk` = 'Pilihan Peminatan') ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkshort`
--
DROP TABLE IF EXISTS `view_mkshort`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkshort`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,if((`mata_kuliah`.`jns_mk` = 'A'),'Wajib',if((`mata_kuliah`.`jns_mk` = 'B'),'Pilihan',if((`mata_kuliah`.`jns_mk` = 'C'),'Wajib Peminatan',if((`mata_kuliah`.`jns_mk` = 'D'),'Pilihan Peminatan',if((`mata_kuliah`.`jns_mk` = 'S'),'Skripsi/Tugas Akhir/Thesis','Tidak Ada Jenis MK'))))) AS `nmajns_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,if((`mata_kuliah`.`kode_mk` like 'K%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Komputer Akuntansi'),if((`mata_kuliah`.`kode_mk` like 'M%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Manajemen Informatika'),if((`mata_kuliah`.`kode_mk` like 'S%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Sistem Informasi'),if((`mata_kuliah`.`kode_mk` like 'T%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Teknik Informatika'),'Jurusan Tidak Terdaftar')))) AS `prodi`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`metode_pelaksanaan_kuliah` AS `metode_pelaksanaan_kuliah`,`mata_kuliah`.`a_sap` AS `a_sap`,`mata_kuliah`.`a_silabus` AS `a_silabus`,`mata_kuliah`.`a_bahan_ajar` AS `a_bahan_ajar`,`mata_kuliah`.`acara_prak` AS `acara_prak`,`mata_kuliah`.`a_diktat` AS `a_diktat` from ((`mata_kuliah` join `jenjang_pendidikan` on((`mata_kuliah`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) join `jurusan` on((`jurusan`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) group by `mata_kuliah`.`kode_mk` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkshortdetail`
--
DROP TABLE IF EXISTS `view_mkshortdetail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkshortdetail`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,if((`mata_kuliah`.`jns_mk` = 'A'),'Wajib',if((`mata_kuliah`.`jns_mk` = 'B'),'Pilihan',if((`mata_kuliah`.`jns_mk` = 'C'),'Wajib Peminatan',if((`mata_kuliah`.`jns_mk` = 'D'),'Pilihan Peminatan',if((`mata_kuliah`.`jns_mk` = 'S'),'Skripsi/Tugas Akhir/Thesis','Tidak Ada Jenis MK'))))) AS `nmajns_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,if((`mata_kuliah`.`kode_mk` like 'K%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Komputer Akuntansi'),if((`mata_kuliah`.`kode_mk` like 'M%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Manajemen Informatika'),if((`mata_kuliah`.`kode_mk` like 'S%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Sistem Informasi'),if((`mata_kuliah`.`kode_mk` like 'T%'),concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - Teknik Informatika'),'Jurusan Tidak Terdaftar')))) AS `prodi`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`metode_pelaksanaan_kuliah` AS `metode_pelaksanaan_kuliah`,if((`mata_kuliah`.`a_sap` = '1'),'Matakuliah Ini Punya SAP',if((`mata_kuliah`.`a_sap` = '0'),'Matakuliah Ini Tidak Punya SAP','Tidak Ada Jawaban')) AS `ket_sap`,if((`mata_kuliah`.`a_silabus` = '1'),'Matakuliah Ini Punya Silabus',if((`mata_kuliah`.`a_silabus` = '0'),'Matakuliah Ini Tidak Punya Silabus','Tidak Ada Jawaban')) AS `ket_silabus`,if((`mata_kuliah`.`a_bahan_ajar` = '1'),'Matakuliah Ini Punya Bahan Ajar',if((`mata_kuliah`.`a_bahan_ajar` = '0'),'Matakuliah Ini Tidak Punya Bahan Ajar','Tidak Ada Jawaban')) AS `ket_bahanajar`,if((`mata_kuliah`.`acara_prak` = '1'),'Matakuliah Ini Punya Agenda Praktek',if((`mata_kuliah`.`acara_prak` = '0'),'Matakuliah Ini Tidak Punya Agenda Praktek','Tidak Ada Jawaban')) AS `ket_acara_prak`,if((`mata_kuliah`.`a_diktat` = '1'),'Matakuliah Ini Punya Diktat',if((`mata_kuliah`.`a_diktat` = '0'),'Matakuliah Ini Tidak Punya Diktat','Tidak Ada Jawaban')) AS `ket_diktat` from ((`mata_kuliah` join `jenjang_pendidikan` on((`mata_kuliah`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) join `jurusan` on((`jurusan`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) group by `mata_kuliah`.`kode_mk` ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkskripsi`
--
DROP TABLE IF EXISTS `view_mkskripsi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkskripsi`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`view_mkshort`.`nmajns_mk` = 'Skripsi/Tugas Akhir/Thesis') ;

-- --------------------------------------------------------

--
-- Structure for view `view_mksms1`
--
DROP TABLE IF EXISTS `view_mksms1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mksms1`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`mata_kuliah`.`mk_semester` = 1) ;

-- --------------------------------------------------------

--
-- Structure for view `view_mksmtganjil`
--
DROP TABLE IF EXISTS `view_mksmtganjil`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mksmtganjil`  AS  select `view_mata_kuliah`.`kode_mk` AS `kode_mk`,`view_mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mata_kuliah`.`jns_mk` AS `jns_mk`,`view_mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mata_kuliah`.`sks_mk` AS `sks_mk`,`view_mata_kuliah`.`mk_semester` AS `mk_semester` from `view_mata_kuliah` where ((`view_mata_kuliah`.`mk_semester` % 2) = 1) group by `view_mata_kuliah`.`kode_mk` order by `view_mata_kuliah`.`mk_semester` desc,`view_mata_kuliah`.`kode_mk` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_mksmtgenap`
--
DROP TABLE IF EXISTS `view_mksmtgenap`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mksmtgenap`  AS  select `view_mata_kuliah`.`kode_mk` AS `kode_mk`,`view_mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mata_kuliah`.`jns_mk` AS `jns_mk`,`view_mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mata_kuliah`.`sks_mk` AS `sks_mk`,`view_mata_kuliah`.`mk_semester` AS `mk_semester` from `view_mata_kuliah` where ((`view_mata_kuliah`.`mk_semester` % 2) = 0) group by `view_mata_kuliah`.`kode_mk` order by `view_mata_kuliah`.`mk_semester` desc,`view_mata_kuliah`.`kode_mk` desc ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkwajib`
--
DROP TABLE IF EXISTS `view_mkwajib`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkwajib`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`view_mkshort`.`nmajns_mk` = 'Wajib') ;

-- --------------------------------------------------------

--
-- Structure for view `view_mkwajibpeminatan`
--
DROP TABLE IF EXISTS `view_mkwajibpeminatan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_mkwajibpeminatan`  AS  select `mata_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`mata_kuliah`.`kel_mk` AS `kel_mk`,`view_mkshort`.`prodi` AS `prodi`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`sks_tatapmuka` AS `sks_tatapmuka`,`mata_kuliah`.`mk_semester` AS `mk_semester` from (`mata_kuliah` join `view_mkshort` on((`view_mkshort`.`kode_mk` = `mata_kuliah`.`kode_mk`))) where (`view_mkshort`.`nmajns_mk` = 'Wajib Peminatan') ;

-- --------------------------------------------------------

--
-- Structure for view `view_nimpecah`
--
DROP TABLE IF EXISTS `view_nimpecah`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_nimpecah`  AS  select `mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,substr(`mahasiswa`.`nim`,1,4) AS `thnmsuk`,substr(`mahasiswa`.`nim`,6,2) AS `kdprodi`,substr(`mahasiswa`.`nim`,9,3) AS `nmrurut` from `mahasiswa` ;

-- --------------------------------------------------------

--
-- Structure for view `view_paketmkka`
--
DROP TABLE IF EXISTS `view_paketmkka`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_paketmkka`  AS  select `view_mkshort`.`kode_mk` AS `kode_mk`,`view_mkshort`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`view_mkshort`.`sks_mk` AS `sks_mk`,`view_mkshort`.`prodi` AS `prodi`,`view_mkshort`.`mk_semester` AS `mk_semester` from `view_mkshort` where (`view_mkshort`.`prodi` = 'D3 - Komputer Akuntansi') order by `view_mkshort`.`mk_semester` ;

-- --------------------------------------------------------

--
-- Structure for view `view_paketmkmi`
--
DROP TABLE IF EXISTS `view_paketmkmi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_paketmkmi`  AS  select `view_mkshort`.`kode_mk` AS `kode_mk`,`view_mkshort`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`view_mkshort`.`sks_mk` AS `sks_mk`,`view_mkshort`.`prodi` AS `prodi`,`view_mkshort`.`mk_semester` AS `mk_semester` from `view_mkshort` where (`view_mkshort`.`prodi` = 'D3 - Manajemen Informatika') order by `view_mkshort`.`mk_semester` ;

-- --------------------------------------------------------

--
-- Structure for view `view_paketmksi`
--
DROP TABLE IF EXISTS `view_paketmksi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_paketmksi`  AS  select `view_mkshort`.`kode_mk` AS `kode_mk`,`view_mkshort`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`view_mkshort`.`sks_mk` AS `sks_mk`,`view_mkshort`.`prodi` AS `prodi`,`view_mkshort`.`mk_semester` AS `mk_semester` from `view_mkshort` where (`view_mkshort`.`prodi` = 'S1 - Sistem Informasi') order by `view_mkshort`.`mk_semester` ;

-- --------------------------------------------------------

--
-- Structure for view `view_paketmkti`
--
DROP TABLE IF EXISTS `view_paketmkti`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_paketmkti`  AS  select `view_mkshort`.`kode_mk` AS `kode_mk`,`view_mkshort`.`nm_mk` AS `nm_mk`,`view_mkshort`.`nmajns_mk` AS `nmajns_mk`,`view_mkshort`.`sks_mk` AS `sks_mk`,`view_mkshort`.`prodi` AS `prodi`,`view_mkshort`.`mk_semester` AS `mk_semester` from `view_mkshort` where (`view_mkshort`.`prodi` = 'S1 - Teknik Informatika') order by `view_mkshort`.`mk_semester` ;

-- --------------------------------------------------------

--
-- Structure for view `view_prodi`
--
DROP TABLE IF EXISTS `view_prodi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_prodi`  AS  select concat(`jenjang_pendidikan`.`nm_jenj_didik`,' - ',`jurusan`.`nm_jur`) AS `prodi` from (`jenjang_pendidikan` join `jurusan` on((`jurusan`.`id_jenj_didik` = `jenjang_pendidikan`.`id_jenj_didik`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliahtemp`
--
DROP TABLE IF EXISTS `view_rancangkuliahtemp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliahtemp`  AS  select `view_mksmtgenap`.`kode_mk` AS `kode_mk`,`view_mksmtgenap`.`nm_mk` AS `nm_mk`,`view_mksmtgenap`.`jns_mk` AS `jns_mk`,`view_mksmtgenap`.`kel_mk` AS `kel_mk`,`view_mksmtgenap`.`sks_mk` AS `sks_mk`,`view_mksmtgenap`.`mk_semester` AS `mk_semester`,`view_mksmtgenap`.`kode_mk` AS `kode_mksmtgenap`,`view_mksmtgenap`.`nm_mk` AS `nm_mksmtgenap`,`view_mksmtgenap`.`jns_mk` AS `jns_mksmtgenap`,`view_mksmtgenap`.`kel_mk` AS `kel_mksmtgenap`,`view_mksmtgenap`.`sks_mk` AS `sks_mksmtgenap`,`view_mksmtgenap`.`mk_semester` AS `mksemester_mksmtgenap` from `view_mksmtgenap` where (not(`view_mksmtgenap`.`kode_mk` in (select `kelas_kuliah`.`kode_mk` from `kelas_kuliah`))) group by `view_mksmtgenap`.`kode_mk` ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_thnajar`
--
DROP TABLE IF EXISTS `view_rancangkuliah_thnajar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_thnajar`  AS  select `kelas_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliah`.`kode_kelas` AS `kode_kelas`,`dosen`.`nm_ptk` AS `nm_ptk`,if((`kelas_kuliah`.`hari` = 'Minggu'),'Sunday',if((`kelas_kuliah`.`hari` = 'Ahad'),'Sunday',if((`kelas_kuliah`.`hari` = 'Senin'),'Monday',if((`kelas_kuliah`.`hari` = 'Selasa'),'Tuesday',if((`kelas_kuliah`.`hari` = 'Rabu'),'Wednesday',if((`kelas_kuliah`.`hari` = 'Kamis'),'Thursday',if((`kelas_kuliah`.`hari` = 'Jumat'),'Friday',if((`kelas_kuliah`.`hari` = 'Sabtu'),'Saturday','Nama hari Tidak Ada')))))))) AS `hari`,concat(`view_kelaskuliahformatjam`.`jam_mulai`,' - ',`view_kelaskuliahformatjam`.`jam_selesai`) AS `waktu`,`ruang_kelas`.`nm_ruangan` AS `nm_ruangan`,if((`semester`.`smt` = '1'),(substr(now(),1,4) - 1),if((`semester`.`smt` = '2'),substr(now(),1,4),'Semester Tidak Terdaftar')) AS `thn_ajar`,`semester`.`smt` AS `smt`,`kelas_kuliah`.`kode_ruangan` AS `kode_ruangan`,`kelas_kuliah`.`id_smt` AS `id_smt`,`kelas_kuliah`.`nidn` AS `nidn` from (((((`kelas_kuliah` join `semester` on((`kelas_kuliah`.`id_smt` = `semester`.`id_smt`))) join `mata_kuliah` on((`kelas_kuliah`.`kode_mk` = `mata_kuliah`.`kode_mk`))) join `dosen` on((`kelas_kuliah`.`nidn` = `dosen`.`nidn`))) join `ruang_kelas` on((`kelas_kuliah`.`kode_ruangan` = `ruang_kelas`.`kode_ruangan`))) join `view_kelaskuliahformatjam` on(((`view_kelaskuliahformatjam`.`kode_kelas` = `kelas_kuliah`.`kode_kelas`) and (`view_kelaskuliahformatjam`.`id_smt` = `semester`.`id_smt`) and (`view_kelaskuliahformatjam`.`kode_mk` = `mata_kuliah`.`kode_mk`) and (`view_kelaskuliahformatjam`.`nidn` = `dosen`.`nidn`) and (`view_kelaskuliahformatjam`.`kode_ruangan` = `ruang_kelas`.`kode_ruangan`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_thnajarbackup`
--
DROP TABLE IF EXISTS `view_rancangkuliah_thnajarbackup`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_thnajarbackup`  AS  select `kelas_kuliah`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliah`.`kode_kelas` AS `kode_kelas`,`dosen`.`nm_ptk` AS `nm_ptk`,if((`kelas_kuliah`.`hari` = 'Minggu'),'Sunday',if((`kelas_kuliah`.`hari` = 'Ahad'),'Sunday',if((`kelas_kuliah`.`hari` = 'Senin'),'Monday',if((`kelas_kuliah`.`hari` = 'Selasa'),'Tuesday',if((`kelas_kuliah`.`hari` = 'Rabu'),'Wednesday',if((`kelas_kuliah`.`hari` = 'Kamis'),'Thursday',if((`kelas_kuliah`.`hari` = 'Jumat'),'Friday',if((`kelas_kuliah`.`hari` = 'Sabtu'),'Saturday','Nama hari Tidak Ada')))))))) AS `hari`,concat(`kelas_kuliah`.`jam_mulai`,' - ',`kelas_kuliah`.`jam_selesai`) AS `waktu`,`ruang_kelas`.`nm_ruangan` AS `nm_ruangan`,if((`semester`.`smt` = '1'),(substr(now(),1,4) - 1),if((`semester`.`smt` = '2'),substr(now(),1,4),'Semester Tidak Terdaftar')) AS `thn_ajar`,`semester`.`smt` AS `smt`,`kelas_kuliah`.`kode_ruangan` AS `kode_ruangan`,`kelas_kuliah`.`id_smt` AS `id_smt`,`kelas_kuliah`.`nidn` AS `nidn` from ((((`kelas_kuliah` join `semester` on((`kelas_kuliah`.`id_smt` = `semester`.`id_smt`))) join `mata_kuliah` on((`kelas_kuliah`.`kode_mk` = `mata_kuliah`.`kode_mk`))) join `dosen` on((`kelas_kuliah`.`nidn` = `dosen`.`nidn`))) join `ruang_kelas` on((`kelas_kuliah`.`kode_ruangan` = `ruang_kelas`.`kode_ruangan`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_thnajarfinal`
--
DROP TABLE IF EXISTS `view_rancangkuliah_thnajarfinal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_thnajarfinal`  AS  select `kelas_kuliahfinal`.`kode_mk` AS `kode_mk`,`mata_kuliah`.`nm_mk` AS `nm_mk`,`mata_kuliah`.`sks_mk` AS `sks_mk`,`mata_kuliah`.`mk_semester` AS `mk_semester`,`kelas_kuliahfinal`.`kode_kelas` AS `kode_kelas`,`dosen`.`nm_ptk` AS `nm_ptk`,if((`kelas_kuliahfinal`.`hari` = 'Minggu'),'Sunday',if((`kelas_kuliahfinal`.`hari` = 'Ahad'),'Sunday',if((`kelas_kuliahfinal`.`hari` = 'Senin'),'Monday',if((`kelas_kuliahfinal`.`hari` = 'Selasa'),'Tuesday',if((`kelas_kuliahfinal`.`hari` = 'Rabu'),'Wednesday',if((`kelas_kuliahfinal`.`hari` = 'Kamis'),'Thursday',if((`kelas_kuliahfinal`.`hari` = 'Jumat'),'Friday',if((`kelas_kuliahfinal`.`hari` = 'Sabtu'),'Saturday','Nama hari Tidak Ada')))))))) AS `hari` from ((`kelas_kuliahfinal` join `mata_kuliah` on((`kelas_kuliahfinal`.`kode_mk` = `mata_kuliah`.`kode_mk`))) join `dosen` on((`kelas_kuliahfinal`.`nidn` = `dosen`.`nidn`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_update`
--
DROP TABLE IF EXISTS `view_rancangkuliah_update`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_update`  AS  select `view_rancangkuliah_thnajar`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_thnajar`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_thnajar`.`sks_mk` AS `sks_mk`,`view_rancangkuliah_thnajar`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_thnajar`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_thnajar`.`nm_ptk` AS `nm_ptk`,`view_rancangkuliah_thnajar`.`hari` AS `hari`,`view_rancangkuliah_thnajar`.`waktu` AS `waktu`,`view_rancangkuliah_thnajar`.`nm_ruangan` AS `nm_ruangan`,`view_rancangkuliah_thnajar`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_thnajar`.`id_smt` AS `id_smt`,`view_rancangkuliah_thnajar`.`nidn` AS `nidn` from `view_rancangkuliah_thnajar` where (`view_rancangkuliah_thnajar`.`thn_ajar` = substr(now(),1,4)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_updateindo`
--
DROP TABLE IF EXISTS `view_rancangkuliah_updateindo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_updateindo`  AS  select `view_rancangkuliah_thnajar`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_thnajar`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_thnajar`.`sks_mk` AS `sks_mk`,`view_rancangkuliah_thnajar`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_thnajar`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_thnajar`.`nm_ptk` AS `nm_ptk`,if((`view_rancangkuliah_thnajar`.`hari` = 'Sunday'),'Ahad',if((`view_rancangkuliah_thnajar`.`hari` = 'Monday'),'Senin',if((`view_rancangkuliah_thnajar`.`hari` = 'Tuesday'),'Selasa',if((`view_rancangkuliah_thnajar`.`hari` = 'Wednesday'),'Rabu',if((`view_rancangkuliah_thnajar`.`hari` = 'Thursday'),'Kamis',if((`view_rancangkuliah_thnajar`.`hari` = 'Friday'),'Jumat',if((`view_rancangkuliah_thnajar`.`hari` = 'Saturday'),'Sabtu','Tidak Ada Hari Lain'))))))) AS `hari`,`view_rancangkuliah_thnajar`.`waktu` AS `waktu`,`view_rancangkuliah_thnajar`.`nm_ruangan` AS `nm_ruangan`,`view_rancangkuliah_thnajar`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_thnajar`.`id_smt` AS `id_smt`,`view_rancangkuliah_thnajar`.`nidn` AS `nidn`,`semester`.`nm_smt` AS `nm_smt` from (`view_rancangkuliah_thnajar` join `semester` on((`view_rancangkuliah_thnajar`.`id_smt` = `semester`.`id_smt`))) where (`view_rancangkuliah_thnajar`.`thn_ajar` = substr(now(),1,4)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_rancangkuliah_updateindo_copy`
--
DROP TABLE IF EXISTS `view_rancangkuliah_updateindo_copy`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_rancangkuliah_updateindo_copy`  AS  select `view_rancangkuliah_thnajar`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_thnajar`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_thnajar`.`sks_mk` AS `sks_mk`,`view_rancangkuliah_thnajar`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_thnajar`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_thnajar`.`nm_ptk` AS `nm_ptk`,if((`view_rancangkuliah_thnajar`.`hari` = 'Sunday'),'Ahad',if((`view_rancangkuliah_thnajar`.`hari` = 'Monday'),'Senin',if((`view_rancangkuliah_thnajar`.`hari` = 'Tuesday'),'Selasa',if((`view_rancangkuliah_thnajar`.`hari` = 'Wednesday'),'Rabu',if((`view_rancangkuliah_thnajar`.`hari` = 'Thursday'),'Kamis',if((`view_rancangkuliah_thnajar`.`hari` = 'Friday'),'Jumat',if((`view_rancangkuliah_thnajar`.`hari` = 'Saturday'),'Sabtu','Tidak Ada Hari Lain'))))))) AS `hari`,`view_rancangkuliah_thnajar`.`waktu` AS `waktu`,`view_rancangkuliah_thnajar`.`nm_ruangan` AS `nm_ruangan`,`view_rancangkuliah_thnajar`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_thnajar`.`id_smt` AS `id_smt`,`view_rancangkuliah_thnajar`.`nidn` AS `nidn`,`semester`.`nm_smt` AS `nm_smt` from (`view_rancangkuliah_thnajar` join `semester` on((`view_rancangkuliah_thnajar`.`id_smt` = `semester`.`id_smt`))) where (`view_rancangkuliah_thnajar`.`thn_ajar` = substr(now(),1,4)) ;

-- --------------------------------------------------------

--
-- Structure for view `view_ruangan`
--
DROP TABLE IF EXISTS `view_ruangan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ruangan`  AS  select `status_ruang`.`kode_ruangan` AS `kode_ruangan`,`ruang_kelas`.`nm_ruangan` AS `nm_ruangan`,`status_ruang`.`status_ruangan` AS `status_ruangan`,`ruang_kelas`.`posisi` AS `posisi`,`status_ruang`.`ket_penggunaan` AS `ket_penggunaan` from (`ruang_kelas` join `status_ruang` on((`status_ruang`.`kode_ruangan` = `ruang_kelas`.`kode_ruangan`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_statusdosen`
--
DROP TABLE IF EXISTS `view_statusdosen`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_statusdosen`  AS  select `statusdosen`.`nidn` AS `nidn`,`statusdosen`.`nm_ptk` AS `nm_ptk`,if((`statusdosen`.`status` = 'A'),'Aktif','Tidak Aktif') AS `statusdosen` from `statusdosen` ;

-- --------------------------------------------------------

--
-- Structure for view `view_statusklskul`
--
DROP TABLE IF EXISTS `view_statusklskul`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_statusklskul`  AS  select `view_rancangkuliah_update`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_update`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_update`.`nm_ruangan` AS `nm_ruangan`,`view_rancangkuliah_update`.`waktu` AS `waktu` from `view_rancangkuliah_update` where (convert(`view_rancangkuliah_update`.`hari` using utf8mb4) = date_format(now(),'%W')) group by `view_rancangkuliah_update`.`kode_mk`,`view_rancangkuliah_update`.`kode_kelas` ;

-- --------------------------------------------------------

--
-- Structure for view `view_statusmhs`
--
DROP TABLE IF EXISTS `view_statusmhs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_statusmhs`  AS  select `view_jmlmhspecah`.`nik` AS `nik`,`view_jmlmhspecah`.`nim` AS `nim`,`view_jmlmhspecah`.`nm_pd` AS `nm_pd`,`view_jmlmhspecah`.`jk` AS `jk`,`view_jmlmhspecah`.`tmpt_lahir` AS `tmpt_lahir`,`view_jmlmhspecah`.`tgl_lahir` AS `tgl_lahir`,if((`view_jmlmhspecah`.`tahun` = '2006'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2007'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2008'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2009'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2010'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2011'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2012'),'Alumni',if((`view_jmlmhspecah`.`tahun` = '2013'),'Mahasiswa',if((`view_jmlmhspecah`.`tahun` = '2014'),'Mahasiswa',if((`view_jmlmhspecah`.`tahun` = '2015'),'Mahasiswa',if((`view_jmlmhspecah`.`tahun` = '2016'),'Mahasiswa','Bukan Mahasiswa Dan Alumni'))))))))))) AS `statusmhs` from `view_jmlmhspecah` ;

-- --------------------------------------------------------

--
-- Structure for view `view_statusruangan`
--
DROP TABLE IF EXISTS `view_statusruangan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_statusruangan`  AS  select `ruang_kelas`.`nm_ruangan` AS `nm_ruangan`,`ruang_kelas`.`posisi` AS `posisi`,`view_rancangkuliah_update`.`kode_kelas` AS `kode_kelas`,`view_rancangkuliah_update`.`nm_mk` AS `nm_mk`,`view_rancangkuliah_update`.`sks_mk` AS `sks_mk`,`view_rancangkuliah_update`.`waktu` AS `waktu`,if((convert(`view_rancangkuliah_update`.`hari` using utf8mb4) = date_format(now(),'%W')),'Digunakan','Tidak Digunakan') AS `statusruangan`,`view_rancangkuliah_update`.`kode_ruangan` AS `kode_ruangan` from (`ruang_kelas` join `view_rancangkuliah_update` on((`view_rancangkuliah_update`.`kode_ruangan` = `ruang_kelas`.`kode_ruangan`))) where (convert(`view_rancangkuliah_update`.`hari` using utf8mb4) = date_format(now(),'%W')) ;

-- --------------------------------------------------------

--
-- Structure for view `view_stsabsendos`
--
DROP TABLE IF EXISTS `view_stsabsendos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_stsabsendos`  AS  select `absen_dsn`.`tgl_absen` AS `tgl_absen`,`absen_dsn`.`kode_kelas` AS `kode_kelas`,`absen_dsn`.`id_smt` AS `id_smt`,`absen_dsn`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_update`.`nm_mk` AS `nm_mk`,`absen_dsn`.`nidn` AS `nidn`,`view_rancangkuliah_update`.`nm_ptk` AS `nm_ptk`,`absen_dsn`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_update`.`nm_ruangan` AS `nm_ruangan`,`absen_dsn`.`hari` AS `hari`,if((`absen_dsn`.`kehadiran` = 'Hadir'),'Hadir','Tidak Hadir') AS `kehadiran`,`absen_dsn`.`keterangan` AS `keterangan`,`view_rancangkuliah_update`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_update`.`sks_mk` AS `sks_mk`,substr(`absen_dsn`.`tgl_absen`,9,2) AS `tgl`,`view_rancangkuliah_update`.`waktu` AS `waktu` from (`absen_dsn` join `view_rancangkuliah_update` on(((`absen_dsn`.`kode_kelas` = `view_rancangkuliah_update`.`kode_kelas`) and (`absen_dsn`.`kode_mk` = `view_rancangkuliah_update`.`kode_mk`)))) where (convert(`view_rancangkuliah_update`.`hari` using utf8mb4) = date_format(now(),'%W')) ;

-- --------------------------------------------------------

--
-- Structure for view `view_stsabsendosbulanan`
--
DROP TABLE IF EXISTS `view_stsabsendosbulanan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_stsabsendosbulanan`  AS  select `absen_dsn`.`tgl_absen` AS `tgl_absen`,`absen_dsn`.`kode_kelas` AS `kode_kelas`,`absen_dsn`.`id_smt` AS `id_smt`,`absen_dsn`.`kode_mk` AS `kode_mk`,`view_rancangkuliah_update`.`nm_mk` AS `nm_mk`,`absen_dsn`.`nidn` AS `nidn`,`view_rancangkuliah_update`.`nm_ptk` AS `nm_ptk`,`absen_dsn`.`kode_ruangan` AS `kode_ruangan`,`view_rancangkuliah_update`.`nm_ruangan` AS `nm_ruangan`,`absen_dsn`.`hari` AS `hari`,if((`absen_dsn`.`kehadiran` = 'Hadir'),'Hadir','Tidak Hadir') AS `kehadiran`,`absen_dsn`.`keterangan` AS `keterangan`,`view_rancangkuliah_update`.`mk_semester` AS `mk_semester`,`view_rancangkuliah_update`.`sks_mk` AS `sks_mk`,substr(`absen_dsn`.`tgl_absen`,9,2) AS `tgl`,`view_rancangkuliah_update`.`waktu` AS `waktu` from (`absen_dsn` join `view_rancangkuliah_update` on(((`absen_dsn`.`kode_kelas` = `view_rancangkuliah_update`.`kode_kelas`) and (`absen_dsn`.`kode_mk` = `view_rancangkuliah_update`.`kode_mk`)))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_thnajaran`
--
DROP TABLE IF EXISTS `view_thnajaran`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_thnajaran`  AS  select `tahun_ajaran`.`id_thn_ajaran` AS `id_thn_ajaran`,`tahun_ajaran`.`nm_thn_ajaran` AS `nm_thn_ajaran`,if((`tahun_ajaran`.`a_periode_aktif` = '0'),'Closed',if((`tahun_ajaran`.`a_periode_aktif` = '1'),'Open','Tahun Ajaran Tidak Terdaftar')) AS `statusthnajaran`,`tahun_ajaran`.`tgl_mulai` AS `tgl_mulai`,`tahun_ajaran`.`tgl_selesai` AS `tgl_selesai` from `tahun_ajaran` ;

-- --------------------------------------------------------

--
-- Structure for view `view_thnajaransmt`
--
DROP TABLE IF EXISTS `view_thnajaransmt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_thnajaransmt`  AS  select `tahun_ajaran`.`id_thn_ajaran` AS `id_thn_ajaran`,`tahun_ajaran`.`nm_thn_ajaran` AS `nm_thn_ajaran`,`semester`.`nm_smt` AS `nm_smt`,if((`semester`.`smt` = '1'),'Semester Ganjil',if((`semester`.`smt` = '2'),'Semester Genap','Semester Tidak Terdaftar')) AS `semester`,if((`semester`.`a_periode_aktif` = '0'),'Closed',if((`semester`.`a_periode_aktif` = '1'),'Open','Semester Tidak Terdaftar')) AS `keadaan_periode`,`semester`.`tgl_mulai` AS `tgl_mulai`,`semester`.`id_smt` AS `id_smt` from (`tahun_ajaran` join `semester` on((`semester`.`id_thn_ajaran` = `tahun_ajaran`.`id_thn_ajaran`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_ttgayahmhs`
--
DROP TABLE IF EXISTS `view_ttgayahmhs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ttgayahmhs`  AS  select `mahasiswa`.`id` AS `id`,`mahasiswa`.`nik` AS `nik`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`stat_pd` AS `stat_pd`,`mahasiswa`.`nm_ayah` AS `nm_ayah`,`mahasiswa`.`tgl_lahir_ayah` AS `tgl_lahir_ayah`,`mahasiswa`.`id_pekerjaan_ayah` AS `id_pekerjaan_ayah`,`pekerjaan`.`nm_pekerjaan` AS `pekerjaanayah`,`mahasiswa`.`id_jenjang_pendidikan_ayah` AS `id_jenjang_pendidikan_ayah`,`jenjang_pendidikan`.`nm_jenj_didik` AS `jenjangdidikayah`,`mahasiswa`.`id_penghasilan_ayah` AS `id_penghasilan_ayah`,`penghasilan`.`penghasilan` AS `penghasilan` from (((`mahasiswa` join `pekerjaan` on((`mahasiswa`.`id_pekerjaan_ayah` = `pekerjaan`.`id_pekerjaan`))) join `jenjang_pendidikan` on((`mahasiswa`.`id_jenjang_pendidikan_ayah` = `jenjang_pendidikan`.`id_jenj_didik`))) join `penghasilan` on((`mahasiswa`.`id_penghasilan_ayah` = `penghasilan`.`id_penghasilan`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_ttgibumhs`
--
DROP TABLE IF EXISTS `view_ttgibumhs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ttgibumhs`  AS  select `mahasiswa`.`id` AS `id`,`mahasiswa`.`nik` AS `nik`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`stat_pd` AS `stat_pd`,`mahasiswa`.`nm_ibu_kandung` AS `nm_ibu_kandung`,`mahasiswa`.`tgl_lahir_ibu` AS `tgl_lahir_ibu`,`mahasiswa`.`id_pekerjaan_ibu` AS `id_pekerjaan_ibu`,`pekerjaan`.`nm_pekerjaan` AS `pekerjaanibu`,`mahasiswa`.`id_jenjang_pendidikan_ibu` AS `id_jenjang_pendidikan_ibu`,`jenjang_pendidikan`.`nm_jenj_didik` AS `jenjangdidikibu`,`mahasiswa`.`id_penghasilan_ibu` AS `id_penghasilan_ibu`,`penghasilan`.`penghasilan` AS `penghasilan` from (((`mahasiswa` join `pekerjaan` on((`mahasiswa`.`id_pekerjaan_ibu` = `pekerjaan`.`id_pekerjaan`))) join `jenjang_pendidikan` on((`mahasiswa`.`id_jenjang_pendidikan_ibu` = `jenjang_pendidikan`.`id_jenj_didik`))) join `penghasilan` on((`mahasiswa`.`id_penghasilan_ibu` = `penghasilan`.`id_penghasilan`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_ttgwalimhs`
--
DROP TABLE IF EXISTS `view_ttgwalimhs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ttgwalimhs`  AS  select `mahasiswa`.`id` AS `id`,`mahasiswa`.`nik` AS `nik`,`mahasiswa`.`nim` AS `nim`,`mahasiswa`.`nm_pd` AS `nm_pd`,`mahasiswa`.`jk` AS `jk`,`mahasiswa`.`tmpt_lahir` AS `tmpt_lahir`,`mahasiswa`.`tgl_lahir` AS `tgl_lahir`,`mahasiswa`.`stat_pd` AS `stat_pd`,`mahasiswa`.`nm_wali` AS `nm_wali`,`mahasiswa`.`tgl_lahir_wali` AS `tgl_lahir_wali`,`mahasiswa`.`id_pekerjaan_wali` AS `id_pekerjaan_wali`,`pekerjaan`.`nm_pekerjaan` AS `pekerjaanwali`,`mahasiswa`.`id_jenjang_pendidikan_wali` AS `id_jenjang_pendidikan_wali`,`jenjang_pendidikan`.`nm_jenj_didik` AS `jenjangdidikwali`,`mahasiswa`.`id_penghasilan_wali` AS `id_penghasilan_wali`,`penghasilan`.`penghasilan` AS `penghasilanwali` from (((`mahasiswa` join `pekerjaan` on((`mahasiswa`.`id_pekerjaan_wali` = `pekerjaan`.`id_pekerjaan`))) join `jenjang_pendidikan` on((`mahasiswa`.`id_jenjang_pendidikan_wali` = `jenjang_pendidikan`.`id_jenj_didik`))) join `penghasilan` on((`mahasiswa`.`id_penghasilan_wali` = `penghasilan`.`id_penghasilan`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen_dsn`
--
ALTER TABLE `absen_dsn`
  ADD PRIMARY KEY (`tgl_absen`,`kode_kelas`,`id_smt`,`kode_mk`);

--
-- Indexes for table `agama`
--
ALTER TABLE `agama`
  ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nidn`);

--
-- Indexes for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  ADD PRIMARY KEY (`id_jenj_didik`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jur`),
  ADD KEY `id_jenj_didik` (`id_jenj_didik`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `kelas_kuliah`
--
ALTER TABLE `kelas_kuliah`
  ADD PRIMARY KEY (`kode_kelas`,`id_smt`,`kode_mk`),
  ADD KEY `kode_ruangan` (`kode_ruangan`),
  ADD KEY `nidn` (`nidn`),
  ADD KEY `kode_mk` (`kode_mk`),
  ADD KEY `id_smt` (`id_smt`);

--
-- Indexes for table `kelas_kuliahfinal`
--
ALTER TABLE `kelas_kuliahfinal`
  ADD PRIMARY KEY (`kode_kelas`,`id_smt`,`kode_mk`),
  ADD KEY `kode_ruangan` (`kode_ruangan`),
  ADD KEY `nidn` (`nidn`),
  ADD KEY `kode_mk` (`kode_mk`),
  ADD KEY `id_smt` (`id_smt`);

--
-- Indexes for table `kelas_kuliahtemp`
--
ALTER TABLE `kelas_kuliahtemp`
  ADD PRIMARY KEY (`kode_kelas`,`id_smt`,`kode_mk`),
  ADD KEY `kode_mk` (`kode_mk`),
  ADD KEY `id_smt` (`id_smt`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_penghasilan_ayah` (`id_penghasilan_ayah`),
  ADD KEY `id_pekerjaan_ayah` (`id_pekerjaan_ayah`),
  ADD KEY `id_jenjang_pendidikan_ayah` (`id_jenjang_pendidikan_ayah`),
  ADD KEY `id_penghasilan_ibu` (`id_penghasilan_ibu`),
  ADD KEY `id_pekerjaan_ibu` (`id_pekerjaan_ibu`),
  ADD KEY `id_jenjang_pendidikan_ibu` (`id_jenjang_pendidikan_ibu`),
  ADD KEY `id_penghasilan_wali` (`id_penghasilan_wali`),
  ADD KEY `id_pekerjaan_wali` (`id_pekerjaan_wali`),
  ADD KEY `id_jenjang_pendidikan_wali` (`id_jenjang_pendidikan_wali`);

--
-- Indexes for table `mahasiswa_pt`
--
ALTER TABLE `mahasiswa_pt`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`kode_mk`,`id_jenj_didik`),
  ADD KEY `fk_matakuliah_matakuliahbidang` (`id_jenj_didik`);

--
-- Indexes for table `mata_kuliah_kurlama`
--
ALTER TABLE `mata_kuliah_kurlama`
  ADD PRIMARY KEY (`kode_mk`,`id_jenj_didik`),
  ADD KEY `fk_matakuliah_matakuliahbidang` (`id_jenj_didik`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `penghasilan`
--
ALTER TABLE `penghasilan`
  ADD PRIMARY KEY (`id_penghasilan`);

--
-- Indexes for table `ruang_kelas`
--
ALTER TABLE `ruang_kelas`
  ADD PRIMARY KEY (`kode_ruangan`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_smt`,`id_thn_ajaran`),
  ADD KEY `id_thn_ajaran` (`id_thn_ajaran`);

--
-- Indexes for table `statusdosen`
--
ALTER TABLE `statusdosen`
  ADD PRIMARY KEY (`nidn`);

--
-- Indexes for table `status_ruang`
--
ALTER TABLE `status_ruang`
  ADD PRIMARY KEY (`tgl_skrg`,`kode_ruangan`),
  ADD KEY `kode_ruangan` (`kode_ruangan`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_thn_ajaran`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1055;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_ibfk_1` FOREIGN KEY (`id_jenj_didik`) REFERENCES `jenjang_pendidikan` (`id_jenj_didik`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelas_kuliah`
--
ALTER TABLE `kelas_kuliah`
  ADD CONSTRAINT `kelas_kuliah_ibfk_2` FOREIGN KEY (`kode_ruangan`) REFERENCES `ruang_kelas` (`kode_ruangan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliah_ibfk_5` FOREIGN KEY (`nidn`) REFERENCES `dosen` (`nidn`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliah_ibfk_6` FOREIGN KEY (`kode_mk`) REFERENCES `mata_kuliah` (`kode_mk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliah_ibfk_7` FOREIGN KEY (`id_smt`) REFERENCES `semester` (`id_smt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliah_ibfk_8` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelas_kuliahfinal`
--
ALTER TABLE `kelas_kuliahfinal`
  ADD CONSTRAINT `kelas_kuliahfinal_ibfk_1` FOREIGN KEY (`kode_ruangan`) REFERENCES `ruang_kelas` (`kode_ruangan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahfinal_ibfk_2` FOREIGN KEY (`nidn`) REFERENCES `dosen` (`nidn`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahfinal_ibfk_3` FOREIGN KEY (`kode_mk`) REFERENCES `mata_kuliah` (`kode_mk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahfinal_ibfk_4` FOREIGN KEY (`id_smt`) REFERENCES `semester` (`id_smt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahfinal_ibfk_5` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kelas_kuliahtemp`
--
ALTER TABLE `kelas_kuliahtemp`
  ADD CONSTRAINT `kelas_kuliahtemp_ibfk_3` FOREIGN KEY (`kode_mk`) REFERENCES `mata_kuliah` (`kode_mk`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahtemp_ibfk_4` FOREIGN KEY (`id_smt`) REFERENCES `semester` (`id_smt`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kelas_kuliahtemp_ibfk_5` FOREIGN KEY (`kode_kelas`) REFERENCES `kelas` (`kode_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_penghasilan_ayah`) REFERENCES `penghasilan` (`id_penghasilan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`id_pekerjaan_ayah`) REFERENCES `pekerjaan` (`id_pekerjaan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_3` FOREIGN KEY (`id_jenjang_pendidikan_ayah`) REFERENCES `jenjang_pendidikan` (`id_jenj_didik`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_4` FOREIGN KEY (`id_penghasilan_ibu`) REFERENCES `penghasilan` (`id_penghasilan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_5` FOREIGN KEY (`id_pekerjaan_ibu`) REFERENCES `pekerjaan` (`id_pekerjaan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_6` FOREIGN KEY (`id_jenjang_pendidikan_ibu`) REFERENCES `jenjang_pendidikan` (`id_jenj_didik`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_7` FOREIGN KEY (`id_penghasilan_wali`) REFERENCES `penghasilan` (`id_penghasilan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_8` FOREIGN KEY (`id_pekerjaan_wali`) REFERENCES `pekerjaan` (`id_pekerjaan`) ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_9` FOREIGN KEY (`id_jenjang_pendidikan_wali`) REFERENCES `jenjang_pendidikan` (`id_jenj_didik`) ON UPDATE CASCADE;

--
-- Constraints for table `semester`
--
ALTER TABLE `semester`
  ADD CONSTRAINT `semester_ibfk_1` FOREIGN KEY (`id_thn_ajaran`) REFERENCES `tahun_ajaran` (`id_thn_ajaran`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `status_ruang`
--
ALTER TABLE `status_ruang`
  ADD CONSTRAINT `status_ruang_ibfk_1` FOREIGN KEY (`kode_ruangan`) REFERENCES `ruang_kelas` (`kode_ruangan`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
