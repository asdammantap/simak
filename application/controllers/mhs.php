<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mhs extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function mahasiswa()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_mhs');
			$data['data']=$this->modul_mhs->viewmhs();
			$this->load->view('admin/mahasiswa/adm_listmhs',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_mhs()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_mhs');
			$data['data']=$this->modul_mhs->viewmhs();
			$this->load->view('admin/mahasiswa/adm_addmhs',$data);
		}
		else {
			redirect('');
		}
	}
		public function savemhs(){
		$this->form_validation->set_rules('nim','NIM','required');
		$this->form_validation->set_rules('nm_pd','Nama Mahasiswa','required');
		$this->form_validation->set_rules('jk','Jenis Kelamin','required');
		$this->form_validation->set_rules('telepon_seluler','No. Telp','required');
		$this->form_validation->set_rules('tmpt_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('id_agama','Agama','required');
		$this->form_validation->set_rules('stat_pd','Status Perkuliahan','required');
		$this->form_validation->set_rules('jln','Jalan','required');
		//$this->form_validation->set_rules('nm_dsn','Nama Dusun','required');
		//$this->form_validation->set_rules('rt','RT','required');
		//$this->form_validation->set_rules('rw','RW','required');
		$this->form_validation->set_rules('ds_kel','Nama Desa / Kelurahan','required');
		$this->form_validation->set_rules('kode_pos','Kode Pos','required');
		$this->form_validation->set_rules('nm_ayah','Nama Ayah','required');
		$this->form_validation->set_rules('nm_ibu_kandung','Nama Ibu','required');
		$this->form_validation->set_rules('nm_wali','Nama Wali','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		$data = array(
				  'nim' =>$this->input->post('nim'),
				  'nm_pd' =>$this->input->post('nm_pd'),
				  'jk' =>$this->input->post('jk'),
				  'nisn' =>$this->input->post('nisn'),
				  'nik' =>$this->input->post('nik'),
				  'tmpt_lahir' =>$this->input->post('tmpt_lahir'),
				  'tgl_lahir' =>$this->input->post('tgl_lahir'),
				  'id_agama' =>$this->input->post('id_agama'),
				  'jln' =>$this->input->post('jln'),
				  'rt' =>$this->input->post('rt'),
				  'rw' =>$this->input->post('rw'),
				  'nm_dsn' =>$this->input->post('nm_dsn'),
				  'ds_kel' =>$this->input->post('ds_kel'),
				  'kode_pos' =>$this->input->post('kode_pos'),
				  'telepon_seluler' =>$this->input->post('telepon_seluler'),
				  'stat_pd' =>$this->input->post('stat_pd'),
				  'nm_ayah' =>$this->input->post('nm_ayah'),
				  'tgl_lahir_ayah' =>$this->input->post('tgl_lahir_ayah'),
				  'id_jenjang_pendidikan_ayah' =>$this->input->post('id_jenjang_pendidikan_ayah'),
				  'id_pekerjaan_ayah' =>$this->input->post('id_pekerjaan_ayah'),
				  'id_penghasilan_ayah' =>$this->input->post('id_penghasilan_ayah'),
				  'nm_ibu_kandung' =>$this->input->post('nm_ibu_kandung'),
				  'tgl_lahir_ibu' =>$this->input->post('tgl_lahir_ibu'),
				  'id_jenjang_pendidikan_ibu' =>$this->input->post('id_jenjang_pendidikan_ibu'),
				  'id_pekerjaan_ibu' =>$this->input->post('id_pekerjaan_ibu'),
				  'id_penghasilan_ibu' =>$this->input->post('id_penghasilan_ibu'),
				  'nm_wali' =>$this->input->post('nm_wali'),
				  'tgl_lahir_wali' =>$this->input->post('tgl_lahir_wali'),
				  'id_jenjang_pendidikan_wali' =>$this->input->post('id_jenjang_pendidikan_wali'),
				  'id_pekerjaan_wali' =>$this->input->post('id_pekerjaan_wali'),
				  'id_penghasilan_wali' =>$this->input->post('id_penghasilan_wali'),
				  'kewarganegaraan' =>$this->input->post('kewarganegaraan')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_mhs');
				$this->modul_mhs->get_insertmhs($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Berhasil Disimpan!!</div></div>");
                redirect('../mhs/mahasiswa'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Gagal Disimpan!!</div></div>");
                redirect('../mhs/add_mhs'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	public function editmhs($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_mhs');
		$data['data']=$this->modul_mhs->get_editmhs($id);
		$this->load->view('admin/mahasiswa/adm_editmhs',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditmhs() { 
		$this->form_validation->set_rules('nim','NIM','required');
		$this->form_validation->set_rules('nm_pd','Nama Mahasiswa','required');
		//$this->form_validation->set_rules('jk','Jenis Kelamin','required');
		//$this->form_validation->set_rules('telepon_seluler','No. Telp','required');
		//$this->form_validation->set_rules('tmpt_lahir','Tempat Lahir','required');
		//$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
		//$this->form_validation->set_rules('id_agama','Agama','required');
		//$this->form_validation->set_rules('stat_pd','Status Perkuliahan','required');
		//$this->form_validation->set_rules('jln','Jalan','required');
		//$this->form_validation->set_rules('nm_dsn','Nama Dusun','required');
		//$this->form_validation->set_rules('rt','RT','required');
		//$this->form_validation->set_rules('rw','RW','required');
		//$this->form_validation->set_rules('ds_kel','Nama Desa / Kelurahan','required');
		//$this->form_validation->set_rules('kode_pos','Kode Pos','required');
		//$this->form_validation->set_rules('nm_ayah','Nama Ayah','required');
		//$this->form_validation->set_rules('nm_ibu_kandung','Nama Ibu','required');
		//$this->form_validation->set_rules('nm_wali','Nama Wali','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_mhs','',TRUE); 
            $this->modul_mhs->moduleditmhs(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../mhs/mahasiswa'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../mhs/mahasiswa'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	public function hapusmhs($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_mhs','',TRUE); 
		$data['data']=$this->modul_mhs->hapus_mhs($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../mhs/mahasiswa');
            }
		$this->load->view('admin/mahasiswa/adm_listmhs', $data);
	}
	
}

# nama file home.php
# folder apllication/controller/