<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dsn extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function dosen()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_dsn');
			$data['data']=$this->modul_dsn->viewdsn();
			$this->load->view('admin/dosen/adm_listdsn',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_dsn()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_dsn');
			$data['data']=$this->modul_dsn->viewdsn();
			$this->load->view('admin/dosen/adm_adddsn',$data);
		}
		else {
			redirect('');
		}
	}
		public function savedsn(){
		$this->form_validation->set_rules('nidn','NIDN','required');
		$this->form_validation->set_rules('nm_ptk','Nama Dosen','required');
		$this->form_validation->set_rules('jk','Jenis Kelamin','required');
		$this->form_validation->set_rules('no_hp','No. HP','required');
		$this->form_validation->set_rules('tmpt_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('id_agama','Agama','required');
		$this->form_validation->set_rules('id_stat_aktif','Status Dosen','required');
		$this->form_validation->set_rules('jln','Jalan','required');
		//$this->form_validation->set_rules('nm_dsn','Nama Dusun','required');
		//$this->form_validation->set_rules('rt','RT','required');
		//$this->form_validation->set_rules('rw','RW','required');
		$this->form_validation->set_rules('ds_kel','Nama Desa / Kelurahan','required');
		$this->form_validation->set_rules('kode_pos','Kode Pos','required');
		//$this->form_validation->set_rules('nm_ibu_kandung','Nama Ibu','required');
		//$this->form_validation->set_rules('nm_wali','Nama Wali','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		$data = array(
				  'nidn' =>$this->input->post('nidn'),
				  'nm_ptk' =>$this->input->post('nm_ptk'),
				  'jk' =>$this->input->post('jk'),
				  'nip' =>$this->input->post('nip'),
				  'nik' =>$this->input->post('nik'),
				  'npwp' =>$this->input->post('npwp'),
				  'stat_kawin' =>$this->input->post('stat_kawin'),
				  'tmpt_lahir' =>$this->input->post('tmpt_lahir'),
				  'tgl_lahir' =>$this->input->post('tgl_lahir'),
				  'id_agama' =>$this->input->post('id_agama'),
				  'jln' =>$this->input->post('jln'),
				  'rt' =>$this->input->post('rt'),
				  'rw' =>$this->input->post('rw'),
				  'nm_dsn' =>$this->input->post('nm_dsn'),
				  'ds_kel' =>$this->input->post('ds_kel'),
				  'kode_pos' =>$this->input->post('kode_pos'),
				  'no_hp' =>$this->input->post('no_hp'),
				  'id_stat_aktif' =>$this->input->post('id_stat_aktif'),
				  'nm_ibu_kandung' =>$this->input->post('nm_ibu_kandung'),
				  'nm_suami_istri' =>$this->input->post('nm_wali'),
				  'nip_suami_istri' =>$this->input->post('nip_suami_istri'),
				  'id_pekerjaan_suami_istri' =>$this->input->post('id_pekerjaan_suami_istri'),
				  'sk_cpns' =>$this->input->post('sk_cpns'),
				  'sk_angkat' =>$this->input->post('sk_angkat'),
				  'tgl_sk_cpns' =>$this->input->post('tgl_sk_cpns'),
				  'tmt_sk_angkat' =>$this->input->post('tmt_sk_angkat'),
				  'id_pangkat_gol' =>$this->input->post('id_pangkat_gol'),
				  'tmt_pns' =>$this->input->post('tmt_pns'),
				  'kewarganegaraan' =>$this->input->post('kewarganegaraan')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_dsn');
				$this->modul_dsn->get_insertdsn($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Berhasil Disimpan!!</div></div>");
                redirect('../dsn/dosen'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Gagal Disimpan!!</div></div>");
                redirect('../dsn/add_dsn'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	function proseseditdsn() { 
		$this->form_validation->set_rules('nidn','NIDN','required');
		$this->form_validation->set_rules('nm_ptk','Nama Dosen','required');
		$this->form_validation->set_rules('jk','Jenis Kelamin','required');
		$this->form_validation->set_rules('no_hp','No. HP','required');
		$this->form_validation->set_rules('tmpt_lahir','Tempat Lahir','required');
		$this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
		$this->form_validation->set_rules('id_agama','Agama','required');
		$this->form_validation->set_rules('id_stat_aktif','Status Dosen','required');
		$this->form_validation->set_rules('jln','Jalan','required');
		//$this->form_validation->set_rules('nm_dsn','Nama Dusun','required');
		//$this->form_validation->set_rules('rt','RT','required');
		//$this->form_validation->set_rules('rw','RW','required');
		$this->form_validation->set_rules('ds_kel','Nama Desa / Kelurahan','required');
		$this->form_validation->set_rules('kode_pos','Kode Pos','required');
		//$this->form_validation->set_rules('nm_ibu_kandung','Nama Ibu','required');
		//$this->form_validation->set_rules('nm_wali','Nama Wali','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_dsn','',TRUE); 
            $this->modul_dsn->moduleditdsn(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../dsn/dosen'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../mhs/dosen'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	public function editdsn($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_dsn');
		$data['data']=$this->modul_dsn->get_editdsn($id);
		$this->load->view('admin/dosen/adm_editdsn',$data);
		}
		else {
			redirect('');
		}
	}
	
	public function hapusdsn($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_dsn','',TRUE); 
		$data['data']=$this->modul_dsn->hapus_dsn($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../dsn/dosen');
            }
		$this->load->view('admin/dosen/adm_listdsn', $data);
	}
	
	
}

# nama file home.php
# folder apllication/controller/