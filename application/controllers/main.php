<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function dashboard()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_mhs');
			$this->load ->model('modul_dsn');
			$this->load ->model('modul_mk');
			$this->load ->model('modul_ruangan');
			$this->load ->model('modul_semester');
			$data['report'] = $this->modul_mhs->report();
			$data['tampiljmlmkbaru']=$this->modul_mk->tampiljumlahmkbaru();
			$data['tampiljmlmhsaktf']=$this->modul_mhs->tampiljumlahmhsaktf();
			$data['tampiljmlalumni']=$this->modul_mhs->tampiljumlahalumni();
			$data['tampiljmldsnall']=$this->modul_dsn->tampiljumlahdsnall();
			$data['stsruangan']=$this->modul_ruangan->tampilstsruangan();
			$data['klskul']=$this->modul_semester->tampilklskul();
			$this->load->view('admin/adm_home',$data);
		}
		else {
			redirect('');
		}
	}
	public function logout() {
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('level');
		session_destroy();
		redirect('');
	}
	
}

# nama file home.php
# folder apllication/controller/