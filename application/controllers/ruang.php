<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ruang extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function ruangan()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_ruangan');
			$data['data']=$this->modul_ruangan->viewruangan();
			$this->load->view('admin/ruangan/adm_listruangan',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_ruangan()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_ruangan');
			$data['data']=$this->modul_ruangan->viewruangan();
			$this->load->view('admin/ruangan/adm_addruangan',$data);
		}
		else {
			redirect('');
		}
	}
	public function saveruangan(){
		$this->form_validation->set_rules('kode_ruangan','Kode Ruangan','required');
		$this->form_validation->set_rules('nm_ruangan','Nama Ruangan','required');
		$this->form_validation->set_rules('posisi','Posisi','required');
		//$this->form_validation->set_rules('jumlah_mhs','Jumlah Mahasiswa','required');
		$data = array(
				  'kode_ruangan' =>$this->input->post('kode_ruangan'),
				  'nm_ruangan' =>$this->input->post('nm_ruangan'),
				  'posisi' =>$this->input->post('posisi')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_ruangan');
				$this->modul_ruangan->get_insertruangan($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Berhasil Disimpan!!</div></div>");
                redirect('../ruang/ruangan'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Gagal Disimpan!!</div></div>");
                redirect('../ruang/add_ruangan'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	public function editruangan($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_ruangan');
		$data['data']=$this->modul_ruangan->get_editruangan($id);
		$this->load->view('admin/ruangan/adm_editruangan',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditruangan() { 
		$this->form_validation->set_rules('kode_ruangan','Kode Ruangan','required');
		$this->form_validation->set_rules('nm_ruangan','Nama Ruangan','required');
		$this->form_validation->set_rules('posisi','Posisi','required');
		//$this->form_validation->set_rules('jumlah_mhs','Jumlah Mahasiswa','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_ruangan','',TRUE); 
            $this->modul_ruangan->moduleditruangan(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../ruang/ruangan'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../ruang/ruangan'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	
}

# nama file home.php
# folder apllication/controller/