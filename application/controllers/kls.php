<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kls extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function kelas()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_kelas');
			$data['data']=$this->modul_kelas->viewkelas();
			$this->load->view('admin/kelas/adm_listkls',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_kls()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_kelas');
			$data['data']=$this->modul_kelas->viewkelas();
			$this->load->view('admin/kelas/adm_addkls',$data);
		}
		else {
			redirect('');
		}
	}
	public function savekls(){
		$this->form_validation->set_rules('kode_kelas','Kelas','required');
		$this->form_validation->set_rules('tahun_msk','Tahun Masuk','required');
		$this->form_validation->set_rules('semester_skrg','Semester','required');
		$this->form_validation->set_rules('jumlah_mhs','Jumlah Mahasiswa','required');
		$data = array(
				  'kode_kelas' =>$this->input->post('kode_kelas'),
				  'tahun_msk' =>$this->input->post('tahun_msk'),
				  'semester_skrg' =>$this->input->post('semester_skrg'),
				  'jumlah_mhs' =>$this->input->post('jumlah_mhs')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_kelas');
				$this->modul_kelas->get_insertkls($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Berhasil Disimpan!!</div></div>");
                redirect('../kls/kelas'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Gagal Disimpan!!</div></div>");
                redirect('../kls/add_kls'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	public function editkelas($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_kelas');
		$data['data']=$this->modul_kelas->get_editkls($id);
		$this->load->view('admin/kelas/adm_editkls',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditkls() { 
		$this->form_validation->set_rules('kode_kelas','Kelas','required');
		$this->form_validation->set_rules('tahun_msk','Tahun Masuk','required');
		$this->form_validation->set_rules('semester_skrg','Semester','required');
		$this->form_validation->set_rules('jumlah_mhs','Jumlah Mahasiswa','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_kelas','',TRUE); 
            $this->modul_kelas->moduleditkls(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../kls/kelas'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../kls/kelas'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	public function hapuskelas($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_kelas','',TRUE); 
		$data['data']=$this->modul_kelas->hapus_kls($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../kls/kelas');
            }
		$this->load->view('admin/kelas/adm_listkls', $data);
	}
}

# nama file home.php
# folder apllication/controller/