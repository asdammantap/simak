<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mk extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function matakuliah()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_mk');
			$data['data']=$this->modul_mk->viewmk();
			$data['tampilmkwajib']=$this->modul_mk->tampiljumlahmkwajib();
			$data['tampilmkpilihan']=$this->modul_mk->tampiljumlahmkpilihan();
			$data['tampilmkwajibminat']=$this->modul_mk->tampiljumlahmkwajibminat();
			$data['tampilmkpilihanminat']=$this->modul_mk->tampiljumlahmkpilihanminat();
			$data['tampilmkskripsi']=$this->modul_mk->tampiljumlahmkskripsi();
			$this->load->view('admin/matakuliah/adm_listmk',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_mk()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_mk');
			$data['data']=$this->modul_mk->viewmk();
			$this->load->view('admin/matakuliah/adm_addmk',$data);
		}
		else {
			redirect('');
		}
	}
		public function savemk(){
		$this->form_validation->set_rules('kode_mk','Kode Matakuliah','required');
		$this->form_validation->set_rules('nm_mk','Kode Matakuliah','required');
		$this->form_validation->set_rules('sks_mk','Kode Matakuliah','required');
		$this->form_validation->set_rules('sks_tatapmuka','Kode Matakuliah','required');
		$this->form_validation->set_rules('mk_semester','Kode Matakuliah','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		$data = array(
				  'kode_mk' =>$this->input->post('kode_mk'),
				  'nm_mk' =>$this->input->post('nm_mk'),
				  'jns_mk' =>$this->input->post('jns_mk'),
				  'kel_mk' =>$this->input->post('kel_mk'),
				  'id_jenj_didik' =>$this->input->post('id_jenj_didik'),
				  'sks_mk' =>$this->input->post('sks_mk'),
				  'sks_tatapmuka' =>$this->input->post('sks_tatapmuka'),
				  'mk_semester' =>$this->input->post('mk_semester'),
				  'metode_pelaksanaan_kuliah' =>$this->input->post('metode_pelaksanaan_kuliah'),
				  'a_sap' =>$this->input->post('a_sap'),
				  'a_silabus' =>$this->input->post('a_silabus'),
				  'a_bahan_ajar' =>$this->input->post('a_bahan_ajar'),
				  'acara_prak' =>$this->input->post('acara_prak'),
				  'a_diktat' =>$this->input->post('a_diktat')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_mk');
				$this->modul_mk->get_insertmk($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Berhasil Disimpan!!</div></div>");
                redirect('../mk/matakuliah'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Gagal Disimpan!!</div></div>");
                redirect('../mk/add_mk'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	public function editmk($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_mk');
		$data['data']=$this->modul_mk->get_editmk($id);
		$this->load->view('admin/matakuliah/adm_editmk',$data);
		}
		else {
			redirect('');
		}
	}
	function proseseditmk() { 
		$this->form_validation->set_rules('kode_mk','Kode Matakuliah','required');
		$this->form_validation->set_rules('nm_mk','Nama Matakuliah','required');
		$this->form_validation->set_rules('sks_mk','SKS Matakuliah','required');
		$this->form_validation->set_rules('sks_tatapmuka','SKS Tatap Muka','required');
		$this->form_validation->set_rules('mk_semester','Semester','required');
		//$this->form_validation->set_rules('metode_pelaksanaan_kuliah','Kode Matakuliah','required');
		//$this->form_validation->set_rules('acara_prak','Kode Matakuliah','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_mk','',TRUE); 
            $this->modul_mk->moduleditmk(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../mk/matakuliah'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Update
				</div>
			 	');
				redirect('../mk/matakuliah'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
	public function hapusmk($id)
	{
	    
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_mk','',TRUE); 
		$data['data']=$this->modul_mk->hapus_mk($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../mk/matakuliah');
            }
		$this->load->view('admin/matakuliah/adm_listmk', $data);
	}
	
}

# nama file home.php
# folder apllication/controller/