<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Smt extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url','form')); //load helper url 
		$this->load->library('form_validation'); //load form validation
    }
	/**
	 * Cotoh penggunaan bootstrap pada codeigniter::index()
	 */
	public function tahunajaran()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_semester');
			$data['thnajar']=$this->modul_semester->viewtahunajaran();
			$this->load->view('admin/tahunajaran/adm_listthnajaran',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_thnajar()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_semester');
			$data['data']=$this->modul_semester->viewtahunajaran();
			$data['tampilthnajar']=$this->modul_semester->getthnajar();
			$this->load->view('admin/tahunajaran/adm_addthnajaran',$data);
		}
		else {
			redirect('');
		}
	}
	public function editthnajaran($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_semester');
		$data['datathnajar']=$this->modul_semester->get_idthnajar($id);
		$this->load->view('admin/tahunajaran/adm_editthnajaran',$data);
		}
		else {
			redirect('');
		}
	}
	public function rancangkul()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_semester');
			$data['smt']=$this->modul_semester->viewsemesteropen();
			//$data['thnajar']=$this->modul_semester->viewtahunajaran();
			$this->load->view('admin/tahunajaran/adm_listsmt',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_semester($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_semester');
			$data['data']=$this->modul_semester->viewsemester();
			$data['datathnajar']=$this->modul_semester->get_idthnajar($id);
			$data['tampilthnajar']=$this->modul_semester->getthnajar();
			$this->load->view('admin/tahunajaran/adm_addsemester',$data);
		}
		else {
			redirect('');
		}
	}
	public function savesemester(){
		$this->form_validation->set_rules('id_smt','ID Tahun Ajaran','required');
		$this->form_validation->set_rules('nm_smt','Deskripsi Semester','required');
		$this->form_validation->set_rules('smt','Semester','required');
		$this->form_validation->set_rules('tgl_mulai','Tanggal Mulai','required');
		$this->form_validation->set_rules('a_periode_aktif','Status Periode','required');
		$data = array(
				  'id_smt' =>$this->input->post('id_smt'),
				  'id_thn_ajaran' =>$this->input->post('id_thn_ajaran'),
				  'nm_smt' =>$this->input->post('nm_smt'),
				  'smt' =>$this->input->post('smt'),
				  'a_periode_aktif' =>$this->input->post('a_periode_aktif'),
				  'tgl_mulai' =>$this->input->post('tgl_mulai')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_semester');
				$this->modul_semester->get_insertsemester($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Semester ".$this->input->post('nm_smt')." Berhasil Dibuat!!</div></div>");
                redirect('../smt/semester'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Semester ".$this->input->post('nm_smt')." Gagal Dibuat!!</div></div>");
                redirect('../smt/add_semester'); //jika gagal maka akan ditampilkan form tambah mk
	}
    }
	public function savethnajaran(){
		$this->form_validation->set_rules('id_thn_ajaran','ID Tahun Ajaran','required');
		$this->form_validation->set_rules('nm_thn_ajaran','Tahun Ajaran','required');
		$this->form_validation->set_rules('tgl_mulai','Tanggal Mulai','required');
		$this->form_validation->set_rules('a_periode_aktif','Status Periode','required');
		$data = array(
				  'id_thn_ajaran' =>$this->input->post('id_thn_ajaran'),
				  'nm_thn_ajaran' =>$this->input->post('nm_thn_ajaran'),
				  'a_periode_aktif' =>$this->input->post('a_periode_aktif'),
				  'tgl_mulai' =>$this->input->post('tgl_mulai')
				  );
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
				$this->load->model('modul_semester');
				$this->modul_semester->get_insertthnajaran($data); //akses model untuk menyimpan ke database
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Tahun Ajaran ".$this->input->post('nm_thn_ajaran'). "  Berhasil Disimpan!!</div></div>");
                redirect('../smt/tahunajaran'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Tahun Ajaran ".$this->input->post('nm_thn_ajaran'). "  Gagal Disimpan!!</div></div>");
                redirect('../smt/add_thnajar'); //jika gagal maka akan ditampilkan form tambah mk
	}         
    }
	function proseseditkelaskuliah() { 
		$this->load->model('modul_semester','',TRUE); 
            $this->modul_semester->moduleditkelaskuliah(); 
             $this->session->set_flashdata('update','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong>Data Berhasil Di Update
				</div>

			 	');
            echo "<script>alert('Data Berhasil Di Update'); window.location = '../home/ruangan'</script>";
        }
	function proseseditthnajaran() { 
		//$this->form_validation->set_rules('id_thn_ajaran','ID Tahun Ajaran','required');
		$this->form_validation->set_rules('nm_thn_ajaran','Tahun Ajaran','required');
		$this->form_validation->set_rules('tgl_mulai','Tanggal Mulai','required');
		$this->form_validation->set_rules('a_periode_aktif','Status Periode','required');
		if($this->form_validation->run()!=FALSE){
                //pesan yang muncul jika berhasil diupload pada session flashdata
		$this->load->model('modul_semester','',TRUE); 
            $this->modul_semester->moduleditthnajaran(); 
             $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Tahun Ajaran '.$this->input->post('nm_thn_ajaran'). ' Berhasil Diupdate!!
				</div>
			 	');
				redirect('../smt/tahunajaran'); //jika berhasil maka akan ditampilkan view matakuliah
			}else{
               $this->session->set_flashdata('pesan','
			 	<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Tahun Ajaran '.$this->input->post('nm_thn_ajaran'). ' Gagal Diupdate!!
				</div>
			 	');
				redirect('../smt/tahunajaran'); //jika berhasil maka akan ditampilkan view matakuliah
			}
        }
		public function kelaskuliah()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
			$data['username'] = $this->session->userdata('username');
			$this->load ->model('modul_semester');
			$data['klskul']=$this->modul_semester->viewlistkelaskuliah();
			$this->load->view('admin/kelas/adm_listklskuliah',$data);
		}
		else {
			redirect('');
		}
	}
	public function listmkrancangkul($a)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_mk');
		$this->load ->model('modul_semester');
		$this->load ->model('modul_dsn');
		$this->load ->model('modul_ruangan');
		$d['kode_kelas']=$a;
		$this->data['smt']=$this->modul_semester->ambil_data('kode_kelas',$d);
		$this->load->view('admin/tahunajaran/adm_listmkrancangkul',$this->data);
	}
	}
	public function listrancangkul()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_semester');
		$data['smtrbar']=$this->modul_semester->viewsemester();
		$data['smt']=$this->modul_semester->viewrancangankelaskuliah();
		$this->load->view('admin/tahunajaran/adm_listrancangkul',$data);
		}
		else {
			redirect('');
		}
	}
	public function listmkrancangkul2()
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_mk');
		$this->load ->model('modul_semester');
		$this->load ->model('modul_dsn');
		$this->load ->model('modul_ruangan');
		$data['smt']=$this->modul_semester->viewrancangmkkelaskuliah2();
		$data['smtrbar']=$this->modul_semester->viewsemester();
		$data['tampilkelas']=$this->modul_semester->getkdkelas();
		$data['tampilmk']=$this->modul_semester->getkdmk();
		$data['tampilsemester']=$this->modul_semester->getidsmt();
		$data['tampilruangan']=$this->modul_ruangan->getkdruangan();
		$data['tampildosen']=$this->modul_dsn->getnidn();
		$this->load->view('admin/tahunajaran/adm_listmkrancangkul',$data);
		}
		else {
			redirect('');
		}
	}
	public function add_kelaskuliah($id)
	{
		if ($this->session->userdata('logged_in')){
			$session_data=$this->session->userdata('logged_in');
		$data['username'] = $this->session->userdata('username');
		$this->load ->model('modul_mk');
		$this->load ->model('modul_semester');
		$this->load ->model('modul_dsn');
		$this->load ->model('modul_ruangan');
		$data['smt']=$this->modul_semester->viewkelaskuliah();
		$data['smtrbar']=$this->modul_semester->viewsemester();
		$data['tampilkelas']=$this->modul_semester->getkdkelas();
		$data['tampilmk']=$this->modul_semester->getkdmk();
		$data['tampilsemester']=$this->modul_semester->getidsmt();
		$data['tampilruangan']=$this->modul_ruangan->getkdruangan();
		$data['tampildosen']=$this->modul_dsn->getnidn();
		$data['data']=$this->modul_semester->get_addkelaskuliah($id);
		$data['datakelas']=$this->modul_semester->get_addcarikelas($id);
		$this->load->view('admin/tahunajaran/adm_addrancangkul',$data);
		}
		else {
			redirect('');
		}
	}
	public function savekelaskuliah(){
		$data = array(
				  'kode_kelas' =>$this->input->post('kode_kelas'),
				  'id_smt' =>$this->input->post('id_smt'),
				  'kode_mk' =>$this->input->post('kode_mk')
				  );  
		$this->load->model('modul_semester');
				$this->modul_semester->get_insertmkkul($data); //akses model untuk menyimpan ke database
				echo json_encode($data);
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Rancang Kuliah Berhasil Disimpan!!</div></div>");
				
    }
	public function saverancangkuliahall(){
		$data = array();
		$kd_mk =$_POST['kode_mk'];
		foreach($kd_mk AS $key => $val){
		$data[] = array(
				  'kode_kelas' =>$_POST['kode_kelas'][$key],
				  'kode_mk' =>$_POST['kode_mk'][$key],
				  'id_smt' =>$_POST['id_smt'][$key],
				  'nidn' =>$_POST['nidn'][$key],
				  'kode_ruangan' =>$_POST['kode_ruangan'][$key],
				  'hari' =>$_POST['hari'][$key],
				  'jam_mulai' =>$_POST['jam_mulai'][$key],
				  'jam_selesai' =>$_POST['jam_selesai'][$key]);
		}
		if($this->form_validation->run == FALSE){
			$this->load->model('modul_semester');
			$this->modul_semester->get_insertkelaskuliah($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Data Rancangan Kuliah Berhasil Disimpan!!</div></div>");
				redirect('../smt/listrancangkul'); //jika berhasil maka akan ditampilkan view vupload
			}else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Data Rancangan Kuliah Gagal Disimpan!!</div></div>");
                redirect('../smt/listrancangkul'); //jika gagal maka akan ditampilkan form upload
			}         
    }
	public function hapusrancangmkkelaskul($id)
	{
	    $id =$this->input->post('id_smt');
		$this->load->model('modul_semester','',TRUE); 
		$data['data']=$this->modul_semester->hapus_mkrancangkelaskul($id);
		
	}
	public function hapusrancangmkkelaskul2($id)
	{
	    //$id =$this->input->post('id_smt');
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_semester','',TRUE); 
		$data['data']=$this->modul_semester->hapus_mkrancangkelaskul($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Matakuliah Rancangan Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../smt/listrancangkul/');
            }
		//$this->load->view('admin/tahunajaran/adm_addrancangkul/', $data);
	}
	public function hapusrancangmkkelaskultemp($id)
	{
	    //$id =$this->input->post('id_smt');
		$data['username'] = $this->session->userdata('username');
		$this->load->model('modul_semester','',TRUE); 
		$data['data']=$this->modul_semester->hapus_mkrancangkelaskultemp($id);
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Matakuliah Rancangan Berhasil Di Hapus
				</div>

            	 	');
            	 redirect('../smt/listrancangkul/');
            }
		//$this->load->view('admin/tahunajaran/adm_addrancangkul/', $data);
	}
	function delete($kode_mk){
        $this->load->model('modul_semester','',TRUE); 
        $this->modul_semester->deletemkrancangkul($kode_mk);    
		if ($res <= 1) {
            	 $this->session->set_flashdata('pesan','
				<div class="alert alert-success alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  Data Matakuliah Rancangan Berhasil Di Hapus
				</div>

            	 	');
            	 header("Location:../../smt/add_kelaskuliah/".$kode_mk);
            }
    }
	
}

# nama file home.php
# folder apllication/controller/