<?php 
	Class modul_dsn extends CI_Model {
		var $tbl_dsn='dosen';
	Function tampiljumlahdsnall()
	{
		$query=$this->db->get('view_jumlahdsnall');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	function getnidn()
	{
		$query = $this->db->query("SELECT * FROM view_statusdosen ORDER BY nm_ptk ASC");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['nidn']] = $row['nm_ptk'];
	}
	}
	return $data;
	}
	Function viewdsn()
	{
		$query=$this->db->get('view_listdosen');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function get_insertdsn($data){
       $this->db->insert($this->tbl_dsn, $data);
       return TRUE;
    }
	Function get_editdsn($id)
	{
		 $this->db->where('nidn',$id); 
         $query = $this->db->get('dosen'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function moduleditdsn() { 
		
		$id = $this->input->post('nidn'); 
		$data = array(
				  'nidn' =>$this->input->post('nidn'),
				  'nm_ptk' =>$this->input->post('nm_ptk'),
				  'jk' =>$this->input->post('jk'),
				  'nip' =>$this->input->post('nip'),
				  'nik' =>$this->input->post('nik'),
				  'npwp' =>$this->input->post('npwp'),
				  'stat_kawin' =>$this->input->post('stat_kawin'),
				  'tmpt_lahir' =>$this->input->post('tmpt_lahir'),
				  'tgl_lahir' =>$this->input->post('tgl_lahir'),
				  'id_agama' =>$this->input->post('id_agama'),
				  'jln' =>$this->input->post('jln'),
				  'rt' =>$this->input->post('rt'),
				  'rw' =>$this->input->post('rw'),
				  'nm_dsn' =>$this->input->post('nm_dsn'),
				  'ds_kel' =>$this->input->post('ds_kel'),
				  'kode_pos' =>$this->input->post('kode_pos'),
				  'no_hp' =>$this->input->post('no_hp'),
				  'id_stat_aktif' =>$this->input->post('id_stat_aktif'),
				  'nm_ibu_kandung' =>$this->input->post('nm_ibu_kandung'),
				  'nm_suami_istri' =>$this->input->post('nm_wali'),
				  'nip_suami_istri' =>$this->input->post('nip_suami_istri'),
				  'id_pekerjaan_suami_istri' =>$this->input->post('id_pekerjaan_suami_istri'),
				  'sk_cpns' =>$this->input->post('sk_cpns'),
				  'sk_angkat' =>$this->input->post('sk_angkat'),
				  'tgl_sk_cpns' =>$this->input->post('tgl_sk_cpns'),
				  'tmt_sk_angkat' =>$this->input->post('tmt_sk_angkat'),
				  'id_pangkat_gol' =>$this->input->post('id_pangkat_gol'),
				  'tmt_pns' =>$this->input->post('tmt_pns'),
				  'kewarganegaraan' =>$this->input->post('kewarganegaraan')
				  );
		$this->db->where('nidn',$id); 
        $this->db->update('dosen',$data); 
	}
	public function hapus_dsn($id){ 
			
			$this->db->where('nidn',$id);
			$query = $this->db->get('dosen');
			$row = $query->row();
			$this->db->delete('dosen', array('nidn' => $id));

		}
		
}