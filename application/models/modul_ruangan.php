<?php 
	Class modul_ruangan extends CI_Model {
	var $tbl_ruangan='ruang_kelas';
	Function tampilstsruangan()
	{
		$this->db->group_by('kode_ruangan');
		$query=$this->db->get('view_statusruangan');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewruangan()
	{
		$this->db->where('kode_ruangan',$this->session->userdata('kode_ruangan'));
		$query=$this->db->get('ruang_kelas');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function get_insertruangan($data){
       $this->db->insert($this->tbl_ruangan, $data);
       return TRUE;
    }
	Function get_editruangan($id)
	{
		 $this->db->where('kode_ruangan',$id); 
         $query = $this->db->get('ruang_kelas'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function moduleditruangan() { 
		
		$id = $this->input->post('kode_ruangan'); 
		$data = array(
				  'kode_ruangan' =>$this->input->post('kode_ruangan'),
				  'nm_ruangan' =>$this->input->post('nm_ruangan'),
				  'posisi' =>$this->input->post('posisi')
				  );
		$this->db->where('kode_ruangan',$id); 
        $this->db->update('ruang_kelas',$data); 
	}
	function getkdruangan()
	{
		$query = $this->db->query("SELECT * FROM ruang_kelas");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['kode_ruangan']] = $row['nm_ruangan'];
	}
	}
	return $data;
	}
}