<?php 
	Class Modul_kelas extends CI_Model {
	var $tbl_kelas='kelas';
	Function viewkelas()
	{
		$this->db->where('kode_kelas',$this->session->userdata('kode_kelas'));
		$query=$this->db->get('kelas');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function get_insertkls($data){
       $this->db->insert($this->tbl_kelas, $data);
       return TRUE;
    }
	Function get_editkls($id)
	{
		 $this->db->where('kode_kelas',$id); 
         $query = $this->db->get('kelas'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function moduleditkls() { 
		
		$id = $this->input->post('kode_kelas'); 
		$data = array(
				  'kode_kelas' =>$this->input->post('kode_kelas'),
				  'tahun_msk' =>$this->input->post('tahun_msk'),
				  'semester_skrg' =>$this->input->post('semester_skrg'),
				  'jumlah_mhs' =>$this->input->post('jumlah_mhs')
				  );
		$this->db->where('kode_kelas',$id); 
        $this->db->update('kelas',$data); 
	}
	public function hapus_kls($id){ 
			
			$this->db->where('kode_kelas',$id);
			$query = $this->db->get('kelas');
			$row = $query->row();
			$this->db->delete('kelas', array('kode_kelas' => $id));

		}
	
}