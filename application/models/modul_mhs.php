<?php 
	Class modul_mhs extends CI_Model {
		
		var $tbl_mhs='mahasiswa';
		
	function report(){
        $query=$this->db->get('view_banyakmhsthn');
		if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
	Function get_insertmhs($data){
       $this->db->insert($this->tbl_mhs, $data);
       return TRUE;
    }
	Function get_editmhs($id)
	{
		 $this->db->where('id',$id); 
         $query = $this->db->get('view_mhscomplete'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function moduleditmhs() { 
		
		$id = $this->input->post('idmhs'); 
		$data = array(
				  'nim' =>$this->input->post('nim'),
				  'nm_pd' =>$this->input->post('nm_pd'),
				  'jk' =>$this->input->post('jk'),
				  'nisn' =>$this->input->post('nisn'),
				  'nik' =>$this->input->post('nik'),
				  'tmpt_lahir' =>$this->input->post('tmpt_lahir'),
				  'tgl_lahir' =>$this->input->post('tgl_lahir'),
				  'id_agama' =>$this->input->post('id_agama'),
				  'jln' =>$this->input->post('jln'),
				  'rt' =>$this->input->post('rt'),
				  'rw' =>$this->input->post('rw'),
				  'nm_dsn' =>$this->input->post('nm_dsn'),
				  'ds_kel' =>$this->input->post('ds_kel'),
				  'kode_pos' =>$this->input->post('kode_pos'),
				  'telepon_seluler' =>$this->input->post('telepon_seluler'),
				  'stat_pd' =>$this->input->post('stat_pd'),
				  'nm_ayah' =>$this->input->post('nm_ayah'),
				  'tgl_lahir_ayah' =>$this->input->post('tgl_lahir_ayah'),
				  'id_jenjang_pendidikan_ayah' =>$this->input->post('id_jenjang_pendidikan_ayah'),
				  'id_pekerjaan_ayah' =>$this->input->post('id_pekerjaan_ayah'),
				  'id_penghasilan_ayah' =>$this->input->post('id_penghasilan_ayah'),
				  'nm_ibu_kandung' =>$this->input->post('nm_ibu_kandung'),
				  'tgl_lahir_ibu' =>$this->input->post('tgl_lahir_ibu'),
				  'id_jenjang_pendidikan_ibu' =>$this->input->post('id_jenjang_pendidikan_ibu'),
				  'id_pekerjaan_ibu' =>$this->input->post('id_pekerjaan_ibu'),
				  'id_penghasilan_ibu' =>$this->input->post('id_penghasilan_ibu'),
				  'nm_wali' =>$this->input->post('nm_wali'),
				  'tgl_lahir_wali' =>$this->input->post('tgl_lahir_wali'),
				  'id_jenjang_pendidikan_wali' =>$this->input->post('id_jenjang_pendidikan_wali'),
				  'id_pekerjaan_wali' =>$this->input->post('id_pekerjaan_wali'),
				  'id_penghasilan_wali' =>$this->input->post('id_penghasilan_wali'),
				  'kewarganegaraan' =>$this->input->post('kewarganegaraan')
				  );
		$this->db->where('id',$id); 
        $this->db->update('mahasiswa',$data); 
	}
	public function hapus_mhs($id){ 
			
			$this->db->where('id',$id);
			$query = $this->db->get('mahasiswa');
			$row = $query->row();
			$this->db->delete('mahasiswa', array('id' => $id));

		}
	Function viewmhs()
	{
		$query=$this->db->get('view_listmahasiswa');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampiljumlahmhsaktf()
	{
		$query=$this->db->get('view_jumlahmhsaktf');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function tampiljumlahalumni()
	{
		$query=$this->db->get('view_jumlahalumni');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
}