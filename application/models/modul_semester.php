<?php 
	Class modul_semester extends CI_Model {

	var $tbl_semester='semester';
	var $tbl_thnajar='tahun_ajaran';
	var $tbl_absen='absen_dsn';
	var $tbl_kelaskuliah='kelas_kuliah';
	var $tbl_kelaskuliahtemp='kelas_kuliahtemp';
	var $tbl_kelaskuliahfinal='kelas_kuliahfinal';
	var $view_kelaskuliahfinal='view_kelaskuliahfinal';
	
	
	Function viewtahunajaran()
	{
		$query=$this->db->get('view_thnajaran');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewsemester()
	{
		$query=$this->db->get('view_thnajaransmt');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewsemesteropen()
	{
		$this->db->where('keadaan_periode','Open'); 
		$query=$this->db->get('view_thnajaransmt');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function cek_thn_ajaran($data1) {
			$query = $this->db->get_where('tahun_ajaran', $data1);
			return $query;
		}
	public function get_thn_ajaran() {
		$query = $this->db->query("SELECT MAX(id_thn_ajaran) as autothnajaran FROM tahun_ajaran"); 
		$row = $query->row_array();
		$autothn_ajar = $row['autothnajaran']; 
		$autothnajaran = $autothn_ajar;
		return $autothnajaran;
	}
	Function get_idthnajar($id)
	{
		 $this->db->where('id_thn_ajaran',$id); 
         $query = $this->db->get('tahun_ajaran'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	function getthnajar()
	{
		$query = $this->db->query("SELECT * FROM tahun_ajaran");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['id_thn_ajaran']] = $row['nm_thn_ajaran'];
	}
	}
	return $data;
	}
	Function moduleditthnajaran() { 
		
		$id = $this->input->post('id_thn_ajaran'); 
		$data = array(
				  'nm_thn_ajaran' =>$this->input->post('nm_thn_ajaran'),
				  'a_periode_aktif' =>$this->input->post('a_periode_aktif'),
				  'tgl_mulai' =>$this->input->post('tgl_mulai')
				  );
		$this->db->where('id_thn_ajaran',$id); 
        $this->db->update('tahun_ajaran',$data); 
	}
	Function get_insertsemester($data){
       $this->db->insert($this->tbl_semester, $data);
       return TRUE;
    }
	Function get_insertthnajaran($data){
       $this->db->insert($this->tbl_thnajar, $data);
       return TRUE;
    }
	Function viewlistkelaskuliah()
	{
		$query=$this->db->get('kelas');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewrancangankelaskuliah()
	{
		$query=$this->db->get('view_rancangkuliah_updateindo');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewmatrikkelaskuliahsenin()
	{
		$this->db->group_by('kode_kelas');
		$this->db->group_by('waktu');
		$this->db->order_by('waktu','asc');
		$this->db->where('hari','Senin');
		$query=$this->db->get('view_matrixkul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewmatrikkelaskuliahselasa()
	{
		$this->db->order_by('waktu','asc');
		$this->db->where('hari','Selasa');
		$query=$this->db->get('view_matrixkul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewcarikelas()
	{
		$this->db->where('kode_kelas',$this->session->userdata('kode_kelas'));
		$query=$this->db->get('kelas');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewmatrikkelaskuliahrabu()
	{
		$this->db->order_by('waktu','asc');
		$this->db->where('hari','Rabu');
		$query=$this->db->get('view_matrixkul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewmatrikkelaskuliahkamis()
	{
		$this->db->order_by('waktu','asc');
		$this->db->where('hari','Kamis');
		$query=$this->db->get('view_matrixkul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewmatrikkelaskuliahjumat()
	{
		$this->db->order_by('waktu','asc');
		$this->db->where('hari','Jumat');
		$query=$this->db->get('view_matrixkul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliahbackup()
	{
		$this->db->order_by('mksemester_mksmtgenap','desc');
		$this->db->order_by('kode_mksmtgenap','desc');
		$query=$this->db->get('view_rancangkuliahtemp');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function ambil_data($view_kelaskuliahfinal,$data)
	{
		$query=$this->db->get_where('view_kelaskuliahtemp',$data);
		Return $query->result();
	}
	Function viewrancangmkkelaskuliah($data1)
	{
		$kd_kelas=$this->input->get('kode_kelas');
		$this->db->where('kode_kelas',$data1);
		$query=$this->db->get('view_kelaskuliahfinal');
		//$query = $this->db->query("SELECT * FROM view_kelaskuliahfinal where kode_kelas like '%KA12%'");
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewrancangmkkelaskuliah2()
	{
		//$kd_kelas=$this->input->get('kode_kelas');
		//$this->db->where('kode_kelas',$data1);
		$query=$this->db->get('view_kelaskuliah');
		//$query = $this->db->query("SELECT * FROM view_kelaskuliahfinal where kode_kelas like '%KA12%'");
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliah()
	{
		// $kd_kelas=$_POST['kode_kelas'];
		// $this->db->where('kode_kelas',$kd_kelas);
		$query=$this->db->get('view_kelaskuliahfinal');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliahsms2()
	{
		$this->db->where('mk_semester = 2');
		$query=$this->db->get('view_mksmtgenap');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliahsms4()
	{
		$this->db->where('mk_semester = 4');
		$query=$this->db->get('view_mksmtgenap');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliahsms6()
	{
		$this->db->where('mk_semester = 6');
		$query=$this->db->get('view_mksmtgenap');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewkelaskuliahsms8()
	{
		$this->db->where('mk_semester = 8');
		$query=$this->db->get('view_mksmtgenap');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	function getkdkelas()
	{
		$query = $this->db->query("SELECT * FROM kelas");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['kode_kelas']] = $row['kode_kelas'];
	}
	}
	return $data;
	}
	function getkdmk()
	{
		$query = $this->db->query("SELECT * FROM mata_kuliah");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['kode_mk']] = $row['nm_mk'];
	}
	}
	return $data;
	}
	Function get_insertmkkul($data){
       $insertdata=$this->db->insert($this->tbl_kelaskuliahtemp, $data);
       return $insertdata;
    }
	function getidsmt()
	{
		$query = $this->db->query("SELECT * FROM semester");
 
		if ($query->num_rows()> 0){
		foreach ($query->result_array() as $row)
	{
		$data[$row['id_smt']] = $row['nm_smt'];
	}
	}
	return $data;
	}
	Function get_insertkelaskuliah($data){
       $this->db->insert_batch($this->tbl_kelaskuliah, $data);
       return TRUE;
    }
	Function get_addkelaskuliah($id)
	{
		 $this->db->where('id_smt',$id); 
         $query = $this->db->get('semester'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function get_addcarikelas($id)
	{
		 $this->db->where('kode_kelas',$id); 
         $query = $this->db->get('kelas'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function get_carikelaskuliah($id)
	{
		 $this->db->where('id_smt',$id); 
         $query = $this->db->get('semester'); 
                If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return null;
	} 
	}
	Function tampilklskul()
	{
		$query=$this->db->get('view_statusklskul');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewabsendsnkelaskuliah()
	{
		$query=$this->db->get('view_stsabsendos');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function viewabsenrancangankelaskuliah()
	{
		$query=$this->db->get('view_absendos');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function get_insertabsen($data){
       $this->db->insert_batch($this->tbl_absen, $data);
       return TRUE;
    }
	Function tampilrancangankuliah()
	{
		$query=$this->db->get('view_rancangkuliah_updateindo');
		If ($query->num_rows()>0)
	{
		Return $query->result();
	}
		Else
	{
		Return array();
	}
	}
	Function moduleditkelaskuliah() { 
		
		$data = array();
		$kd_mk =$_POST['kode_mk'];
		$kd_kelas =$_POST['kode_kelas'];
		foreach($kd_mk AS $key => $val){
		$data[] = array(
				  'nidn' =>$_POST['nidn'][$key],
				  'kode_ruangan' =>$_POST['kode_ruangan'][$key],
				  'hari' =>$_POST['hari'][$key],
				  'jam_mulai' =>$_POST['jam_mulai'][$key],
				  'jam_selesai' =>$_POST['jam_selesai'][$key]);
		}
		//$this->db->where('kode_kelas',$kd_kelas); 
        $this->db->update_batch('kelas_kuliah',$data,$kd_mk); 
	}
	public function hapus_mkrancangkelaskul($id){ 
			
			$this->db->where('kode_mk',$id);
			$query = $this->db->get('kelas_kuliah');
			$row = $query->row();
			$this->db->delete('kelas_kuliah', array('kode_mk' => $id));

		}
		public function hapus_mkrancangkelaskultemp($id){ 
			
			$this->db->where('kode_mk',$id);
			$query = $this->db->get('kelas_kuliahtemp');
			$row = $query->row();
			$this->db->delete('kelas_kuliahtemp', array('kode_mk' => $id));

		}
	public function hapus_rancangkelaskul($kode_kelas){ 
			
			$this->db->where('kode_mk',$id);
			$query = $this->db->get('kelas_kuliah');
			$row = $query->row();
			$this->db->delete('kelas_kuliah', array('kode_mk' => $id));

		}
function deletemkrancangkul($kode_mk) {
        $this->db->where('kode_mk',$kode_mk)->delete('kelas_kuliahtemp');    
    }		
}