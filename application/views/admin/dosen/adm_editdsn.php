<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="../../assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../../assets/css/charisma-app.css" rel="stylesheet">
	<link href="../../assets/css/bootstrap-table.css" rel="stylesheet">
    <link href='../../assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='../../assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='../../assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='../../assets/css/jquery.noty.css' rel='stylesheet'>
	<link href='../../assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='../../assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='../../assets/css/animate.min.css' rel='stylesheet'>
	<script type="text/javascript" src="../../assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="../../assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="../../assets/img/logosinul.ico"/>
</head>
<body>
	<!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <div id="content" class="col-lg-12 col-sm-12">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../../dsn/dosen">Dosen</a>
        </li>
		<li>
            Edit Data Dosen
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Edit Data Dosen Di STTIKOM Insan Unggul</div>
					<div class="panel-body">
					<?=$this->session->flashdata('pesan')?>
						<form action="<?=base_url()?>dsn/proseseditdsn" method="post" enctype="multipart/form-data" role="form">
						<?php foreach ($data as $row) {?>
				<div style="float:right;margin-bottom:20px;">
						<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-check icon-white"></i>&nbsp;&nbsp;Submit</button>
						<button type="reset" class="btn btn-default"><i class="glyphicon glyphicon-refresh icon-white"></i>&nbsp;&nbsp;Reset</button>&nbsp;
						<a class="btn btn-success" href='../../dsn/dosen'">
						<i class="glyphicon glyphicon-list icon-white"></i>&nbsp;&nbsp;Daftar Dosen</a>
				</div>
				<div class="col-lg-6">
				<div class="panel-body"></div>
				<div class="panel-body"></div>
				<div class="input-group col-md-11">
						<span class="input-group-addon">NIDN</span>
						<input type="text" class="form-control" placeholder="NIDN" name="nidn" style="width:150px;" value="<?=$row->nidn;?>" required><i class="help-block">*required</i>
                </div><p></p>
				<div class="input-group col-md-11">
						<span class="input-group-addon">Nama</span>
						<input type="text" class="form-control" placeholder="Nama Dosen" name="nm_ptk" style="width:400px;" value="<?=$row->nm_ptk;?>" required><i class="help-block">*required</i>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Jenis Kelamin ?</span>
					<?php if ($row->jk=='P'){?>
					<div class="radio">
						<label>
                        <input type="radio" name="jk" id="optionsRadios1" value="P" checked>Pria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="jk" id="optionsRadios1" value="W">Wanita
                    </label>
					</div>
					<?php } ?>
					<?php 
					if ($row->jk=='W') {?>
					<div class="radio">
						<label>
                        <input type="radio" name="jk" id="optionsRadios1" value="P">Pria&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="jk" id="optionsRadios1" value="W" checked>Wanita
                    </label>
					</div>
					<?php } ?>
				</div><p></p>
				<div class="input-group col-md-11">
						<span class="input-group-addon">No. Hp</span>
						<input type="number" class="form-control" placeholder="No. Hp" value="<?=$row->no_hp;?>" style="width:150px;" name="no_hp" required><i class="help-block">*required</i>
                </div><p></p>
				</div>
				<div class="col-lg-6">
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Tempat Lahir</span>
                    <input type="text" class="form-control" style="width:150px;"  placeholder="Tempat Lahir" value="<?=$row->tmpt_lahir;?>" name="tmpt_lahir" required><i class="help-block">*required</i>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Tanggal Lahir</span>
                    <input type="date" class="form-control" style="width:200px;"  value="<?=$row->tgl_lahir;?>" placeholder="yyyy-MM-dd" name="tgl_lahir" required><i class="help-block">*required</i>
                </div><p></p>
                <div class="input-group col-md-11">
                    <span class="input-group-addon">Agama</span>
						 <div class="controls">
                        <select class="form-control" style="width:150px;height:40px;border-radius: 0 5px 5px 0;" name="id_agama" required>
                            <?php
								$tampil=mysql_query("SELECT * FROM agama ORDER BY id_agama");
									while($w=mysql_fetch_array($tampil)){
										if ($row->id_agama==$w[id_agama]){
											echo "<option value=$w[id_agama] selected>$w[nm_agama]</option>";
											}
											else{
											echo "<option value=$w[id_agama]>$w[nm_agama]</option>";
											}
										} 
							?>
                        </select>
                    </div>
                </div><p></p>
				<div class="input-group col-md-11">
									<span class="input-group-addon">Status Dosen</span>
									<?php if ($row->id_stat_aktif=='1'){?>
										<div class="controls">
										<select class="form-control" style="width:150px;height:40px;border-radius: 0 5px 5px 0;" name="id_stat_aktif" required>
											<option value="1" selected>Aktif</option>
											<option value="0">Non-Aktif</option>
										</select>
										</div>
									<?php } ?>
									<?php if ($row->id_stat_aktif=='0'){?>
										<div class="controls">
										<select class="form-control" style="width:150px;height:40px;border-radius: 0 5px 5px 0;" name="id_stat_aktif" required>
											<option value="1">Aktif</option>
											<option value="0" selected>Non-Aktif</option>
										</select>
										</div>
									<?php } ?>
				</div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Menikah ?</span>
					<?php if ($row->stat_kawin=='0'){?>
					<div class="radio">
						<label>
                        <input type="radio" name="stat_kawin" id="optionsRadios1" value="0" checked>Belum Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="stat_kawin" id="optionsRadios1" value="1">Sudah Menikah
                    </label>
					</div>
					<?php } ?>
					<?php if ($row->stat_kawin=='1'){?>
					<div class="radio">
						<label>
                        <input type="radio" name="stat_kawin" id="optionsRadios1" value="0">Belum Menikah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="stat_kawin" id="optionsRadios1" value="1" checked>Sudah Menikah
                    </label>
					</div>
					<?php } ?>
				</div><p></p>
				</div>
				</div>
			<div class="panel-body tabs">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1" data-toggle="tab">Alamat</a></li>
							<li><a href="#tab2" data-toggle="tab">Keluarga</a></li>
							<li><a href="#tab3" data-toggle="tab">Pangkat</a></li>
						</ul>
		
						<div class="tab-content">
							<div class="tab-pane fade in active" id="tab1">
							<div class="panel-body"></div>
								<div class="input-group col-md-11">
									<span class="input-group-addon">NIK</span>
									<input type="text" class="form-control" style="width:250px;" value="<?=$row->nik;?>" placeholder="NIK" name="nik"><i class="help-block">*Nomor Ditulis Tanpa Tanda Baca</i>
								</div><p></p>
								<div class="input-group col-md-11">
									<span class="input-group-addon">NIP</span>
									<input type="text" class="form-control" style="width:250px;" value="<?=$row->nip;?>" placeholder="NIP" name="nip"><i class="help-block">*Jika PNS</i>
								</div><p></p>
								<div class="input-group col-md-11">
									<span class="input-group-addon">NPWP</span>
									<input type="text" class="form-control" style="width:250px;" value="<?=$row->npwp;?>" placeholder="NPWP" name="npwp">
								</div><p></p>
							<div class="input-group col-md-11">
									<span class="input-group-addon">Kewarganegaraan</span>
									<select class="form-control" style="width:150px;height:40px;border-radius: 0 5px 5px 0;" name="kewarganegaraan" required>
											<option value="WNI">WNI</option>
											<option value="WNA">WNA</option>
										</select>
							</div><p></p>
							<div class="input-group col-md-11">
									<span class="input-group-addon">Jalan</span>
									<textarea class="autogrow" name="jln" cols="40" rows="3"><?=$row->jln;?></textarea><i class="help-block">*required</i>
							</div><p></p>
							<div class="input-group col-md-9">
									<span class="input-group-addon">Dusun</span>
									<input type="text" class="form-control" style="width:250px;" value="<?=$row->nm_dsn;?>" placeholder="Nama Dusun" name="nm_dsn"><i class="help-block">*required</i>
									<div></div>
									<span class="input-group-addon">RT</span>
									<input type="text" class="form-control" style="width:70px;" placeholder="RT" value="<?=$row->rt;?>" name="rt"><i class="help-block">*required</i>
									<span class="input-group-addon">RW</span>
									<input type="text" class="form-control" style="width:70px;" placeholder="RW" value="<?=$row->rw;?>" name="rw"><i class="help-block">*required</i>
							</div><p></p>
							<div class="input-group col-md-9">
									<span class="input-group-addon">Kelurahan</span>
									<input type="text" class="form-control" style="width:250px;" placeholder="Kelurahan" value="<?=$row->ds_kel;?>" name="ds_kel"><i class="help-block">*required</i>
									<span class="input-group-addon">Kode Pos</span>
									<input type="number" class="form-control" style="width:250px;" value="<?=$row->kode_pos;?>" placeholder="Kode Pos" name="kode_pos"><i class="help-block">*required</i>
							</div><p></p>
							</div>
							<div class="tab-pane fade" id="tab2">
							<div class="panel-body"></div>
							<div class="col-lg-4">
							<div class="panel panel-primary">
							<div class="panel-heading">Data Ibu</div>
							<div class="panel-body"></div>
								<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Nama Ibu</span>
									<input type="text" class="form-control" style="width:200px;" value="<?=$row->nm_ibu_kandung;?>" placeholder="Nama Ibu" name="nm_ibu_kandung"><i class="help-block">*required</i>
								</div><p></p>
							<div class="panel-body"></div>
							</div>
							</div>
							<div class="col-lg-6">
							<div class="panel panel-primary">
							<div class="panel-heading">Data Pribadi</div>
							<div class="panel-body"></div>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Nama Suami / Istri</span>
									<input type="text" class="form-control" style="width:170px;" placeholder="Nama Suami / Istri" name="nm_suami_istri"><i class="help-block">*Khusus Untuk Yang Sudah Nikah</i>
							</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">NIP Suami / Istri</span>
									<input type="text" class="form-control" style="width:170px;" placeholder="NIP Suami / Istri" name="nip_suami_istri"><i class="help-block">*Jika PNS</i>
								</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Pekerjaan Suami / Istri</span>
									<select data-placeholder="Pilih Pekerjaan" style="height:40px;width:200px;" name="id_pekerjaan_suami_istri">
									<?php
										$tampil=mysql_query("SELECT * FROM pekerjaan ORDER BY id_pekerjaan");
											while($w=mysql_fetch_array($tampil)){
											if ($row->id_pekerjaan_ayah==$w[id_pekerjaan]){
											echo "<option value=$w[id_pekerjaan] selected>$w[nm_pekerjaan]</option>";
											}
											else{
											echo "<option value=$w[id_pekerjaan]>$w[nm_pekerjaan]</option>";
											}
										} 
										?>
									</select>
							</div><p></p>
							<div class="panel-body"></div>
							
							</div>
							</div>
							</div>
							<div class="tab-pane fade" id="tab3">
							<div class="panel-body"></div>
							<div class="col-lg-4">
							<div class="panel panel-primary">
							<div class="panel-heading">PNS</div>
							<div class="panel-body"></div>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">No. SK CPNS</span>
									<input type="text" class="form-control" style="width:200px;" placeholder="SK CPNS" name="sk_cpns">
							</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Tanggal SK CPNS</span>
									<input type="date" class="form-control" style="width:170px;" placeholder="Tanggal SK" name="tgl_sk_cpns">
							</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Pangkat / Golongan</span>
									<input type="text" class="form-control" style="width:200px;" placeholder="Pangkat" name="id_pangkat_gol">
							</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Terhitung Mulai</span>
									<input type="date" class="form-control" style="width:170px;" placeholder="Tanggal Pengangkatan PNS" name="tmt_sk_angkat">
							</div><p></p>
							<div class="panel-body"></div>
							</div>
							</div>
							<div class="col-lg-4">
							<div class="panel panel-primary">
							<div class="panel-heading">Instansi</div>
							<div class="panel-body"></div>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">No. SK</span>
									<input type="text" class="form-control" style="width:200px;" placeholder="SK" name="sk_angkat">
							</div><p></p>
							<div class="input-group col-md-11" style="margin-left:10px;">
									<span class="input-group-addon">Terhitung Mulai</span>
									<input type="date" class="form-control" style="width:170px;" placeholder="Tanggal Pengangkatan" name="tmt_sk_angkat">
							</div><p></p>
							<div class="panel-body"></div>
							
							</div>
							</div>
							</div>
							<?php } ?>
						</form>
						</div>
					</div>
			</div><!--/.row-->
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="../../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../../assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='../../assets/bower_components/moment/min/moment.min.js'></script>
<script src='../../assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='../../assets/js/jquery.dataTables.min.js'></script>
<!-- select or dropdown enhancer -->
<script src="../../assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="../../assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="../../assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../../assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="../../assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../../assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../../assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../../assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../../assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../../assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../../assets/js/charisma.js"></script>
<script src="../../assets/js/bootstrap-table.js"></script>
</body>
</html>
