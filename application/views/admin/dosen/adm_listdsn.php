<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="../assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../assets/css/charisma-app.css" rel="stylesheet">
	<link href="../assets/css/bootstrap-table.css" rel="stylesheet">
    <link href='../assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='../assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='../assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='../assets/css/jquery.noty.css' rel='stylesheet'>
	<link href='../assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='../assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='../assets/css/animate.min.css' rel='stylesheet'>
	<script type="text/javascript" src="../assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="../assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="../assets/img/logosinul.ico"/>
</head>
<body>
	<div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="../main/dashboard"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Dosen Per-Jurusan</span></a>
                            <ul class="nav nav-pills nav-stacked">
								<li><a class="ajax-link" href="../dsn/dsnti"><span> Teknik Informatika</span></a>
								</li>
								<li><a class="ajax-link" href="../dsn/dsnsi"><span> Sistem Informasi</span></a>
								</li>
								<li><a class="ajax-link" href="../dsn/dsnmi"><span> Manajemen Informatika</span></a>
								</li>
								<li><a class="ajax-link" href="../dsn/dsnka"><span> Komputer Akutansi</span></a>
								</li>
                            </ul>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Status Dosen</span></a>
                            <ul class="nav nav-pills nav-stacked">
								<li><a class="ajax-link" href="../dsn/dsnaktif"><span> Dosen Aktif</span></a>
								</li>
								<li><a class="ajax-link" href="../dsn/dsnnonaktif"><span> Dosen Non-Aktif</span></a>
								</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../dsn/dosen">Dosen</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Daftar Dosen Di STTIKOM Insan Unggul</div>
				<div class="panel-body">
				<?=$this->session->flashdata('pesan')?>
					<a class="btn btn-primary" href='../dsn/add_dsn' style="width:120px;float:right;margin-bottom:10px;">
					<i class="glyphicon glyphicon-plus icon-white"></i>  Tambah Data</a></div>
					<div class="panel-body">
						<table data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="nm_ptk" data-sort-order="asc" data-add="true">
						    <thead>
						    <tr style="background-color:skyblue;">
						        <th data-field="nidn" data-sortable="true">NIDN</th>
								<th data-field="nm_ptk"  data-sortable="true">Nama Dosen</th>
						        <th data-field="no_hp"  data-sortable="true">No. HP</th>
								<th data-field="statusdsn"  data-sortable="true">Status Dosen</th>
						        <th data-field="edit" data-sortable="true">Action</th>
						    </tr>
							</thead>
							<tbody>
								<tr>
							<?php 
							foreach ($data as $row) {?>	 
									<td> <?=$row->nidn;?></td>
									<td> <?=$row->nm_ptk;?></td>
									<td> <?=$row->no_hp;?></td>
									<td> <?=$row->statusdsn;?></td>
									<td><a class="btn btn-success" href='detaildsn/<?=$row->nidn;?>'><i class="glyphicon glyphicon-zoom-in icon-white"></i></a>
									<a class="btn btn-primary" href='editdsn/<?=$row->nidn;?>'><i class="glyphicon glyphicon-edit icon-white"></i></a>
									<a class="btn btn-danger" href='hapusdsn/<?=$row->nidn;?>'><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
								</tr>
							<?php
								}
							?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
</div><!--/row-->
	<!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='../assets/bower_components/moment/min/moment.min.js'></script>
<script src='../assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='../assets/js/jquery.dataTables.min.js'></script>
<!-- select or dropdown enhancer -->
<script src="../assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="../assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="../assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="../assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../assets/js/charisma.js"></script>
<script src="../assets/js/bootstrap-table.js"></script>
</body>
</html>
