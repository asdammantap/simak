<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="<?=base_url()?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/charisma-app.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-table.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/jquery.noty.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/jquery.iphone.toggle.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/noty_theme_default.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.timepicker.min.js"></script>
	<link href="<?=base_url()?>assets/js/jquery.timepicker.min.css" rel="stylesheet"/>
   
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="<?=base_url()?>assets/img/logosinul.ico"/>
</head>
<body>
	<!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
	<!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">
                        <li class="nav-header">Main</li>
                        <li><a class="ajax-link" href="../main/dashboard"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Jenis MK</span></a>
                            <ul class="nav nav-pills nav-stacked">
                               <li><a class="ajax-link" href="../mk/matakuliahwajib"><span class="badge badge-warning pull-right"><?php 
							foreach ($tampilmkwajib as $row) {
							echo $row->jumlahmkwajib;}?></span><span> MK Wajib</span></a>
                        </li>
						<li><a class="ajax-link" href="../mk/matakuliahpilihan"><span class="badge badge-success pull-right"><?php 
							foreach ($tampilmkpilihan as $row) {
							echo $row->jumlahmkpilihan;}?></span><span> MK Pilihan</span></a>
                        </li>
						<li><a class="ajax-link" href="../mk/matakuliahwajibminat"><span class="badge badge-success pull-right"><?php 
							foreach ($tampilmkwajibminat as $row) {
							echo $row->jumlahmkwajibminat;}?></span><span> MK Wajib Peminatan</span></a>
                        </li>
						<li><a class="ajax-link" href="../mk/matakuliahpilihanminat"><span class="badge badge-success pull-right"><?php 
							foreach ($tampilmkpilihanminat as $row) {
							echo $row->jumlahmkpilihanminat;}?></span><span> MK Pilihan Peminatan</span></a>
                        </li>
						<li><a class="ajax-link" href="../mk/matakuliahskripsi"><span class="badge badge-warning pull-right"><?php 
							foreach ($tampilmkskripsi as $row) {
							echo $row->jumlahmkskripsi;}?></span><span> Skripsi/TA/Thesis</span></a>
                        </li>
                            </ul>
                        </li>
						<li class="accordion">
                            <a href="#"><i class="glyphicon glyphicon-chevron-down"></i><span> Paket MK Semester</span></a>
                            <ul class="nav nav-pills nav-stacked">
								<li><a class="ajax-link" href="../mk/paketmatakuliahti"><span> Teknik Informatika</span></a>
								</li>
								<li><a class="ajax-link" href="../mk/paketmatakuliahsi"><span> Sistem Informasi</span></a>
								</li>
								<li><a class="ajax-link" href="../mk/paketmatakuliahmi"><span> Manajemen Informatika</span></a>
								</li>
								<li><a class="ajax-link" href="../mk/paketmatakuliahka"><span> Komputer Akutansi</span></a>
								</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--/span-->
        <!-- left menu ends -->
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../smt/tahunajaran">Tahun Ajaran</a>
        </li>
		<li>
            Daftar Rancangan Kuliah
        </li>
    </ul>
</div>
<div class="row">
<div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Daftar Rancangan MK Di STTIKOM Insan Unggul</div>
				<div class="panel-body">
				<?=$this->session->flashdata('pesan')?>
					</div>
					<div class="panel-body">
   <table border="1" data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="mk_semester" data-sort-order="desc" data-add="true" width="100%">
						    <thead>
						    <tr style="background-color:skyblue;">
						        <th data-field="nm_mk" data-sortable="true">Matakuliah</th>
								<th data-field="sks_mk"  data-sortable="true">SKS</th>
						        <th data-field="mk_semester  data-sortable="true">Smt</th>
								<th data-field="kode_kelas  data-sortable="true">Kelas</th>
								<th data-field="kode_kelas"  data-sortable="true">Dosen</th>
								<th data-field="hari"  data-sortable="true">Hari</th>
								<th data-field="jam_mulai"  data-sortable="true">Waktu</th>
								<th data-field="id_smt"  data-sortable="true">Ruangan</th>
								<th data-field="edit" data-sortable="true">Action</th>
						    </tr>
							</thead>
							<tbody>
								<tr>
							<?php 
							foreach ($smt as $row) {?>	 
									<td><?=$row->nm_mk;?> </td>
									<td><?=$row->sks_mk;?></td>
									<td><?=$row->mk_semester;?></td>
									<td><?=$row->kode_kelas;?></td>
									<td><?=$row->nm_ptk;?></td>
									<td><?=$row->hari;?></td>
									<td><?=$row->waktu;?></td>
									<td><?=$row->nm_ruangan;?></td>
									<td><a class="btn btn-danger" href='../smt/hapusrancangmkkelaskul2/<?=$row->kode_mk;?>'><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
							</tr>
							<?php
								}
							?>
							</tbody>
						</table>
						<div class="panel-body"></div>
						</div>
						</div>
						</div>
</div><!--/row-->
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="<?=base_url()?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="../assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
<!-- select or dropdown enhancer -->
<script src="<?=base_url()?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?=base_url()?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?=base_url()?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?=base_url()?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?=base_url()?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- application script for Charisma demo -->
<script src="<?=base_url()?>assets/js/charisma.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
</body>
</html>
