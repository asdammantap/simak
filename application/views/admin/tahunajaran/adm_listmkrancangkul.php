<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="<?=base_url()?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/charisma-app.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-table.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/jquery.noty.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/jquery.iphone.toggle.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/noty_theme_default.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/js/jquery.timepicker.min.js"></script>
	<link href="<?=base_url()?>assets/js/jquery.timepicker.min.css" rel="stylesheet"/>


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="<?=base_url()?>assets/img/logosinul.ico"/>
</head>
<body>
<form action="<?=base_url()?>smt/saverancangkuliahall" method="post" enctype="multipart/form-data" role="form">
				<table  data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="mk_semester" data-sort-order="desc" data-add="true">
						    <thead>
						    <tr style="background-color:skyblue;height:40px;text-transform:uppercase;padding-bottom:10px;">
								<th data-field="kode_mk" data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Kode</th>
								<th data-field="nm_mk" data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Matakuliah</th>
								<th data-field="sks_mk"  data-sortable="true" style="padding-bottom:10px;padding-left:5px;padding-right:5px;">SKS</th>
						        <th data-field="mk_semester  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Smt</th>
								<th data-field="kode_kelas  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Kelas</th>
								<th data-field="nidn"  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Dosen</th>
								<th data-field="hari"  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Hari</th>
								<th data-field="jam_mulai"  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Waktu</th>
								<th data-field="id_smt"  data-sortable="true" style="padding-bottom:10px;padding-left:10px;">Ruangan</th>
								<th data-field="edit" data-sortable="true">Action</th>
						    </tr>
							</thead>
							<tbody id="list">
								<tr class="record">
							<?php 
							foreach ($smt as $row) {?>	
									<td><?=$row->kode_mk;?></td>
									<td style="width:200px;"><input type="hidden" name="kode_mk[]" value="<?=$row->kode_mk;?>"><?=$row->nm_mk;?> </td>
									<td><input type="hidden" name="id_smt[]" value="<?=$row->id_smt;?>"><?=$row->sks_mk;?></td>
									<td style="width:70px;"><?=$row->mk_semester;?></td>
									<td><input type="hidden" name="kode_kelas[]" value="<?=$row->kode_kelas;?>"><?=$row->kode_kelas;?></td>
							<td><span class="input-group-addon">
										<select data-placeholder="Pilih Dosen" style="width:170px;" name="nidn[]">
											<option value=''>--Pilih Dosen--</option>
											<?php
										$tampil=mysql_query("SELECT * FROM dosen ORDER BY nm_ptk ASC");
										while($r=mysql_fetch_array($tampil))
										{
										  echo "<option value=$r[nidn]>$r[nm_ptk]</option>";
										}  
									?>
										</select></span></td>
									<td><span class="input-group-addon">
										<select style="width:70px;" name="hari[]">
											<option value='Senin'>Senin</option>
											<option value='Selasa'>Selasa</option>
											<option value='Rabu'>Rabu</option>
											<option value='Kamis'>Kamis</option>
											<option value='Jumat'>Jumat</option>
											<option value='Sabtu'>Sabtu</option>
											<option value='Ahad'>Ahad</option>
										</select></span></td>
										<td>
										<script>
											$('.timepicker').timepicker({
											timeFormat: 'h:mm p',
											interval: 10,
											minTime: '00',
											maxTime: '23:00PM',
											defaultTime: '08',
											startTime: '08:00',
											dynamic: false,
											dropdown: true,
											scrollbar: true
										});</script><span class="input-group-addon">
										<input type="text" class="timepicker" name="jam_mulai[]" onkeydown="timefunction()" style="width:100px;"></br>
										s/d</br>
										<script>
											$('.timepicker2').timepicker({
											timeFormat: 'h:mm p',
											interval: 10,
											minTime: '00',
											maxTime: '23:00PM',
											defaultTime: '09.40',
											startTime: '09:40',
											dynamic: false,
											dropdown: true,
											scrollbar: true
										});</script>
										<input type="text" class="timepicker2" name="jam_selesai[]" style="width:100px;"></span></td>
									<td><span class="input-group-addon">
										<select data-placeholder="Ruangan" style="width:100px;" name="kode_ruangan[]">
											<option value=''>--Pilih Ruangan--</option>
											<?php
										$tampil=mysql_query("SELECT * FROM ruang_kelas ORDER BY nm_ruangan ASC");
										while($r=mysql_fetch_array($tampil))
										{
										  echo "<option value=$r[kode_ruangan]>$r[nm_ruangan]</option>";
										}  
									?>
										</select></span></td>
									<td><?php echo anchor('../smt/delete/'.$row->kode_mk,'Delete',array('onClick'=>'return hapus('.$row->kode_mk.')')) ?></td>
							</tr>
							<?php
								}
							?>
							</tbody>
						</table>
						<div class="panel-body"></div>
						<button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk icon-white"></i>&nbsp;&nbsp;Simpan Rancangan Kuliah</button>
				
						</form>
<script type="text/javascript">
	$(document).ready(function(){
		$("#simpan").click(function(){ 
			var data = $('#form-kelaskuliah').serialize();
			$.ajax({
				type: 'POST',
				url: "<?=base_url()?>smt/saverancangkuliahall",
				data: data,
				dataType:"json",
				success: function(response) {
					window.alert('Rancang Kuliah Berhasil Ditambahkan');
				}
			});
		});
	});
	</script>
	<script type="text/javascript">
	function hapus(kode_mk){
            if(confirm('Yakin Menghapus?')){
            $().ready(function(){                                        
                $.ajax({                    
                    url : "<?php echo base_url() ?>smt/delete/"+kode_mk,        
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                    },                                                
                    success:    function(html){
                                 $("#data").load('<?php echo base_url() ?>smt/add_kelaskuliah/ #data');                                                                                                    
                                }                
                });                    
            });    
        }        
        }
	</script>
<!-- external javascript -->
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
</body>
</html>
