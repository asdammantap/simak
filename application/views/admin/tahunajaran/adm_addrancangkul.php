<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="<?=base_url()?>assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="<?=base_url()?>assets/css/charisma-app.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/bootstrap-table.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/colorbox/example3/colorbox.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/bower_components/responsive-tables/responsive-tables.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/jquery.noty.css" rel="stylesheet">
	<link href="<?=base_url()?>assets/css/jquery.iphone.toggle.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/noty_theme_default.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/animate.min.css" rel="stylesheet">
	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="<?=base_url()?>assets/js/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="<?=base_url()?>assets/img/logosinul.ico"/>
</head>
<body>
	<!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <div id="content" class="col-lg-12 col-sm-12">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../../smt/tahunajaran">Tahun Ajaran</a>
        </li>
		<li>
            Input Data Semester Baru
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Input Rancangan Perkuliahan Di STTIKOM Insan Unggul</div>
					<div class="panel-body">
					<div id="notif"></div>
					<form id="form-mk" method="post" enctype="multipart/form-data" role="form">
					<div style="float:right;margin-bottom:20px;">
						<a class="btn btn-success" href='../../smt/listrancangkul'">
						<i class="glyphicon glyphicon-list icon-white"></i>&nbsp;&nbsp;Daftar Rancangan Kuliah</a>
				</div>
				<div class="panel-body">
				<?php foreach ($data as $rowsmt) {?>
				<input type="text" id="smt" name="id_smt" value="<?=$rowsmt->id_smt;?>">
				<?php } ?>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Kelas</span>
						<select id="kls" data-rel="chosen" data-placeholder="Pilih Kelas" style="width:170px;" name="kode_kelas">
							<option value='0'>--Pilih Kelas--</option>
							<?php
										$tampil=mysql_query("SELECT * FROM kelas ORDER BY kode_kelas");
										while($r=mysql_fetch_array($tampil))
										{
										  echo "<option value=$r[kode_kelas]>$r[kode_kelas]</option>";
										}  
									?>
						</select>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Matakuliah</span>
					<select id="selectError" data-rel="chosen" data-placeholder="Pilih Matakuliah" style="width:270px;" name="kode_mk">
							<option value=0 selected>- Pilih Mata Kuliah -</option>
							<?php
										$tampil=mysql_query("SELECT * FROM mata_kuliah ORDER BY kode_mk");
										while($r=mysql_fetch_array($tampil))
										{
										  echo "<option value=$r[kode_mk]>$r[kode_mk]&nbsp;&nbsp;|&nbsp;&nbsp; $r[nm_mk]</option>";
										}  
									?>
						</select>&nbsp;
                <button id="tambah" class="btn btn-primary"><i class="glyphicon glyphicon-plus icon-white"></i>&nbsp;&nbsp;Tambah MK</button>
				</div>
				</div>
				</form>
				<div id="hasil"></div>
					</div>
				</div>
			</div>
</div><!--/row-->
<script type="text/javascript">
	$(document).ready(function(){
		$("#tambah").click(function(){ 
			var data = $('#form-mk').serialize();
			$.ajax({
				type: 'POST',
				url: "../../smt/savekelaskuliah",
				data: data,
				dataType:"json",
				success: function(response) {
					window.alert('Matakuliah Berhasil Ditambahkan');
				}
			});
		});
	});
	</script>
	<script type="text/javascript">
	$("#kls").change(function(e) {
      var id_kelas = $("#kls").val();
	  var smt = $("#smt").val();
      var url = "<?php echo base_url();?>smt/listmkrancangkul/"+id_kelas;
	  $("#hasil").html("<img src='../../assets/img/ajax-loaders/hourglass.gif'/>")
      $.ajax({
        url: url,
        type: "POST",
        dataType: "html",
        success: function(data) {
          $('#hasil').html(data);
        }
      });
    });
	</script>
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="<?=base_url()?>assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src="<?=base_url()?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="../../assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<!-- data table plugin -->
<script src="<?=base_url()?>assets/js/jquery.dataTables.min.js"></script>
<!-- select or dropdown enhancer -->
<script src="<?=base_url()?>assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?=base_url()?>assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?=base_url()?>assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?=base_url()?>assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?=base_url()?>assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?=base_url()?>assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?=base_url()?>assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?=base_url()?>assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?=base_url()?>assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?=base_url()?>assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="<?=base_url()?>assets/js/charisma.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-table.js"></script>
</body>
</html>
