<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="../../assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../../assets/css/charisma-app.css" rel="stylesheet">
	<link href="../../assets/css/bootstrap-table.css" rel="stylesheet">
    <link href='../../assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='../../assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='../../assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='../../assets/css/jquery.noty.css' rel='stylesheet'>
	<link href='../../assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='../../assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='../../assets/css/animate.min.css' rel='stylesheet'>
	<script type="text/javascript" src="../../assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="../../assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="../../assets/img/logosinul.ico"/>
</head>
<body>
	<!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <div id="content" class="col-lg-12 col-sm-12">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../smt/tahunajaran">Tahun Ajaran</a>
        </li>
		<li>
            Input Data Semester Baru
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Setup Semester STTIKOM Insan Unggul</div>
					<div class="panel-body">
					<?=$this->session->flashdata('pesan')?>
						<form action="<?=base_url()?>smt/savesemester" method="post" enctype="multipart/form-data" role="form">
				<div style="float:right;margin-bottom:20px;">
						<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-check icon-white"></i>&nbsp;&nbsp;Submit</button>
						<button type="reset" class="btn btn-default"><i class="glyphicon glyphicon-refresh icon-white"></i>&nbsp;&nbsp;Reset</button>&nbsp;
						<a class="btn btn-success" href='../tahunajaran'">
						<i class="glyphicon glyphicon-list icon-white"></i>&nbsp;&nbsp;Daftar Tahun Ajaran</a>
				</div>
				<div class="input-group col-md-11">
						<?php foreach ($datathnajar as $row) {?>
						<input type="hidden" class="form-control" style="width:150px;" placeholder="Tahun Ajar" value="<?=$row->id_thn_ajaran;?>" name="id_thn_ajaran"><?php } ?>
                </div><p></p>
				<div class="input-group col-md-11">
						<span class="input-group-addon">ID Semester</span>
						<input type="text" class="form-control" style="width:150px;" placeholder="ID Semester" name="id_smt"><i class="help-block">*ex: 16171 (2016 / 2017 Periode Pertama)</i>
                </div><p></p>
               <div class="input-group col-md-11">
                    <span class="input-group-addon">Deskripsi Semester</span>
                    <input type="text" class="form-control" placeholder="Deskripsi Semester" name="nm_smt" required>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Kategori Semester</span>
					<div class="radio">
						<label>
                        <input type="radio" name="smt" id="optionsRadios1" value="1" checked>Ganjil&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="smt" id="optionsRadios1" value="2">Genap&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
					</div>
				</div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Keaktifan Periode Tahun Ajaran</span>
					<div class="radio">
						<label>
                        <input type="radio" name="a_periode_aktif" id="optionsRadios1" value="1" checked>Aktif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="a_periode_aktif" id="optionsRadios1" value="0">Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
					</div>
				</div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Tanggal Mulai</span>
                    <input type="date" class="form-control" name="tgl_mulai">
                </div><p></p>
				</form>
				<div></div><p></p>
					</div>
				</div>
			</div>
			
</div><!--/row-->
<script type="text/javascript">
	$(document).ready(function(){
		$("#tambah").click(function(){
			var data = $('.form-mk').serialize();
			$.ajax({
				type: 'POST',
				url: "../main/savemk",
				data: data,
				success: function() {
					$('.tampildata').load("../home/matakuliah");
				}
			});
		});
	});
	</script>
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="../../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../../assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='../../assets/bower_components/moment/min/moment.min.js'></script>
<script src='../../assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='../../assets/js/jquery.dataTables.min.js'></script>
<!-- select or dropdown enhancer -->
<script src="../../assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="../../assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="../../assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../../assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="../../assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../../assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../../assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../../assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../../assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../../assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../../assets/js/charisma.js"></script>
<script src="../../assets/js/bootstrap-table.js"></script>
</body>
</html>
