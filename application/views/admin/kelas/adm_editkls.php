<!DOCTYPE html>
<html>
<head>
<title>SIMAK - Sistem Informasi Manajemen Akademik</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
<meta name="description" content="sistem informasi manajemen akademik">
<meta name="keyword" content="simak">
<meta name="author" content="Wong Mantap">
<!-- The styles -->
    <link href="../../assets/css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="../../assets/css/charisma-app.css" rel="stylesheet">
	<link href="../../assets/css/bootstrap-table.css" rel="stylesheet">
    <link href='../../assets/bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='../../assets/bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='../../assets/bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='../../assets/css/jquery.noty.css' rel='stylesheet'>
	<link href='../../assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='../../assets/css/noty_theme_default.css' rel='stylesheet'>
    <link href='../../assets/css/animate.min.css' rel='stylesheet'>
	<script type="text/javascript" src="../../assets/js/jquery.js"></script>
    <!-- jQuery -->
    <script src="../../assets/bower_components/jquery/jquery.min.js"></script>

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
<link rel="shortcut icon" href="../../assets/img/logosinul.ico"/>
</head>
<body>
	<!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>SIMAK IU</span></a>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="../main/profile">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="../main/logout">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->
            <ul class="collapse navbar-collapse nav navbar-nav top-menu">
				<li><a href="../main/dashboard"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Home</a></li>
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i>  Master Data <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../mk/matakuliah"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Matakuliah</a></li>
						<li class="divider"></li>
						<li><a href="../kls/kelas"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;&nbsp;Kelas</a></li>
						<li class="divider"></li>
                        <li><a href="../ruang/ruangan"><i class="glyphicon glyphicon-new-window"></i>&nbsp;&nbsp;&nbsp;Ruangan</a></li>
						<li class="divider"></li>
                        <li><a href="../mhs/mahasiswa"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
						<li><a href="../dsn/dosen"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-tower"></i>  Setup <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="../smt/tahunajaran"><i class="glyphicon glyphicon-road"></i>&nbsp;&nbsp;&nbsp;Tahun Ajaran</a></li>
						<li class="divider"></li>
                        <li><a href="../smt/rancangkul"><i class="glyphicon glyphicon-open"></i>&nbsp;&nbsp;&nbsp;Rancangan Kuliah</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-bookmark"></i>  Perkuliahan <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../kuliah/krs"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;KRS (Kartu Rencana Studi)</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/listhadir"><i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;&nbsp;Absensi</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/tugas"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Tugas / Kuis</a></li>
						<li class="divider"></li>
                        <li><a href="../kuliah/ujian"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;UTS / UAS</a></li>
						<li class="divider"></li>
						<li><a href="../kuliah/nilai"><i class="glyphicon glyphicon-list"></i>&nbsp;&nbsp;&nbsp;Penilaian</a></li>
                    </ul>
                </li>
				<li class="dropdown">
                    <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-paperclip"></i>  Rekapitulasi <span
                            class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
						<li><a href="../rpt/rptmhs"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Mahasiswa</a></li>
						<li class="divider"></li>
                        <li><a href="../rpt/rptdsn"><i class="glyphicon glyphicon-book"></i>&nbsp;&nbsp;&nbsp;Dosen</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <div id="content" class="col-lg-12 col-sm-12">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="../../main/dashboard">Home</a>
        </li>
		<li>
            <a href="../../kls/kelas">Kelas</a>
        </li>
		<li>
            Edit Data Kelas
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Edit Data Kelas Di STTIKOM Insan Unggul</div>
					<div class="panel-body">
					<?=$this->session->flashdata('pesan')?>
						<form action="<?=base_url()?>kls/proseseditkls" method="post" enctype="multipart/form-data" role="form">
						<?php foreach ($data as $row) {?>
				<div style="float:right;margin-bottom:20px;">
						<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-check icon-white"></i>&nbsp;&nbsp;Submit</button>
						<button type="reset" class="btn btn-default"><i class="glyphicon glyphicon-refresh icon-white"></i>&nbsp;&nbsp;Reset</button>&nbsp;
						<a class="btn btn-success" href='../kls/kelas'">
						<i class="glyphicon glyphicon-list icon-white"></i>&nbsp;&nbsp;Daftar Kelas</a>
				</div>
				<div class="col-lg-6">
				<div class="panel-body"></div>
				<div class="panel-body"></div>
				<div class="input-group col-md-11">
						<span class="input-group-addon">Kelas</span>
						<input type="text" class="form-control" style="width:150px;" placeholder="Kelas" name="kode_kelas" value="<?=$row->kode_kelas;?>" required>
                </div><p></p>
               <div class="input-group col-md-11">
                    <span class="input-group-addon">Tahun Masuk</span>
                    <input type="number" class="form-control" placeholder="Tahun Masuk" name="tahun_msk" value="<?=$row->tahun_msk;?>" required>
                </div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Semester</span>
					<?php if ($row->semester_skrg=='1'){?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1" selected>Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='2')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2" selected>Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='3')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3" selected>Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='4')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4" selected>Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='5')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5" selected>Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='6')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6" selected>Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='7')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7" selected>Semester 7</option>
							<option value="8">Semester 8</option>
                        </select>
					<?php } 
					if ($row->semester_skrg=='8')  {?>
					<select class="form-control" style="height:40px;border-radius: 0 5px 5px 0;" name="semester_skrg" required>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
							<option value="3">Semester 3</option>
							<option value="4">Semester 4</option>
							<option value="5">Semester 5</option>
							<option value="6">Semester 6</option>
							<option value="7">Semester 7</option>
							<option value="8" selected>Semester 8</option>
                        </select>
					<?php } ?>
				</div><p></p>
				<div class="input-group col-md-11">
                    <span class="input-group-addon">Jumlah Mahasiswa</span>
                    <input type="number" class="form-control" placeholder="Jumlah Mhs" name="jumlah_mhs" value="<?=$row->jumlah_mhs;?>" required>
                </div><p></p>
				</div>
				</form>
				<?php
				}
				?>
				</div>
			</div>
</div><!--/row-->
	<!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->
</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="../../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="../../assets/js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='../../assets/bower_components/moment/min/moment.min.js'></script>
<script src='../../assets/bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='../../assets/js/jquery.dataTables.min.js'></script>
<!-- select or dropdown enhancer -->
<script src="../../assets/bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="../../assets/bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="../../assets/js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="../../assets/bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="../../assets/bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="../../assets/js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="../../assets/js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="../../assets/js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="../../assets/js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="../../assets/js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="../../assets/js/charisma.js"></script>
<script src="../../assets/js/bootstrap-table.js"></script>
</body>
</html>
